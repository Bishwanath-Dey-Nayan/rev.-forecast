// Babel configuration
// https://babeljs.io/docs/usage/api/
module.exports = {
  presets: [
    [
      '@babel/preset-env',
      {
        useBuiltIns: 'entry',
        targets: {
          node: 'current',
        },
      },
    ],
    '@babel/react',
    '@babel/preset-flow',
  ],
  plugins: [['@babel/plugin-proposal-class-properties', { loose: false }]],
  ignore: ['node_modules'],
};
