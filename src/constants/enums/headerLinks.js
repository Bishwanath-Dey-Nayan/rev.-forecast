import pageNames from './pageNames';

const { dashboard } = pageNames;

export default [
  {
    cmsKey: dashboard,
    route: `/${dashboard}`,
  },
];
