export const headerDesc = {
  rev: 'Revenue',
  cto: 'CTO',
  adjToGrossProf: 'Adjustment To Gross Profit',
  adjGrossProf: 'Adjusted Gross Profit',
  gAndA: 'G&A',
  otherExpense: 'Other Expenses',
  netIncome: 'Net Income',
};

export const rowDesc = {
  contrAwrdInPy: 'Contracts Awarded in Prior Years',
  contrAwrdYTD: 'Contracts Awarded YTD',
  pendingAwards: 'Pending Awards',
  otherWork: 'Other Future Work',
  total: 'Total',
  plan: 'PLAN',
  retro: 'Retro',
  yardShop: 'Yard & Shop',
};

export const colDesc = {
  backlogBeg: 'backlogBeginning',
  backlogEnd: 'backlogEnding',
  company: 'company',
  contractBOY: 'contractBOY',
  contractTot: 'contractTotal',
  contractYTD: 'contractYTD',
  awrdBOY: 'growthBOY',
  awrdTot: 'growthTotal',
  awrdYTD: 'growthYTD',
  nyActivity: 'nextYearActivity',
  nyGrowth: 'nextYearGrowth',
};

export const reportModeLists = {
  companyDivisionBothAll: 'ALL', //  ALLALL
  onlyCompanySelected: 'CMP', // ___ALL
  bothSelected: '_',
};
