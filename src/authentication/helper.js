/* eslint-disable no-param-reassign */
/*
 *  Copyright (c) Microsoft. All rights reserved. Licensed under the MIT license.
 *  See LICENSE in the source repository root for complete license information.
 */

import { Client } from '@microsoft/microsoft-graph-client';
import async from 'async';

export default class GraphSdkHelper {
  constructor(props) {
    // Initialize the Graph SDK.
    this.client = Client.init({
      debugLogging: false,
      authProvider: done => {
        done(null, window.hello('aad').getAuthResponse().access_token);
      },
    });
    // console.log('LINE 19 IN CONSTRACTOR client = ',this.client);
    this._handleError = this._handleError.bind(this);
    this.props = props;
  }

  // GET me
  getMe(callback) {
    // console.log('LINE 26 client:',this.client);
    this.client
      .api('/me')
      .select('displayName', 'userPrincipalName')
      .get((err, res) => {
        // console.log('LINE 31 err',err);
        // console.log('LINE 32 me',res);
        if (!err) {
          callback(null, res);
        } else this._handleError(err);
      });
  }

  getUser(user, callback) {
    this.client
      .api(`/users/${user}`)
      .version('beta')
      .select(
        'id,displayName,givenName,surname,emailAddresses,userPrincipalName,jobTitle',
      )
      .get((err, res) => {
        // console.log('err',err);
        // console.log('me',res);
        if (!err) {
          callback(null, res);
        } else {
          this._handleError(err);
          callback(err, null);
        }
      });
  }

  // GET me/people
  getPeople(callback) {
    this.client
      .api('/me/people')
      .version('beta')
      .filter("personType eq 'Person'")
      .select(
        'id,displayName,givenName,surname,emailAddresses,userPrincipalName',
      )
      .filter("mailboxType eq 'Mailbox'")
      .top(800)
      .get((err, res) => {
        // console.log("LINE 66 ERROR: ", err);
        // console.log('RES: ', res);
        if (err) {
          this._handleError(err);
          callback(err, res ? res.value : []);
        } else if (res) {
          this.getProfilePics(res.value, callback);
        } else {
          callback(err, []);
        }
      });
  }

  getMyPhoto(callback) {
    this.client
      .api('/me/photo/$value')
      .header('Cache-Control', 'no-cache')
      .responseType('blob')
      .get((err, res, rawResponse) => {
        if (!err) {
          callback(null, window.URL.createObjectURL(rawResponse.xhr.response));
        } else this._handleError(err);
      });
  }

  getUserPhoto(user, callback) {
    this.client
      .api(`users/${user}/photo/$value`)
      .header('Cache-Control', 'no-cache')
      .responseType('blob')
      .get((err, res, rawResponse) => {
        if (!err) {
          // console.log("YAY!!!!!!: ", rawResponse);
          callback(null, window.URL.createObjectURL(rawResponse.xhr.response));
        } else {
          this._handleError(err);
          callback(err, null);
        }
      });
  }

  // GET user/id/photo/$value for each person
  getProfilePics(users, callback) {
    const pic = (p, done) => {
      if (p.userPrincipalName !== '') {
        this.client
          .api(`users/${p.userPrincipalName}/photo/$value`)
          .header('Cache-Control', 'no-cache')
          .responseType('blob')
          .get((err, res, rawResponse) => {
            if (err) {
              p.profilePic = null;
              done();
            } else {
              p.profilePic = window.URL.createObjectURL(
                rawResponse.xhr.response,
              );
              done();
            }
          });
      }
    };
    async.each(users, pic, err => {
      callback(err, users);
    });
  }

  // GET users?$filter=displayName startswith('{searchText}')
  searchForPeople(searchText, callback) {
    this.client
      .api('/users')
      .filter(`startswith(displayName,'${searchText}')`)
      .select('displayName,givenName,surname,mail,userPrincipalName,id')
      .top(20)
      .get((err, res) => {
        if (err) {
          this._handleError(err);
        }
        callback(err, res ? res.value : []);
      });
  }

  // POST me/sendMail
  sendMail(recipients, callback) {
    const email = {
      Subject: 'Email from the Microsoft Graph Sample with Office UI Fabric',
      Body: {
        ContentType: 'HTML',
        Content: `<p>Thanks for trying out Office UI Fabric!</p>
          <p>See what else you can do with <a href="http://dev.office.com/fabric#/components">
          Fabric React components</a>.</p>`,
      },
      ToRecipients: recipients,
    };
    this.client
      .api('/me/sendMail')
      .post(
        { message: email, saveToSentItems: true },
        (err, res, rawResponse) => {
          if (err) {
            this._handleError(err);
          }
          callback(err, rawResponse.req._data.message.ToRecipients); // eslint-disable-line
        },
      );
  }

  // GET drive/root/children
  getFiles(nextLink, callback) {
    let request;
    if (nextLink) {
      request = this.client.api(nextLink);
    } else {
      request = this.client
        .api('/me/drive/root/children')
        .select(
          'name,createdBy,createdDateTime,lastModifiedBy,lastModifiedDateTime,webUrl,file',
        )
        .top(100); // default result set is 200
    }
    request.get((err, res) => {
      if (err) {
        this._handleError(err);
      }
      callback(err, res);
    });
  }

  _handleError(err) {
    // console.log(`${err.code} - ${err.message}`);
    // console.log("ERROR: ", err);

    // This sample just redirects to the login function when the token is expired.
    // Production apps should implement more robust token management.
    if (
      err.statusCode === 401 &&
      (err.message === 'Access token has expired.' ||
        err.message === 'Access token validation failure.')
    ) {
      // console.log("Trying Login Again", this.props);
      this.props.login();
    }
  }
}
