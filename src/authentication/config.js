const prefix =
  window.location.host.indexOf('localhost') > -1 ||
  window.location.host.indexOf('127.0.0.1') > -1
    ? 'http://'
    : 'https://';
const host =
  prefix +
  (window.location.host.indexOf(':') > -1
    ? window.location.host.substring(0, window.location.host.indexOf(':'))
    : window.location.host);
// import api from '../components/webapifunctions.js';
export const applicationId = '65a6c284-6cdb-4ee3-8dc4-c054a4ab927d';

// TODO: Change this when we know where in production it's going.
export const redirectUri =
  host.indexOf('localhost') > 1 ? `${host}:3000/` : `${host}/`;
