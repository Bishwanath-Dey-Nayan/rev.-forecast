/* eslint-disable react/no-unused-state, react/destructuring-assignment, class-methods-use-this */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { HashRouter as Router, Route, BrowserRouter } from 'react-router-dom';
import { Button } from 'semantic-ui-react';
import hello from 'hellojs';

// import injectTapEventPlugin from 'react-tap-event-plugin';

import Cookies from 'universal-cookie';
import * as gfrActions from './store/gfr/gfr.actions';

import GraphSdkHelper from './authentication/helper';
import { applicationId, redirectUri } from './authentication/config';

import Dashboard from './routes/dashboard/Dashboard';
import GfrReport from './routes/gfrReport/GfrReport';

// injectTapEventPlugin();
window.hello = hello; // eslint-disable-line
const cookies = new Cookies();

const Logout = ({ logout }) => {
  logout();
  return <div />;
};

Logout.propTypes = {
  logout: PropTypes.func.isRequired,
};

class RouterApp extends Component {
  constructor(props) {
    super(props);
    hello.init({
      aad: {
        name: 'Azure Active Directory',
        oauth: {
          version: 2,
          auth:
            'https://login.microsoftonline.com/common/oauth2/v2.0/authorize',
        },
        form: false,
      },
    });

    this.sdkHelper = new GraphSdkHelper({ login: this.login.bind(this) });
    window.sdkHelper = this.sdkHelper;

    const authResp = window.hello('aad').getAuthResponse();

    this.state = {
      isAuthenticated: !!hello('aad').getAuthResponse(),
      gfrAuthenticated: false,
      userPrincipalName: null,
      displayName: null,
      token: authResp !== null ? authResp.access_token : '',
    };
    if (authResp !== null) {
      this.props.gfrActions.setUserToken(authResp.access_token); //eslint-disable-line
    }
    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
  }

  componentWillMount() {
    const ths = this;
    this.setState(
      {
        cookies,
        logout: this.logout,
        login: this.login,
      },
      () => {
        if (ths.state.isAuthenticated) {
          ths.sdkHelper.getMe((err, me) => {
            if (!err && (me !== null && me !== undefined)) {
              this.props.gfrActions.setUserInfo(
                me.userPrincipalName,
                me.displayName,
              ); //eslint-disable-line
              this.setState({
                userPrincipalName: me.userPrincipalName,
                displayName: me.displayName,
                gfrAuthenticated: true,
              });
            } else {
              console.error('getMe Error:',err);
              console.error('Me:',me);
            }
          });
        }
      },
    );
  }

  login() {
    // Initialize the auth request.
    hello.init(
      {
        aad: applicationId,
      },
      {
        redirect_uri: redirectUri,
        scope:
          'user.read+user.readbasic.all+mail.send+files.read+people.read+user.readwrite',
        force: false,
      },
    );

    hello.login('aad', {
      display: 'page',
      state: 'abcd',
    });
  }

  // Sign the user out of the session.
  logout() {
    hello('aad').logout();
    this.setState({
      isAuthenticated: false,
      gfrAuthenticated: false,
      userPrincipalName: null,
      displayName: null,
    });
    window.location = '#';
  }

  render() {
    const { gfrAuthenticated, token, isAuthenticated } = this.state;
    if (gfrAuthenticated) {
      return (
        <BrowserRouter>
          <Router>
            <div
              style={{
                position: 'absolute',
                top: '0px',
                left: '0px',
                height: '100%',
                minWidth: '100%',
                backgroundColor: '#d0dfe3',
              }}
            >
              <section>
                <Route
                  exact
                  path="/"
                  render={props => <Dashboard cookies={cookies} {...props} />}
                />
                <Route
                  path="/dashboard"
                  render={props => <Dashboard cookies={cookies} {...props} />}
                />
                <Route
                  path="/gfrReport/:pcompany"
                  render={props => (
                    <GfrReport cookies={cookies} token={token} {...props} />
                  )}
                />
                <Route
                  path="/logout"
                  render={props => <Logout logout={this.logout} {...props} />}
                />
              </section>
            </div>
          </Router>
        </BrowserRouter>
      );
    }
    if (!isAuthenticated) {
      return (
        <div
          style={{
            backgroundColor: 'white',
            textAlign: 'center',
            height: '100%',
            width: '100%',
            color: 'black',
          }}
        >
          {'You are not logged into Graycor Forecast Report: '}
          <Button onClick={() => this.login()}>Login</Button>
        </div>
      );
    }
    return (
      <div
        style={{
          backgroundColor: 'white',
          textAlign: 'center',
          height: '100%',
          width: '100%',
          color: 'black',
        }}
      >
        {'Error Retrieving your Profile, please try to relogin: '}
        <Button onClick={() => this.login()}>Login</Button>
      </div>
    );
  }
}

RouterApp.propTypes = {
  gfrActions: PropTypes.func.isRequired,
}

function mapStateToProps(state) {
  const { gfrReducer } = state;
  return {
    token: gfrReducer.token,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    gfrActions: bindActionCreators(gfrActions, dispatch),
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RouterApp);
