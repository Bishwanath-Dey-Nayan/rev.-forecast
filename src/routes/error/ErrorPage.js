/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ErrorPage.css';

class ErrorPage extends Component {
  static propTypes = {
    error: PropTypes.shape({
      name: PropTypes.string.isRequired,
      message: PropTypes.string.isRequired,
      stack: PropTypes.string.isRequired,
    }),
  };

  static defaultProps = {
    error: null,
  };

  render() {
    const { error } = this.props;
    const { name, stack } = error;
    if (__DEV__ && error) {
      return (
        <div>
          <h1>{name}</h1>
          <pre>{stack}</pre>
        </div>
      );
    }

    return (
      <div>
        <h1>Error</h1>
        <p>
          Sorry, a critical error occurred on this page please contact tech
          support.
        </p>
      </div>
    );
  }
}

export { ErrorPage as ErrorPageWithoutStyle };
export default withStyles(s)(ErrorPage);
