/* eslint-disable  */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { filter } from 'lodash';
import * as gfrActions from '../../store/gfr/gfr.actions';
import * as gfrSelector from '../../store/gfr/gfr.selectors';
import {getCTOValueTotalforMaintenanceFlag, getCTOValueTotalforBothFlag} from '../../store/pendingawards/pa.selectors';
import GroupHeader from '../../components/GroupHeader/GroupHeader';
import GridRows from '../../components/GridRows/GridRows';
import GridRowsColumn1 from '../../components/GridRowsColumn1/GridRowsColumn1';
import GridRowsColumn2 from '../../components/GridRowsColumn2/GridRowsColumn2';
import GridRowsColumn3 from '../../components/GridRowsColumn3/GridRowsColumn3';
import GridRowsColumn4 from '../../components/GridRowsColumn4/GridRowsColumn4';
import GridRowsColumn5 from '../../components/GridRowsColumn5/GridRowsColumn5'; 
import { headerDesc, rowDesc,reportModeLists } from '../../constants/GfrReport.constant';
import { getData } from '../../utils/GfrReportHelper';
import { getMonthValue } from '../../utils/ForecastPlanHelper';
import { getTotal } from '../../utils/mathAndCurrency';

class GfrReportCtoSection extends Component {
  static propTypes = {
    token: PropTypes.string.isRequired,
    data: PropTypes.instanceOf(Array).isRequired,
  };
  updateGfrCellData = cellObj => {
    if (!cellObj.headerDescription) cellObj.headerDescription = headerDesc.cto;
    this.props.updateGfrCellData(cellObj);
  };

  updateforecastPlanCellData = cellObj => {
    if (!cellObj.headerDescription) cellObj.headerDescription = headerDesc.cto;
    this.props.updateforecastPlanCellData(cellObj);
  };


  render() {
    const { setNetIncome } = this;
    const { data, token, paTotals, jdeData, companyCode, contractData, cyContractData, reportMode , forecastPlanData } = this.props;
    const row1DataJdeData = getData(
      jdeData,
      headerDesc.cto,
      rowDesc.contrAwrdInPy,
    );

    let isBbRow1Editable = true;
    const row1Data = getData(data, headerDesc.cto, rowDesc.contrAwrdInPy);
    // if ((+row1Data.backlogBeginning || 0) == 0) {
    //   row1Data.backlogBeginning = row1DataJdeData.backlogBeginning;
    // }
    if (companyCode === 'GCC') {
      row1Data.backlogBeginning = (+contractData.CTO_FORECAST_PY || 0) - (+contractData.OH_PRIOR_YEAR || 0);
    }
    if (companyCode === 'ALL' || companyCode === 'GCC') {
      isBbRow1Editable = false;
    }
    let bb_row1Data = row1Data.backlogBeginning;

    let pendingCTOValueForBothFlag = getCTOValueTotalforBothFlag(this.props.pendingAwardsData);

    let ag_row1DataEditAble = false;

    if (companyCode === 'GCC') {
      row1Data.growthYTD = (+contractData.FORECAST_CTO || 0) - (+contractData.CTO_FORECAST_PY || 0);
    }
    else if (companyCode === 'GSI') {
      row1Data.growthYTD = (+contractData.CURR_YEAR_CTO || 0) + (+contractData.BACKLOG_CTO || 0) - (+row1Data.backlogBeginning || 0) - pendingCTOValueForBothFlag;
    }
    else if (companyCode === 'GIC') {

      if ((+row1Data.growthYTD || 0) == 0) {
        //if no data in gfr , calculate it using jde data 
        row1Data.growthYTD = (+contractData.CURR_YEAR_CTO || 0) + (+contractData.BACKLOG_CTO || 0) - (+row1Data.backlogBeginning || 0) -pendingCTOValueForBothFlag;
      }
      ag_row1DataEditAble = true;

    }

    let ag_row1Data = row1Data.growthYTD;
    let ag_row1Col2Data = getData(data, headerDesc.cto, rowDesc.contrAwrdInPy);
    let ag_row1Col3Data = getTotal(
      ag_row1Data,
      ag_row1Col2Data.growthBOY,
      '',
      '',
    );

    row1Data.contractYTD = (+contractData.CURR_YEAR_CTO);
    let ca_row1Data = row1Data.contractYTD;;
    let ca_row1Col2Data = getData(data, headerDesc.cto, rowDesc.contrAwrdInPy);
    let ca_row1Col3Data = getTotal(
      ca_row1Data,
      ca_row1Col2Data.contractBOY,
      '',
      '',
    );
    let be_row1Data = bb_row1Data + ag_row1Col3Data - ca_row1Col3Data;

    // const row2Data = getData(jdeData, headerDesc.cto, rowDesc.contrAwrdYTD);
    const row2Data = getData(data, headerDesc.cto, rowDesc.contrAwrdYTD);

    let pendingCTOValueForMaintenanceFlag = getCTOValueTotalforMaintenanceFlag(this.props.pendingAwardsData);


    let bb_row2Data = +row2Data.backlogBeginning || 0;
    let ag_row2DataEditAble = false;
    let isagrow2Editable = false;
    if (companyCode === 'GCC') {
      ag_row2DataEditAble = true;
      if (+(row2Data.growthYTD || 0) === 0) {
        row2Data.growthYTD = (+cyContractData.FORECAST_CTO ||0);
      }
    }
    else if (companyCode === 'GSI') {
      row2Data.growthYTD = (+cyContractData.CURR_YEAR_CTO || 0) + (+cyContractData.BACKLOG_CTO || 0) -pendingCTOValueForMaintenanceFlag;
    }
    else if (companyCode === 'GIC') {
      row2Data.growthYTD = (+cyContractData.CURR_YEAR_CTO || 0) + (+cyContractData.BACKLOG_CTO || 0)-pendingCTOValueForMaintenanceFlag;
      ag_row2DataEditAble = false;
    }
    // else data is already collected from the gfr.
    // let ag_row2Data = getData(jdeData, headerDesc.cto, rowDesc.contrAwrdYTD);
    let ag_row2Data = row2Data.growthYTD;
    let ag_row2Col2Data = getData(data, headerDesc.cto, rowDesc.contrAwrdYTD);
    let ag_row2Col3Data = getTotal(
      ag_row2Data,
      ag_row2Col2Data.growthBOY,
      '',
      '',
    );
    // let ca_row2Data = getData(jdeData, headerDesc.cto, rowDesc.contrAwrdYTD);
    let iscarow2Editable = false;
    if (companyCode === 'GCC') {
      iscarow2Editable = true;
      if (+(row2Data.contractYTD || 0) === 0) {
        row2Data.contractYTD = (+cyContractData.CURR_YEAR_CTO || 0);
      }
    }
    else if (companyCode === 'GIC' || companyCode === 'GSI') {
      row2Data.contractYTD = (+cyContractData.CURR_YEAR_CTO || 0);
    }
    // else data is already collected from the gfr data.
    let ca_row2Data = row2Data.contractYTD;
    let ca_row2Col2Data = getData(data, headerDesc.cto, rowDesc.contrAwrdYTD);
    let ca_row2Col3Data = getTotal(
      ca_row2Data,
      ca_row2Col2Data.contractBOY,
      '',
      '',
    );
    let be_row2Data = bb_row2Data + ag_row2Col3Data - ca_row2Col3Data;

    const row3Data = getData(jdeData, headerDesc.cto, rowDesc.pendingAwards);
    let bb_row3Data = +row3Data.backlogBeginning || 0;
    let ag_row3Data = getData(jdeData, headerDesc.cto, rowDesc.pendingAwards);
    let ag_row3Col2Data = +paTotals.pendingAwardCTOValue || 0;
    let ag_row3Col3Data = getTotal(
      ag_row3Data.growthYTD,
      ag_row3Col2Data,
      '',
      '',
    );
    let ca_row3Data = getData(jdeData, headerDesc.cto, rowDesc.pendingAwards);
    let ca_row3Col2Data = (+paTotals.pendingAwardFYEPrevCTO || 0);
    let ca_row3Col3Data = getTotal(
      ca_row3Data.contractYTD,
      ca_row3Col2Data,
      '',
      '',
    );
    let be_row3Data = bb_row3Data + ag_row3Col3Data - ca_row3Col3Data;

    const row4Data = getData(jdeData, headerDesc.cto, rowDesc.otherWork);
    let bb_row4Data = +row4Data.backlogBeginning || 0;
    let ag_row4Data = getData(jdeData, headerDesc.cto, rowDesc.otherWork);
    let ag_row4Col2Data = getData(data, headerDesc.cto, rowDesc.otherWork);
    let ag_row4Col3Data = getTotal(
      ag_row4Data.growthYTD,
      ag_row4Col2Data.growthBOY,
      '',
      '',
    );
    let ca_row4Data = getData(jdeData, headerDesc.cto, rowDesc.otherWork);
    let ca_row4Col2Data = getData(data, headerDesc.cto, rowDesc.otherWork);
    let ca_row4Col3Data = getTotal(
      ca_row4Data.contractYTD,
      ca_row4Col2Data.contractBOY,
      '',
      '',
    );
    let be_row4Data = bb_row4Data + ag_row4Col3Data - ca_row4Col3Data;

    let ca_row3Col2DataObj = { contractBOY: (+paTotals.pendingAwardFYEPrevCTO || 0).toString() }


    // row6Data calculation
    //....................Revenue Data of row6....................//
    let rev_row6Data = getData(forecastPlanData, headerDesc.rev, rowDesc.plan);
    //....................End of Revenue Data of row6.............//
    const row6Data = getData(forecastPlanData,headerDesc.cto,rowDesc.plan);
    
     row6Data.growthTotal = ((+rev_row6Data.growthTotal)||0) * 0.098;

     let fiscalMonthValue = getMonthValue(this.props.selectedMonth.label);
     let row6Data_growthYTD = ((row6Data.growthTotal)/12)*fiscalMonthValue;
     row6Data.growthYTD = row6Data_growthYTD;

     let row6Data_growthBOY = (+(row6Data.growthTotal)||0) - row6Data_growthYTD;
     row6Data.growthBOY = row6Data_growthBOY;

     //.................Plan Data calculation(Contract activity)...................//
     let row6Data_contractYTD = ((row6Data.contractTotal) / 12) * fiscalMonthValue;
     row6Data.contractYTD = row6Data_contractYTD;
 
     let row6Data_contractBOY = (+(row6Data.contractTotal) || 0) - row6Data_contractYTD;
     row6Data.contractBOY = row6Data_contractBOY;
     //.................End of Plan Data Calculation(Contract activity)............//

    return (
      <GroupHeader
        is2ColHeader={false}
        cssPosition="full setPadding"
        colorCode={this.props.companyColor}
        title="CTO"
        column1Top="Backlog"
        column1Bottom="Beginning"
        column2Top="Awards/Growth"
        column2Left="Year-to-Date"
        column2Center="Balance of Year"
        column2Right="Total"
        column3Top="Contract Activity"
        column3Left="Year-to-Date"
        column3Center="Balance of Year"
        column3Right="Total"
        column4Top="Backlog"
        column4Bottom="Ending"
        column5Top="Next Year"
        column5Left="Awards/Growth"
        column5Right="Activity"
        column1Data={
          <GridRowsColumn1
            row1Data={row1Data}
            row2Data={getData(data, headerDesc.cto, rowDesc.contrAwrdYTD)}
            row3Data={getData(data, headerDesc.cto, rowDesc.pendingAwards)}
            row4Data={getData(data, headerDesc.cto, rowDesc.otherWork)}
            row5Data="3243"
            row6Data="65432"
            token={token}
            columnVisible
            greyOut
            updateGfrCellData={this.updateGfrCellData}
            isRow1Editable={isBbRow1Editable}
            isRow2Editable={true}
            reportMode={reportMode}
          />
        }
        column2Data={
          <GridRowsColumn2
            greyOut
            row1Data={row1Data}
            row2Data={row2Data}
            row3Data={getData(data, headerDesc.cto, rowDesc.pendingAwards)}
            row4Data={getData(data, headerDesc.cto, rowDesc.otherWork)}
            row5Data="36646"
            row6Data={row6Data}
            row1Col2Data={getData(data, headerDesc.cto, rowDesc.contrAwrdInPy)}
            row2Col2Data={getData(data, headerDesc.cto, rowDesc.contrAwrdYTD)}
            row3Col2Data={(+paTotals.pendingAwardCTOValue || 0).toString()}
            row4Col2Data={getData(data, headerDesc.cto, rowDesc.otherWork)}
            row5Col2Data="36646"
            row6Col2Data={row6Data}
            row1Col3Data={getData(data, headerDesc.cto, rowDesc.contrAwrdInPy)}
            row2Col3Data={getData(data, headerDesc.cto, rowDesc.contrAwrdYTD)}
            row3Col3Data={getData(data, headerDesc.cto, rowDesc.pendingAwards)}
            row4Col3Data={getData(data, headerDesc.cto, rowDesc.otherWork)}
            row5Col3Data="36646"
            row6Col3Data={row6Data}
            token={token}
            updateGfrCellData={this.updateGfrCellData}
            isRow1Editable={false}
            isrow2DataEditable={ag_row2DataEditAble}
            isRow3Col2Editable={false}
            reportMode={reportMode}
            ag_row1DataEditAble={ag_row1DataEditAble}
            updateforecastPlanCellData = {this.updateforecastPlanCellData}
            isrow6Col3Data = {false}
          />
        }
        column3Data={
          <GridRowsColumn3
            setNetIncome={setNetIncome}
            yellowBack
            greyOut
            headerDescProp={headerDesc.cto}
            row1Data={row1Data}
            row2Data={row2Data}
            row3Data={getData(data, headerDesc.cto, rowDesc.pendingAwards)}
            row4Data={getData(data, headerDesc.cto, rowDesc.otherWork)}
            row5Data="3464"
            row6Data={row6Data}
            row1Col2Data={getData(data, headerDesc.cto, rowDesc.contrAwrdInPy)}
            row2Col2Data={getData(data, headerDesc.cto, rowDesc.contrAwrdYTD)}
            row3Col2Data={ca_row3Col2DataObj}
            row4Col2Data={getData(data, headerDesc.cto, rowDesc.otherWork)}
            row5Col2Data="3464"
            row6Col2Data={row6Data}
            row1Col3Data={getData(data, headerDesc.cto, rowDesc.contrAwrdInPy)}
            row2Col3Data={getData(data, headerDesc.cto, rowDesc.contrAwrdYTD)}
            row3Col3Data={getData(data, headerDesc.cto, rowDesc.pendingAwards)}
            row4Col3Data={getData(data, headerDesc.cto, rowDesc.otherWork)}
            row5Col3Data="3464"
            row6Col3Data={row6Data}
            token={token}
            updateGfrCellData={this.updateGfrCellData}
            isRow1Editable={false}
            isRow2Editable={iscarow2Editable}
            isRow3Col2Editable={false}
            reportMode={reportMode}
            isRow1Col2DataEditable={true}
            updateforecastPlanCellData = {this.updateforecastPlanCellData}
          />
        }
        column4Data={
          <GridRowsColumn4
            row1Data={be_row1Data.toString()}
            row2Data={be_row2Data.toString()}
            row3Data={be_row3Data.toString()}
            row4Data={be_row4Data.toString()}
            row5Data="66565"
            row6Data="66565"
            token={token}
            updateGfrCellData={this.updateGfrCellData}
            paValueCol3={paTotals.pendingAwardCTOValue}
            reportMode={reportMode}
          />
        }
        column5Data={
          <GridRowsColumn5
            headerDescProp={headerDesc.rev}
            row1Data={getData(data, headerDesc.cto, rowDesc.contrAwrdInPy)}
            row2Data={getData(data, headerDesc.cto, rowDesc.contrAwrdYTD)}
            row3Data={getData(data, headerDesc.cto, rowDesc.pendingAwards)}
            row4Data={getData(data, headerDesc.cto, rowDesc.otherWork)}
            row5Data="6464"
            row6Data="6464"
            row1Col2Data={getData(data, headerDesc.cto, rowDesc.contrAwrdInPy)}
            row2Col2Data={getData(data, headerDesc.cto, rowDesc.contrAwrdYTD)}
            row3Col2Data={getData(data, headerDesc.cto, rowDesc.pendingAwards)}
            row4Col2Data={getData(data, headerDesc.cto, rowDesc.otherWork)}
            row5Col2Data="6464"
            row6Col2Data="6464"
            token={token}
            updateGfrCellData={this.updateGfrCellData}
            reportMode={reportMode}
          />
        }
      >
        <GridRows
          row1Title="Contracts Awarded in Prior Years"
          row2Title="Contracts Awarded YTD"
          row3Title={this.props.reportMode == reportModeLists.bothSelected ?
            (<span>
              Pending Awards ({' '}
              <button onClick={this.props.addPendingAwards} type="button">
                Pending Awards
              </button>{' '}
              )
            </span>) :
            (<span>
              Pending Awards
            </span>)
          } //"Pending Awards (list below)"
          row4Title="Other Future Work"
          row5Title="Total"
          row6Title={`${this.props.selectedYear.value} Plan`}
          row7Title={`${this.props.selectedYear.value-1} Plan`}
        />
      </GroupHeader>
    );
  }
}

function mapStateToProps({ gfrReducer,fpReducer, paReducer }) {
  return {
    token: gfrReducer.token,
    data: gfrReducer.data,
    jdeData: gfrReducer.jdeData,
    contractData: gfrReducer.contractData,
    cyContractData: gfrReducer.cyContractData,
    companyCode: gfrSelector.getCompanyCode(gfrReducer),
    forecastPlanData:fpReducer.forecastPlanData,
    selectedMonth: gfrReducer.selectedMonth,
    pendingAwardsData: paReducer.pendingAwardsData,
    selectedYear: gfrReducer.selectedYear,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    gfrActions: bindActionCreators(gfrActions, dispatch),
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(GfrReportCtoSection); 
