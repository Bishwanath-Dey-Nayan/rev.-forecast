/* eslint-disable  */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { filter } from 'lodash';
import * as gfrActions from '../../store/gfr/gfr.actions';
import * as gfrSelector from '../../store/gfr/gfr.selectors';
import {getRevenueValueTotalforMaintenanceFlag, getRevenueValueTotalforBothFlag} from '../../store/pendingawards/pa.selectors';

import GroupHeader from '../../components/GroupHeader/GroupHeader';
import GridRows from '../../components/GridRows/GridRows';
import GridRowsColumn1 from '../../components/GridRowsColumn1/GridRowsColumn1';
import GridRowsColumn2 from '../../components/GridRowsColumn2/GridRowsColumn2';
import GridRowsColumn3 from '../../components/GridRowsColumn3/GridRowsColumn3';
import GridRowsColumn4 from '../../components/GridRowsColumn4/GridRowsColumn4';
import GridRowsColumn5 from '../../components/GridRowsColumn5/GridRowsColumn5';
import { headerDesc, rowDesc, reportModeLists } from '../../constants/GfrReport.constant';
import { getData } from '../../utils/GfrReportHelper';
import { getMonthValue } from '../../utils/ForecastPlanHelper';
import { getTotal } from '../../utils/mathAndCurrency';

class GfrReportRevenueSection extends Component {
  static propTypes = {
    token: PropTypes.string.isRequired,
    data: PropTypes.instanceOf(Array).isRequired,
  };
  updateGfrCellData = cellObj => {
    if (!cellObj.headerDescription) cellObj.headerDescription = headerDesc.rev;
    this.props.updateGfrCellData(cellObj);
  };

  //method for updating the ForecastPlan Datacell

  updateforecastPlanCellData = cellObj => {
    if (!cellObj.headerDescription) cellObj.headerDescription = headerDesc.rev;
    this.props.updateforecastPlanCellData(cellObj);
  };

  render() {
    const { data, token, paTotals, jdeData, contractData, companyCode, cyContractData, reportMode, forecastPlanData } = this.props;

    console.log("Forecast Plan Data...................................... ", this.props.forecastPlanData);

    let isBbRow1Editable = true;
    //let revenueBacklogBeginning=0;
    const row1Data = getData(data, headerDesc.rev, rowDesc.contrAwrdInPy);
    //revenueBacklogBeginning=row1Data.backlogBeginning;

    if (companyCode === 'GCC') {
      row1Data.backlogBeginning = (+contractData.REVENUE_FORECAST_PY || 0) -
        ((+contractData.PRIOR_YEAR_COST || 0) + (+contractData.OH_PRIOR_YEAR || 0));
    }
    if (companyCode === 'ALL' || companyCode === 'GCC') {
      isBbRow1Editable = false;
    }
    let bb_row1Data = row1Data.backlogBeginning;
    let pendingRevValueForBothFlag = getRevenueValueTotalforBothFlag(this.props.pendingAwardsData);

    if (companyCode === 'GCC') {
      row1Data.growthYTD = (+contractData.REVENUE_FORECAST || 0) - (+contractData.REVENUE_FORECAST_PY || 0);
    }

    if (companyCode === 'GIC' || companyCode === 'GSI') {
      row1Data.growthYTD = (contractData.CURR_YEAR_REVENUE || 0) + (contractData.REVENUE_BACKLOG || 0) - (row1Data.backlogBeginning || 0) -pendingRevValueForBothFlag ;
    }
    let ag_row1DataEditAble = false;
    let ag_row1Data = row1Data.growthYTD;

    let ag_row1Col2Data = getData(data, headerDesc.rev, rowDesc.contrAwrdInPy);
    let ag_row1Col3Data = getTotal(
      ag_row1Data,
      ag_row1Col2Data.growthBOY,
      '',
      '',
    );

    if (companyCode === 'GIC') {
      if ((+row1Data.growthYTD || 0) == 0) {
        //if no data in gfr , calculate it using jde data 
        row1Data.contractYTD = (+contractData.CURR_YEAR_REVENUE || 0);
      }
      ag_row1DataEditAble = true;
    }
    else {
      row1Data.contractYTD = (+contractData.CURR_YEAR_REVENUE || 0);
    }

    let ca_row1Data = row1Data.contractYTD;

    let ca_row1Col2Data = getData(data, headerDesc.rev, rowDesc.contrAwrdInPy);
    let ca_row1Col3Data = getTotal(
      ca_row1Data,
      ca_row1Col2Data.contractBOY,
      '',
      '',
    );
    let be_row1Data = (
      (+bb_row1Data || 0) +
      (+ag_row1Col3Data || 0) -
      (+ca_row1Col3Data || 0) 
    ).toString();

    let isrow2DataEditable = false;
    const row2Data = getData(data, headerDesc.rev, rowDesc.contrAwrdYTD);
    let bb_row2Data = +row2Data.backlogBeginning || 0;

    let pendingRevValueForMaintenanceFlag = getRevenueValueTotalforMaintenanceFlag(this.props.pendingAwardsData);
    console.log("pendingRevValueForMaintenanceFlag",pendingRevValueForMaintenanceFlag);

    if (companyCode === 'GIC' || companyCode === "GSI") {
      isrow2DataEditable = false;
      //if no data in gfr , calculate it using jde data 
      row2Data.growthYTD = (cyContractData.CURR_YEAR_REVENUE || 0) + (cyContractData.REVENUE_BACKLOG || 0) -pendingRevValueForMaintenanceFlag;
    }
    else if (companyCode === "GCC") {
      isrow2DataEditable = true;
      if ((+row2Data.growthYTD || 0) == 0) {
        row2Data.growthYTD = (cyContractData.REVENUE_FORECAST || 0);
      }
    }

    //else the data is alredy collected from the gfr data.
    let ag_row2Data = row2Data.growthYTD;
    let ag_row2Col2Data = getData(data, headerDesc.rev, rowDesc.contrAwrdYTD);
    let ag_row2Col3Data = getTotal(
      ag_row2Data,
      ag_row2Col2Data.growthBOY,
      '',
      '',
    );

    
    let iscaYTDDataEditable = false
    if (companyCode === 'GCC') {
      iscaYTDDataEditable = true;
      if ((+row2Data.contractYTD || 0) == 0) {
        //if no data found on gfr,take it from the jdedata
        row2Data.contractYTD = (cyContractData.CURR_YEAR_REVENUE || 0);
      }
    }

    else if (companyCode === 'GIC' || companyCode === 'GSI') {
      row2Data.contractYTD = (cyContractData.CURR_YEAR_REVENUE || 0);
    }
    //else data is already collected from the gft data.
    let ca_row2Data = row2Data.contractYTD;

    let ca_row2Col2Data = getData(data, headerDesc.rev, rowDesc.contrAwrdYTD);
    let ca_row2Col3Data = getTotal(
      ca_row2Data,
      ca_row2Col2Data.contractBOY,
      '',
      '',
    );
    let be_row2Data = (
      (+bb_row2Data || 0) +
      (+ag_row2Col3Data || 0) -
      (+ca_row2Col3Data || 0)
    ).toString();

    const row3Data = getData(jdeData, headerDesc.rev, rowDesc.pendingAwards);
    let bb_row3Data = +row3Data.backlogBeginning || 0;
    let ag_row3Data = getData(jdeData, headerDesc.rev, rowDesc.pendingAwards);
    let ag_row3Col2Data = +paTotals.pendingAwardRevenueValue || 0;
    let ag_row3Col3Data = getTotal(
      ag_row3Data.growthYTD,
      ag_row3Col2Data,
      '',
      '',
    );
    let ca_row3Data = getData(jdeData, headerDesc.rev, rowDesc.pendingAwards);
    let ca_row3Col2Data = (+paTotals.pendingAwardFYEPrevRev || 0);
    let ca_row3Col3Data = getTotal(
      ca_row3Data.contractYTD,
      ca_row3Col2Data,
      '',
      '',
    );
    let be_row3Data = (
      (+bb_row3Data || 0) +
      (+ag_row3Col3Data || 0) -
      (+ca_row3Col3Data || 0)
    ).toString();

    const row4Data = getData(jdeData, headerDesc.rev, rowDesc.otherWork);
    let bb_row4Data = +row4Data.backlogBeginning || 0;
    let ag_row4Data = getData(jdeData, headerDesc.rev, rowDesc.otherWork);
    let ag_row4Col2Data = getData(data, headerDesc.rev, rowDesc.otherWork);
    let ag_row4Col3Data = getTotal(
      ag_row4Data.growthYTD,
      ag_row4Col2Data.growthBOY,
      '',
      '',
    );
    let ca_row4Data = getData(jdeData, headerDesc.rev, rowDesc.otherWork);
    let ca_row4Col2Data = getData(data, headerDesc.rev, rowDesc.otherWork);
    let ca_row4Col3Data = getTotal(
      ca_row4Data.contractYTD,
      ca_row4Col2Data.contractBOY,
      '',
      '',
    );
    let be_row4Data = (
      (+bb_row4Data || 0) +
      (+ag_row4Col3Data || 0) -
      (+ca_row4Col3Data || 0)
    ).toString();
    let ca_row3Col2DataObj = { contractBOY: (+paTotals.pendingAwardFYEPrevRev || 0).toString() }



    // ForecastPlan data calculation(Award/Growth)
    let row6Data = getData(forecastPlanData, headerDesc.rev, rowDesc.plan);

    let fiscalMonthValue = getMonthValue(this.props.selectedMonth.label);
    let row6Data_growthYTD = ((row6Data.growthTotal) / 12) * fiscalMonthValue;
    row6Data.growthYTD = row6Data_growthYTD;

    let row6Data_growthBOY = (+(row6Data.growthTotal) || 0) - row6Data_growthYTD;
    row6Data.growthBOY = row6Data_growthBOY;

    //ForecastPlan data calculation(ContractActivity)
    let row6Data_contractYTD = ((row6Data.contractTotal) / 12) * fiscalMonthValue;
    row6Data.contractYTD = row6Data_contractYTD;

    let row6Data_contractBOY = (+(row6Data.contractTotal) || 0) - row6Data_contractYTD;
    row6Data.contractBOY = row6Data_contractBOY;
    console.log("row6Data.......",row6Data);





    return (
      <GroupHeader
        companyId="00"
        cssPosition="full"
        colorCode={this.props.companyColor}
        is2ColHeader={false}
        title={headerDesc.rev}
        column1Top="Backlog"
        column1Bottom="Beginning"
        column2Top="Awards/Growth"
        column2Left="Year-to-Date"
        column2Center="Balance of Year"
        column2Right="Total"
        column3Top="Contract Activity"
        column3Left="Year-to-Date"
        column3Center="Balance of Year"
        column3Right="Total"
        column4Top="Backlog"
        column4Bottom="Ending"
        column5Top="Next Year"
        column5Left="Awards/Growth"
        column5Right="Activity"
        column1Data={
          <GridRowsColumn1
            row1Data={row1Data}
            row2Data={getData(data, headerDesc.rev, rowDesc.contrAwrdYTD)}
            row3Data={getData(data, headerDesc.rev, rowDesc.pendingAwards)}
            row4Data={getData(data, headerDesc.rev, rowDesc.otherWork)}
            row5Data=""
            row6Data={row6Data}
            token={token}
            columnVisible
            greyOut
            updateGfrCellData={this.updateGfrCellData}
            isRow1Editable={isBbRow1Editable}
            isRow2Editable={true}
            reportMode={reportMode}
          />
        }
        column2Data={
          <GridRowsColumn2
            row1Data={row1Data}
            row2Data={row2Data}
            row3Data={getData(data, headerDesc.rev, rowDesc.pendingAwards)}
            row4Data={getData(data, headerDesc.rev, rowDesc.otherWork)}
            row5Data="784542"
            row6Data={row6Data}
            row7Data={row6Data}
            greyOut
            row1Col2Data={getData(data, headerDesc.rev, rowDesc.contrAwrdInPy)}
            row2Col2Data={getData(data, headerDesc.rev, rowDesc.contrAwrdYTD)}
            row3Col2Data={(+paTotals.pendingAwardRevenueValue || 0).toString()}
            row4Col2Data={getData(data, headerDesc.rev, rowDesc.otherWork)}
            row5Col2Data="784542"
            row6Col2Data={row6Data}
            row7Col2Data = {row6Data}
            row1Col3Data={getData(data, headerDesc.rev, rowDesc.contrAwrdInPy)}
            row2Col3Data={getData(data, headerDesc.rev, rowDesc.contrAwrdYTD)}
            row3Col3Data={getData(data, headerDesc.rev, rowDesc.pendingAwards)}
            row4Col3Data={getData(data, headerDesc.rev, rowDesc.otherWork)}
            row5Col3Data="784542"
            row6Col3Data={row6Data}
            row7Col3Data={row6Data}
            token={token}
            updateGfrCellData={this.updateGfrCellData}
            updateforecastPlanCellData={this.updateforecastPlanCellData}
            isRow1Editable={false}
            isrow2DataEditable={isrow2DataEditable}
            isRow3Col2Editable={false}
            reportMode={reportMode}
            isrow6Col3Data={true}
          />
        }
        column3Data={
          <GridRowsColumn3
            yellowBack
            greyOut
            headerDescProp={headerDesc.rev}
            row1Data={row1Data}
            row2Data={row2Data}
            row3Data={getData(data, headerDesc.rev, rowDesc.pendingAwards)}
            row4Data={getData(data, headerDesc.rev, rowDesc.otherWork)}
            row5Data="6646799"
            row6Data={row6Data}
            row1Col2Data={getData(data, headerDesc.rev, rowDesc.contrAwrdInPy)}
            row2Col2Data={getData(data, headerDesc.rev, rowDesc.contrAwrdYTD)}
            row3Col2Data={ca_row3Col2DataObj}
            row4Col2Data={getData(data, headerDesc.rev, rowDesc.otherWork)}
            row5Col2Data="6646799"
            row6Col2Data={row6Data}
            row1Col3Data={getData(data, headerDesc.rev, rowDesc.contrAwrdInPy)}
            row2Col3Data={getData(data, headerDesc.rev, rowDesc.contrAwrdYTD)}
            row3Col3Data={getData(data, headerDesc.rev, rowDesc.pendingAwards)}
            row4Col3Data={getData(data, headerDesc.rev, rowDesc.otherWork)}
            row5Col3Data="6646799"
            row6Col3Data={row6Data}
            token={token}
            updateGfrCellData={this.updateGfrCellData}
            netIncome={false}
            isRow1Editable={false}
            isRow2Editable={iscaYTDDataEditable}
            reportMode={reportMode}
            isRow1Col2DataEditable={true}
            isrow6Col3Data={true}
            updateforecastPlanCellData={this.updateforecastPlanCellData}
          />
        }
        column4Data={
          <GridRowsColumn4
            row1Data={be_row1Data}
            row2Data={be_row2Data}
            row3Data={be_row3Data}
            row4Data={be_row4Data}
            row5Data="656454"
            row6Data="656454"
            token={token}
            updateGfrCellData={this.updateGfrCellData}
            paValueCol3={paTotals.pendingAwardRevenueValue}
            reportMode={reportMode}
          />
        }
        column5Data={
          <GridRowsColumn5
            headerDescProp={headerDesc.rev}
            row1Data={getData(data, headerDesc.rev, rowDesc.contrAwrdInPy)}
            row2Data={getData(data, headerDesc.rev, rowDesc.contrAwrdYTD)}
            row3Data={getData(data, headerDesc.rev, rowDesc.pendingAwards)}
            row4Data={getData(data, headerDesc.rev, rowDesc.otherWork)}
            row5Data="2155"
            row6Data="2155"
            row1Col2Data={getData(data, headerDesc.rev, rowDesc.contrAwrdInPy)}
            row2Col2Data={getData(data, headerDesc.rev, rowDesc.contrAwrdYTD)}
            row3Col2Data={getData(data, headerDesc.rev, rowDesc.pendingAwards)}
            row4Col2Data={getData(data, headerDesc.rev, rowDesc.otherWork)}
            row5Col2Data="2155"
            row6Col2Data="2155"
            token={token}
            updateGfrCellData={this.updateGfrCellData}
            reportMode={reportMode}
          />
        }
      >
        <GridRows
          row1Title="Contracts Awarded in Prior Years"
          row2Title="Contracts Awarded YTD"
          row3Title={this.props.reportMode == reportModeLists.bothSelected ?
            (<span>
              Pending Awards ({' '}
              <button onClick={this.props.addPendingAwards} type="button">
                Pending Awards
              </button>{' '}
              )
            </span>) :
            (<span>
              Pending Awards
            </span>)
          }
          row4Title="Other Future Work"
          row5Title="Total"
          row6Title={`${this.props.selectedYear.value} Plan`}
          row7Title={`${this.props.selectedYear.value-1} Plan`}
        />
      </GroupHeader>
    );
  }
}

function mapStateToProps({ gfrReducer, fpReducer, paReducer }) {
  return {
    token: gfrReducer.token,
    data: gfrReducer.data,
    jdeData: gfrReducer.jdeData,
    contractData: gfrReducer.contractData,
    cyContractData: gfrReducer.cyContractData,
    reportMode: gfrSelector.getReportMode(gfrReducer),
    companyCode: gfrSelector.getCompanyCode(gfrReducer),
    forecastPlanData: fpReducer.forecastPlanData,
    selectedMonth: gfrReducer.selectedMonth,
    selectedYear: gfrReducer.selectedYear,
    pendingAwardsData: paReducer.pendingAwardsData,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    gfrActions: bindActionCreators(gfrActions, dispatch),
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(GfrReportRevenueSection);
