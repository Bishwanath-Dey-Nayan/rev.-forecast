/* eslint-disable  */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { filter } from 'lodash';
import * as gfrActions from '../../store/gfr/gfr.actions';

import GridRows from '../../components/GridRows/GridRows';
import GridRowsColumn1 from '../../components/GridRowsColumn1/GridRowsColumn1';
import GridRowsColumn3 from '../../components/GridRowsColumn3/GridRowsColumn3';
import GridRowsColumn5 from '../../components/GridRowsColumn5/GridRowsColumn5';
import { headerDesc, rowDesc } from '../../constants/GfrReport.constant';

import GroupHeaderForThree from '../../components/GroupHeader/GroupHeaderForThree';
import { getData } from '../../utils/GfrReportHelper';
import { getMonthValue } from '../../utils/ForecastPlanHelper';

class GfrReportOtherExpensesOrIncomeSection extends Component {
  static propTypes = {
    token: PropTypes.string.isRequired,
    data: PropTypes.instanceOf(Array).isRequired,
  };
  updateGfrCellData = cellObj => {
    if (!cellObj.headerDescription) cellObj.headerDescription = headerDesc.otherExpense;

    this.props.updateGfrCellData(cellObj);
  };
  updateforecastPlanCellData = cellObj => {
    if (!cellObj.headerDescription) cellObj.headerDescription = headerDesc.otherExpense;
    this.props.updateforecastPlanCellData(cellObj);
  };
  /*
    constructor(props) {
        super(props);
    } 
    */
  /*
    getData = (data, header, row) => {
        if (data === undefined || data === null) {
          return {};
        }
        
        if (data.length === 0) {
          return {};
        } 
        else {
          const filtered = filter(data, {
            headerDescription: header,
            rowDescription: row,
          });
          return filtered.length > 0 ? filtered[0] : {};
        }
        
      };
*/
  render() {
    const { data, token, jdeData, oe_row1Col3Data, reportMode, oe_row1Data, forecastPlanData } = this.props;
    let row2Data = getData(forecastPlanData,
      headerDesc.otherExpense,
      rowDesc.plan);
    let fiscalMonthValue = getMonthValue(this.props.selectedMonth.label);
    let row2Data_YTD = ((row2Data.contractTotal) / 12) * fiscalMonthValue;
    row2Data.contractYTD = row2Data_YTD;
    row2Data.contractBOY = row2Data.contractTotal - row2Data_YTD;
    console.log('row2Data                   ', row2Data);

    return (
      <GroupHeaderForThree
        is2ColHeader
        cssPosition="rightSide topMarginZeor "
        gridCategory="doubleGrid nextYearcss"
        colorCode={this.props.companyColor}
        title="Other Expense/(Income)"
        column1Top="Backlog"
        column1Bottom="Beginning"
        column2Top="Awards/Growth"
        column2Left="Year-to-Date"
        column2Center="Balance of Year"
        column2Right="Total"
        column3Top="Contract Activity"
        column3Left="Year-to-Date"
        column3Center="Balance of Year"
        column3Right="Total"
        column4Top="Backlog"
        column4Bottom="Ending"
        column5Top="Next Year"
        column5Left="Awards/Growth"
        column5Right="Next Year"
        column1Data={
          <GridRowsColumn1
            row1Data="6454"
            row2Data="6454"
            row3Data="6454"
            row4Data="6454"
            token={token}
            updateGfrCellData={this.updateGfrCellData}
            reportMode={reportMode}
          />
        }
        column3Data={
          <GridRowsColumn3
            yellowBack
            twoRowGroup
            fourRowGroup
            headerDescProp={headerDesc.otherExpense}
            row1Data={oe_row1Data}
            row2Data={row2Data}
            // row1Col2Data={getData(data,
            //   headerDesc.otherExpense,
            //   rowDesc.total,)}
            row1Col2Data={oe_row1Data}
            row2Col2Data={row2Data}
            row1Col3Data={oe_row1Col3Data}
            row2Col3Data={row2Data}
            token={token}
            updateGfrCellData={this.updateGfrCellData}
            isRow1Editable={true}
            isRow2Editable={false}
            reportMode={reportMode}
            updateforecastPlanCellData={this.updateforecastPlanCellData}
          />
        }
        column5Data={
          <GridRowsColumn5
            col1Hidden
            fourRowGroup
            twoRowGroup
            row1Col2Data="5646"
            row2Col2Data="5646"
            updateGfrCellData={this.updateGfrCellData}
            reportMode={reportMode}
          />
        }
      >
        <GridRows row1Title="Total" row2Title="2019 Plan" row3Title="2018 Plan" />
      </GroupHeaderForThree>
    );
  }
}

function mapStateToProps({ gfrReducer, fpReducer }) {
  return {
    token: gfrReducer.token,
    data: gfrReducer.data,
    jdeData: gfrReducer.jdeData,
    forecastPlanData: fpReducer.forecastPlanData,
    selectedMonth: gfrReducer.selectedMonth,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    gfrActions: bindActionCreators(gfrActions, dispatch),
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(GfrReportOtherExpensesOrIncomeSection);
