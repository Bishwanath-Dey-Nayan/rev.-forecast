/* eslint-disable  */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import * as gfrActions from '../../store/gfr/gfr.actions';
import GridRows from '../../components/GridRows/GridRows';
import GridRowsColumn1 from '../../components/GridRowsColumn1/GridRowsColumn1';
import GridRowsColumn3 from '../../components/GridRowsColumn3/GridRowsColumn3';
import GridRowsColumn5 from '../../components/GridRowsColumn5/GridRowsColumn5';
import { headerDesc, rowDesc } from '../../constants/GfrReport.constant';
import { filter } from 'lodash';
import GroupHeaderForThree from '../../components/GroupHeader/GroupHeaderForThree';
import { getData } from '../../utils/GfrReportHelper';
import { getMonthValue } from '../../utils/ForecastPlanHelper';
import * as gfrSelector from '../../store/gfr/gfr.selectors';

class GfrReportAdjustmentToGrossProfitSection extends Component {
  static propTypes = {
    token: PropTypes.string.isRequired,
    data: PropTypes.instanceOf(Array).isRequired,
  };
  updateGfrCellData = cellObj => {
    if (!cellObj.headerDescription)
      cellObj.headerDescription = headerDesc.adjToGrossProf;
    this.props.updateGfrCellData(cellObj);
  };

  updateforecastPlanCellData = cellObj => {
    if (!cellObj.headerDescription) cellObj.headerDescription = headerDesc.adjToGrossProf;
    this.props.updateforecastPlanCellData(cellObj);
  };

  render() {
    //const { getData } = this;
    
    const { data, token, jdeData, atgp_row3Col3Data, plrData, atgp_row1Data ,atgp_row1Col2Data,
      forecastPlanData,atgp_row1Col3Data, reportMode, companyCode } = this.props;
    let isRow2Editable = false;
    if(this.props.companyCode==="GIC")
    {
      isRow2Editable = true;
    }
    let row2Data = getData(forecastPlanData,
      headerDesc.adjToGrossProf,
      rowDesc.plan);
    let fiscalMonthValue = getMonthValue(this.props.selectedMonth.label);
    let row2Data_YTD = ((row2Data.contractTotal) / 12) * fiscalMonthValue;
    row2Data.contractYTD = row2Data_YTD;
    row2Data.contractBOY = row2Data.contractTotal - row2Data_YTD;

    return (
      <GroupHeaderForThree
        is2ColHeader
        cssPosition="leftSide BottomZeor"
        gridCategory="doubleGrid"
        colorCode={this.props.companyColor}
        title="Adjustment to Gross Profit"
        column1Top="Backlog" 
        column1Bottom="Beginning"
        column2Top="Awards/Growth"
        column2Left="Year-to-Date"
        column2Center="Balance of Year"
        column2Right="Total"
        column3Top="Contract Activity"
        column3Left="Year-to-Date"
        column3Center="Balance of Year"
        column3Right="Total"
        column4Top="Backlog"
        column4Bottom="Ending"
        column5Top="Next Year"
        column5Left="Awards/Growth"
        column5Right="Next Year"
        column1Data={
          <GridRowsColumn1
            token={token}
            updateGfrCellData={this.props.gfrActions.updateGfrCellDataRequest}
            reportMode={reportMode}
          />
        }
        column3Data={(companyCode === "GCC")? 
          (<GridRowsColumn3
            yellowBack
            twoRowGroup
            fourRowGroup
            headerDescProp={headerDesc.adjToGrossProf}
            row1Data={atgp_row1Data}
            row1Col2Data={atgp_row1Col2Data}
            // row1Col3Data={atgp_row1Col3Data} 
            row2Data={row2Data}
            row2Col2Data={row2Data}
            row2Col3Data={row2Data}
            token={token}
            updateGfrCellData={this.updateGfrCellData}
            reportMode={reportMode}
            isRow1Editable={true}
            isRow2Editable={false}
            updateforecastPlanCellData={this.updateforecastPlanCellData}
          />):(<GridRowsColumn3
            fourRowYellow
            yellowBack
            fourRowGroup
            headerDescProp={headerDesc.adjToGrossProf}
            row1Data={atgp_row1Data}
            row1Col2Data={atgp_row1Col2Data}
            row1Col3Data={atgp_row1Col3Data} 
        
            row2Data={getData(
              jdeData,
              headerDesc.adjToGrossProf,
              rowDesc.yardShop,
            )}
            row3Data="646464"
            row4Data={row2Data}
           
            row2Col2Data={getData(
              data,
              headerDesc.adjToGrossProf,
              rowDesc.yardShop,
            )}
            row3Col2Data="646464"
            row4Col2Data={row2Data}
            
            row2Col3Data={getData(
              data,
              headerDesc.adjToGrossProf,
              rowDesc.yardShop,
            )}
            row3Col3Data={atgp_row3Col3Data}
            row4Col3Data={row2Data}
            token={token}
            updateGfrCellData={this.updateGfrCellData}
            isRow1Editable={true}
            isRow2Editable={isRow2Editable}
            reportMode={reportMode}
            updateforecastPlanCellData={this.updateforecastPlanCellData}
          />)
        }
        column5Data={ (companyCode === "GCC")?
          (<GridRowsColumn5
            col1Hidden
            fourRowGroup
            twoRowGroup
            row1Col2Data={(
              getData(data, headerDesc.cto, rowDesc.contrAwrdInPy)
                .nextYearActivity +
              getData(data, headerDesc.cto, rowDesc.contrAwrdYTD)
                .nextYearActivity +
              getData(data, headerDesc.cto, rowDesc.pendingAwards)
                .nextYearActivity +
              getData(data, headerDesc.cto, rowDesc.otherWork)
                .nextYearActivity -
              (getData(data, headerDesc.adjToGrossProf, rowDesc.retro)
                .nextYearActivity +
                getData(data, headerDesc.adjToGrossProf, rowDesc.yardShop)
                  .nextYearActivity)
            ).toString()}
            row2Col2Data="464"
            updateGfrCellData={this.props.gfrActions.updateGfrCellDataRequest}
            reportMode={reportMode}
          />):(
            <GridRowsColumn5
            fourRowGroup
            col1Hidden
            row1Col2Data={getData(
              data,
              headerDesc.adjToGrossProf,
              rowDesc.retro,
            )}

            row2Col2Data={getData(
              data,
              headerDesc.adjToGrossProf,
              rowDesc.yardShop,
            )}
            row3Col2Data="454"
            row4Col2Data="646464"
            updateGfrCellData={this.updateGfrCellData}
            reportMode={reportMode}
          />
          )
        }
      >
        {(companyCode==="GCC")?(<GridRows
          row1Title="Retro"
          row2Title="2019 Plan"
          row3Title="2018 Plan"
        />):(<GridRows
          row1Title="Retro"
          row2Title="Yard & Shop"
          row3Title="Total"
          row4Title="2019 Plan"
        />)}
      </GroupHeaderForThree>
    );
  }
}

function mapStateToProps({ gfrReducer, fpReducer }) {
  return {
    token: gfrReducer.token,
    data: gfrReducer.data,
    jdeData: gfrReducer.jdeData,
    plrData:gfrReducer.plrData,
    companyCode: gfrSelector.getCompanyCode(gfrReducer),
    forecastPlanData: fpReducer.forecastPlanData,
    selectedMonth: gfrReducer.selectedMonth,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    gfrActions: bindActionCreators(gfrActions, dispatch),
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(GfrReportAdjustmentToGrossProfitSection);
