/* eslint-disable  */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { filter } from 'lodash';
import s from './GfrReport.module.css';
import {
  getMonths,
  getForecastReport,
  getCompanies,
  getDivByCompany,
  getJDEInfo,
  getForecastReportByCompanyDivition,
} from '../../data';

import Layout from '../../components/Layout';
import ReportHeader from '../../components/ReportHeader/ReportHeader';
import GroupHeader from '../../components/GroupHeader/GroupHeader';
import GridRows from '../../components/GridRows/GridRows';
import GridRowsColumn1 from '../../components/GridRowsColumn1/GridRowsColumn1';
import GridRowsColumn2 from '../../components/GridRowsColumn2/GridRowsColumn2';
import GridRowsColumn3 from '../../components/GridRowsColumn3/GridRowsColumn3';
import GridRowsColumn4 from '../../components/GridRowsColumn4/GridRowsColumn4';
import GridRowsColumn5 from '../../components/GridRowsColumn5/GridRowsColumn5';
import PendingAwards from '../../components/PendingAwards/PendingAwards';
import DataRows from '../../components/PendingAwards/DataRows/DataRows';
import { calcNetIncome } from '../../utils/mathAndCurrency';

const headerDesc = {
  rev: 'Revenue',
  cto: 'CTO',
  adjToGrossProf: 'Adjustment to Gross Profit',
  adjGrossProf: 'Adjusted Gross Profit',
  gAndA: 'G&A',
  otherExpense: 'Other Expense/(Income)',
  netIncome: 'Net Income',
};

const rowDesc = {
  contrAwrdInPy: 'Contracts Awarded in Prior Years',
  contrAwrdYTD: 'Contracts Awarded YTD',
  pendingAwards: 'Pending Awards',
  otherWork: 'Other Future Work',
  total: 'Total',
  plan: '2018 Plan',
  retro: 'Retro',
  yardShop: 'Yard & Shop',
};

const colDesc = {
  backlogBeg: 'backlogBeginning',
  backlogEnd: 'backlogEnding',
  company: 'company',
  contractBOY: 'contractBOY',
  contractTot: 'contractTotal',
  contractYTD: 'contractYTD',
  awrdBOY: 'growthBOY',
  awrdTot: 'growthTotal',
  awrdYTD: 'growthYTD',
  nyActivity: 'nextYearActivity',
  nyGrowth: 'nextYearGrowth',
};

import { calcAdjGrossProfit } from '../../utils/mathAndCurrency';

import loading from '../../components/Images/loading.gif';
import grycorIndustrialIncImage from '../../components/Images/grycorIndustrialInc.png';

export default class GraycorSouthInc extends Component {
  static propTypes = {
    token: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);

    let today = new Date();
    let todayMonth = today.getMonth();
    let currentMonthValue = todayMonth + 1;
    var monthStaticList = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December',
    ];
    let currentMonthName = monthStaticList[todayMonth];

    let day = today.getDate();
    let yyyy = today.getFullYear();
    if (day < 10) {
      day = '0' + day;
    }
    if (currentMonthValue < 10) {
      currentMonthValue = '0' + currentMonthValue;
    }

    let currentDay = currentMonthValue + '/' + day + '/' + yyyy;

    this.state = {
      data: [],
      jdeData: [],
      companies: [],
      divisions: [],
      months: [],
      selectedCompany: [],
      selectedDivision: null,
      netIncomeTotal: '',
      selectedCompanyId: null,
      selectedMonth: [
        {
          value: currentMonthValue,
          label: currentMonthName,
        },
      ],
      isLoaderShow: 1, //1 for showing
      todayDate: currentDay,
      showAwardInput: false,
      pendingAwardsInputRow: [],
      pendingAwardsList: [],
      pendingAwardAction: -1, //-1 post action, other put action
      CompanyId: 0,
      totalOfPendingAwardList: {
        titleBottomLine: 'Total',
        totalPendingAwardRevenueValue: 0,
        totalPendingAwardCTOValue: 0,
        totalPendingAwardFYE18Revenue: 0,
        totalPendingAwardFYE18CTO: 0,
        totalPendingAwardBacklogRev2019: 0,
        totalPendingAwardBacklogCTO2019: 0,
        totalPendingAwardBacklogRev2020: 0,
        totalPendingAwardBacklogCTO2020: 0,
      },
    };
  }

  componentDidMount() {
    const { token } = this.props;
    const { companies, divisions } = this.state;
    const { setNetIncome } = this;

    getJDEInfo(token, '00300', '302', '11-30-2018')
      .then(results => {
        this.setState({
          jdeData: results,
        });
      })
      .catch(err => console.error(err));

    setNetIncome();

    let divitionValue = null;

    this.getReportByCompanyDivition(null, '00200', divitionValue);

    const getMonthList = getMonths;
    if (getMonths) {
      this.setState({
        months: getMonthList,
      });
    }
  }

  updatePendingAwardValue = (
    headerDescription,
    rowDescription,
    newValue,
    getSpecificObjId,
    columnName,
  ) => {
    this.setData(
      headerDescription,
      rowDescription,
      newValue,
      getSpecificObjId,
      columnName,
    );
  };
  setData = (header, row, newValue, getSpecificObjId, columnName) => {
    if (this.state.data === undefined || this.state.data === null) {
      return '';
    }
    let dataList = this.state.data;
    if (columnName === 'growthBOY')
      dataList[getSpecificObjId].growthBOY = newValue;

    if (columnName === 'contractBOY') {
      dataList[getSpecificObjId].contractBOY = newValue;
    }

    this.setState({
      data: dataList,
    });
  };

  getData = (data, header, row) => {
    if (data === undefined || data === null) {
      return '';
    }
    if (data.length === 0) {
      return '';
    } else {
      const filtered = filter(data, {
        headerDescription: header,
        rowDescription: row,
      });
      return filtered.length > 0 ? filtered[0] : '';
    }
  };

  onChangeHandler = e => {
    this.setState({
      pendingAwardsInputRow: {
        ...this.state.pendingAwardsInputRow,
        [e.target.name]: e.target.value,
      },
    });
  };
  onChangeHandlerDate = date => {
    const valueOfInput = Moment.date('Fri Feb 01 2019 00:00:00 GMT+0600');

    this.setState({
      pendingAwardsInputRow: {
        ...this.state.pendingAwardsInputRow,
        pendingAwardExpDate: valueOfInput,
      },
    });
  };

  onChangeHandlerObject = (event, id, columnName) => {
    let list = this.state.pendingAwardsList;
    if (columnName === 'pendingAwardTitle') {
      list[id].pendingAwardTitle = event.target.value;
    } else if (columnName === 'pendingAwardExpDate') {
      list[id].pendingAwardExpDate = event.target.value;
    } else if (columnName === 'pendingAwardRevenueValue') {
      list[id].pendingAwardRevenueValue = event.target.value;
    } else if (columnName === 'pendingAwardCTOValue') {
      list[id].pendingAwardCTOValue = event.target.value;
    } else if (columnName === 'pendingAwardFYE18Revenue') {
      list[id].pendingAwardFYE18Revenue = event.target.value;
    } else if (columnName === 'pendingAwardFYE18CTO') {
      list[id].pendingAwardFYE18CTO = event.target.value;
    } else if (columnName === 'pendingAwardBacklogRev2019') {
      list[id].pendingAwardBacklogRev2019 = event.target.value;
    } else if (columnName === 'pendingAwardBacklogCTO2019') {
      list[id].pendingAwardBacklogCTO2019 = event.target.value;
    } else if (columnName === 'pendingAwardBacklogRev2020') {
      list[id].pendingAwardBacklogRev2020 = event.target.value;
    } else if (columnName === 'pendingAwardBacklogCTO2020') {
      list[id].pendingAwardBacklogCTO2020 = event.target.value;
    }

    this.setState({
      pendingAwardsList: list,
    });
  };

  onPendingAwardUpdate = (columnName, id, value) => {
    this.totalOfPendingAward();
  };
  onPendingAwardSubmit = e => {
    e.preventDefault();

    let getAllPendingAwardList = this.state.pendingAwardsList;

    if (this.state.pendingAwardAction > -1) {
      // pendingAwardAction contain object serial number
      let id = this.state.pendingAwardAction;

      getAllPendingAwardList[id][
        clumnName
      ] = this.state.pendingAwardsInputRow.pendingAwardTitle;
    } else {
      getAllPendingAwardList.push(this.state.pendingAwardsInputRow); //POST request
    }

    this.setState({
      pendingAwardsList: getAllPendingAwardList,
      pendingAwardsInputRow: {
        pendingAwardTitle: '',
        pendingAwardExpDate: '',
        pendingAwardRevenueValue: '',
        pendingAwardCTOValue: '',
        pendingAwardFYE18Revenue: '',
        pendingAwardFYE18CTO: '',
        pendingAwardBacklogRev2019: '',
        pendingAwardBacklogCTO2019: '',
        pendingAwardBacklogRev2020: '',
        pendingAwardBacklogCTO2020: '',
      },
      pendingAwardAction: -1,
      showAwardInput: false,
    });

    this.totalOfPendingAward();
  };

  totalOfPendingAward() {
    let getAllPendingAwardList = this.state.pendingAwardsList;

    let totalPendingAwardRevenueValue = 0;
    let totalPendingAwardCTOValue = 0;
    let totalPendingAwardFYE18Revenue = 0;
    let totalPendingAwardFYE18CTO = 0;
    let totalPendingAwardBacklogRev2019 = 0;
    let totalPendingAwardBacklogCTO2019 = 0;
    let totalPendingAwardBacklogRev2020 = 0;
    let totalPendingAwardBacklogCTO2020 = 0;

    getAllPendingAwardList.forEach(item => {
      if (
        typeof parseInt(item.pendingAwardRevenueValue) == 'number' &&
        item.pendingAwardRevenueValue
      ) {
        totalPendingAwardRevenueValue += parseInt(
          item.pendingAwardRevenueValue,
        );
      }

      if (
        typeof parseInt(item.pendingAwardCTOValue) == 'number' &&
        item.pendingAwardCTOValue
      ) {
        totalPendingAwardCTOValue += parseInt(item.pendingAwardCTOValue);
      }

      if (
        typeof parseInt(item.pendingAwardFYE18Revenue) == 'number' &&
        item.pendingAwardFYE18Revenue
      ) {
        totalPendingAwardFYE18Revenue += parseInt(
          item.pendingAwardFYE18Revenue,
        );
      }

      if (
        typeof parseInt(item.pendingAwardFYE18CTO) == 'number' &&
        item.pendingAwardFYE18CTO
      ) {
        totalPendingAwardFYE18CTO += parseInt(item.pendingAwardFYE18CTO);
      }

      if (
        typeof parseInt(item.pendingAwardBacklogRev2019) == 'number' &&
        item.pendingAwardBacklogRev2019
      ) {
        totalPendingAwardBacklogRev2019 += parseInt(
          item.pendingAwardBacklogRev2019,
        );
      }

      if (
        typeof parseInt(item.pendingAwardBacklogCTO2019) == 'number' &&
        item.pendingAwardBacklogCTO2019
      ) {
        totalPendingAwardBacklogCTO2019 += parseInt(
          item.pendingAwardBacklogCTO2019,
        );
      }

      if (
        typeof parseInt(item.pendingAwardBacklogRev2020) == 'number' &&
        item.pendingAwardBacklogRev2020
      ) {
        totalPendingAwardBacklogRev2020 += parseInt(
          item.pendingAwardBacklogRev2020,
        );
      }

      if (
        typeof parseInt(item.pendingAwardBacklogCTO2020) == 'number' &&
        item.pendingAwardBacklogCTO2020
      ) {
        totalPendingAwardBacklogCTO2020 += parseInt(
          item.pendingAwardBacklogCTO2020,
        );
      }
    });

    this.setState({
      totalOfPendingAwardList: {
        titleBottomLine: 'Total',
        totalPendingAwardRevenueValue: totalPendingAwardRevenueValue,
        totalPendingAwardCTOValue: totalPendingAwardCTOValue,
        totalPendingAwardFYE18Revenue: totalPendingAwardFYE18Revenue,
        totalPendingAwardFYE18CTO: totalPendingAwardFYE18CTO,
        totalPendingAwardBacklogRev2019: totalPendingAwardBacklogRev2019,
        totalPendingAwardBacklogCTO2019: totalPendingAwardBacklogCTO2019,
        totalPendingAwardBacklogRev2020: totalPendingAwardBacklogRev2020,
        totalPendingAwardBacklogCTO2020: totalPendingAwardBacklogCTO2020,
      },
    });

    let getAllForecastData = this.state.data;

    let getSpecificObjId = 0;
    for (var i = 0; i < getAllForecastData.length; i++) {
      if (
        getAllForecastData[i].headerDescription == 'Revenue' &&
        getAllForecastData[i].rowDescription == 'Pending Awards'
      ) {
        getSpecificObjId = i;
        if (totalPendingAwardRevenueValue) {
          let columnName = 'growthBOY';
          this.updatePendingAwardValue(
            getAllForecastData[i].headerDescription,
            getAllForecastData[i].rowDescription,
            totalPendingAwardRevenueValue,
            getSpecificObjId,
            columnName,
          );
        }
        if (totalPendingAwardFYE18Revenue) {
          let columnName = 'contractBOY';
          this.updatePendingAwardValue(
            getAllForecastData[i].headerDescription,
            getAllForecastData[i].rowDescription,
            totalPendingAwardFYE18Revenue,
            getSpecificObjId,
            columnName,
          );
        }
      }
      if (
        getAllForecastData[i].headerDescription == 'CTO' &&
        getAllForecastData[i].rowDescription == 'Pending Awards'
      ) {
        if (totalPendingAwardCTOValue) {
          let columnName = 'growthBOY';
          getSpecificObjId = i;

          this.updatePendingAwardValue(
            getAllForecastData[i].headerDescription,
            getAllForecastData[i].rowDescription,
            totalPendingAwardCTOValue,
            getSpecificObjId,
            columnName,
          );
        }

        if (totalPendingAwardFYE18CTO) {
          let columnName = 'contractBOY';

          this.updatePendingAwardValue(
            getAllForecastData[i].headerDescription,
            getAllForecastData[i].rowDescription,
            totalPendingAwardFYE18CTO,
            getSpecificObjId,
            columnName,
          );
        }
      }
    }
  }

  setNetIncome = () => {
    const { getData } = this;
    const { data } = this.state;

    const newStateVal = calcNetIncome(
      (
        getData(data, headerDesc.cto, rowDesc.contrAwrdInPy).contractBOY +
        getData(data, headerDesc.cto, rowDesc.contrAwrdYTD).contractBOY +
        getData(data, headerDesc.cto, rowDesc.pendingAwards).contractBOY +
        getData(data, headerDesc.cto, rowDesc.otherWork).contractBOY -
        (getData(data, headerDesc.adjToGrossProf, rowDesc.retro).contractBOY +
          getData(data, headerDesc.adjToGrossProf, rowDesc.yardShop)
            .contractBOY)
      ).toString(),
      '656465',
      '54646',
    );

    this.setState({ netIncomeTotal: newStateVal.toString() });
  };

  getReportByCompanyDivition(
    selectedDivision,
    CompanyValue,
    selectedDivisionValue, // null 202 null
  ) {
    const { token } = this.props;
    let selectedConpanyValue;
    var divisionValue;
    if (CompanyValue == '00200') {
      selectedConpanyValue = 'GIC';
    } else if (CompanyValue == '00260') {
      selectedConpanyValue = 'GSI';
    } else if (CompanyValue == '00300') {
      selectedConpanyValue = 'GCI';
    } else {
      selectedConpanyValue = 'GIC';
    }
    if (selectedDivisionValue != null) {
      divisionValue = selectedDivisionValue;
    }

   
    getForecastReportByCompanyDivition(token, 'GIC', divisionValue) //(token,selectedConpanyValue,divisionValue)
      .then(resp => resp.json())
      .then(results => {
        const { forecastReportItemQuery } = results.data;

        if (forecastReportItemQuery.length != 0) {
          this.setState({
            data: forecastReportItemQuery,
            selectedDivision,
            isLoaderShow: 0,
          });

          console.log('check data indusconst');
          console.dir(this.state.data);
        } else {
          this.setState({
            data: 'No data is available.',
            selectedDivision,
            isLoaderShow: 0,
          });

          console.log(' else check data indusconst');
          console.dir(this.state.data);
        }
      })
      .catch(err => {
        console.error(err);

        this.setState({
          selectedDivision,
          isLoaderShow: 0,
        });
      });
  }

  addPendingAwards = () => {
    this.setState({
      showAwardInput: this.state.showAwardInput ? false : true,
    });
  };

  pendingAwardEditbtn = id => {
    let getAllPendingAwardsList = this.state.pendingAwardsList;

    let getSinglePendingAward = getAllPendingAwardsList[id];

    this.setState({
      pendingAwardAction: id,
      showAwardInput: true,
      pendingAwardsInputRow: {
        pendingAwardTitle: getSinglePendingAward.pendingAwardTitle,
        pendingAwardExpDate: getSinglePendingAward.pendingAwardExpDate,
        pendingAwardRevenueValue:
          getSinglePendingAward.pendingAwardRevenueValue,
        pendingAwardCTOValue: getSinglePendingAward.pendingAwardCTOValue,
        pendingAwardFYE18Revenue:
          getSinglePendingAward.pendingAwardFYE18Revenue,
        pendingAwardFYE18CTO: getSinglePendingAward.pendingAwardFYE18CTO,
        pendingAwardBacklogRev2019:
          getSinglePendingAward.pendingAwardBacklogRev2019,
        pendingAwardBacklogCTO2019:
          getSinglePendingAward.pendingAwardBacklogCTO2019,
        pendingAwardBacklogRev2020:
          getSinglePendingAward.pendingAwardBacklogRev2020,
        pendingAwardBacklogCTO2020:
          getSinglePendingAward.pendingAwardBacklogCTO2020,
      },
    });

    this.totalOfPendingAward();
  };

  componentWillReceiveProps(nextProps, prevState) {
    this.setState({
      isLoaderShow: 0,
    });

    console.log('component will receive');
    let divitionValue = null;

    this.getReportByCompanyDivition(
      nextProps.selectedDivision,
      nextProps.CompanyValue,
      divitionValue,
    );
  }

  pendingAwardDeletebtn = id => {
    if (confirm('Are you sure you want to delete this row?')) {
      let getAllPendingAwardsList = this.state.pendingAwardsList;

      getAllPendingAwardsList.splice(id, 1);

      this.setState({
        getAllPendingAwardsList: getAllPendingAwardsList,
      });
    }

    this.totalOfPendingAward();
  };

  render() {
    const { data } = this.state;

    if (!data.length) {
      this.setState({
        data: 'No data is available.',
      });
    }

    if (!data) {
      this.setState({
        data: 'No data is available.',
      });
    }

    const { getData } = this;
    const { token } = this.props;

    const newStateVal = calcNetIncome(
      (
        getData(data, headerDesc.cto, rowDesc.contrAwrdInPy).contractBOY +
        getData(data, headerDesc.cto, rowDesc.contrAwrdYTD).contractBOY +
        getData(data, headerDesc.cto, rowDesc.pendingAwards).contractBOY +
        getData(data, headerDesc.cto, rowDesc.otherWork).contractBOY -
        (getData(data, headerDesc.adjToGrossProf, rowDesc.retro).contractBOY +
          getData(data, headerDesc.adjToGrossProf, rowDesc.yardShop)
            .contractBOY)
      ).toString(),
      '656465',
      '54646',
    );

    this.setState({ netIncomeTotal: newStateVal.toString() });
  }

  render() {
    const { data, jdeData } = this.state;
    if (!data.length) {
      this.setState({
        data: 'No data is available.',
      });
    }
    const { getData, setNetIncome } = this;
    const { token } = this.props;

    const pendingdr = [
      {
        idx: 1,
        title: 'the Title for this',
        col1: 'yada',
        col2: 'yada',
        col3: 'yada',
        col4: 'yada',
        col5: 'yada',
        col6: 'yada',
        col7: 'yada',
        col8: 'yada',
        col9: 'yada',
      },
      {
        idx: 2,
        title: 'the other Title for this',
        col1: 'Blah',
        col2: 'Blah',
        col3: 'Blah',
        col4: 'Blah',
        col5: 'Blah',
        col6: 'Blah',
        col7: 'Blah',
        col8: 'Blah',
        col9: 'Blah',
      },
    ];

    const bottomLine = [
      {
        idx: 1,
        titleBottomLine: 'Total',
        col1BottomLine: 'yada',
        col2BottomLine: 'yada',
        col3BottomLine: 'yada',
        col4BottomLine: 'yada',
        col5BottomLine: 'yada',
        col6BottomLine: 'yada',
        col7BottomLine: 'yada',
        col8BottomLine: 'yada',
        col9BottomLine: 'yada',
      },
    ];

    const {
      companies,
      divisions,
      selectedCompany,
      selectedDivision,
      netIncomeTotal,
    } = this.state;

    const headerDesc = {
      rev: 'Revenue',
      cto: 'CTO',
      adjToGrossProf: 'Adjustment to Gross Profit',
      adjGrossProf: 'Adjusted Gross Profit',
      gAndA: 'G&A',
      otherExpense: 'Other Expense/(Income)',
      netIncome: 'Net Income',
    };

    const rowDesc = {
      contrAwrdInPy: 'Contracts Awarded in Prior Years',
      contrAwrdYTD: 'Contracts Awarded YTD',
      pendingAwards: 'Pending Awards',
      otherWork: 'Other Future Work',
      total: 'Total',
      plan: '2018 Plan',
      retro: 'Retro',
      yardShop: 'Yard & Shop',
    };

    const colDesc = {
      backlogBeg: 'backlogBeginning',
      backlogEnd: 'backlogEnding',
      company: 'company',
      contractBOY: 'contractBOY',
      contractTot: 'contractTotal',
      contractYTD: 'contractYTD',
      awrdBOY: 'growthBOY',
      awrdTot: 'growthTotal',
      awrdYTD: 'growthYTD',
      nyActivity: 'nextYearActivity',
      nyGrowth: 'nextYearGrowth',
    };

    const {
      months,
      selectedMonth,
      isLoaderShow,
      todayDate,
      showAwardInput,
      addPendingAwards,
      pendingAwardsList,
      pendingAwardsInputRow,
      totalOfPendingAwardList,
    } = this.state;

    return (
      <div>
        <div
          className={s.loaderContainer}
          style={
            isLoaderShow == 1
              ? { display: 'inline-block' }
              : { display: 'none' }
          }
        >
          <div className={s.loaderImageContainer}>
            <img src={loading} />
          </div>
        </div>

        {data != 'No data is available.' ? (
          <div className="withData">
            <GroupHeader
              companyId="00260"
              colorCode="#23376F"
              is2ColHeader={false}
              title={headerDesc.rev}
              column1Top="Backlog"
              column1Bottom="Beginning"
              column2Top="Awards/Growth"
              column2Left="Year-to-Date"
              column2Center="Balance of Year"
              column2Right="Total"
              column3Top="Contract Activity"
              column3Left="Year-to-Date"
              column3Center="Balance of Year"
              column3Right="Total"
              column4Top="Backlog"
              column4Bottom="Ending"
              column5Top="Next Year"
              column5Left="Awards/Growth"
              column5Right="Activity"
              column1Data={
                <GridRowsColumn1
                  row1Data={getData(
                    data,
                    headerDesc.rev,
                    rowDesc.contrAwrdInPy,
                  )}
                  row2Data={getData(data, headerDesc.rev, rowDesc.contrAwrdYTD)}
                  row3Data={getData(
                    data,
                    headerDesc.rev,
                    rowDesc.pendingAwards,
                  )}
                  row4Data={getData(data, headerDesc.rev, rowDesc.otherWork)}
                  row5Data="3243"
                  row6Data="65432"
                  token={token}
                  columnVisible
                  greyOut
                />
              }
              column2Data={
                <GridRowsColumn2
                  row1Data={getData(
                    data,
                    headerDesc.rev,
                    rowDesc.contrAwrdInPy,
                  )}
                  row2Data={getData(data, headerDesc.rev, rowDesc.contrAwrdYTD)}
                  row3Data={getData(
                    data,
                    headerDesc.rev,
                    rowDesc.pendingAwards,
                  )}
                  row4Data={getData(data, headerDesc.rev, rowDesc.otherWork)}
                  row5Data="784542"
                  row6Data="784542"
                  greyOut
                  row1Col2Data={getData(
                    data,
                    headerDesc.rev,
                    rowDesc.contrAwrdInPy,
                  )}
                  row2Col2Data={getData(
                    data,
                    headerDesc.rev,
                    rowDesc.contrAwrdYTD,
                  )}
                  row3Col2Data={getData(
                    data,
                    headerDesc.rev,
                    rowDesc.pendingAwards,
                  )}
                  row4Col2Data={getData(
                    data,
                    headerDesc.rev,
                    rowDesc.otherWork,
                  )}
                  row5Col2Data="784542"
                  row6Col2Data="784542"
                  row1Col3Data={getData(
                    data,
                    headerDesc.rev,
                    rowDesc.contrAwrdInPy,
                  )}
                  row2Col3Data={getData(
                    data,
                    headerDesc.rev,
                    rowDesc.contrAwrdYTD,
                  )}
                  row3Col3Data={getData(
                    data,
                    headerDesc.rev,
                    rowDesc.pendingAwards,
                  )}
                  row4Col3Data={getData(
                    data,
                    headerDesc.rev,
                    rowDesc.otherWork,
                  )}
                  row5Col3Data="784542"
                  row6Col3Data="784542"
                  token={token}
                />
              }
              column3Data={
                <GridRowsColumn3
                  yellowBack
                  greyOut
                  row1Data={getData(
                    data,
                    headerDesc.rev,
                    rowDesc.contrAwrdInPy,
                  )}
                  row2Data={getData(data, headerDesc.rev, rowDesc.contrAwrdYTD)}
                  row3Data={getData(
                    data,
                    headerDesc.rev,
                    rowDesc.pendingAwards,
                  )}
                  row4Data={getData(data, headerDesc.rev, rowDesc.otherWork)}
                  row5Data="6646799"
                  row6Data="6646799"
                  row1Col2Data={getData(
                    data,
                    headerDesc.rev,
                    rowDesc.contrAwrdInPy,
                  )}
                  row2Col2Data={getData(
                    data,
                    headerDesc.rev,
                    rowDesc.contrAwrdYTD,
                  )}
                  row3Col2Data={getData(
                    data,
                    headerDesc.rev,
                    rowDesc.pendingAwards,
                  )}
                  row4Col2Data={getData(
                    data,
                    headerDesc.rev,
                    rowDesc.otherWork,
                  )}
                  row5Col2Data="6646799"
                  row6Col2Data="6646799"
                  row1Col3Data={getData(
                    data,
                    headerDesc.rev,
                    rowDesc.contrAwrdInPy,
                  )}
                  row2Col3Data={getData(
                    data,
                    headerDesc.rev,
                    rowDesc.contrAwrdYTD,
                  )}
                  row3Col3Data={getData(
                    data,
                    headerDesc.rev,
                    rowDesc.pendingAwards,
                  )}
                  row4Col3Data={getData(
                    data,
                    headerDesc.rev,
                    rowDesc.otherWork,
                  )}
                  row5Col3Data="6646799"
                  row6Col3Data="6646799"
                  token={token}
                />
              }
              column4Data={
                <GridRowsColumn4
                  row1Data={getData(
                    data,
                    headerDesc.rev,
                    rowDesc.contrAwrdInPy,
                  )}
                  row2Data={getData(data, headerDesc.rev, rowDesc.contrAwrdYTD)}
                  row3Data={getData(
                    data,
                    headerDesc.rev,
                    rowDesc.pendingAwards,
                  )}
                  row4Data={getData(data, headerDesc.rev, rowDesc.otherWork)}
                  row5Data="656454"
                  row6Data="656454"
                  token={token}
                />
              }
              column5Data={
                <GridRowsColumn5
                  row1Data={getData(
                    data,
                    headerDesc.rev,
                    rowDesc.contrAwrdInPy,
                  )}
                  row2Data={getData(data, headerDesc.rev, rowDesc.contrAwrdYTD)}
                  row3Data={getData(
                    data,
                    headerDesc.rev,
                    rowDesc.pendingAwards,
                  )}
                  row4Data={getData(data, headerDesc.rev, rowDesc.otherWork)}
                  row5Data="2155"
                  row6Data="2155"
                  row1Col2Data={getData(
                    data,
                    headerDesc.rev,
                    rowDesc.contrAwrdInPy,
                  )}
                  row2Col2Data={getData(
                    data,
                    headerDesc.rev,
                    rowDesc.contrAwrdYTD,
                  )}
                  row3Col2Data={getData(
                    data,
                    headerDesc.rev,
                    rowDesc.pendingAwards,
                  )}
                  row4Col2Data={getData(
                    data,
                    headerDesc.rev,
                    rowDesc.otherWork,
                  )}
                  row5Col2Data="2155"
                  row6Col2Data="2155"
                  token={token}
                />
              }
            >
              <GridRows
                row1Title="Contracts Awarded in Prior Years"
                row2Title="Contracts Awarded YTD"
                row3Title="Pending Awards (list below)"
                row4Title="Other Future Work"
                row5Title="Total"
                row6Title="2018 Plan"
              />
            </GroupHeader>
            <GroupHeader
              is2ColHeader={false}
              colorCode="#23376F"
              title="CTO"
              column1Top="Backlog"
              column1Bottom="Beginning"
              column2Top="Awards/Growth"
              column2Left="Year-to-Date"
              column2Center="Balance of Year"
              column2Right="Total"
              column3Top="Contract Activity"
              column3Left="Year-to-Date"
              column3Center="Balance of Year"
              column3Right="Total"
              column4Top="Backlog"
              column4Bottom="Ending"
              column5Top="Next Year"
              column5Left="Awards/Growth"
              column5Right="Activity"
              column1Data={
                <GridRowsColumn1
                  row1Data={getData(
                    data,
                    headerDesc.cto,
                    rowDesc.contrAwrdInPy,
                  )}
                  row2Data={getData(data, headerDesc.cto, rowDesc.contrAwrdYTD)}
                  row3Data={getData(
                    data,
                    headerDesc.cto,
                    rowDesc.pendingAwards,
                  )}
                  row4Data={getData(data, headerDesc.cto, rowDesc.otherWork)}
                  row5Data="3243"
                  row6Data="65432"
                  token={token}
                  columnVisible
                  greyOut
                />
              }
              column2Data={
                <GridRowsColumn2
                  greyOut
                  row1Data={getData(
                    data,
                    headerDesc.cto,
                    rowDesc.contrAwrdInPy,
                  )}
                  row2Data={getData(data, headerDesc.cto, rowDesc.contrAwrdYTD)}
                  row3Data={getData(
                    data,
                    headerDesc.cto,
                    rowDesc.pendingAwards,
                  )}
                  row4Data={getData(data, headerDesc.cto, rowDesc.otherWork)}
                  row5Data="36646"
                  row6Data="36646"
                  row1Col2Data={getData(
                    data,
                    headerDesc.cto,
                    rowDesc.contrAwrdInPy,
                  )}
                  row2Col2Data={getData(
                    data,
                    headerDesc.cto,
                    rowDesc.contrAwrdYTD,
                  )}
                  row3Col2Data={getData(
                    data,
                    headerDesc.cto,
                    rowDesc.pendingAwards,
                  )}
                  row4Col2Data={getData(
                    data,
                    headerDesc.cto,
                    rowDesc.otherWork,
                  )}
                  row5Col2Data="36646"
                  row6Col2Data="36646"
                  row1Col3Data={getData(
                    data,
                    headerDesc.cto,
                    rowDesc.contrAwrdInPy,
                  )}
                  row2Col3Data={getData(
                    data,
                    headerDesc.cto,
                    rowDesc.contrAwrdYTD,
                  )}
                  row3Col3Data={getData(
                    data,
                    headerDesc.cto,
                    rowDesc.pendingAwards,
                  )}
                  row4Col3Data={getData(
                    data,
                    headerDesc.cto,
                    rowDesc.otherWork,
                  )}
                  row5Col3Data="36646"
                  row6Col3Data="36646"
                  token={token}
                />
              }
              column3Data={
                <GridRowsColumn3
                  setNetIncome={setNetIncome}
                  yellowBack
                  greyOut
                  row1Data={getData(
                    data,
                    headerDesc.cto,
                    rowDesc.contrAwrdInPy,
                  )}
                  row2Data={getData(data, headerDesc.cto, rowDesc.contrAwrdYTD)}
                  row3Data={getData(
                    data,
                    headerDesc.cto,
                    rowDesc.pendingAwards,
                  )}
                  row4Data={getData(data, headerDesc.cto, rowDesc.otherWork)}
                  row5Data="3464"
                  row6Data="3464"
                  row1Col2Data={getData(
                    data,
                    headerDesc.cto,
                    rowDesc.contrAwrdInPy,
                  )}
                  row2Col2Data={getData(
                    data,
                    headerDesc.cto,
                    rowDesc.contrAwrdYTD,
                  )}
                  row3Col2Data={getData(
                    data,
                    headerDesc.cto,
                    rowDesc.pendingAwards,
                  )}
                  row4Col2Data={getData(
                    data,
                    headerDesc.cto,
                    rowDesc.otherWork,
                  )}
                  row5Col2Data="3464"
                  row6Col2Data="3464"
                  row1Col3Data={getData(
                    data,
                    headerDesc.cto,
                    rowDesc.contrAwrdInPy,
                  )}
                  row2Col3Data={getData(
                    data,
                    headerDesc.cto,
                    rowDesc.contrAwrdYTD,
                  )}
                  row3Col3Data={getData(
                    data,
                    headerDesc.cto,
                    rowDesc.pendingAwards,
                  )}
                  row4Col3Data={getData(
                    data,
                    headerDesc.cto,
                    rowDesc.otherWork,
                  )}
                  row5Col3Data="3464"
                  row6Col3Data="3464"
                  token={token}
                />
              }
              column4Data={
                <GridRowsColumn4
                  row1Data={getData(
                    data,
                    headerDesc.cto,
                    rowDesc.contrAwrdInPy,
                  )}
                  row2Data={getData(data, headerDesc.cto, rowDesc.contrAwrdYTD)}
                  row3Data={getData(
                    data,
                    headerDesc.cto,
                    rowDesc.pendingAwards,
                  )}
                  row4Data={getData(data, headerDesc.cto, rowDesc.otherWork)}
                  row5Data="66565"
                  row6Data="66565"
                  token={token}
                />
              }
              column5Data={
                <GridRowsColumn5
                  row1Data={getData(
                    data,
                    headerDesc.cto,
                    rowDesc.contrAwrdInPy,
                  )}
                  row2Data={getData(data, headerDesc.cto, rowDesc.contrAwrdYTD)}
                  row3Data={getData(
                    data,
                    headerDesc.cto,
                    rowDesc.pendingAwards,
                  )}
                  row4Data={getData(data, headerDesc.cto, rowDesc.otherWork)}
                  row5Data="6464"
                  row6Data="6464"
                  row1Col2Data={getData(
                    data,
                    headerDesc.cto,
                    rowDesc.contrAwrdInPy,
                  )}
                  row2Col2Data={getData(
                    data,
                    headerDesc.cto,
                    rowDesc.contrAwrdYTD,
                  )}
                  row3Col2Data={getData(
                    data,
                    headerDesc.cto,
                    rowDesc.pendingAwards,
                  )}
                  row4Col2Data={getData(
                    data,
                    headerDesc.cto,
                    rowDesc.otherWork,
                  )}
                  row5Col2Data="6464"
                  row6Col2Data="6464"
                  token={token}
                />
              }
            >
              <GridRows
                row1Title="Contracts Awarded in Prior Years"
                row2Title="Contracts Awarded YTD"
                row3Title="Pending Awards (list below)"
                row4Title="Other Future Work"
                row5Title="Total"
                row6Title="2018 Plan"
              />
            </GroupHeader>
            <GroupHeader
              is2ColHeader
              colorCode="#23376F"
              title="Adjustment to Gross Profit"
              column1Top="Backlog"
              column1Bottom="Beginning"
              column2Top="Awards/Growth"
              column2Left="Year-to-Date"
              column2Center="Balance of Year"
              column2Right="Total"
              column3Top="Contract Activity"
              column3Left="Year-to-Date"
              column3Center="Balance of Year"
              column3Right="Total"
              column4Top="Backlog"
              column4Bottom="Ending"
              column5Top="Next Year"
              column5Left="Awards/Growth"
              column5Right="Next Year"
              column1Data={<GridRowsColumn1 token={token} />}
              column3Data={
                <GridRowsColumn3
                  fourRowYellow
                  yellowBack
                  fourRowGroup
                  row1Data={getData(
                    data,
                    headerDesc.adjToGrossProf,
                    rowDesc.retro,
                  )}
                  row2Data={getData(
                    data,
                    headerDesc.adjToGrossProf,
                    rowDesc.yardShop,
                  )}
                  row3Data="646464"
                  row4Data="646464"
                  row1Col2Data={getData(
                    data,
                    headerDesc.adjToGrossProf,
                    rowDesc.retro,
                  )}
                  row2Col2Data={getData(
                    data,
                    headerDesc.adjToGrossProf,
                    rowDesc.yardShop,
                  )}
                  row3Col2Data="646464"
                  row4Col2Data="646464"
                  row1Col3Data={getData(
                    data,
                    headerDesc.adjToGrossProf,
                    rowDesc.retro,
                  )}
                  row2Col3Data={getData(
                    data,
                    headerDesc.adjToGrossProf,
                    rowDesc.yardShop,
                  )}
                  row3Col3Data="646464"
                  row4Col3Data="646464"
                  token={token}
                />
              }
              column5Data={
                <GridRowsColumn5
                  fourRowGroup
                  col1Hidden
                  row1Col2Data={getData(
                    data,
                    headerDesc.adjToGrossProf,
                    rowDesc.retro,
                  )}
                  row2Col2Data={getData(
                    data,
                    headerDesc.adjToGrossProf,
                    rowDesc.yardShop,
                  )}
                  row3Col2Data="454"
                  row4Col2Data="646464"
                />
              }
            >
              <GridRows
                row1Title="Retro"
                row2Title="Yard & Shop"
                row3Title="Total"
                row4Title="2018 Plan"
              />
            </GroupHeader>
            <GroupHeader
              is2ColHeader
              colorCode="#23376F"
              title="Adjusted Gross Profit"
              column1Top="Backlog"
              column1Bottom="Beginning"
              column2Top="Awards/Growth"
              column2Left="Year-to-Date"
              column2Center="Balance of Year"
              column2Right="Total"
              column3Top="Contract Activity"
              column3Left="Year-to-Date"
              column3Center="Balance of Year"
              column3Right="Total"
              column4Top="Backlog"
              column4Bottom="Ending"
              column5Top="Next Year"
              column5Left="Awards/Growth"
              column5Right="Next Year"
              column1Data={
                <GridRowsColumn1
                  row1Data="5456465"
                  row2Data="5456465"
                  row3Data="5456465"
                  row4Data="5456465"
                  token={token}
                />
              }
              column3Data={
                <GridRowsColumn3
                  yellowBack
                  twoRowGroup
                  fourRowGroup
                  row1Data={(
                    getData(data, headerDesc.cto, rowDesc.contrAwrdInPy)
                      .contractYTD +
                    getData(data, headerDesc.cto, rowDesc.contrAwrdYTD)
                      .contractYTD +
                    getData(data, headerDesc.cto, rowDesc.pendingAwards)
                      .contractYTD +
                    getData(data, headerDesc.cto, rowDesc.otherWork)
                      .contractYTD -
                    (getData(data, headerDesc.adjToGrossProf, rowDesc.retro)
                      .contractYTD +
                      getData(data, headerDesc.adjToGrossProf, rowDesc.yardShop)
                        .contractYTD)
                  ).toString()}
                  row2Data="454654"
                  row1Col2Data={(
                    getData(data, headerDesc.cto, rowDesc.contrAwrdInPy)
                      .contractBOY +
                    getData(data, headerDesc.cto, rowDesc.contrAwrdYTD)
                      .contractBOY +
                    getData(data, headerDesc.cto, rowDesc.pendingAwards)
                      .contractBOY +
                    getData(data, headerDesc.cto, rowDesc.otherWork)
                      .contractBOY -
                    (getData(data, headerDesc.adjToGrossProf, rowDesc.retro)
                      .contractBOY +
                      getData(data, headerDesc.adjToGrossProf, rowDesc.yardShop)
                        .contractBOY)
                  ).toString()}
                  row2Col2Data="454654"
                  row1Col3Data={(
                    getData(data, headerDesc.cto, rowDesc.contrAwrdInPy)
                      .contractTotal +
                    getData(data, headerDesc.cto, rowDesc.contrAwrdYTD)
                      .contractTotal +
                    getData(data, headerDesc.cto, rowDesc.pendingAwards)
                      .contractTotal +
                    getData(data, headerDesc.cto, rowDesc.otherWork)
                      .contractTotal -
                    (getData(data, headerDesc.adjToGrossProf, rowDesc.retro)
                      .contractTotal +
                      getData(data, headerDesc.adjToGrossProf, rowDesc.yardShop)
                        .contractTotal)
                  ).toString()}
                  row2Col3Data="454654"
                  token={token}
                />
              }
              column5Data={
                <GridRowsColumn5
                  col1Hidden
                  fourRowGroup
                  twoRowGroup
                  row1Col2Data={(
                    getData(data, headerDesc.cto, rowDesc.contrAwrdInPy)
                      .nextYearActivity +
                    getData(data, headerDesc.cto, rowDesc.contrAwrdYTD)
                      .nextYearActivity +
                    getData(data, headerDesc.cto, rowDesc.pendingAwards)
                      .nextYearActivity +
                    getData(data, headerDesc.cto, rowDesc.otherWork)
                      .nextYearActivity -
                    (getData(data, headerDesc.adjToGrossProf, rowDesc.retro)
                      .nextYearActivity +
                      getData(data, headerDesc.adjToGrossProf, rowDesc.yardShop)
                        .nextYearActivity)
                  ).toString()}
                  row2Col2Data="464"
                />
              }
            >
              <GridRows row1Title="Total" row2Title="2018 Plan" />
            </GroupHeader>
            <GroupHeader
              is2ColHeader
              colorCode="#23376F"
              title="G&A"
              column1Top="Backlog"
              column1Bottom="Beginning"
              column2Top="Awards/Growth"
              column2Left="Year-to-Date"
              column2Center="Balance of Year"
              column2Right="Total"
              column3Top="Contract Activity"
              column3Left="Year-to-Date"
              column3Center="Balance of Year"
              column3Right="Total"
              column4Top="Backlog"
              column4Bottom="Ending"
              column5Top="Next Year"
              column5Left="Awards/Growth"
              column5Right="Next Year"
              column1Data={
                <GridRowsColumn1 row1Data="Row" row2Data="Row" token={token} />
              }
              column3Data={
                <GridRowsColumn3
                  yellowBack
                  twoRowGroup
                  fourRowGroup
                  row1Data="656465"
                  row2Data="656465"
                  row1Col2Data="656465"
                  row2Col2Data="656465"
                  row1Col3Data="656465"
                  row2Col3Data="656465"
                  token={token}
                />
              }
              column5Data={
                <GridRowsColumn5
                  col1Hidden
                  fourRowGroup
                  twoRowGroup
                  row1Col2Data="5445"
                  row2Col2Data="5445"
                />
              }
            >
              <GridRows row1Title="Total" row2Title="2018 Plan" />
            </GroupHeader>
            <GroupHeader
              is2ColHeader
              colorCode="#23376F"
              title="Other Expense/(Income)"
              column1Top="Backlog"
              column1Bottom="Beginning"
              column2Top="Awards/Growth"
              column2Left="Year-to-Date"
              column2Center="Balance of Year"
              column2Right="Total"
              column3Top="Contract Activity"
              column3Left="Year-to-Date"
              column3Center="Balance of Year"
              column3Right="Total"
              column4Top="Backlog"
              column4Bottom="Ending"
              column5Top="Next Year"
              column5Left="Awards/Growth"
              column5Right="Next Year"
              column1Data={
                <GridRowsColumn1
                  row1Data="6454"
                  row2Data="6454"
                  row3Data="6454"
                  row4Data="6454"
                  token={token}
                />
              }
              column3Data={
                <GridRowsColumn3
                  twoRowGroup
                  fourRowGroup
                  row1Data="54646"
                  row2Data="54646"
                  row1Col2Data="54646"
                  row2Col2Data="54646"
                  row1Col3Data="54646"
                  row2Col3Data="54646"
                  token={token}
                />
              }
              column5Data={
                <GridRowsColumn5
                  col1Hidden
                  fourRowGroup
                  twoRowGroup
                  row1Col2Data="5646"
                  row2Col2Data="5646"
                />
              }
            >
              <GridRows row1Title="Total" row2Title="2018 Plan" />
            </GroupHeader>
            <GroupHeader
              is2ColHeader
              colorCode="#23376F"
              title="Net Income"
              column1Top="Backlog"
              column1Bottom="Beginning"
              column2Top="Awards/Growth"
              column2Left="Year-to-Date"
              column2Center="Balance of Year"
              column2Right="Total"
              column3Top="Contract Activity"
              column3Left="Year-to-Date"
              column3Center="Balance of Year"
              column3Right="Total"
              column4Top="Backlog"
              column4Bottom="Ending"
              column5Top="Next Year"
              column5Left="Awards/Growth"
              column5Right="Next Year"
              column1Data={
                <GridRowsColumn1
                  row1Data="54646"
                  row2Data="54646"
                  row3Data="54646"
                  row4Data="54646"
                  row5Data="54646"
                  row6Data="54646"
                  token={token}
                  columnVisible
                />
              }
              column2Data={
                <GridRowsColumn2
                  row1Data="5646"
                  row2Data="5646"
                  row3Data="5646"
                  row4Data="5646"
                  row5Data="5646"
                  row6Data="5646"
                  row1Col2Data="5646"
                  row2Col2Data="5646"
                  row3Col2Data="5646"
                  row4Col2Data="5646"
                  row5Col2Data="5646"
                  row6Col2Data="5646"
                  row1Col3Data="5646"
                  row2Col3Data="5646"
                  row3Col3Data="5646"
                  row4Col3Data="5646"
                  row5Col3Data="5646"
                  row6Col3Data="5646"
                />
              }
              column3Data={
                <GridRowsColumn3
                  twoRowGroup
                  fourRowGroup
                  netIncome
                  row1Data={calcNetIncome(
                    (
                      getData(data, headerDesc.cto, rowDesc.contrAwrdInPy)
                        .contractYTD +
                      getData(data, headerDesc.cto, rowDesc.contrAwrdYTD)
                        .contractYTD +
                      getData(data, headerDesc.cto, rowDesc.pendingAwards)
                        .contractYTD +
                      getData(data, headerDesc.cto, rowDesc.otherWork)
                        .contractYTD -
                      (getData(data, headerDesc.adjToGrossProf, rowDesc.retro)
                        .contractYTD +
                        getData(
                          data,
                          headerDesc.adjToGrossProf,
                          rowDesc.yardShop,
                        ).contractYTD)
                    ).toString(),
                    '656465',
                    '54646',
                  )}
                  row2Data="64646"
                  row1Col2Data="56"
                  row2Col2Data="64646"
                  row1Col3Data="64646"
                  row2Col3Data="64646"
                  token={token}
                  setNetIncome={setNetIncome}
                  netIncomeTotal={netIncomeTotal}
                />
              }
              column4Data={<GridRowsColumn4 row1Data="54" row2Data="54" />}
              column5Data={
                <GridRowsColumn5
                  col1Hidden
                  fourRowGroup
                  twoRowGroup
                  row1Col2Data={calcNetIncome(
                    (
                      getData(data, headerDesc.cto, rowDesc.contrAwrdInPy)
                        .nextYearActivity +
                      getData(data, headerDesc.cto, rowDesc.contrAwrdYTD)
                        .nextYearActivity +
                      getData(data, headerDesc.cto, rowDesc.pendingAwards)
                        .nextYearActivity +
                      getData(data, headerDesc.cto, rowDesc.otherWork)
                        .nextYearActivity -
                      (getData(data, headerDesc.adjToGrossProf, rowDesc.retro)
                        .nextYearActivity +
                        getData(
                          data,
                          headerDesc.adjToGrossProf,
                          rowDesc.yardShop,
                        ).nextYearActivity)
                    ).toString(),
                    '5445',
                    '5646',
                  )}
                  row2Col2Data="4646"
                />
              }
            >
              <GridRows row1Title="Total" row2Title="2018 Plan" />
            </GroupHeader>

            <PendingAwards
              title="Pending Awards"
              colorCode="#23376F"
              totalOfPendingAwardList={totalOfPendingAwardList}
              addPendingAwards={this.addPendingAwards}
              openAwardInputField={showAwardInput}
              onChangeHandler={this.onChangeHandler}
              onChangeHandlerDate={this.onChangeHandlerDate}
              onPendingAwardSubmit={this.onPendingAwardSubmit}
              pendingAwardsInputRow={pendingAwardsInputRow}
            >
              <DataRows
                pendingAwardsList={pendingAwardsList}
                pendingAwardEditbtn={this.pendingAwardEditbtn}
                pendingAwardDeletebtn={this.pendingAwardDeletebtn}
                onPendingAwardUpdate={this.onPendingAwardUpdate}
                onChangeHandlerObject={this.onChangeHandlerObject}
                pendingAwardsInputRow={pendingAwardsInputRow}
              />
            </PendingAwards>
          </div>
        ) : (
          <h2 style={{ textAlign: 'center' }}>No data is available.</h2>
        )}
      </div>
    );
  }
}
