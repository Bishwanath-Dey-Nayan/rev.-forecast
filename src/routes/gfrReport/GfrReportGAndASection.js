/* eslint-disable  */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { filter } from 'lodash';
import * as gfrActions from '../../store/gfr/gfr.actions';

import GridRows from '../../components/GridRows/GridRows';
import GridRowsColumn1 from '../../components/GridRowsColumn1/GridRowsColumn1';
import GridRowsColumn3 from '../../components/GridRowsColumn3/GridRowsColumn3';
import GridRowsColumn5 from '../../components/GridRowsColumn5/GridRowsColumn5';
import { headerDesc, rowDesc } from '../../constants/GfrReport.constant';
import GroupHeaderForThree from '../../components/GroupHeader/GroupHeaderForThree';
import * as gfrSelector from '../../store/gfr/gfr.selectors';
import { getData } from '../../utils/GfrReportHelper';
import { getMonthValue } from '../../utils/ForecastPlanHelper';

class GfrReportGAndASection extends Component {
  static propTypes = {
    token: PropTypes.string.isRequired,
    data: PropTypes.instanceOf(Array).isRequired,
  };

  updateGfrCellData = cellObj => {
    if (!cellObj.headerDescription) cellObj.headerDescription = headerDesc.gAndA;
  
    this.props.updateGfrCellData(cellObj); 
  };

  updateforecastPlanCellData = cellObj => {
    if (!cellObj.headerDescription) cellObj.headerDescription = headerDesc.gAndA;
    this.props.updateforecastPlanCellData(cellObj);
  };

  render() {
    const { data, token, jdeData, ga_row1Col3Data, reportMode,ga_row1Data,companyCode, forecastPlanData } = this.props;
    let row2Data = getData(forecastPlanData,
      headerDesc.gAndA,
      rowDesc.plan);
    let fiscalMonthValue = getMonthValue(this.props.selectedMonth.label);
    let row2Data_YTD = ((row2Data.contractTotal) / 12) * fiscalMonthValue;
    row2Data.contractYTD = row2Data_YTD;
    row2Data.contractBOY = row2Data.contractTotal - row2Data_YTD;

    return (
      <GroupHeaderForThree
        is2ColHeader
        cssPosition="leftSide topMarginZeor "
        gridCategory="doubleGrid nextYearcss"
        colorCode={this.props.companyColor}
        title="G&A" 
        column1Top="Backlog"
        column1Bottom="Beginning"
        column2Top="Awards/Growth"
        column2Left="Year-to-Date"
        column2Center="Balance of Year"
        column2Right="Total"
        column3Top="Contract Activity"
        column3Left="Year-to-Date"
        column3Center="Balance of Year"
        column3Right="Total"
        column4Top="Backlog"
        column4Bottom="Ending"
        column5Top="Next Year"
        column5Left="Awards/Growth"
        column5Right="Next Year"
        column1Data={
          <GridRowsColumn1
            row1Data="Row"
            row2Data="Row"
            token={token}
            updateGfrCellData={this.props.gfrActions.updateGfrCellDataRequest}
            reportMode={reportMode}
          />
        }
        column3Data={
          <GridRowsColumn3 
            yellowBack
            twoRowGroup
            fourRowGroup
            headerDescProp={headerDesc.gAndA}
            // row1Data={getData(jdeData, headerDesc.gAndA, rowDesc.total)}
            row1Data={ga_row1Data}
            row2Data={row2Data}
            row1Col2Data={ga_row1Data}
            row2Col2Data={row2Data}
            row2Col3Data={row2Data}
            token={token}
            updateGfrCellData={this.updateGfrCellData}
            isRow1Editable={true}
            isRow2Editable={false}
            reportMode={reportMode}
            updateforecastPlanCellData={this.updateforecastPlanCellData}
          />
        }
        column5Data={
          <GridRowsColumn5
            col1Hidden
            fourRowGroup
            twoRowGroup
            row1Col2Data="5445"
            row2Col2Data="5445"
            updateGfrCellData={this.updateGfrCellData}
            reportMode={reportMode}
          />
        }
      >
        <GridRows row1Title="Total" row2Title="2019 Plan" row3Title="2018 Plan" />
      </GroupHeaderForThree>
    );
  }
}

function mapStateToProps({ gfrReducer, fpReducer  }) { 
  return {
    token: gfrReducer.token,
    data: gfrReducer.data,
    jdeData: gfrReducer.jdeData,
    companyCode: gfrSelector.getCompanyCode(gfrReducer),
    forecastPlanData: fpReducer.forecastPlanData,
    selectedMonth: gfrReducer.selectedMonth,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    gfrActions: bindActionCreators(gfrActions, dispatch),
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(GfrReportGAndASection);
