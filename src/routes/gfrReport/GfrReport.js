/* eslint-disable  */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { filter } from 'lodash';
import s from './GfrReport.module.css';
import * as gfrActions from '../../store/gfr/gfr.actions';
import * as paActions from '../../store/pendingawards/pa.actions';
import * as fpActions from '../../store/forecastPlan/fp.actions';
import * as gfrSelector from '../../store/gfr/gfr.selectors';
import { getMonths, getDivByCompany, getCompanyCode } from '../../data';

import Layout from '../../components/Layout';
import ReportHeader from '../../components/ReportHeader/ReportHeader';
import PendingAwards from '../../components/PendingAwards/PendingAwards';
import DataRows from '../../components/PendingAwards/DataRows/DataRows';
import GfrReportRevenueSection from './GfrReportRevenueSection';
import GfrReportCtoSection from './GfrReportCtoSection';
import GfrReportAdjustmentToGrossProfitSection from './GfrReportAdjustmentToGrossProfitSection';
import GfrReportAdjustedGrossProfitSection from './GfrReportAdjustedGrossProfitSection';
import GfrReportGAndASection from './GfrReportGAndASection';
import GfrReportOtherExpensesOrIncomeSection from './GfrReportOtherExpensesOrIncomeSection';
import GfrReportNetIncomeSection from './GfrReportNetIncomeSection';
import loading from '../../components/Images/loading.gif';
import moment from 'moment';
import {
  headerDesc,
  rowDesc,
  reportModeLists,
} from '../../constants/GfrReport.constant';
import { getData } from '../../utils/GfrReportHelper';
import { getTotal } from '../../utils/mathAndCurrency';
import FileSaver from 'file-saver';
import { pdf } from '@react-pdf/renderer';
import GfrPdfDocument from '../../components/Pdf/GfrPdfDocument';
import { ConsoleLogger } from '@aws-amplify/core';

const formValid = formErrors => {
  let valid = true;
  Object.values(formErrors).forEach(val => {
    val.length > 0 && (valid = false)
  });
  return valid;
}

class GfrReport extends Component {
  static propTypes = {
    token: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);
    let today = new Date();
    let todayMonth = today.getMonth();
    let currentMonthValue = todayMonth + 1;
    var monthStaticList = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December',
    ];
    let currentMonthName = monthStaticList[todayMonth];

    let day = today.getDate();
    let yyyy = today.getFullYear();
    if (day < 10) {
      day = '0' + day;
    }
    if (currentMonthValue < 10) {
      currentMonthValue = '0' + currentMonthValue;
    }

    let currentDay = currentMonthValue + '/' + day + '/' + yyyy;

    this.state = {
      data: [],
      jdeData: [],
      companies: [{ value: 'ALL', label: 'ALL' }],
      divisions: [{ value: 'ALL', label: 'ALL', company: '' }],

      months: [],
      years: [],
      selectedCompany: {},
      netIncomeTotal: '',
      selectedCompanyId: null,

      isLoaderShow: 1, //1 for showing
      todayDate: currentDay,
      showAwardInput: false,

      pendingAwardFormErrors: {
        pendingAwardTitle: ''
      },

      pendingAwardsInputRow: {
        company: '',
        monthEnd: '',
        division: '',
        pendingAwardTitle: '',
        pendingAwardExpDate: '',
        pendingAwardRevenueValue: '',
        pendingAwardCTOValue: '',
        pendingAwardFYEPrevRev: '',
        pendingAwardFYEPrevCTO: '',
        pendingAwardCurBacklogRev: '',
        pendingAwardCurBacklogCTO: '',
        pendingAwardFutureBacklogRev: '',
        pendingAwardFutureBacklogCTO: '',
        updateDate: '',
        updateUser: '',
        maintenanceFlag: false,
        priorYearFlag: false,
      },
      pendingAwardsList: [],
      pendingAwardAction: -1, //-1 post action, other put action

      newChangeCompanyDdl: 0,
      tCompany: { value: 'ALL', label: 'ALL' },
      tDivision: { value: 'ALL', label: 'ALL' },
      tYear: this.props.selectedYear,
      tMonth: this.props.selectedMonth,
      isDDLChanged: false,
      pdfDownloadStatus: 'default',
      allDivisions: [],
      isMaintenanceClicked:false
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    //
    //test
    if (nextProps !== prevState) {
      //nextProps.netIncomeTotal !== prevState.netIncomeTotal
      return {
        data: nextProps.data,
        jdeData: nextProps.jdeData,
        pendingAwardsList: nextProps.pendingAwardsData,
        allDivisions: nextProps.allDivisions,
        companies: nextProps.companies
      };
    } else return null;
  }
  componentDidMount() {
    const { token } = this.props;
    const { companies } = this.props;
    const { setNetIncome } = this;

    this.setState({
      companies: this.props.companies,
    });
    this.initilizeComponent().then(response => {
      // if(this.props.location.state &&  this.props.location.state.pcompany && this.props.location.state.pcompany!== ""){
      if (this.props.match.params) {

        // const { pcompany } = this.props.location.state;
        const { pcompany } = this.props.match.params;


        if (pcompany !== '0000') {
          let filterCompany = this.state.companies.filter(val => val.value === pcompany)[0];
          this.setState({ tCompany: filterCompany })
          this.onGoButtonClicked();
          this.pdivisions(pcompany);
        }
        else {
          this.onGoButtonClicked();
        }
      }

      setInterval(this.setDownloadbutton, 160000);
    });

  }



  pdivisions = (pcompany) => {
    let filter = this.props.allDivisions.filter(val => val.company === pcompany);
    filter.splice(0, 0, { value: 'ALL', label: 'ALL', company: pcompany });
    this.setState({ divisions: filter });
  }


  initilizeComponent = async () => {
    this.setState({
      isLoaderShow: 1,
    });

    let years = [];
    let currentYear = new Date().getFullYear();

    years.push({ value: currentYear, label: currentYear });
    currentYear--;
    years.push({ value: currentYear, label: currentYear });
    currentYear--;
    years.push({ value: currentYear, label: currentYear });
    currentYear--;
    years.push({ value: currentYear, label: currentYear });
    currentYear--;
    years.push({ value: currentYear, label: currentYear });
    currentYear--;

    this.setState({
      years: years,
    });

    let today = new Date();
    let monthValue = today.getMonth(); //current month
    if (monthValue < 10) {
      monthValue = '0' + monthValue;
    }

    //await this.props.gfrActions.selectedYearChanged(years[0]);

    const monthList = getMonths;
    if (monthList) {
      this.setState({
        months: this.enableDisableMonth(monthList),
      });
    }
    //await this.props.gfrActions.selectedMonthChanged(monthList[monthValue - 1]);
    if (this.props.companies.length <= 1) {//check if company is already loaded
      await this.props.gfrActions.fetchCompanyDataRequest(this.props.token);
    }
    await this.props.gfrActions.fetchAllDivisionDataRequest(this.props.token);
    //await this.changeCompany(this.props.companies[0]);
    this.setState({ isLoaderShow: 0 });
    this.setState({ tYear: years[0], tMonth: monthList[monthValue - 1] })
  };
  reloadGridData = async () => {
    let currentYear = new Date().getFullYear();
    let nextYear = this.state.tYear.value + 1;
    await Promise.all([
      this.props.gfrActions.fetchForecastPlrDataRequest(
        this.props.token,
        this.props.selectedCompany.value,
        this.props.selectedDivision.value,
        this.props.monthEnd,
      ),
      this.props.gfrActions.fetchContractStatusDataRequest(
        this.props.token,
        this.props.selectedCompany.value,
        this.props.selectedDivision.value,
        this.props.monthEnd,
        false
      ),
      this.props.gfrActions.fetchContractStatusDataRequest(
        this.props.token,
        this.props.selectedCompany.value,
        this.props.selectedDivision.value,
        this.props.monthEnd,
        true
      ),
      this.props.gfrActions.fetchGFRDataRequest(
        this.props.token,
        getCompanyCode(this.props.selectedCompany.value),
        this.props.selectedDivision.value,
        this.props.monthEnd,
      ),
      this.props.paActions.fetchPADataRequest(
        this.props.token,
        this.props.selectedCompany.value,
        this.props.selectedDivision.value,
        this.props.monthEnd,
      ),
      this.props.fpActions.fetchFPDataRequest(
        this.props.token,
        this.props.selectedCompany.value,
        this.props.selectedDivision.value,
        this.props.selectedYear.value,
        this.props.monthEnd
      ),

    ]);
  };


  changeCompany = async selectedCompany => {

    this.setState({ tCompany: selectedCompany, pdfDownloadStatus: 'default' })
    if (selectedCompany.value === 'ALL') {
      //await this.props.gfrActions.receiveDivisionData([{ value: 'ALL', label: 'ALL' },]);
      var data = [{ value: 'ALL', label: 'ALL', company: '' }];
      //when company is "ALL" then we return the allDivisions 0 index that is {value:'ALL',label:'ALL'}
      this.setState({ divisions: data });

      //await this.props.gfrActions.selectedDivisionChanged( this.props.divisions[0], );
    } else {
      let filter = this.props.allDivisions.filter(val => val.company === selectedCompany.value);
      filter.splice(0, 0, { value: 'ALL', label: 'ALL', company: selectedCompany.value });
      this.setState({ divisions: filter });
      this.setState({ isDDLChanged: true });
    }
    this.setState({ isLoaderShow: 0 });
  };

  updatePendingAwardValue = (
    headerDescription,
    rowDescription,
    newValue,
    getSpecificObjId,
    columnName,
  ) => {
    //headerDesc.rev, rowDesc.pendingAwards
    this.setData(
      headerDescription,
      rowDescription,
      newValue,
      getSpecificObjId,
      columnName,
    );
  };
  enableDisableMonth = monthList => {
    let currentYear = new Date().getFullYear();
    let selectedYear = this.props.selectedYear.value;
    let currentMonth = new Date().getMonth() + 1;


    monthList.forEach(month => {
      if (
        selectedYear === currentYear &&
        month.value > currentMonth
      ) {
        month.disabled = true;
      } else {
        month.disabled = false;
      }
    });

    return monthList;
  };
  changeMonth = async selectedMonth => {
    this.setState({ tMonth: selectedMonth, pdfDownloadStatus: 'default' });
    this.setState({ isDDLChanged: true })
  };

  changeyear = async selectedYear => {
    this.setState({ tYear: selectedYear, pdfDownloadStatus: 'default' });
    let newMonths = this.enableDisableMonth(this.state.months);
    this.setState({
      months: [...newMonths],
    });
    this.setState({ isDDLChanged: true });
    //await this.reloadGridData();
    //this.setState({ isLoaderShow: 0 });
  };

  onDownloadPdfClicked = async () => {


    // token: gfrReducer.token,
    // data: gfrReducer.data,
    // jdeData: gfrReducer.jdeData,
    // companies: gfrReducer.companies,
    // selectedCompany: gfrReducer.selectedCompany,
    // selectedDivision: gfrReducer.selectedDivision,
    // selectedMonth: gfrReducer.selectedMonth,
    // selectedYear: gfrReducer.selectedYear,
    // monthEnd: gfrSelector.getMonthEnd(gfrReducer),
    // userPrincipalName: gfrReducer.userPrincipalName,
    // pendingAwardsData: paReducer.pendingAwardsData,
    // selectedYear: gfrReducer.selectedYear,
    // reportMode: gfrSelector.getReportMode(gfrReducer),
    // plrData: gfrReducer.plrData,
    // companyCode: gfrSelector.getCompanyCode(gfrReducer),
    // contractData: gfrReducer.contractData,
    // cyContractData: gfrReducer.cyContractData,
    // allDivisions: gfrReducer.allDivisions

    //pdfDownloadStatus:"defaultstate"

    var _context = this;
    _context.setState({ isLoaderShow: 1, pdfDownloadStatus: "downloading" });

    var data = pdf(<GfrPdfDocument
      selectedCompany={this.props.selectedCompany}
      selectedDivision={this.props.selectedDivision}
      selectedMonth={this.props.selectedMonth}
      selectedYear={this.props.selectedYear}
      todayDate={this.state.todayDate}
      data={this.props.data}
      contractData={this.props.contractData}
      companyCode={this.props.companyCode}
      cyContractData={this.props.cyContractData}
      jdeData={this.props.jdeData}
      pendingAwardsData={this.props.pendingAwardsData}
      plrData={this.props.plrData}




    />).toBlob();
    data.then(function (value) {
      FileSaver.saveAs(value, 'gfrReport.pdf');

      _context.setState({ isLoaderShow: 0, pdfDownloadStatus: "downloaded" });
      //setInterval(setDownloadbutton,2000);
    });


  }

  setDownloadbutton = () => {
    this.setState({ pdfDownloadStatus: "default" })
    console.log('downloadStatus in setdownloadbutton', this.state.pdfDownloadStatus);
  };

  onGoButtonClicked = async () => {
    this.setState({ isLoaderShow: 1 });
    await this.props.gfrActions.selectedCompanyChanged(this.state.tCompany);
    await this.props.gfrActions.selectedDivisionChanged(this.state.tDivision);
    await this.props.gfrActions.selectedYearChanged(this.state.tYear);
    await this.props.gfrActions.selectedMonthChanged(this.state.tMonth);
    await this.reloadGridData();
    this.setState({ isLoaderShow: 0 });
    this.setState({ isDDLChanged: false });


  }
  onChangeHandler = e => {
    let pendingAwardFormErrors = this.state.pendingAwardFormErrors;

    switch (e.target.name) {
      case "pendingAwardTitle":
        pendingAwardFormErrors.pendingAwardTitle = e.target.value.length > 0 ? " " : "Project Name Required";
        break;
      case "maintenanceFlag":
        this.setState({isMaintenanceClicked:!this.state.isMaintenanceClicked})
        this.setState({
          pendingAwardsInputRow: {
            ...this.state.pendingAwardsInputRow,
            maintenanceFlag:true
          },
        });
        break;
      case "priorYearFlag":
        this.setState({
          pendingAwardsInputRow: {
            ...this.state.pendingAwardsInputRow,
            priorYearFlag:true
          },
        });
      default:
        break;
    }
    this.setState({ pendingAwardFormErrors });

    this.setState({
      pendingAwardsInputRow: {
        ...this.state.pendingAwardsInputRow,
        [e.target.name]: e.target.value,
      },
    });
  };

  onChangeHandlerDate = date => {
    this.setState({
      pendingAwardsInputRow: {
        ...this.state.pendingAwardsInputRow,
        pendingAwardExpDate: moment(date.toDate()).format('YYYY-MM-DD hh:mm:ss.SSS'),
      },
    });
  };

  onChangeHandlerObject = (event, id, columnName) => {
    let list = this.state.pendingAwardsList;
    if (columnName === 'pendingAwardTitle') {
      list[id].pendingAwardTitle = event.target.value;
    } else if (columnName === 'pendingAwardExpDate') {
      list[id].pendingAwardExpDate = event.toDate();
    } else if (columnName === 'pendingAwardRevenueValue') {
      list[id].pendingAwardRevenueValue = event.target.value;
    } else if (columnName === 'pendingAwardCTOValue') {
      list[id].pendingAwardCTOValue = event.target.value;
    } else if (columnName === 'pendingAwardFYEPrevRev') {
      list[id].pendingAwardFYEPrevRev = event.target.value;
    } else if (columnName === 'pendingAwardFYEPrevCTO') {
      list[id].pendingAwardFYEPrevCTO = event.target.value;
    } else if (columnName === 'pendingAwardCurBacklogRev') {
      list[id].pendingAwardCurBacklogRev = event.target.value;
    } else if (columnName === 'pendingAwardCurBacklogCTO') {
      list[id].pendingAwardCurBacklogCTO = event.target.value;
    } else if (columnName === 'pendingAwardFutureBacklogRev') {
      list[id].pendingAwardFutureBacklogRev = event.target.value;
    } else if (columnName === 'pendingAwardFutureBacklogCTO') {
      list[id].pendingAwardFutureBacklogCTO = event.target.value;
    }

    this.setState({
      pendingAwardsList: list,
    });
  };

  onPendingAwardUpdate = async (columnName, id, value) => {
    let list = this.state.pendingAwardsList;
    let paRow = list[id];
    paRow[columnName] = value;

    paRow.division = this.props.selectedDivision.value;
    paRow.company = this.props.selectedCompany.value;

    paRow.updateUser = this.props.userPrincipalName;
    paRow.updateDate = moment(new Date()).format('YYYY-MM-DD hh:mm:ss.SSS');
    paRow.monthEnd = this.props.monthEnd;

    await this.props.paActions.updatePADataRequest(this.props.token, paRow);
  };

  validatePendingAwardData = () => {
    let pendingAwardFormErrors = this.state.pendingAwardFormErrors;

    let isValid = false;
    let paData = this.state.pendingAwardsInputRow;
    if (paData.pendingAwardTitle && paData.pendingAwardTitle.length > 0) {
      isValid = true;
      pendingAwardFormErrors.pendingAwardTitle = '';
    }
    else {
      pendingAwardFormErrors.pendingAwardTitle = "Project Name Required";
    }
    this.setState({ pendingAwardFormErrors });

    return isValid;
  }


  onPendingAwardSubmit = async e => {
    this.setState({ isLoaderShow: 1 })
    e.preventDefault();

    if (this.validatePendingAwardData()) {
      let paRow = this.state.pendingAwardsInputRow;
      if (paRow.pendingAwardRevenueValue == "") {
        paRow.pendingAwardRevenueValue = 0;
      }
      if (paRow.pendingAwardCTOValue == "") {
        paRow.pendingAwardCTOValue = 0;
      }
      if (paRow.pendingAwardFYEPrevRev == "") {
        paRow.pendingAwardFYEPrevRev = 0;
      }
      if (paRow.pendingAwardCurBacklogCTO == "") {
        paRow.pendingAwardCurBacklogCTO = 0;
      }
      if (paRow.pendingAwardCurBacklogRev == "") {
        paRow.pendingAwardCurBacklogRev = 0;
      }
      if (paRow.pendingAwardFutureBacklogRev == "") {
        paRow.pendingAwardFutureBacklogRev = 0;
      }
      if (paRow.pendingAwardFutureBacklogCTO == "") {
        paRow.pendingAwardFutureBacklogCTO = 0;
      }
      if (paRow.pendingAwardExpDate == "") {
        paRow.pendingAwardExpDate = moment(new Date()).format('YYYY-MM-DD hh:mm:ss.SSS');
      }
      if (paRow.pendingAwardFYEPrevCTO == "") {
        paRow.pendingAwardFYEPrevCTO = 0;
      }

      paRow.company = this.props.selectedCompany.value;
      paRow.monthEnd = this.props.monthEnd;
      paRow.division = this.props.selectedDivision.value;
      paRow.updateUser = this.props.userPrincipalName;
      paRow.updateDate = moment(new Date()).format('YYYY-MM-DD hh:mm:ss.SSS');

      await this.props.paActions.createPADataRequest(this.props.token, paRow);
      console.log("Pending award inputrow", this.state.pendingAwardsInputRow)

      this.setState({
        pendingAwardsInputRow: {
          company: '',
          monthEnd: '',
          division: '',
          pendingAwardTitle: '',
          pendingAwardExpDate: '',
          pendingAwardRevenueValue: '',
          pendingAwardCTOValue: '',
          pendingAwardFYEPrevRev: '',
          pendingAwardFYEPrevCTO: '',
          pendingAwardCurBacklogRev: '',
          pendingAwardCurBacklogCTO: '',
          pendingAwardFutureBacklogRev: '',
          pendingAwardFutureBacklogCTO: '',
          updateDate: '',
          updateUser: '',
        },
        pendingAwardAction: -1,
        showAwardInput: false,
      });
    }
    else {
      console.log('invalid pa form');
    }
    this.setState({ isLoaderShow: 0 })
  };

  colseAddPendingAward = () => {
    this.setState({
      showAwardInput: false,
      isMaintenanceClicked:false
    });
  };

  showDivisionDDL(selectedCompany) {
    const { token } = this.props;
    const { value } = selectedCompany;

    this.setState({ isLoaderShow: 1, selectedCompany, divisions: [] }, () => {
      if (token && value) {
        getDivByCompany(token, value).then(divs => {
          if (divs) {
            const divisions = [];
            for (let i = 0; i < divs.length; i += 1) {
              const { DIVISION, DIVISION_DESC } = divs[i];

              divisions.push({
                value: DIVISION,
                label: DIVISION_DESC,
              });
            }
            this.setState({
              divisions,
              selectedCompanyId: value,
              //isLoaderShow:0
            });

            this.getReportByCompanyDivition(null, value, null);
          }
        });
      }
    });
  }

  changeDivision = async selectedDivision => {
    //this.setState({ isLoaderShow: 1 });
    this.setState({ tDivision: selectedDivision, pdfDownloadStatus: 'default' })
    this.setState({ isDDLChanged: true });
    //await this.props.gfrActions.selectedDivisionChanged(selectedDivision);
    //await this.reloadGridData();
    //this.setState({ isLoaderShow: 0 });
  };

  addPendingAwards = () => {

    this.setState({
      pendingAwardsInputRow: {
        pendingAwardTitle: '',
        pendingAwardExpDate: moment(new Date()).format('YYYY-MM-DD hh:mm:ss.SSS'),
        pendingAwardRevenueValue: '',
        pendingAwardCTOValue: '',
        pendingAwardFYEPrevRev: '',
        pendingAwardFYEPrevCTO: '',
        pendingAwardCurBacklogRev: '',
        pendingAwardCurBacklogCTO: '',
        pendingAwardFutureBacklogRev: '',
        pendingAwardFutureBacklogCTO: '',

      }
    });
    this.setState({
      showAwardInput: this.state.showAwardInput ? false : true,
    });
  };

  pendingAwardEditbtn = id => {
    let getAllPendingAwardsList = this.state.pendingAwardsList;

    let getSinglePendingAward = getAllPendingAwardsList[id];

    this.setState({
      pendingAwardAction: id,
      showAwardInput: true,
      pendingAwardsInputRow: {
        pendingAwardTitle: getSinglePendingAward.pendingAwardTitle,
        pendingAwardExpDate: getSinglePendingAward.pendingAwardExpDate,
        pendingAwardRevenueValue:
          getSinglePendingAward.pendingAwardRevenueValue,
        pendingAwardCTOValue: getSinglePendingAward.pendingAwardCTOValue,
        pendingAwardFYE18Revenue:
          getSinglePendingAward.pendingAwardFYE18Revenue,
        pendingAwardFYE18CTO: getSinglePendingAward.pendingAwardFYE18CTO,
        pendingAwardBacklogRev2019:
          getSinglePendingAward.pendingAwardBacklogRev2019,
        pendingAwardBacklogCTO2019:
          getSinglePendingAward.pendingAwardBacklogCTO2019,
        pendingAwardBacklogRev2020:
          getSinglePendingAward.pendingAwardBacklogRev2020,
        pendingAwardBacklogCTO2020:
          getSinglePendingAward.pendingAwardBacklogCTO2020,
      },
    });
  };

  pendingAwardDeletebtn = async id => {
    if (confirm('Are you sure you want to delete this row?')) {
      await this.props.paActions.deletePADataRequest(this.props.token, id);
    } else {
    }
  };

  updateGfrCellData = async cellObj => {
    //  if(!cellObj.company) cellObj.company=getCompanyCode(this.props.selectedCompany.value);
    //  if(!cellObj.division) cellObj.division=this.props.selectedDivision.value;

    //  if(!cellObj.updateUser) cellObj.updateUser='test';
    //  if(!cellObj.updateDate) cellObj.updateDate=moment(new Date()).format('YYYY-MM-DD');
    //  if(!cellObj.monthEnd) cellObj.monthEnd=this.props.monthEnd;

    if (this.props.reportMode == reportModeLists.bothSelected) {
      cellObj.company = getCompanyCode(this.props.selectedCompany.value);
      cellObj.division = this.props.selectedDivision.value;
    } else if (this.props.reportMode == reportModeLists.onlyCompanySelected) {
      cellObj.company = getCompanyCode(this.props.selectedCompany.value);
      delete cellObj.division;

    } else if (
      this.props.reportMode == reportModeLists.companyDivisionBothAll
    ) {
      delete cellObj.company;
      delete cellObj.division;
    }

    cellObj.updateUser = this.props.userPrincipalName;
    cellObj.updateDate = moment(new Date()).format('YYYY-MM-DD hh:mm:ss.SSS');
    cellObj.monthEnd = this.props.monthEnd;

    const data = {
      createForecastReportItem: {
        ...cellObj,
      },
    };
    //this.props.gfrActions.insertGfrCellDataComplete(data)

    if (cellObj.id) {
      // cellObj.id="18";
      this.props.gfrActions.updateGfrCellDataRequest(this.props.token, cellObj);
    } else {
      delete cellObj.id;
      this.props.gfrActions.insertGfrCellDataRequest(this.props.token, cellObj);
    }
  };

  updateforecastPlanCellData = async cellObj => {


    if (this.props.reportMode == reportModeLists.bothSelected) {
      cellObj.company = this.props.selectedCompany.value;
      cellObj.division = this.props.selectedDivision.value;
      cellObj.monthEnd = this.props.monthEnd;
    } else if (this.props.reportMode == reportModeLists.onlyCompanySelected) {
      cellObj.company = this.props.selectedCompany.value;
      delete cellObj.division;

    } else if (
      this.props.reportMode == reportModeLists.companyDivisionBothAll
    ) {
      delete cellObj.company;
      delete cellObj.division;
    }

    cellObj.updateUser = this.props.userPrincipalName;
    cellObj.CreateDate = moment(new Date()).format('YYYY-MM-DD hh:mm:ss.SSS');
    cellObj.year = this.props.selectedYear.value;

    if (cellObj.id) {

      this.props.fpActions.updateFPDataRequest(this.props.token, cellObj).then(res =>

        this.props.fpActions.fetchFPDataRequest(
          this.props.token,
          this.props.selectedCompany.value,
          this.props.selectedDivision.value,
          this.props.selectedYear.value,
          this.props.monthEnd
        ),
      )
    } else {
      delete cellObj.id;
      this.props.fpActions.createFPDataRequest(this.props.token, cellObj).then(res =>
        this.props.fpActions.fetchFPDataRequest(
          this.props.token,
          this.props.selectedCompany.value,
          this.props.selectedDivision.value,
          this.props.selectedYear.value,
          this.props.monthEnd
        ),
      )
    }
  };

  render() {
    // console.log("Forecast Plan Data",this.props.forecastPlanData);

    const { jdeData } = this.state;
    const { setNetIncome } = this;
    const {
      data,
      contractData,
      token,
      selectedCompany,
      divisions,
      //allDivisions,
      selectedDivision,
      pendingAwardsData,
      selectedMonth,
      selectedYear,
      cyContractData
    } = this.props;

    const { netIncomeTotal, tCompany } = this.state;
    const { companies, plrData, companyCode, forecastPlanData } = this.props;
    var config = require('Config');
    let companyImageConfig =
      config['companies'][this.props.selectedCompany.value]['HEADER_IMAGE'];
    let companyColor =
      config['companies'][this.props.selectedCompany.value]['COLOR'];

    var imageFolder = require.context('../../components/Images', true);
    let companyImage = require('../../components/Images/' + companyImageConfig); //imageFolder(''+companyImageConfig);

    let companyIdNumber = this.props.selectedCompany.value;

    const companyNumber = companyIdNumber;

    const {
      months,
      years,
      isLoaderShow,
      todayDate,
      showAwardInput,
      addPendingAwards,
      pendingAwardsInputRow,
    } = this.state;

    var paTotals = {
      pendingAwardRevenueValue: 0,
      pendingAwardCTOValue: 0,
      pendingAwardFYEPrevRev: 0,
      pendingAwardFYEPrevCTO: 0,
      pendingAwardCurBacklogRev: 0,
      pendingAwardCurBacklogCTO: 0,
      pendingAwardFutureBacklogRev: 0,
      pendingAwardFutureBacklogCTO: 0,
    };

    if (pendingAwardsData) {
      pendingAwardsData.forEach(function (data, index) {
        paTotals.pendingAwardRevenueValue +=
          +data.pendingAwardRevenueValue || 0;
        paTotals.pendingAwardCTOValue += +data.pendingAwardCTOValue;
        paTotals.pendingAwardFYEPrevRev += +data.pendingAwardFYEPrevRev || 0;
        paTotals.pendingAwardFYEPrevCTO += +data.pendingAwardFYEPrevCTO || 0;
        paTotals.pendingAwardCurBacklogRev +=
          +data.pendingAwardCurBacklogRev || 0;
        paTotals.pendingAwardCurBacklogCTO +=
          +data.pendingAwardCurBacklogCTO || 0;
        paTotals.pendingAwardFutureBacklogRev +=
          +data.pendingAwardFutureBacklogRev || 0;
        paTotals.pendingAwardFutureBacklogCTO +=
          +data.pendingAwardFutureBacklogCTO || 0;
      });
    }

    /************************CTO calc start */
    let cto_row1Data = getData(jdeData, headerDesc.cto, rowDesc.contrAwrdInPy);
    let cto_row2Data = getData(data, headerDesc.cto, rowDesc.contrAwrdYTD);
    let cto_row3Data = getData(jdeData, headerDesc.cto, rowDesc.pendingAwards);
    let cto_row4Data = getData(jdeData, headerDesc.cto, rowDesc.otherWork);

    let cto_row1Col2Data = getData(data, headerDesc.cto, rowDesc.contrAwrdInPy);
    let cto_row2Col2Data = getData(data, headerDesc.cto, rowDesc.contrAwrdYTD);
    let cto_row3Col2Data = getData(data, headerDesc.cto, rowDesc.pendingAwards);
    let cto_row4Col2Data = getData(data, headerDesc.cto, rowDesc.otherWork);
    if (companyCode === 'GCC') {
      if (+(cto_row2Data.contractYTD || 0) === 0) {
        cto_row2Data.contractYTD = (+cyContractData.CURR_YEAR_CTO || 0);
      }
    }
    else if (companyCode === 'GIC' || companyCode === 'GSI') {
      cto_row2Data.contractYTD = (+cyContractData.CURR_YEAR_CTO || 0);
    }

    cto_row3Col2Data.contractBOY = +paTotals.pendingAwardFYEPrevCTO || 0;
    // else data is already collected from the gfr data.

    var cto_row5Data = getTotal(
      (+contractData.CURR_YEAR_CTO),
      cto_row2Data.contractYTD,
      cto_row3Data.contractYTD,
      cto_row4Data.contractYTD,
    );
    var cto_row5Col2Data = getTotal(
      cto_row1Col2Data.contractBOY,
      cto_row2Col2Data.contractBOY,
      cto_row3Col2Data.contractBOY,
      cto_row4Col2Data.contractBOY,
    );

    var cto_row5Col3Data = getTotal(cto_row5Data, cto_row5Col2Data);
    /************************CTO calc start end */

    /************************AdjustmentToGrossProfit calc start */
    // let atgp_row1Data = {contractYTD:(+plrData.C_RETRO_AMNT||0)};
    let atgp_row1Data = getData(
      data,
      headerDesc.adjToGrossProf,
      rowDesc.retro,
    );
    atgp_row1Data.contractYTD = (+plrData.C_RETRO_AMNT || 0);

    let atgp_row1Col2Data = getData(
      data,
      headerDesc.adjToGrossProf,
      rowDesc.retro,
    );



    let atgp_row2Data = getData(
      jdeData,
      headerDesc.adjToGrossProf,
      rowDesc.yardShop,
    );
    let atgp_row2Col2Data = getData(
      data,
      headerDesc.adjToGrossProf,
      rowDesc.yardShop,
    );

    let atgp_row3Data = getTotal(
      atgp_row1Data.contractYTD,
      atgp_row2Data.contractYTD,
    );
    let atgp_row3Col2Data = getTotal(
      atgp_row1Col2Data.contractBOY,
      atgp_row2Col2Data.contractBOY,
    );
    let atgp_row1Col3Data = getTotal(atgp_row1Data.contractYTD, atgp_row1Col2Data.contractBOY);

    let atgp_row2Col3Data = getTotal(
      atgp_row2Data.contractYTD,
      atgp_row2Col2Data.contractBOY,
    );

    let atgp_row3Col3Data = getTotal(atgp_row3Data, atgp_row3Col2Data);


    /************************AdjustmentToGrossProfit calc start end */

    /************************Adjusted Gross profit  calc start */
    let agp_row1Data = getData(
      data,
      headerDesc.adjGrossProf,
      rowDesc.total,
    );

    agp_row1Data.contractYTD = ((+cto_row5Data || 0)) - (+atgp_row3Data || 0);
    //agp_row1Data.contractBOY = cto_row5Col2Data - atgp_row3Col2Data;
    // if (companyCode === "GCC") {
    //   agp_row1Data.contractYTD = (+plrData.D_ADJ_GROSS_PROF || 0);
    // }
    // else if (companyCode === "GIC" || companyCode === "GSI") {
    //   // cto_row5Data = (+cto_row1Data.contractYTD||0)+(+cto_row2Data.contractYTD||0);
    //   agp_row1Data.contractYTD = ((+cto_row5Data || 0)) - (+atgp_row3Data || 0);
    // }
    let agp_row1Col3Data = getTotal(agp_row1Data.contractYTD, agp_row1Data.contractBOY);




    /************************Adjusted Gross profit  calc start end */

    /************************GAndA  calc start */

    // let ga_row1Data = (+plrData.A_GA_EXPENSES||0);
    // let ga_row1Col2Data = 0; //TODO- not sureabout
    // let ga_row1Col3Data = getTotal(ga_row1Data, ga_row1Col2Data);

    let ga_row1Data = getData(data,
      headerDesc.gAndA,
      rowDesc.total);

    ga_row1Data.contractYTD = (+plrData.A_GA_EXPENSES || 0);
    let ga_row1Col3Data = getTotal(ga_row1Data, ga_row1Data.contractBOY);




    // let ga_row1Col3Data = (+plrData.A_GA_EXPENSES||0);
    /************************GAndA  calc start end */

    /************************OtherExpenses  calc start */

    // let oe_row1Data = (+plrData.B_OTHER_INCOME_EXP||0);
    let oe_row1Data = getData(data,
      headerDesc.otherExpense,
      rowDesc.total);

    oe_row1Data.contractYTD = (+plrData.B_OTHER_INCOME_EXP || 0);

    let oe_row1Col2Data = 0; //TODO- not sureabout
    let oe_row1Col3Data = getTotal(oe_row1Data, oe_row1Col2Data);


    /************************OtherExpenses  calc start end */

    /************************NetIncome  calc start */

    //Adjusted Gross Profit Total – G&A Total – Other Expense
    let ni_row1Data =
      agp_row1Data.contractYTD - ga_row1Data.contractYTD - oe_row1Data.contractYTD;

    let ni_row1Col2Data = (+agp_row1Data.contractBOY || 0) - (+ga_row1Data.contractBOY || 0) - (+oe_row1Data.contractBOY || 0);
    // let ni_row1Col2Data = agp_row1Col2Data - ga_row1Col2Data - oe_row1Col2Datav;

    let ni_row1Col3Data = ni_row1Data + ni_row1Col2Data;
    /************************NetIncome  calc end */

    let today = new Date();
    let monthValue = today.getMonth() + 1;
    let yearValue = today.getFullYear();
    // let fiscalYear = monthValue >= 10 ? yearValue + 1 : yearValue;

    //method for check wheather an object is empty or not.
    function isEmpty(obj) {
      for (var key in obj) {
        if (obj.hasOwnProperty(key))
          return false;
      }
      return true;
    }

    let currentDate = new Date();
    if (isEmpty(selectedYear)) {
      selectedYear.value = currentDate.getFullYear();
    }

    if (isEmpty(selectedMonth)) {
      selectedMonth.label = currentDate.getMonth();
    }
    let p = (this.state.pdfDownloadStatus === "default");

    console.log("Selected Company.....................",this.props.selectedCompany);

    return (
      <Layout>
        <div
          className={s.loaderContainer}
          style={
            isLoaderShow == 1
              ? { display: 'inline-block' }
              : { display: 'none' }
          }
        >
          <div className={s.loaderImageContainer}>
            <img src={loading} />
          </div>
        </div>

        <div className={s.root}>
          <ReportHeader
            title="GRAYCOR Companies"
            line1="Industrial Unit - Overall"
            line2={'Business Projections as Of ' + selectedMonth.label + " " + selectedYear.value}
            line3="All numbers in 000's"
            line4=""
            companyLogo={companyImage}
            token={token}
            date={todayDate}
            token={this.props.token}
            companies={companies}
            divisions={this.state.divisions}
            years={years}
            months={months}
            selectedCompany={this.state.tCompany}
            selectedDivision={this.state.tDivision}
            selectedMonth={this.state.tMonth}
            changeCompany={this.changeCompany}
            changeDivision={this.changeDivision}
            changeMonth={this.changeMonth}
            selectedYear={this.state.tYear} // eslint-disable-line
            changeYear={this.changeyear}
            onGoButtonClicked={this.onGoButtonClicked}
            isDDLChanged={this.state.isDDLChanged}
            onDownloadPdfClicked={this.onDownloadPdfClicked}
            pdfDownloadStatus={this.state.pdfDownloadStatus}
          />

          {data != 'No data is available.' ? (
            <div className={s.reportBodyContainer}>
              <GfrReportRevenueSection
                companyColor={companyColor}
                paTotals={paTotals}
                addPendingAwards={this.addPendingAwards}
                updateGfrCellData={this.updateGfrCellData}
                reportMode={this.props.reportMode}
                updateforecastPlanCellData={this.updateforecastPlanCellData}
              />
              <GfrReportCtoSection
                companyColor={companyColor}
                paTotals={paTotals}
                addPendingAwards={this.addPendingAwards}
                updateGfrCellData={this.updateGfrCellData}
                reportMode={this.props.reportMode}
                updateforecastPlanCellData={this.updateforecastPlanCellData}
              />
              <GfrReportAdjustmentToGrossProfitSection
                companyColor={companyColor}
                paTotals={paTotals}
                updateGfrCellData={this.updateGfrCellData}
                atgp_row3Col3Data={atgp_row3Col3Data}
                atgp_row1Col2Data={atgp_row1Col2Data}
                atgp_row1Col3Data={atgp_row1Col3Data}
                atgp_row1Data={atgp_row1Data}
                reportMode={this.props.reportMode}
                updateforecastPlanCellData={this.updateforecastPlanCellData}
              />
              <GfrReportAdjustedGrossProfitSection
                companyColor={companyColor}
                paTotals={paTotals}
                agp_row1Col3Data={agp_row1Col3Data}
                agp_row1Data={agp_row1Data}
                updateGfrCellData={this.updateGfrCellData}
                reportMode={this.props.reportMode}
                updateforecastPlanCellData={this.updateforecastPlanCellData}
              />
              <GfrReportGAndASection
                companyColor={companyColor}
                paTotals={paTotals}
                ga_row1Data={ga_row1Data}
                ga_row1Col3Data={ga_row1Col3Data}
                updateGfrCellData={this.updateGfrCellData}
                reportMode={this.props.reportMode}
                updateforecastPlanCellData={this.updateforecastPlanCellData}
              />
              <GfrReportOtherExpensesOrIncomeSection
                companyColor={companyColor}
                paTotals={paTotals}
                oe_row1Data={oe_row1Data}
                oe_row1Col3Data={oe_row1Col3Data}
                updateGfrCellData={this.updateGfrCellData}
                reportMode={this.props.reportMode}
                updateforecastPlanCellData={this.updateforecastPlanCellData}
              />
              <GfrReportNetIncomeSection
                netIncomeTotal={netIncomeTotal}
                companyColor={companyColor}
                paTotals={paTotals}
                ni_row1Data={ni_row1Data}
                ni_row1Col2Data={ni_row1Col2Data}
                ni_row1Col3Data={ni_row1Col3Data}
                reportMode={this.props.reportMode}
              />

              <PendingAwards
                title="Pending Awards"
                colorCode={companyColor}
                paTotals={paTotals}
                addPendingAwards={this.addPendingAwards}
                colseAddPendingAward={this.colseAddPendingAward}
                openAwardInputField={showAwardInput}
                onChangeHandler={this.onChangeHandler}
                onChangeHandlerDate={this.onChangeHandlerDate}
                onPendingAwardSubmit={this.onPendingAwardSubmit}
                pendingAwardsInputRow={pendingAwardsInputRow}
                selectedYear={selectedYear.value}
                pendingAwardFormErrors={this.state.pendingAwardFormErrors}
                reportMode={this.props.reportMode}
                companyCode={this.props.companyCode}
                selectedMonth={this.props.selectedMonth.value}
                isMaintenanceClicked={this.state.isMaintenanceClicked}
              >
                <DataRows
                  pendingAwardsList={pendingAwardsData}
                  pendingAwardEditbtn={this.pendingAwardEditbtn}
                  pendingAwardDeletebtn={this.pendingAwardDeletebtn}
                  onPendingAwardUpdate={this.onPendingAwardUpdate}
                  onChangeHandlerObject={this.onChangeHandlerObject}
                  pendingAwardsInputRow={pendingAwardsInputRow}
                  companyCode={this.props.companyCode}
                />
              </PendingAwards>
            </div>
          ) : (
              <h2 style={{ textAlign: 'center' }}>No data is available.</h2>
            )}
        </div>
      </Layout>
    );
  }
}

function mapStateToProps({ gfrReducer, paReducer, fpReducer }) {
  return {
    token: gfrReducer.token,
    data: gfrReducer.data,
    jdeData: gfrReducer.jdeData,
    companies: gfrReducer.companies,
    selectedCompany: gfrReducer.selectedCompany,
    selectedDivision: gfrReducer.selectedDivision,
    selectedMonth: gfrReducer.selectedMonth,
    selectedYear: gfrReducer.selectedYear,
    monthEnd: gfrSelector.getMonthEnd(gfrReducer),
    userPrincipalName: gfrReducer.userPrincipalName,
    pendingAwardsData: paReducer.pendingAwardsData,
    selectedYear: gfrReducer.selectedYear,
    reportMode: gfrSelector.getReportMode(gfrReducer),
    plrData: gfrReducer.plrData,
    companyCode: gfrSelector.getCompanyCode(gfrReducer),
    contractData: gfrReducer.contractData,
    cyContractData: gfrReducer.cyContractData,
    allDivisions: gfrReducer.allDivisions,
    forecastPlanData: fpReducer.forecastPlanData
  };
}
function mapDispatchToProps(dispatch) {
  return {
    gfrActions: bindActionCreators(gfrActions, dispatch),
    paActions: bindActionCreators(paActions, dispatch),
    fpActions: bindActionCreators(fpActions, dispatch),
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(GfrReport);
