/* eslint-disable  */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { filter } from 'lodash';
import * as gfrActions from '../../store/gfr/gfr.actions';

import GridRows from '../../components/GridRows/GridRows';
import GridRowsColumn3 from '../../components/GridRowsColumn3/GridRowsColumn3';
import GridRowsColumn5 from '../../components/GridRowsColumn5/GridRowsColumn5';
import { calcNetIncome } from '../../utils/mathAndCurrency';
import { headerDesc, rowDesc } from '../../constants/GfrReport.constant';

import GroupHeaderForThree from '../../components/GroupHeader/GroupHeaderForThree';
import { getData } from '../../utils/GfrReportHelper';

class GfrReportNetIncomeSection extends Component {
  static propTypes = {
    token: PropTypes.string.isRequired,
    data: PropTypes.instanceOf(Array).isRequired,
  };

  render() {
    const { setNetIncome } = this;

    const {
      data,
      token,
      netIncomeTotal,
      paTotals,
      jdeData,
      ni_row1Data,
      ni_row1Col2Data,
      ni_row1Col3Data,
      reportMode
    } = this.props;

    
    return (
      <GroupHeaderForThree
        is2ColHeader
        colorCode={this.props.companyColor}
        cssPosition="singleCenter "
        gridCategory="doubleGrid nextYearcss"
        title="Net Income"
        column1Top="Backlog"
        column1Bottom="Beginning"
        column2Top="Awards/Growth"
        column2Left="Year-to-Date"
        column2Center="Balance of Year"
        column2Right="Total"
        column3Top="Contract Activity"
        column3Left="Year-to-Date"
        column3Center="Balance of Year"
        column3Right="Total"
        column4Top="Backlog"
        column4Bottom="Ending"
        column5Top="Next Year"
        column5Left="Awards/Growth"
        column5Right="Next Year"
        column3Data={
          <GridRowsColumn3
            twoRowGroup
            fourRowGroup
            netIncome
            headerDescProp={headerDesc.netIncome}
            row1Data={ni_row1Data.toString()}
            row2Data="64646"
            row1Col2Data={ni_row1Col2Data.toString()}
            row2Col2Data="64646"
            row1Col3Data={ni_row1Col3Data}
            row2Col3Data="64646"
            token={token}
            setNetIncome={setNetIncome}
            netIncomeTotal={netIncomeTotal}
            updateGfrCellData={this.props.gfrActions.updateGfrCellDataRequest}
            isRow1Editable={false}
            isRow2Editable={false}
            reportMode={reportMode}
          />
        }
        column5Data={
          <GridRowsColumn5
            col1Hidden
            fourRowGroup
            twoRowGroup
            row1Col2Data={calcNetIncome(
              (
                getData(jdeData, headerDesc.cto, rowDesc.contrAwrdInPy)
                  .nextYearActivity +
                getData(jdeData, headerDesc.cto, rowDesc.contrAwrdYTD)
                  .nextYearActivity +
                getData(jdeData, headerDesc.cto, rowDesc.pendingAwards)
                  .nextYearActivity +
                getData(data, headerDesc.cto, rowDesc.otherWork)
                  .nextYearActivity -
                (getData(jdeData, headerDesc.adjToGrossProf, rowDesc.retro)
                  .nextYearActivity +
                  getData(jdeData, headerDesc.adjToGrossProf, rowDesc.yardShop)
                    .nextYearActivity)
              ).toString(),
              '5445',
              '5646',
            ).toString()}
            row2Col2Data="4646"
            updateGfrCellData={this.props.gfrActions.updateGfrCellDataRequest}
            reportMode={reportMode}
          />
        }
      >
        <GridRows row1Title="Total" row2Title="2019 Plan" row3Title="2018 Plan" />
      </GroupHeaderForThree>
    );
  }
}

function mapStateToProps({ gfrReducer }) {
  return {
    token: gfrReducer.token,
    data: gfrReducer.data,
    jdeData: gfrReducer.jdeData,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    gfrActions: bindActionCreators(gfrActions, dispatch),
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(GfrReportNetIncomeSection);
