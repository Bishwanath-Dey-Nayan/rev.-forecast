/* eslint-disable react/prefer-stateless-function */
/* eslint-disable react/jsx-curly-brace-presence */
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import s from './Dashboard.module.css';
import Layout from '../../components/Layout';
import gs from '../../components/Images/gs-dropdown.png';
import gsi from '../../components/Images/GSI Button.png'; // eslint-disable-line
import gcc from '../../components/Images/gcc-dropdown.png'; // eslint-disable-line
import gic from '../../components/Images/gic-dropdown.png'; // eslint-disable-line

class Dashboard extends Component {
  render() {


    return (
      <Layout>
        <div className={s.dashroot} style={{ marginLeft: '' }}>
          <div style={{ marginLeft: 'auto', marginRight: 'auto', width: '901px' }}>
            <div style={{ marginTop: '50px' }}>
              <p className={s.Title}>Welcome to the Launchpad</p>
              <p className={s.SubTitle}>Please hover over a company and select an option</p>
            </div>
          </div>

          <div style={{ marginLeft: 'auto', marginRight: 'auto', width: '2000px' }}>
            <div className={s.GraycorServices}>
              <li className="drop" style={{ position: 'relative',zIndex:'2' }}>


                {/* Hoverable dropdown for graycor constructor */}
                <div>
                  <img alt='' src={gcc} style={{
                    display: 'block', width: '100%', height: '200px', borderRadius: '35px 35px 0px 0px'
                  }} />

                  <ul style={{
                    display: 'block', backgroundColor: "#78A22F", padding: "20px", width: '300px', marginLeft: '40px',
                    borderRadius: '0px 0px 35px 35px'
                  }}>
                    <p>Reports</p>
                    <hr style={{ borderTop: '1px solid #0e0e0e', height: '2px', backgroundColor: 'black', marginTop: '-10px' }} />

                    {/* <li><Link to={{pathname:`/gfrReport`, state: {   pcompany: '00300'} }}>Forecast Report</Link></li> */}
                    <li><Link to={{pathname:`/gfrReport/00300` }} target="_blank">Forecast Report</Link></li>
                    <hr style={{ borderTop: '1px solid #0e0e0e', marginLeft: '25px', height: '2px', backgroundColor: 'black', marginTop: '-10px' }} />
                    <li><a href="/#/">Accounting Reports</a></li>
                    <hr style={{ borderTop: '1px solid #0e0e0e', marginLeft: '25px', height: '2px', backgroundColor: 'black', marginTop: '-10px' }} />
                    <p>Administrative Tasks</p>
                    <hr style={{ borderTop: '1px solid #0e0e0e', height: '2px', backgroundColor: 'black', marginTop: '-10px' }} />
                    <li><a href="/subapps/newvendor/index.html" target="_blank">Vendor List</a></li>
                    <hr style={{ borderTop: '1px solid #0e0e0e', marginLeft: '25px', height: '2px', backgroundColor: 'black', marginTop: '-10px' }} />
                    <li><a href="/subapps/newvendor/index.html" target="_blank">New Vendor Form</a></li>
                    <hr style={{ borderTop: '1px solid #0e0e0e', marginLeft: '25px', height: '2px', backgroundColor: 'black', marginTop: '-10px' }} />
                  </ul>
                </div>
              </li>
            </div>

            {/* Hoverable dropdown for Graycor industrial */}

            <div className={s.GraycorConstructor}>
              <li className="drop" style={{ position: 'relative',zIndex:'2' }}>

                <div>
                  <img alt='' src={gic} style={{
                    display: 'block', width: '100%', height: '200px', borderRadius: '35px 35px 0px 0px'
                  }} />

                  <ul style={{
                    display: 'block', backgroundColor: "#89A3D4", padding: "20px", width: '300px', marginLeft: '40px',
                    borderRadius: '0px 0px 35px 35px'
                  }}>
                    <p>Reports</p>
                    <hr style={{ borderTop: '1px solid #0e0e0e', height: '2px', backgroundColor: 'black', marginTop: '-10px' }} />
                    {/* <li><Link to={{pathname:`/gfrReport`, state: {   pcompany: '00200'} }} target="_blank">Forecast Report</Link></li> */}
                    <li><Link to={{pathname:`/gfrReport/00200` }} target="_blank">Forecast Report</Link></li>
                    <hr style={{ borderTop: '1px solid #0e0e0e', marginLeft: '25px', height: '2px', backgroundColor: 'black', marginTop: '-10px' }} />
                    <li><a href="/#/">Accounting Reports</a></li>
                    <hr style={{ borderTop: '1px solid #0e0e0e', marginLeft: '25px', height: '2px', backgroundColor: 'black', marginTop: '-10px' }} />
                    <p>Administrative Tasks</p>
                    <hr style={{ borderTop: '1px solid #0e0e0e', height: '2px', backgroundColor: 'black', marginTop: '-10px' }} />
                    <li><a href="/subapps/newvendor/index.html" target="_blank">Vendor List</a></li>
                    <hr style={{ borderTop: '1px solid #0e0e0e', marginLeft: '25px', height: '2px', backgroundColor: 'black', marginTop: '-10px' }} />
                    <li><a href="/subapps/newvendor/index.html" target="_blank">New Vendor Form</a></li>
                    <hr style={{ borderTop: '1px solid #0e0e0e', marginLeft: '25px', height: '2px', backgroundColor: 'black', marginTop: '-10px' }} />
                  </ul>
                </div>
              </li>
            </div>


            {/* Hoverable dropdown for Graycor southern */}
            <div style={{ marginTop: '' }}>
              <div className={s.GraycorIndustrial}>
                <li className="drop" style={{ position: 'relative'}}>

                  <div>
                    <img alt='' src={gsi} style={{
                      display: 'block', width: '100%', height: '200px', borderRadius: '35px 35px 0px 0px'
                    }} />

                    <ul style={{
                      display: 'block', backgroundColor: "#26418F", padding: "20px", width: '300px', marginLeft: '40px',
                      borderRadius: '0px 0px 35px 35px'
                    }}>
                      <p>Reports</p>
                      <hr style={{ borderTop: '1px solid #0e0e0e', height: '2px', backgroundColor: 'black', marginTop: '-10px' }} />
                      {/* <li><Link to={{pathname:`/gfrReport`, state: {   pcompany: '00260'} }} target="_blank">Forecast Report</Link></li> */}
                      <li><Link to={{pathname:`/gfrReport/00260` }} target="_blank">Forecast Report</Link></li>

                      <hr style={{ borderTop: '1px solid #0e0e0e', marginLeft: '25px', height: '2px', backgroundColor: 'black', marginTop: '-10px' }} />
                      <li><a href="/#/">Accounting Reports</a></li>
                      <hr style={{ borderTop: '1px solid #0e0e0e', marginLeft: '25px', height: '2px', backgroundColor: 'black', marginTop: '-10px' }} />
                      <p>Administrative Tasks</p>
                      <hr style={{ borderTop: '1px solid #0e0e0e', height: '2px', backgroundColor: 'black', marginTop: '-10px' }} />
                      <li><a href="/subapps/newvendor/index.html" target="_blank">Vendor List</a></li>
                      <hr style={{ borderTop: '1px solid #0e0e0e', marginLeft: '25px', height: '2px', backgroundColor: 'black', marginTop: '-10px' }} />
                      <li><a href="/subapps/newvendor/index.html" target="_blank">New Vendor Form</a></li>
                      <hr style={{ borderTop: '1px solid #0e0e0e', marginLeft: '25px', height: '2px', backgroundColor: 'black', marginTop: '-10px' }} />
                    </ul>
                  </div>
                </li>
              </div>


              {/* Hoverable dropdown for Graycor  */}
              <div className={s.GraycorSouthern}>
                <li className="drop" style={{ position: 'relative'}}>

                  <div>
                    <img alt='' src={gs} style={{
                      display: 'block', width: '100%', height: '200px', borderRadius: '35px 35px 0px 0px'
                    }} />

                    <ul style={{
                      display: 'block', backgroundColor: "#fdb924", padding: "20px", width: '300px', marginLeft: '40px',
                      borderRadius: '0px 0px 35px 35px'
                    }}>
                      <p>Reports</p>
                      <hr style={{ borderTop: '1px solid #0e0e0e', height: '2px', backgroundColor: 'black', marginTop: '-10px' }} />
                      {/* <li><Link to={{pathname:`/gfrReport`, state: {   pcompany: '0000'} }} target="_blank">Forecast Report</Link></li> */}
                      <li><Link to={{pathname:`/gfrReport/0000` }} target="_blank">Forecast Report</Link></li>
                      <hr style={{ borderTop: '1px solid #0e0e0e', marginLeft: '25px', height: '2px', backgroundColor: 'black', marginTop: '-10px' }} />
                      <li><a href="/#/">Accounting Reports</a></li>
                      <hr style={{ borderTop: '1px solid #0e0e0e', marginLeft: '25px', height: '2px', backgroundColor: 'black', marginTop: '-10px' }} />
                      <p>Administrative Tasks</p>
                      <hr style={{ borderTop: '1px solid #0e0e0e', height: '2px', backgroundColor: 'black', marginTop: '-10px' }} />
                      <li><a href="/subapps/newvendor/index.html" target="_blank">Vendor List</a></li>
                      <hr style={{ borderTop: '1px solid #0e0e0e', marginLeft: '25px', height: '2px', backgroundColor: 'black', marginTop: '-10px' }} />
                      <li><a href="/subapps/newvendor/index.html" target="_blank">New Vendor Form</a></li>
                      <hr style={{ borderTop: '1px solid #0e0e0e', marginLeft: '25px', height: '2px', backgroundColor: 'black', marginTop: '-10px' }} />
                    </ul>
                  </div>
                </li>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}

export default Dashboard;
