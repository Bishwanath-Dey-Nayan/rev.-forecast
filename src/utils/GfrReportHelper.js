/* eslint-disable  */
import { filter } from 'lodash';

export const getData = (data, header, row) => {
  if (data === undefined || data === null) {
    return {};
  }

  if (data.length === 0) {
    return {};
  } else {
    const filtered = filter(data, {
      headerDescription: header,
      rowDescription: row,
    });
    return filtered.length > 0 ? filtered[0] : {};
  }
};


