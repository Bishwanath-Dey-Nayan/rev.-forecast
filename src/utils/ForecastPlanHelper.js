/* eslint-disable  */
import { filter } from 'lodash';


export const getMonthValue=(data)=>
{
    let monthValue = 0; 
    if (data === undefined || data === null) {
        return {};
      }
    if(data === "October")
    {
      monthValue = 1;
    }
    if(data === "November")
    {
      monthValue = 2;
    }
    if(data === "December")
    {
      monthValue = 3;
    }
    if(data === "January")
    {
      monthValue = 4;
    }
    if(data === "February")
    {
      monthValue = 5;
    }
    if(data === "March")
    {
      monthValue = 6;
    }
    if(data === "April")
    {
      monthValue = 7;
    }
    if(data === "May")
    {
      monthValue = 8;
    }
    if(data === "June")
    {
      monthValue = 9;
    }
    if(data === "July")
    {
      monthValue = 10;
    }
    if(data == 'August')
    {
      monthValue = 11;
    }
    if(data === "September")
    {
      monthValue = 12;
    }
    return monthValue;
    
}

export const getDataByYear = (data, header, row, year) => {
  if (data === undefined || data === null) {
    return {};
  }

  if (data.length === 0) {
    return {};
  } else {
    const filtered = filter(data, {
      headerDescription: header,
      rowDescription: row,
      year:row,
    });
    return filtered.length > 0 ? filtered[0] : {};
  }
};