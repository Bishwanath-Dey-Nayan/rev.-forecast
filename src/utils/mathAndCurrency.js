/* eslint-disable */
export const currencyFormat = (input: string) => {
  if (isNaN(input) || input === '') input = '0.0';
  const formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 0,
  });

  return formatter.format(input);
};

export const getTotal = (
  input1: string,
  input2: string,
  input3: string,
  input4: string,
) => {
  /*
  const retVal1 = input1 === '' ? 0 : parseFloat(input1);
  const retVal2 = input2 === '' ? 0 : parseFloat(input2);
  const retVal3 = input3 === '' ? 0 : parseFloat(input3);
  const retVal4 = input4 === '' ? 0 : parseFloat(input4);
  */
  const retVal1 = +input1 || 0;
  const retVal2 = +input2 || 0;
  const retVal3 = +input3 || 0;
  const retVal4 = +input4 | 0;

  return retVal1 + retVal2 + retVal3 + retVal4;
};

export const calcAdjGrossProfit = (
  ctoTotal: string,
  adjToGrossTotal: string,
) => {
  const retVal1 = ctoTotal === '' ? 0 : parseFloat(ctoTotal);
  const retVal2 = adjToGrossTotal === '' ? 0 : parseFloat(adjToGrossTotal);

  return retVal1 - retVal2;
};

export const calcNetIncome = (
  adjGrossTotal: string,
  gaTotal: string,
  otherTotal: string,
  setTotal: func,
) => {
  const retVal1 =
    isNaN(adjGrossTotal) || adjGrossTotal === ''
      ? 0
      : parseFloat(adjGrossTotal);
  const retVal2 = isNaN(gaTotal) || gaTotal === '' ? 0 : parseFloat(gaTotal);
  const retVal3 =
    isNaN(otherTotal) || otherTotal === '' ? 0 : parseFloat(otherTotal);

  return retVal1 - retVal2 - retVal3;
};
