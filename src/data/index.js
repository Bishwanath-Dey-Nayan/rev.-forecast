/* eslint-disable prefer-template */
const endpoint =
  'https://ihrxm0ubw3.execute-api.us-east-1.amazonaws.com/latest/graphql';
// const endpoint = 'http://localhost:5000/graphql';

export const getJDEInfo = (token, company, bu, selectedDate) => {
  const query = `query ($input: ForecastRptGetJDEinfoInputType!){
    getForecastRptGetJDEinfoQuery(input: $input) {
      headerDescription,
      rowDescription,
      backlogBeginning,
      growthYTD,
      contractYTD,
    }
  }`;
  const variables = {
    input: {
      COMPANY: company,
      BU: bu,
      SELECTED_DATE: selectedDate,
    },
  };
  return fetch(endpoint, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Basic ${Buffer.from('user:' + token).toString('base64')}`,
    },
    body: JSON.stringify({
      query,
      variables,
    }),
  })
    .then(resp => resp.json())
    .then(results => results.data.getForecastRptGetJDEinfoQuery)
    .catch(err => console.error('Get JDE Info Error:', err));
};
export const getMonths = [
  {
    value: 1,
    label: 'January',
  },
  {
    value: 2,
    label: 'February',
  },
  {
    value: 3,
    label: 'March',
  },
  {
    value: 4,
    label: 'April',
  },
  {
    value: 5,
    label: 'May',
  },
  {
    value: 6,
    label: 'June',
  },
  {
    value: 7,
    label: 'July',
  },
  {
    value: 8,
    label: 'August',
  },
  {
    value: 9,
    label: 'September',
  },
  {
    value: 10,
    label: 'October',
  },
  {
    value: 11,
    label: 'November',
  },
  {
    value: 12,
    label: 'December',
  },
];
export const getCompanies = token => {
  const query = '{getTsGetCurrentCompaniesQuery{COMPANY, COMPANY_NAME}}';
  return fetch(endpoint, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Basic ${Buffer.from('user:' + token).toString('base64')}`,
    },
    body: JSON.stringify({
      query,
    }),
  })
    .then(resp => resp.json())
    .then(results => results.data.getTsGetCurrentCompaniesQuery)
    .catch(err => console.error('Get Companie Error:', err));
};
export const getDivByCompany = (token, company) => {
  const query = `query ($input: TsGetCurrentDivByCompanyInputType!){
    getTsGetCurrentDivByCompanyQuery(input: $input) {
      DIVISION,
      DIVISION_DESC,
    }
  }`;
  const variables = {
    input: {
      COMPANY: company,
    },
  };
  return fetch(endpoint, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Basic ${Buffer.from('user:' + token).toString('base64')}`,
    },
    body: JSON.stringify({
      query,
      variables,
    }),
  })
    .then(resp => resp.json())
    .then(results => results.data.getTsGetCurrentDivByCompanyQuery)
    .catch(err => console.error('Get Companie Error:', err));
};

export const getForecastReport = token => {
  const query = `query ($input: ForecastReportItemsDataTypeInputArgs!){
    forecastReportItemQuery(input: $input) {
      id,
      company,
      headerRow,
      headerDescription,
      row,
      rowDescription,
      backlogBeginning,
      growthYTD,
      growthBOY,
      growthTotal,
      contractYTD,
      contractBOY,
      contractTotal,
      backlogEnding,
      nextYearGrowth,
      nextYearActivity,
      monthEnd,
      division,
      updateDate,
      updateUser
    }
  }`;

  const variables = {
    input: {
      company: 'gic',
    },
  };

  return fetch(endpoint, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Basic ${Buffer.from('user:' + token).toString('base64')}`,
    },
    body: JSON.stringify({
      query,
      variables,
    }),
  });
};

export const insertUpdateForecastReport = (token, data) => {
  let query = '';
  if (data.id) {
    // Update
    query = `mutation updateForecastReportItem($input: ForecastReportItemDataUpdateInput!){
      updateForecastReportItem(input: $input) {
        id
      }
    }`;
  } else {
    // Insert
    query = `mutation createForecastReportItem($input: ForecastReportItemDataCreateInput!){
      createForecastReportItem(input: $input) {
        id
      }
    }`;
  }

  const variables = {
    input: {
      ...data,
    },
  };

  return fetch(endpoint, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Basic ${Buffer.from('user:' + token).toString('base64')}`,
    },
    body: JSON.stringify({
      query,
      variables,
    }),
  });
};
export const getForecastReportByCompanyDivition = (
  token,
  selectedConpanyValue,
  divisionValue,
) => {
  const query = `query ($input: ForecastReportItemsDataTypeInputArgs!){
    forecastReportItemQuery(input: $input) {
      id,
      company,
      headerRow,
      headerDescription,
      row,
      rowDescription,
      backlogBeginning,
      growthYTD,
      growthBOY,
      growthTotal,
      contractYTD,
      contractBOY,
      contractTotal,
      backlogEnding,
      nextYearGrowth,
      nextYearActivity,
      monthEnd,
      division,
      updateDate,
      updateUser
    }
  }`;

  const variables = {
    input: {
      company: selectedConpanyValue,
      division: divisionValue,
    },
  };

  return fetch(endpoint, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Basic ${Buffer.from('user:' + token).toString('base64')}`,
    },
    body: JSON.stringify({
      query,
      variables,
    }),
  });
};

export const getCompanyCode = CompanyValue => {
  let selectedConpanyValue = '';
  if (CompanyValue === '00200') {
    selectedConpanyValue = 'GIC';
  } else if (CompanyValue === '00260') {
    selectedConpanyValue = 'GSI';
  } else if (CompanyValue === '00300') {
    selectedConpanyValue = 'GCI';
  } else if (CompanyValue === '00200') {
    selectedConpanyValue = 'GIC';
  } else {
    selectedConpanyValue = 'ALL';
  }
  return selectedConpanyValue;
};
