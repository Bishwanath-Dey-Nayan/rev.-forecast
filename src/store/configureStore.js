import { createStore, compose, applyMiddleware } from 'redux';
import { createDataLoaderMiddleware } from 'redux-dataloader';
import rootReducer from './rootReducer';
import dataloaders from './dataloaders';

const dataLoaderMiddleware = createDataLoaderMiddleware(dataloaders);

export default function configureStore() {
  return createStore(
    rootReducer,
    compose(
      applyMiddleware(dataLoaderMiddleware),
      // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(), // commnet or uncomment this line to use or remove redux devtools in browser
    ),
  );
}
