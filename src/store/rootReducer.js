import { combineReducers } from 'redux';

import gfrReducer from './gfr/gfr.reducer';
import paReducer from './pendingawards/pa.reducer';
import fpReducer from './forecastPlan/fp.reducer';

const rootReducer = combineReducers({
  gfrReducer,
  paReducer,
  fpReducer
});

export default rootReducer;
