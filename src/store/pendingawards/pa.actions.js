import { load } from 'redux-dataloader';
import types from './pa.actionTypes';

export function fetchPADataRequest(token, company, bu, selectedDate) {
  return load({
    type: types.PA__FETCH_DATA_REQUEST,
    token,
    company,
    bu,
    selectedDate,
  });
}

export function fetchPADataFailure(error) {
  return {
    type: types.PA__FETCH_DATA_FAILURE,
    error,
  };
}

export function receivePAData(data) {
  return {
    type: types.PA__FETCH_DATA_RESPONSE,
    data,
  };
}

export function createPADataRequest(token, paObject) {
  return load({
    type: types.PA__CREATE_DATA_REQUEST,
    token,
    paObject,
  });
}

export function createPADataFailure(error) {
  return {
    type: types.PA__CREATE_DATA_FAILURE,
    error,
  };
}

export function createPADataResponse(data) {
  return {
    type: types.PA__CREATE_DATA_RESPONSE,
    data,
  };
}

export function updatePADataRequest(token, paObject) {
  return load({
    type: types.PA__UPDATE_DATA_REQUEST,
    token,
    paObject,
  });
}

export function updatePADataFailure(error) {
  return {
    type: types.PA__UPDATE_DATA_FAILURE,
    error,
  };
}

export function updatePADataResponse(data) {
  
  return {
    type: types.PA__UPDATE_DATA_RESPONSE,
    data,
  };
}

export function deletePADataRequest(token, id) {
  return load({
    type: types.PA__DELETE_DATA_REQUEST,
    token,
    id,
  });
}

export function deletePADataResponse(data) {
  
  return {
    type: types.PA__DELETE_DATA_RESPONSE,
    data,
  };
}

export function deletePADataFailure(error) {
  return {
    type: types.PA__DELETE_DATA_FAILURE,
    error,
  };
}
