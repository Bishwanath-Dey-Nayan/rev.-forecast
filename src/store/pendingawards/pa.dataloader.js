import { createLoader, fixedWait } from 'redux-dataloader';
import actionTypes from './pa.actionTypes';

import {
  fetchPADataFailure,
  receivePAData, // eslint-disable-line
  updatePADataResponse,
  updatePADataFailure,
  createPADataResponse,
  createPADataFailure,
  deletePADataResponse,
  deletePADataFailure,
} from './pa.actions';

const config = require('Config'); // eslint-disable-line

export const paDataRequestLoader = createLoader(
  actionTypes.PA__FETCH_DATA_REQUEST,
  {
    success: ({ dispatch }, { data }) => {
      dispatch(receivePAData(data));
      return; // eslint-disable-line
    },
    loading: ({ action }) => action,
    error: ({ dispatch }, error) => dispatch(fetchPADataFailure(error)),
    fetch: async context => {
      const { token, company, bu, selectedDate } = context.action;

      const query = `query ($input: PendingAwardsDataTypeInputArgs){
	pendingAwardsItemQuery(input: $input) {
		id,
		company,
		monthEnd,
		division,
		pendingAwardTitle,
		pendingAwardExpDate,
		pendingAwardRevenueValue,
		pendingAwardCTOValue,
		pendingAwardFYEPrevRev,
		pendingAwardFYEPrevCTO,
		pendingAwardCurBacklogRev,
		pendingAwardCurBacklogCTO,
		pendingAwardFutureBacklogRev,
		pendingAwardFutureBacklogCTO,
		updateDate,
    updateUser,
    maintenanceFlag,
    priorYearFlag,
	}
}`;

      let input = {};
      input = { monthEnd: selectedDate };
      if (company && company !== 'ALL') input.company = company;
      if (bu && bu !== 'ALL') input.division = bu;

      const variables = {
        input,
      };

      const response = await fetch("//185.90.39.146:5000/graphql", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Basic ${Buffer.from(`user:${token}`).toString(
            'base64',
          )}`, // eslint-disable-line
        },
        body: JSON.stringify({
          query,
          variables,
        }),
      })
        .then(resp => {
          console.info('resp:', resp);
          return resp.json();
        })
        .then(results => {
          const { data } = results;
          return data;
        })
        .catch(err => console.error('getGFRData error:', err)); // TODO - need a better way to handle error

      return { data: response };
    },
  },
  {
    ttl: 1000,
    retryTimes: 0,
    retryWait: fixedWait(500),
  },
);

export const paCreateDataRequestLoader = createLoader(
  actionTypes.PA__CREATE_DATA_REQUEST,
  {
    success: ({ dispatch }, { data }) => {     
      dispatch(createPADataResponse(data));
      return; // eslint-disable-line
    },
    loading: ({ action }) => action,
    error: ({ dispatch }, error) => dispatch(createPADataFailure(error)),
    fetch: async context => {
      const { token, paObject } = context.action;
      let query = '';

      if (paObject.id) {
        query = `mutation{
			  updatePendingAwardsItem(input:$input
			  ){id, pendingAwardTitle}
			}`;
      } else {
        query = `mutation createPendingAwardsItem($input: PendingAwardsDataCreateInput!){
				createPendingAwardsItem(input: $input) {
			  id,
		company,
		monthEnd,
		division,
		pendingAwardTitle,
		pendingAwardExpDate,
		pendingAwardRevenueValue,
		pendingAwardCTOValue,
		pendingAwardFYEPrevRev,
		pendingAwardFYEPrevCTO,
		pendingAwardCurBacklogRev,
		pendingAwardCurBacklogCTO,
		pendingAwardFutureBacklogRev,
		pendingAwardFutureBacklogCTO,
		updateDate,
    updateUser,
    maintenanceFlag,
    priorYearFlag,
			}
		  }`;
      }

      const variables = {
        input: {
          ...paObject,
        },
      };

      const response = await fetch("//185.90.39.146:5000/graphql", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Basic ${Buffer.from(`user:${token}`).toString(
            'base64',
          )}`, // eslint-disable-line
        },
        body: JSON.stringify({
          query,
          variables,
        }),
      })
        .then(resp => {
          console.info('resp:', resp);
          return resp.json();
        })
        .then(results => {
          const { data } = results;
          return data;
        })
        .catch(err => console.error('paData error:', err)); // TODO - need a better way to handle error

      return { data: response };
    },
  },
  {
    ttl: 1000,
    retryTimes: 0,
    retryWait: fixedWait(500),
  },
);

export const paUpdateDataRequestLoader = createLoader(
  actionTypes.PA__UPDATE_DATA_REQUEST,
  {
    success: ({ dispatch }, { data }) => {
      console.info(data);
      dispatch(updatePADataResponse(data));
      return; // eslint-disable-line
    },
    loading: ({ action }) => action,
    error: ({ dispatch }, error) => dispatch(updatePADataFailure(error)),
    fetch: async context => {
      const { token, paObject } = context.action;
      let query = '';

      query = `mutation updatePendingAwardsItem($input: PendingAwardsDataUpdateInput!){
					updatePendingAwardsItem(input: $input) {
					id,
			company,
			monthEnd,
			division,
			pendingAwardTitle,
			pendingAwardExpDate,
			pendingAwardRevenueValue,
			pendingAwardCTOValue,
			pendingAwardFYEPrevRev,
			pendingAwardFYEPrevCTO,
			pendingAwardCurBacklogRev,
			pendingAwardCurBacklogCTO,
			pendingAwardFutureBacklogRev,
			pendingAwardFutureBacklogCTO,
			updateDate,
      updateUser,
      maintenanceFlag,
    priorYearFlag,
				}
				}`;

      const variables = {
        input: {
          ...paObject,
        },
      };

      const response = await fetch("//185.90.39.146:5000/graphql", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Basic ${Buffer.from(`user:${token}`).toString(
            'base64',
          )}`, // eslint-disable-line
        },
        body: JSON.stringify({
          query,
          variables,
        }),
      })
        .then(resp => {
          console.info('resp:', resp);
          return resp.json();
        })
        .then(results => {
          const { data } = results;
          return data;
        })
        .catch(err => console.error('paData error:', err)); // TODO - need a better way to handle error

      return { data: response };
    },
  },
  {
    ttl: 1000,
    retryTimes: 0,
    retryWait: fixedWait(500),
  },
);

export const paDeleteDataRequestLoader = createLoader(
  actionTypes.PA__DELETE_DATA_REQUEST,
  {
    success: ({ dispatch }, { data }) => {
      console.info(data); // eslint-disable-line
      dispatch(deletePADataResponse(data));

      return; // eslint-disable-line
    },
    loading: ({ action }) => action,
    error: ({ dispatch }, error) => dispatch(deletePADataFailure(error)),
    fetch: async context => {
      const { token, id } = context.action;
      let query = '';

      query = `mutation{deletePendingAwardsItem(input:{id:${id}}){id,}}`;

      const response = await fetch(config['api-endpoint'], {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Basic ${Buffer.from(`user:${token}`).toString(
            'base64',
          )}`, // eslint-disable-line
        },
        body: JSON.stringify({
          query,
        }),
      })
        .then(resp => {
          console.info('resp:', resp);
          return resp.json();
        })
        .then(results => {
          const { data } = results;
          return data;
        })
        .catch(err => console.error('getGFRData error:', err)); // TODO - need a better way to handle error

      return { data: response };
    },
  },
  {
    ttl: 1000,
    retryTimes: 0,
    retryWait: fixedWait(500),
  },
);
