import types from './pa.actionTypes';
import { createReducer } from '../utils';

const defaultState = {
  pendingAwardsData: [],
};

export const receivePAData = (state, { data: { pendingAwardsItemQuery } }) =>{
  //  filter out null elemenet to prevent blank screen
  const filteredItems = pendingAwardsItemQuery.filter(
    el => el!= null,
  );

  const retState = {
    ...state,
    pendingAwardsData: filteredItems, 
  }; 
  return retState;
};



export const fetchPADataFailure = (state, { error }) => ({
  ...state,
  error: error.message,
});

export const createPADataResponse = (
  state,
  { data: { createPendingAwardsItem } },
) => {
  let { pendingAwardsData } = state; // eslint-disable-line

  pendingAwardsData.push(createPendingAwardsItem);

  return {
    ...state,
    pendingAwardsData: [...pendingAwardsData],
  };
};
export const createPADataFailure = (state, { error }) => ({
  ...state,
  error: error.message,
});

export const updatePADataResponse = (
  state,
  { data: { updatePendingAwardsItem } },
) => {
  let { pendingAwardsData } = state; // eslint-disable-line


  pendingAwardsData = state.data.map(item => {
    if (item.id === updatePendingAwardsItem.id) {
      return updatePendingAwardsItem;
    }

    return item;
  });
 
  return {
    ...state,
    pendingAwardsData: [...pendingAwardsData],
  };
};
export const updatePADataFailure = (state, { error }) => ({
  ...state,
  error: error.message,
});

export const deletePADataFailure = (state, { error }) => ({
  ...state,
  error: error.message,
});

export const deletePADataResponse = (
  state,
  { data: { deletePendingAwardsItem } },
) => {
  let { pendingAwardsData } = state; // eslint-disable-line
  

  pendingAwardsData = pendingAwardsData.filter(
    element => element.id !== deletePendingAwardsItem.id,
  );
  
  return {
    ...state,
    pendingAwardsData: [...pendingAwardsData],
  };
};
export default createReducer(defaultState, {
  [types.PA__FETCH_DATA_RESPONSE]: receivePAData,
  [types.PA__FETCH_DATA_FAILURE]: fetchPADataFailure,
  [types.PA__CREATE_DATA_RESPONSE]: createPADataResponse,
  [types.PA__CREATE_DATA_FAILURE]: createPADataFailure,
  [types.PA__UPDATE_DATA_RESPONSE]: updatePADataResponse,
  [types.PA__UPDATE_DATA_FAILURE]: updatePADataFailure,
  [types.PA__DELETE_DATA_RESPONSE]: deletePADataResponse,
  [types.PA__DELETE_DATA_FAILURE]: deletePADataFailure,
});
