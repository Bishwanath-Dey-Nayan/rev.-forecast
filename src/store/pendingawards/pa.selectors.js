

export const getRevenueValueTotalforMaintenanceFlag = (value) =>
{
    let RevenueValTotal = 0;
    value.forEach(val =>
    {
        if(val.maintenanceFlag === true && val.priorYearFlag === null)
        {
            RevenueValTotal += parseFloat(val.pendingAwardRevenueValue);
        }
    }); 
    return RevenueValTotal;
    
}

export const getCTOValueTotalforMaintenanceFlag = (value) =>
{
    let CTOValTotal = 0;
    value.forEach(val =>
    {
        if(val.maintenanceFlag === true && val.priorYearFlag === null)
        {
            CTOValTotal += parseFloat(val.pendingAwardCTOValue);
        }
    }); 
    return CTOValTotal;
    
}

export const getRevenueValueTotalforBothFlag = (value) =>
{
    let RevenueValTotal = 0;
    value.forEach(val =>
    {
        if(val.maintenanceFlag === true && val.priorYearFlag === true)
        {
            RevenueValTotal += parseFloat(val.pendingAwardRevenueValue);
        }
    }); 
    return RevenueValTotal;
    
}

export const getCTOValueTotalforBothFlag = (value) =>
{
    let RevenueValTotal = 0;
    value.forEach(val =>
    {
        if(val.maintenanceFlag === true && val.priorYearFlag === true)
        {
            RevenueValTotal += parseFloat(val.pendingAwardRevenueValue);
        }
    }); 
    return RevenueValTotal;
    
}