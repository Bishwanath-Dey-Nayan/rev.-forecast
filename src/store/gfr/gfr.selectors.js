import { createSelector } from 'reselect';
import moment from 'moment';

import { reportModeLists } from '../../constants/GfrReport.constant';

const getSelectedDivision = state => state.selectedDivision;
const getSelectedCompany = state => state.selectedCompany;

export const getReportMode = createSelector(
  [getSelectedDivision, getSelectedCompany],
  (selectedDivision, selectedCompany) => {
    let reportMode = '';
    if (selectedCompany.value === 'ALL')
      reportMode = reportModeLists.companyDivisionBothAll;
    else if (selectedDivision.value === 'ALL')
      reportMode = reportModeLists.onlyCompanySelected;
    else reportMode = reportModeLists.bothSelected;
    return reportMode;
  },
);

const getSelecteMonth = state => state.selectedMonth;
const getSelectedYear = state => state.selectedYear;
export const getMonthEnd = createSelector(
  [getSelecteMonth, getSelectedYear],
  (selectedMonth, selectedYear) => {
    let monthEnd = '';
    if (selectedYear && selectedMonth) {
      monthEnd = moment(
        new Date(selectedYear.value, selectedMonth.value, 0),
      ).format('YYYY-MM-DD');
    }
    return monthEnd;
  },
);


export const getCompanyCode = createSelector(
  [getSelectedCompany],
  (selectedCompany) => {

    let companyCode = '';
  
      if (selectedCompany.value === '00200') {
        companyCode = 'GIC';
      } else if (selectedCompany.value === '00260') {
        companyCode = 'GSI';
      } else if (selectedCompany.value === '00300') {
        companyCode = 'GCC';
      } else {
        companyCode = 'ALL';
      }

    return companyCode;
  },
);

export const getCompanyCodeForPA = (value) =>
{
    let companyCode = '';
  
      if (value === '00200') {
        companyCode = 'GIC';
      } else if (value === '00260') {
        companyCode = 'GSI';
      } else if (value === '00300') {
        companyCode = 'GCC';
      } else {
        companyCode = 'ALL';
      }

    return companyCode;
  }
