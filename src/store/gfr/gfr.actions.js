import { load } from 'redux-dataloader';
import types from './gfr.actionTypes';

export function setUserToken(token) {
  return {
    type: types.GFR__SET_USER_TOKEN,
    token,
  };
}

export function setUserInfo(userPrincipalName, displayName) {
  return {
    type: types.GFR__SET_USER_INFO,
    userPrincipalName,
    displayName,
  };
}

export function fetchJDEDataRequest(token, company, bu, selectedDate) {
  return load({
    type: types.GFR__FETCH_JDE_DATA_REQUEST,
    token,
    company,
    bu,
    selectedDate,
  });
}

export function fetchJDEDataFailure(error) {
  return {
    type: types.GFR__FETCH_JDE_DATA_FAILURE,
    error,
  };
}

export function fetchGFRDataRequest(token, company, division, monthEnd) {
  return load({
    type: types.GFR__FETCH_GFR_DATA_REQUEST,
    token,

    company,
    division,
    monthEnd,
  });
}

export function fetchGFRDataFailure(error) {
  return {
    type: types.GFR__FETCH_GFR_DATA_FAILURE,
    error,
  };
}

export function fetchCompanyDataRequest(token) {
  return load({
    type: types.GFR__FETCH_COMPANY_DATA_REQUEST,
    token,
  });
}

export function fetchCompanyDataFailure(error) {
  return {
    type: types.GFR__FETCH_COMPANY_DATA_FAILURE,
    error,
  };
}

export function receiveJDEData(data) {
  return {
    type: types.GFR__RECEIVE_JDE_DATA,
    data,
  };
}

export function receiveGFRData(data) {
  return {
    type: types.GFR__RECEIVE_GFR_DATA,
    data,
  };
}

export function receiveCompanyData(data) {
  return {
    type: types.GFR__RECEIVE_COMPANY_DATA,
    data,
  };
}

export function selectedCompanyChanged(selectedCompany) {
  return {
    type: types.GFR__COMPANY_DDL_CHANGED,
    selectedCompany,
  };
}

export function selectedDivisionChanged(selectedDivision) {
  return {
    type: types.GFR__DIVISION_DDL_CHANGED,
    selectedDivision,
  };
}

export function fetchDivisionDataRequest(token, company) {
  return load({
    type: types.GFR__FETCH_DIVISION_DATA_REQUEST,
    token,
    company,
  });
}

export function fetchDivisionDataFailure(error) {
  return {
    type: types.GFR__FETCH_DIVISION_DATA_FAILURE,
    error,
  };
}

export function receiveDivisionData(data) {
  return {
    type: types.GFR__RECEIVE_DIVISION_DATA,
    data,
  };
}

export function updateGfrCellDataRequest(token, cellObj) {
  
  return load({
    type: types.GFR__UPDATE_GFR_CELL_DATA_REQUEST,
    token,
    cellObj,
  });
}

export function updateGfrCellDataComplete(data) {
  return {
    type: types.GFR__UPDATE_GFR_CELL_DATA_COMPLETE,
    data,
  };
}

export function updateGfrCellDataFailure(error) {
  return {
    type: types.GFR__UPDATE_GFR_CELL_DATA_FAILURE,
    error,
  };
}

export function insertGfrCellDataRequest(token, cellObj) {
  return load({
    type: types.GFR__INSERT_GFR_CELL_DATA_REQUEST,
    token,
    cellObj,
  });
}

export function insertGfrCellDataComplete(data) {
  return {
    type: types.GFR__INSERT_GFR_CELL_DATA_COMPLETE,
    data,
  };
}

export function insertGfrCellDataFailure(error) {
  return {
    type: types.GFR__INSERT_GFR_CELL_DATA_FAILURE,
    error,
  };
}

export function selectedMonthChanged(selectedMonth) {
  return {
    type: types.GFR__MONTH_DDL_CHANGED,
    selectedMonth,
  };
}

export function selectedYearChanged(selectedYear) {
  return {
    type: types.GFR__YEAR_DDL_CHANGED,
    selectedYear,
  };
}

export function fetchForecastPlrDataRequest(token, company, bu, selectedDate) {
  return load({
    type: types.GET_FORECAST_PLR_REPORT_REQUEST,
    token,
    company,
    bu,
    selectedDate,
  });
}

export function receiveForeCastPLRData(data) {
  return {
    type: types.GET_FORECAST_PLR_REPORT_COMPLETE,
    data,
  };
}

export function fetchForecastPlrDataFailure(error) {
  return {
    type: types.GET_FORECAST_PLR_REPORT_FAILURE,
    error,
  };
}

export function fetchContractStatusDataRequest(token, company, bu, selectedDate, selectCurrentYear) {
  return load({
    type: types.GET_CONTRACT_STATUS_REPORT_REQUEST,
    token,
    company,
    bu,
    selectedDate,
    selectCurrentYear,
  });
}

export function receiveContractStatusData(data, selectCurrentYear) {
  return {
    type: types.GET_CONTRACT_STATUS_REPORT_COMPLETE,
    data,
    selectCurrentYear,
  };
}

export function contractStatusDataFailure(error) {
  return {
    type: types.GET_CONTRACT_STATUS_REPORT_FAILURE,
    error,
  };
}

export function fetchAllDivisionDataRequest(token) {
  return load({
    type: types.GFR__FETCH_ALL_DIVISION_DATA_REQUEST,
    token,
  });
}

export function receiveAllDivisionData(data) {
  return {
    type: types.GFR__RECEIVE_ALL_DIVISION_DATA,
    data,
  };
}

export function fetchAllDivisionDataFailure(error) {
  return {
    type: types.GFR__FETCH_ALL_DIVISION_DATA_FAILURE, 
    error,
  };
}
