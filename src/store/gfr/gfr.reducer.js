import types from './gfr.actionTypes';
import { createReducer } from '../utils';


const defaultState = {
  token: null,
  userPrincipalName: '',
  displayName: '',
  data: [],
  jdeData: [],
  plrData:{
    CDATE: '',
    A_GA_EXPENSES: '',
    B_OTHER_INCOME_EXP: '',
    C_RETRO_AMNT: '',
    D_ADJ_GROSS_PROF: ''
    },
  contractData:{
    CDATE: '',
    REVENUE_FORECAST: '',
    CURR_YEAR_REVENUE: '',
    TOTAL_REVENUE: '',
    REVENUE_BACKLOG: '',
    PRIOR_YEAR_COST: '',
    CURR_YEAR_COST: '',
    TOTAL_COST: '',
    UNADJUSTED_CTO: '',
    FORECAST_CTO: '',
    OH_PRIOR_YEAR: '',
    CURR_YEAR_CTO: '',
    TOTAL_CTO: '',
    BACKLOG_CTO: '',
    WIP: '',
    REVENUE_FORECAST_PY: '',
    CTO_FORECAST_PY: ''
    },
    cyContractData:{
      CDATE: '',
      REVENUE_FORECAST: '',
      CURR_YEAR_REVENUE: '',
      TOTAL_REVENUE: '',
      REVENUE_BACKLOG: '',
      PRIOR_YEAR_COST: '',
      CURR_YEAR_COST: '',
      TOTAL_COST: '',
      UNADJUSTED_CTO: '',
      FORECAST_CTO: '',
      OH_PRIOR_YEAR: '',
      CURR_YEAR_CTO: '',
      TOTAL_CTO: '',
      BACKLOG_CTO: '',
      WIP: '',
      REVENUE_FORECAST_PY: '',
      CTO_FORECAST_PY: ''
  },
  companies: [{ value: 'ALL', label: 'ALL' }],
  error: '',
  selectedCompany: { value: 'ALL', label: 'ALL' },
  divisions: [{ value: 'ALL', label: 'ALL' }],
  selectedDivision: { value: 'ALL', label: 'All' },
  selectedMonth: {
    value: 1,
    label: 'January',
  },
  selectedYear: {},
  allDivisions:[{value:'ALL',label:'ALL',company:''}]
};

export const setUserToken = (state, { token }) => ({
  ...state,
  token,
});

export const setUserInfo = (state, { userPrincipalName, displayName }) => ({
  ...state,
  userPrincipalName,
  displayName,
});

export const receiveJDEData = (state, { data }) => ({
  ...state,
  jdeData: data,
});

export const fetchJDEDataFailure = (state, { error }) => ({
  ...state,
  error: error.message,
});

export const receiveGFRData = (
  state,
  { data: { forecastReportItemQuery } },
) => {
  const filteredItems = forecastReportItemQuery.filter(
    el => el.rowDescription != null,
  );

  // console.log("gfr.reducer-receiveGFRData-forecastReportItemQuery  :", forecastReportItemQuery);//eslint-disable-line

  const retState = {
    ...state,
    data: filteredItems,
  };
  // console.log("gfr.reducer-receiveGFRData-data  :", filteredItems); //eslint-disable-line
  return retState;
};

export const fetchGFRDataFailure = (state, { error }) => ({ 
  ...state,
  error: error.message,
});

export const receiveCompanyData = (state, { data }) => {
  let { companies } = state; // eslint-disable-line
  for (let i = 0; i < data.length; i += 1) {
    const { COMPANY, COMPANY_NAME } = data[i];
    if (COMPANY !== '00100') {
      companies.push({
        value: COMPANY,
        label: COMPANY_NAME,
      });
    }
  }
  return {
    ...state,
    companies,
  };
};

export const fetchCompanyDataFailure = (state, { error }) => ({
  ...state,
  error: error.message,
});

export const updateCellValueComplete = (
  state,
  { data: { updateForecastReportItem } },
) => {
  let { data } = state; // eslint-disable-line
  // console.log("gfr.reducer-updateCellValueComplete-updateForecastReportItem  :", updateForecastReportItem);//eslint-disable-line
  data = data.map(item => {
    if (item.id === updateForecastReportItem.id) {
      return updateForecastReportItem;
    }
    return item;
  });
  // console.log("gfr.reducer-updateCellValueComplete-data  :", data); //eslint-disable-line
  const retState = {
    ...state,
    data: [...data],
  };
  return retState;
};

export const updateCellValueFailure = (state, { error }) => ({
  ...state,
  error: error.message,
});

export const changeSelectedCompany = (state, { selectedCompany }) => ({
  ...state,
  selectedCompany,
});

export const changeSelectedDivision = (state, { selectedDivision }) => ({
  ...state,
  selectedDivision,
});

export const receiveDivisionData = (state, { data }) => {
  let divisions = [{ value: 'ALL', label: 'ALL' }]; // eslint-disable-line

  for (let i = 0; i < data.length; i += 1) {
    const { DIVISION, DIVISION_DESC } = data[i];
    divisions.push({
      value: DIVISION,
      label: DIVISION_DESC,
    });
  }

  return {
    ...state,
    divisions: [...divisions],
  };
};

export const fetchDivisionDataFailure = (state, { error }) => ({
  ...state,
  error: error.message,
});

export const insertCellValueComplete = (
  state,
  { data: { createForecastReportItem } },
) => {
  let { data } = state; // eslint-disable-line
  console.log("gfr.reducer-insertCellValueComplete-createForecastReportItem  :", createForecastReportItem);//eslint-disable-line
  data.push(createForecastReportItem);

  const retState = {
    ...state,
    data: [...data],
  };
  // console.log("gfr.reducer-insertCellValueComplete-data  :", data); //eslint-disable-line
  return retState;
};

export const insertCellValueFailure = (state, { error }) => ({
  ...state,
  error: error.message,
});

export const changeSelectedMonth = (state, { selectedMonth }) => ({
  ...state,
  selectedMonth,
});

export const changeSelectedYear = (state, { selectedYear }) => ({
  ...state,
  selectedYear,
});

export const receiveForeCastPLRData = (state, { data }) => ({
  ...state,
  plrData: data[0],
});

export const fetchForecastPlrDataFailure = (state, { error }) => ({
  ...state,
  error: error.message,
});

export const receiveContarctStatusData= (state, { data, selectCurrentYear }) => {
 if (selectCurrentYear) {
   return ({
     ...state,
     cyContractData: data[0],
   });
 }
 return ({
   ...state,
   contractData: data[0],
 });
}

export const fetchReceiveContractStatusFailure = (state, { error }) => ({
  ...state,
  error: error.message,
});

export const receiveAllDivisionData = (state, { data }) => {
  let divisions = [{ value: 'ALL', label: 'ALL', company:'' }]; // eslint-disable-line

  for (let i = 0; i < data.length; i += 1) {
    const { COMPANY,DIVISION, DIVISION_DESC } = data[i];
    divisions.push({
      value: DIVISION,
      label: DIVISION_DESC,
      company:COMPANY
    });
  }

  return {
    ...state,
    allDivisions: [...divisions],
  };
};

export const fetchAllDivisionDataFailure = (state, { error }) => ({
  ...state,
  error: error.message,
});

export default createReducer(defaultState, {
   [types.GFR__RECEIVE_JDE_DATA]: receiveJDEData,
  [types.GFR__RECEIVE_GFR_DATA]: receiveGFRData,
  [types.GFR__RECEIVE_COMPANY_DATA]: receiveCompanyData,
  [types.GFR__FETCH_JDE_DATA_FAILURE]: fetchJDEDataFailure,
  [types.GFR__FETCH_GFR_DATA_FAILURE]: fetchGFRDataFailure,
  [types.GFR__FETCH_COMPANY_DATA_FAILURE]: fetchCompanyDataFailure,
  [types.GFR__SET_USER_TOKEN]: setUserToken,
  [types.GFR__SET_USER_INFO]: setUserInfo,
  [types.GFR__UPDATE_GFR_CELL_DATA_COMPLETE]: updateCellValueComplete,
  [types.GFR__UPDATE_GFR_CELL_DATA_FAILURE]: updateCellValueFailure,
  [types.GFR__COMPANY_DDL_CHANGED]: changeSelectedCompany,
  [types.GFR__RECEIVE_DIVISION_DATA]: receiveDivisionData,
  [types.GFR__FETCH_DIVISION_DATA_FAILURE]: fetchDivisionDataFailure,
  [types.GFR__DIVISION_DDL_CHANGED]: changeSelectedDivision,
  [types.GFR__MONTH_DDL_CHANGED]: changeSelectedMonth,
  [types.GFR__INSERT_GFR_CELL_DATA_COMPLETE]: insertCellValueComplete,
  [types.GFR__INSERT_GFR_CELL_DATA_FAILURE]: insertCellValueFailure,
  [types.GFR__YEAR_DDL_CHANGED]: changeSelectedYear,
  [types.GET_FORECAST_PLR_REPORT_COMPLETE]:receiveForeCastPLRData,
  [types.GET_FORECAST_PLR_REPORT_FAILURE]:fetchForecastPlrDataFailure,
  [types.GET_CONTRACT_STATUS_REPORT_COMPLETE]:receiveContarctStatusData,
  [types.GET_CONTRACT_STATUS_REPORT_FAILURE]:fetchReceiveContractStatusFailure,

  [types.GFR__RECEIVE_ALL_DIVISION_DATA]:receiveAllDivisionData,
  [types.GFR__FETCH_ALL_DIVISION_DATA_FAILURE]:fetchAllDivisionDataFailure,

});
