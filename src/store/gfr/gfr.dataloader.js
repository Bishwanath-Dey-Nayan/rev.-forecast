import { createLoader, fixedWait } from 'redux-dataloader';
import actionTypes from './gfr.actionTypes';

import {
  receiveGFRData,
  receiveJDEData,
  receiveCompanyData, // eslint-disable-line
  fetchJDEDataFailure,
  fetchGFRDataFailure,
  fetchCompanyDataFailure,
  updateGfrCellDataComplete, // eslint-disable-line
  updateGfrCellDataFailure,
  fetchDivisionDataFailure,
  receiveDivisionData,
  insertGfrCellDataComplete,
  insertGfrCellDataFailure,
  receiveForeCastPLRData,
  fetchForecastPlrDataFailure,
  receiveContractStatusData,
  contractStatusDataFailure,
  fetchAllDivisionDataFailure,
  receiveAllDivisionData

} from './gfr.actions';

const config = require('Config'); // eslint-disable-line

const isFilteredDiv = (div) => {
  switch (div) {
    case "200":
    case "206":
    case "214":
    case "260":
    case "300":
    case "301":
    case "309":
      return true;
    default:
      return false;
  }
}

export const gfrDataRequestLoader = createLoader(
  actionTypes.GFR__FETCH_GFR_DATA_REQUEST,
  {
    success: ({ dispatch }, { data }) => {
      dispatch(receiveGFRData(data));
      return; // eslint-disable-line
    },
    loading: ({ action }) => action,
    error: ({ dispatch }, error) => dispatch(fetchGFRDataFailure(error)),
    fetch: async context => {
      const { token, company, division, monthEnd } = context.action;

      const query = `query ($input: ForecastReportItemsDataTypeInputArgs!){
        forecastReportItemQuery(input: $input) {
          id,
          company,
          headerRow,
          headerDescription,
          row,
          rowDescription,
          backlogBeginning,
          growthYTD,
          growthBOY,
          growthTotal,
          contractYTD,
          contractBOY,
          contractTotal,
          backlogEnding,
          nextYearGrowth,
          nextYearActivity,
          monthEnd,
          division,
          updateDate,
          updateUser
        }
      }`;

      let input = {};
      input = { monthEnd };
      if (company && company !== 'ALL') input.company = company;
      if (division && division !== 'ALL') input.division = division;

      const variables = {
        input,
      };

      const response = await fetch(config['api-endpoint'], {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Basic ${Buffer.from(`user:${token}`).toString(
            'base64',
          )}`, // eslint-disable-line
        },
        body: JSON.stringify({
          query,
          variables,
        }),
      })
        .then(resp => {
          console.info('resp:', resp);
          return resp.json();
        })
        .then(results => {
          const { data } = results;
          return data;
        })
        .catch(err => console.error('getGFRData error:', err)); // TODO - need a better way to handle error

      return { data: response };
    },
  },
  {
    ttl: 1000,
    retryTimes: 0,
    retryWait: fixedWait(500),
  },
);

export const getContractStatusReportLoader = createLoader(
  actionTypes.GET_CONTRACT_STATUS_REPORT_REQUEST,
  {
    success: ({ dispatch }, { data, selectCurrentYear }) => {

      let objDataArr = data; // eslint-disable-line
      if (objDataArr && objDataArr.length > 0) {
        let modifiedData = objDataArr[0];// eslint-disable-line
        modifiedData.REVENUE_FORECAST = Math.round(modifiedData.REVENUE_FORECAST / 1000.00); // eslint-disable-line
        modifiedData.CURR_YEAR_REVENUE = Math.round(modifiedData.CURR_YEAR_REVENUE / 1000.00); // eslint-disable-line
        modifiedData.TOTAL_REVENUE = Math.round(modifiedData.TOTAL_REVENUE / 1000.00); // eslint-disable-line
        modifiedData.REVENUE_BACKLOG = Math.round(modifiedData.REVENUE_BACKLOG / 1000.00); // eslint-disable-line
        modifiedData.PRIOR_YEAR_COST = Math.round(modifiedData.PRIOR_YEAR_COST / 1000.00); // eslint-disable-line
        modifiedData.CURR_YEAR_COST = Math.round(modifiedData.CURR_YEAR_COST / 1000.00); // eslint-disable-line
        modifiedData.TOTAL_COST = Math.round(modifiedData.TOTAL_COST / 1000.00); // eslint-disable-line
        modifiedData.UNADJUSTED_CTO = Math.round(modifiedData.UNADJUSTED_CTO / 1000.00); // eslint-disable-line
        modifiedData.FORECAST_CTO = Math.round(modifiedData.FORECAST_CTO / 1000.00); // eslint-disable-line
        modifiedData.OH_PRIOR_YEAR = Math.round(modifiedData.OH_PRIOR_YEAR / 1000.00); // eslint-disable-line
        modifiedData.CURR_YEAR_CTO = Math.round(modifiedData.CURR_YEAR_CTO / 1000.00);// eslint-disable-line
        modifiedData.TOTAL_CTO = Math.round(modifiedData.TOTAL_CTO / 1000.00); // eslint-disable-line
        modifiedData.BACKLOG_CTO = Math.round(modifiedData.BACKLOG_CTO / 1000.00); // eslint-disable-line
        modifiedData.WIP = Math.round(modifiedData.WIP / 1000.00); // eslint-disable-line
        modifiedData.REVENUE_FORECAST_PY = Math.round(modifiedData.REVENUE_FORECAST_PY / 1000.00); // eslint-disable-line
        modifiedData.CTO_FORECAST_PY = Math.round(modifiedData.CTO_FORECAST_PY / 1000.00); // eslint-disable-line
        objDataArr[0] = modifiedData;
      } else {
        let modifiedData = {};// eslint-disable-line
        modifiedData.CDATE = ''; // eslint-disable-line
        modifiedData.REVENUE_FORECAST = 0.0; // eslint-disable-line
        modifiedData.CURR_YEAR_REVENUE = 0.0; // eslint-disable-line
        modifiedData.TOTAL_REVENUE = 0.0; // eslint-disable-line
        modifiedData.REVENUE_BACKLOG = 0.0; // eslint-disable-line
        modifiedData.PRIOR_YEAR_COST = 0.0; // eslint-disable-line
        modifiedData.CURR_YEAR_COST = 0.0; // eslint-disable-line
        modifiedData.TOTAL_COST = 0.0;// eslint-disable-line
        modifiedData.UNADJUSTED_CTO = 0.0; // eslint-disable-line
        modifiedData.FORECAST_CTO = 0.0; // eslint-disable-line
        modifiedData.OH_PRIOR_YEAR = 0.0;// eslint-disable-line
        modifiedData.CURR_YEAR_CTO = 0.0;// eslint-disable-line
        modifiedData.TOTAL_CTO = 0.0;// eslint-disable-line
        modifiedData.BACKLOG_CTO = 0.0; // eslint-disable-line
        modifiedData.WIP = 0.0; // eslint-disable-line
        modifiedData.REVENUE_FORECAST_PY = 0.0; // eslint-disable-line
        modifiedData.CTO_FORECAST_PY = 0.0; // eslint-disable-line
        objDataArr = [];
        objDataArr[0] = modifiedData;
      }

      dispatch(receiveContractStatusData(objDataArr, selectCurrentYear));
      return; // eslint-disable-line
    },
    loading: ({ action }) => action,
    error: ({ dispatch }, error) => dispatch(contractStatusDataFailure(error)),
    fetch: async context => {
      const { token, company, bu, selectedDate, selectCurrentYear } = context.action;

      const query = `query ($input: ContractStatusInputType!){
        getContractStatusReportQuery(input: $input) {
          CDATE,
          REVENUE_FORECAST,
          CURR_YEAR_REVENUE,
          TOTAL_REVENUE,
          REVENUE_BACKLOG,
          PRIOR_YEAR_COST,
          CURR_YEAR_COST,
          TOTAL_COST,
          UNADJUSTED_CTO,
          FORECAST_CTO,
          OH_PRIOR_YEAR,
          CURR_YEAR_CTO,
          TOTAL_CTO,
          BACKLOG_CTO,
          WIP,
          REVENUE_FORECAST_PY,
          CTO_FORECAST_PY
        }
      }`;

      const input = {
        SELECTED_DATE: selectedDate,
        SELECT_CURRENT_YEAR: selectCurrentYear,
      };

      if (company && company !== 'ALL') input.COMPANY = company;
      if (bu && bu !== 'ALL') input.BU = bu;

      const variables = {
        input,
      };

      const body = JSON.stringify({
        query,
        variables,
      });



      const response = await fetch(config['api-endpoint'], {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Basic ${Buffer.from(`user:${token}`).toString(
            'base64',
          )}`, //eslint-disable-line
        },
        body,
      })
        .then(resp => resp.json())
        .then(results => results.data.getContractStatusReportQuery)
        .catch(err => console.error('Error in getContractStatusReportLoader:', err)); // TODO - need a better way to handle error

      return { data: response, selectCurrentYear };
    },
  },
  {
    ttl: 1000,
    retryTimes: 0,
    retryWait: fixedWait(500),
  },
);

export const getForecastPLReportLoader = createLoader(
  actionTypes.GET_FORECAST_PLR_REPORT_REQUEST,
  {
    success: ({ dispatch }, { data }) => {
      let objDataArr = data; // eslint-disable-line
      if (objDataArr && objDataArr.length > 0) {
        let modifiedData = objDataArr[0]; // eslint-disable-line
        modifiedData.A_GA_EXPENSES = Math.round(modifiedData.A_GA_EXPENSES / 1000.00);  // eslint-disable-line
        modifiedData.B_OTHER_INCOME_EXP = Math.round(modifiedData.B_OTHER_INCOME_EXP / 1000.00); // eslint-disable-line
        modifiedData.C_RETRO_AMNT = Math.round(modifiedData.C_RETRO_AMNT / 1000.00); // eslint-disable-line
        modifiedData.D_ADJ_GROSS_PROF = Math.round(modifiedData.D_ADJ_GROSS_PROF / 1000.00); // eslint-disable-line
        objDataArr[0]=modifiedData;
      }
      else {
        let modifiedData = {}; // eslint-disable-line
        modifiedData.A_GA_EXPENSES = 0.0;  // eslint-disable-line
        modifiedData.B_OTHER_INCOME_EXP = 0.0; // eslint-disable-line
        modifiedData.C_RETRO_AMNT = 0.0; // eslint-disable-line
        modifiedData.D_ADJ_GROSS_PROF = 0.0; // eslint-disable-line
        objDataArr = [];
        objDataArr[0] = modifiedData;
      }
     
      dispatch(receiveForeCastPLRData(objDataArr));
      return; // eslint-disable-line
    },
    loading: ({ action }) => action,
    error: ({ dispatch }, error) => dispatch(fetchForecastPlrDataFailure(error)),
    fetch: async context => {
      const { token, company, bu, selectedDate } = context.action;

            const query = `query ($input: ForecastPLInputType!){
              getForecastPLReportQuery(input: $input) {
                CDATE,
                A_GA_EXPENSES,
                B_OTHER_INCOME_EXP,
                C_RETRO_AMNT,
                D_ADJ_GROSS_PROF
              }
            }`;
      let input = {};
      input = { SELECTED_DATE: selectedDate };
      if (company && company !== 'ALL') input.COMPANY = company;
      if (bu && bu !== 'ALL') input.BU = bu;

      const variables = {
        input,
      };

      const response = await fetch(config['api-endpoint'], {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Basic ${Buffer.from(`user:${token}`).toString(
            'base64',
          )}`, //eslint-disable-line
        },
        body: JSON.stringify({
          query,
          variables,
        }),
      })
        .then(resp => resp.json())
        .then(results => results.data.getForecastPLReportQuery)
        .catch(err => console.error('Error in getForecastPLReportLoader:', err)); // TODO - need a better way to handle error

      return { data: response };
    },
  },
  {
    ttl: 1000,
    retryTimes: 0,
    retryWait: fixedWait(500),
  },
);


export const jdeDataRequestLoader = createLoader(
  actionTypes.GFR__FETCH_JDE_DATA_REQUEST,
  {
    success: ({ dispatch }, { data }) => {
      dispatch(receiveJDEData(data));
      return; // eslint-disable-line
    },
    loading: ({ action }) => action,
    error: ({ dispatch }, error) => dispatch(fetchJDEDataFailure(error)),
    fetch: async context => {
      const { token, company, bu, selectedDate } = context.action;

      const query = `query ($input: ForecastRptGetJDEinfoInputType!){
        getForecastRptGetJDEinfoQuery(input: $input) {
          headerDescription,
          rowDescription,
          backlogBeginning,
          growthYTD,
          contractYTD,
        }
      }`;

      let input = {};
      input = { SELECTED_DATE: selectedDate };
      if (company && company !== 'ALL') input.COMPANY = company;
      if (bu && bu !== 'ALL') input.BU = bu;

      const variables = {
        input,
      };

      const response = await fetch(config['api-endpoint'], {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Basic ${Buffer.from(`user:${token}`).toString(
            'base64',
          )}`, //eslint-disable-line
        },
        body: JSON.stringify({
          query,
          variables,
        }),
      })
        .then(resp => resp.json())
        .then(results => results.data.getForecastRptGetJDEinfoQuery)
        .catch(err => console.error('Error in jdeDataRequestLoader:', err)); // TODO - need a better way to handle error

      return { data: response };
    },
  },
  {
    ttl: 1000,
    retryTimes: 0,
    retryWait: fixedWait(500),
  },
);

export const companiesDataRequestLoader = createLoader(
  actionTypes.GFR__FETCH_COMPANY_DATA_REQUEST,
  {
    success: ({ dispatch }, { data }) => {
      dispatch(receiveCompanyData(data));
      return; // eslint-disable-line
    },
    loading: ({ action }) => action,
    error: ({ dispatch }, error) => dispatch(fetchCompanyDataFailure(error)),
    fetch: async context => {
      const { token } = context.action;

      const query = '{getTsGetCurrentCompaniesQuery{COMPANY, COMPANY_NAME}}';
      const response = await fetch(config['api-endpoint'], {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Basic ${Buffer.from(`user:${token}`).toString(
            'base64',
          )}`, // eslint-disable-line
        },
        body: JSON.stringify({
          query,
        }),
      })
        .then(resp => resp.json())
        .then(results => results.data.getTsGetCurrentCompaniesQuery)
        .catch(err => console.error('Get Companie Error:', err)); // TODO - need a better way to handle error

      return { data: response };
    },
  },
  {
    ttl: 1000,
    retryTimes: 0,
    retryWait: fixedWait(500),
  },
);

export const divisionsDataRequestLoader = createLoader(
  actionTypes.GFR__FETCH_DIVISION_DATA_REQUEST,
  {
    success: ({ dispatch }, { data }) => {
      dispatch(receiveDivisionData(data));
      return; // eslint-disable-line
    },
    loading: ({ action }) => action,
    error: ({ dispatch }, error) => dispatch(fetchDivisionDataFailure(error)),
    fetch: async context => {
      const { token, company } = context.action;

      const query = `query ($input: TsGetCurrentDivByCompanyInputType!){
        getTsGetCurrentDivByCompanyQuery(input: $input) {
          DIVISION,
          DIVISION_DESC,
        }
      }`;
      const variables = {
        input: {
          COMPANY: company,
        },
      };

      const response = await fetch(config['api-endpoint'], {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Basic ${Buffer.from(`user:${token}`).toString(
            'base64',
          )}`, // eslint-disable-line
        },
        body: JSON.stringify({
          query,
          variables,
        }),
      })
        .then(resp => resp.json())
        .then(results => results.data.getTsGetCurrentDivByCompanyQuery)
        .catch(err => console.error('Get Companie Error:', err)); // TODO - need a better way to handle error

      const divs = [];
      for (let i = 0; i < response.length; i += 1) {
        if (parseInt(response[i].DIVISION, 10) !== parseInt(company, 10) && !isFilteredDiv(response[i].DIVISION)) {
          divs.push(response[i]);
        }
      }

      return { data: divs };
    },
  },
  {
    ttl: 1000,
    retryTimes: 0,
    retryWait: fixedWait(500),
  },
);

export const gfrUpdateDataRequestLoader = createLoader(
  actionTypes.GFR__UPDATE_GFR_CELL_DATA_REQUEST,
  {
    success: ({ dispatch }, { data }) => {
      // you can get original request action from context

      dispatch(updateGfrCellDataComplete(data));
      return; // eslint-disable-line
    },
    loading: ({ action }) => action,
    error: ({ dispatch }, error) => dispatch(updateGfrCellDataFailure(error)),
    fetch: async context => {
      const { token, cellObj } = context.action;
      let query = '';
      // Update
      query = `mutation updateForecastReportItem($input: ForecastReportItemDataUpdateInput!){
          updateForecastReportItem(input: $input) {
            id,
            company,
            headerRow,
            headerDescription,
            row,
            rowDescription,
            backlogBeginning,
            growthYTD,
            growthBOY,
            growthTotal,
            contractYTD,
            contractBOY,
            contractTotal,
            backlogEnding,
            nextYearGrowth,
            nextYearActivity,
            monthEnd,
            division,
            updateDate,
            updateUser
          }
        }`;

      const variables = {
        input: {
          ...cellObj,
        },
      };

      const response = await fetch(config['api-endpoint'], {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Basic ${Buffer.from(`user:${token}`).toString(
            'base64',
          )}`, // eslint-disable-line
        },
        body: JSON.stringify({
          query,
          variables,
        }),
      })
        .then(resp => {
          console.info('resp:', resp);
          return resp.json();
        })
        .then(results => {
          const { data } = results;
          // console.log("gfr.dataloader-gfrUpdateDataRequestLoader-data  :", data);//eslint-disable-line
          return data;
        })
        .catch(err => console.error('getGFRData error:', err)); // TODO - need a better way to handle error
      return { data: response };
    },
  },
  {
    ttl: 0,
    retryTimes: 0,
    retryWait: fixedWait(500),
  },
);

export const gfrInsertDataRequestLoader = createLoader(
  actionTypes.GFR__INSERT_GFR_CELL_DATA_REQUEST,
  {
    success: ({ dispatch }, { data }) => {
      // you can get original request action from context

      dispatch(insertGfrCellDataComplete(data));
      return; // eslint-disable-line
    },
    loading: ({ action }) => action,
    error: ({ dispatch }, error) => dispatch(insertGfrCellDataFailure(error)),
    fetch: async context => {
      const { token, cellObj } = context.action;
      let query = '';

      // Insert
      query = `mutation createForecastReportItem($input: ForecastReportItemDataCreateInput!){
          createForecastReportItem(input: $input) {
            id,
            company,
            headerRow,
            headerDescription,
            row,
            rowDescription,
            backlogBeginning,
            growthYTD,
            growthBOY,
            growthTotal,
            contractYTD,
            contractBOY,
            contractTotal,
            backlogEnding,
            nextYearGrowth,
            nextYearActivity,
            monthEnd,
            division,
            updateDate,
            updateUser
          }
        }`;

      const variables = {
        input: {
          ...cellObj,
        },
      };

      const body = JSON.stringify({
        query,
        variables,
      });



      const response = await fetch(config['api-endpoint'], {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Basic ${Buffer.from(`user:${token}`).toString(
            'base64',
          )}`, // eslint-disable-line
        },
        body,
      })
        .then(resp => {
          console.info('resp:', resp);
          return resp.json();
        })
        .then(results => {
          const { data } = results;

          return data;
        })
        .catch(err => console.error('getGFRData error:', err)); // TODO - need a better way to handle error

      return { data: response };
    },
  },
  {
    ttl: 1000,
    retryTimes: 0,
    retryWait: fixedWait(500),
  },
);

export const allDivisionsDataRequestLoader = createLoader(
  actionTypes.GFR__FETCH_ALL_DIVISION_DATA_REQUEST,
  {
    success: ({ dispatch }, { data }) => {
      dispatch(receiveAllDivisionData(data));
      return; // eslint-disable-line
    },
    loading: ({ action }) => action,
    error: ({ dispatch }, error) => dispatch(fetchAllDivisionDataFailure(error)),
    fetch: async context => {
      const { token, } = context.action;

      const query = '{getTsGetCurrentDivisionsQuery{COMPANY,DIVISION, DIVISION_DESC}}';
      const response = await fetch(config['api-endpoint'], {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Basic ${Buffer.from(`user:${token}`).toString(
            'base64',
          )}`, // eslint-disable-line
        },
        body: JSON.stringify({
          query,
        }),
      })
        .then(resp => resp.json())
        .then(results => results.data.getTsGetCurrentDivisionsQuery)
        .catch(err => console.error('Get Companie Error:', err)); // TODO - need a better way to handle error

      const resp = [];
      for (let i = 0; i < response.length; i += 1) {
        const div = response[i].DIVISION;
        if (!isFilteredDiv(div)) {
          if (div === "203") {
            resp.push({
              ...response[i],
              DIVISION_DESC: 'Industrial-Power East',
            });
          } else {
            resp.push(response[i]);
          }
        }
      }

      return { data: resp };
    },
  },
  {
    ttl: 1000,
    retryTimes: 0,
    retryWait: fixedWait(500),
  },
);
