import {
  gfrDataRequestLoader,
  jdeDataRequestLoader,
  companiesDataRequestLoader,
  gfrUpdateDataRequestLoader,
  divisionsDataRequestLoader,
  gfrInsertDataRequestLoader,
  getContractStatusReportLoader,
  getForecastPLReportLoader,
  allDivisionsDataRequestLoader
} from './gfr/gfr.dataloader';
import {
  paDataRequestLoader,
  paCreateDataRequestLoader,
  paUpdateDataRequestLoader,
  paDeleteDataRequestLoader,
} from './pendingawards/pa.dataloader';

import{
  fpDataRequestLoader,
  fpCreateDataRequestLoader,
  fpUpdateDataRequestLoader
} from './forecastPlan/fp.dataloader';
 
export default [
  gfrDataRequestLoader,
  jdeDataRequestLoader,
  companiesDataRequestLoader,
  gfrUpdateDataRequestLoader,
  divisionsDataRequestLoader,
  paDataRequestLoader,
  paCreateDataRequestLoader,
  paUpdateDataRequestLoader,
  paDeleteDataRequestLoader,
  gfrInsertDataRequestLoader,
  getContractStatusReportLoader,
  getForecastPLReportLoader,
  allDivisionsDataRequestLoader,
  fpDataRequestLoader,
  fpCreateDataRequestLoader,
  fpUpdateDataRequestLoader
];
