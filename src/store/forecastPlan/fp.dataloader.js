/* eslint-disable import/prefer-default-export */
import { createLoader, fixedWait } from 'redux-dataloader';
import actionTypes from './fp.actionTypes';

import {
  fetchFPDataFailure,
  receiveFPData, // eslint-disable-line
  updateFPDataResponse,
  updateFPDataFailure,
  createFPDataResponse,
   createFPDataFailure,
  // deleteFPDataResponse,
  // deleteFPDataFailure,
} from './fp.actions';

const config = require('Config'); // eslint-disable-line

export const fpDataRequestLoader = createLoader(
  actionTypes.FP__FETCH_DATA_REQUEST,
  {
    success: ({ dispatch }, { data }) => {
      dispatch(receiveFPData(data));
      return; // eslint-disable-line
    },
    loading: ({ action }) => action,
    error: ({ dispatch }, error) => dispatch(fetchFPDataFailure(error)),
    fetch: async context => {
      const { token, company, year, division } = context.action;

      const query = `query ($input:ForecastPlanDataTypeInputArgs!){forecastPlanItemQuery(input: $input) {id,headerDescription,rowDescription,backlogBegining,backlogEnd,contractYTD,contractBOY,growthYTD,growthBOY,growthTotal,contractTotal,company,division,CreateDate,updateUser,year,monthEnd}}`;

      let input = {};
      input = { year };
      if (company && company !== 'ALL') input.company = company;
      if (division && division !== 'ALL') input.division = division;

      const variables = {
        input,
      };

      const response = await fetch("http://185.90.39.146:5000/graphql", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Basic ${Buffer.from(`user:${token}`).toString(
            'base64',
          )}`, // eslint-disable-line
        },
        body: JSON.stringify({
          query,
          variables,
        }),
      })
        .then(resp => {
          console.info('resp:', resp);
          return resp.json();
        })
        .then(results => {
          const { data } = results;
          return data;
        })
        .catch(err => console.error('forecastPlanData error:', err)); // TODO - need a better way to handle error

      return { data: response };
    },
  },
  {
    ttl: 1000,
    retryTimes: 0,
    retryWait: fixedWait(500),
  },
);


export const fpCreateDataRequestLoader = createLoader(
  actionTypes.FP__CREATE_DATA_REQUEST,
  {
    success: ({ dispatch }, { data }) => {
      dispatch(createFPDataResponse(data));
      return; // eslint-disable-line
    },
    loading: ({ action }) => action,
    error: ({ dispatch }, error) => dispatch(createFPDataFailure(error)),
    fetch: async context => {
      const { token, fpObject } = context.action;
      let query = '';


      query = `mutation createForecastPlanItem($input: ForecastPlanDataCreateInput!){ createForecastPlanItem(input: $input) {monthEnd,headerDescription,rowDescription,backlogBegining,backlogEnd,contractYTD,contractBOY,growthYTD
          ,growthBOY,growthTotal,contractTotal,company,CreateDate,updateUser,division,year}}`;


      const variables = {
        input: {
          ...fpObject,
        },
      };

      const response = await fetch("http://185.90.39.146:5000/graphql", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Basic ${Buffer.from(`user:${token}`).toString(
            'base64',
          )}`, // eslint-disable-line
        },
        body: JSON.stringify({
          query,
          variables,
        }),
      })
        .then(resp => {
          console.info('resp:', resp);
          return resp.json();
        })
        .then(results => {
          const { data } = results;
          return data;
        })
        .catch(err => console.error('fpData error:', err)); // TODO - need a better way to handle error

      return { data: response };
    },
  },
  {
    ttl: 1000,
    retryTimes: 0,
    retryWait: fixedWait(500),
  },
);

export const fpUpdateDataRequestLoader = createLoader(
  actionTypes.FP__UPDATE_DATA_REQUEST,
  {
    success: ({ dispatch }, { data }) => {
      console.info(data);
      dispatch(updateFPDataResponse(data));
      return; // eslint-disable-line
    },
    loading: ({ action }) => action,
    error: ({ dispatch }, error) => dispatch(updateFPDataFailure(error)),
    fetch: async context => {
      const { token, fpObject } = context.action;
      let query = '';

      query = `mutation updateForecastPlanItem($input: ForecastPlanDataUpdateInput!)
      { updateForecastPlanItem(input: $input) {id,headerDescription,rowDescription,backlogBegining,backlogEnd,contractYTD,
      contractBOY,growthYTD,growthBOY,growthTotal,contractTotal,company,division,year,CreateDate,updateUser,monthEnd}}`;

      const variables = {
        input: {
          ...fpObject,
        },
      };

      const response = await fetch("http://185.90.39.146:5000/graphql", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Basic ${Buffer.from(`user:${token}`).toString(
            'base64',
          )}`, // eslint-disable-line
        },
        body: JSON.stringify({
          query,
          variables,
        }),
      })
        .then(resp => {
          console.info('resp:', resp);
          return resp.json();
        })
        .then(results => {
          const { data } = results;
          return data;
        })
        .catch(err => console.error('fpData error:', err)); // TODO - need a better way to handle error

      return { data: response };
    },
  },
  {
    ttl: 1000,
    retryTimes: 0,
    retryWait: fixedWait(500),
  },
);


