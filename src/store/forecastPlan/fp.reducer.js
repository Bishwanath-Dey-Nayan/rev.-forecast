import types from './fp.actionTypes';
import { createReducer } from '../utils';

const defaultState = {
  // forecastPlanData: {
  //   id:'',
  //   headerDescription:'',
  //   rowDescription:'',
  //   backlogBegining:'',
  //   backlogEnd:'',
  //   contractYTD:'',
  //   contractBOY:'',
  //   growthYTD:'',
  //   growthBOY:'',
  //   growthTotal:'',
  //   contractTotal:'',
  //   company:'',
  //   CreateDate:'',
  //   updateUser:'',
  //   division:'',
  //   year:''

  // },
  forecastPlanData:[],
  error:""
};

export const receiveFPData = (
  state,
  { data: { forecastPlanItemQuery } },
) => {
  const filteredItems = forecastPlanItemQuery.filter(
    el => el!= null,
  );
  const retState = {
    ...state,
    forecastPlanData: filteredItems,
  };
  return retState;
};



export const fetchFPDataFailure = (state, { error }) => ({
  ...state,
  error: error.message,
});

export const createFPDataResponse = (
  state,
  { data: { createForecastPlanItem } },
) => {
  let { forecastPlanData } = state; // eslint-disable-line

  forecastPlanData.push(createForecastPlanItem);

  return {
    ...state,
    forecastPlanData: [...forecastPlanData],
  };
};
export const createFPDataFailure = (state, { error }) => ({
  ...state,
  error: error.message,
});

export const updateFPDataResponse = (
  state,
  { data: { updateforecastPlanItem } },
) => {
  let { forecastPlanData } = state; // eslint-disable-line


  forecastPlanData = state.data.map(item => {
    if (item.id === updateforecastPlanItem.id) {
      return updateforecastPlanItem;
    }

    return item;
  });
 
  return {
    ...state,
    forecastPlanData: [...forecastPlanData],
  };
};

export const updateFPDataFailure = (state, { error }) => ({
  ...state,
  error: error.message,
});

// export const deletePADataFailure = (state, { error }) => ({
//   ...state,
//   error: error.message,
// });

// export const deletePADataResponse = (
//   state,
//   { data: { deletePendingAwardsItem } },
// ) => {
//   let { pendingAwardsData } = state; // eslint-disable-line
  

//   pendingAwardsData = pendingAwardsData.filter(
//     element => element.id !== deletePendingAwardsItem.id,
//   );
  
//   return {
//     ...state,
//     pendingAwardsData: [...pendingAwardsData],
//   };
// };
export default createReducer(defaultState, {
  [types.FP__FETCH_DATA_RESPONSE]: receiveFPData,
  [types.FP__FETCH_DATA_FAILURE]: fetchFPDataFailure,
  [types.FP__CREATE_DATA_RESPONSE]: createFPDataResponse,
  [types.FP__CREATE_DATA_FAILURE]: createFPDataFailure,
  [types.FP__UPDATE_DATA_RESPONSE]: updateFPDataResponse,
  [types.FP__UPDATE_DATA_FAILURE]: updateFPDataFailure,
  // [types.PA__DELETE_DATA_RESPONSE]: deletePADataResponse,
  // [types.PA__DELETE_DATA_FAILURE]: deletePADataFailure,
});
