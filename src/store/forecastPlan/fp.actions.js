import { load } from 'redux-dataloader';
import types from './fp.actionTypes';

export function fetchFPDataRequest(token, company, division, year) {
 
  return load({
    type: types.FP__FETCH_DATA_REQUEST,
    token,
    company,
    division,
    year,
  });
}

export function fetchFPDataFailure(error) {
  return {
    type: types.FP__FETCH_DATA_FAILURE,
    error,
  };
}

export function receiveFPData(data) {
  return {
    type: types.FP__FETCH_DATA_RESPONSE,
    data,
  };
}






export function createFPDataRequest(token, fpObject) {
 
  return load({
    type: types.FP__CREATE_DATA_REQUEST,
    token,
    fpObject,
  });
}

export function createFPDataFailure(error) {
  return {
    type: types.FP__CREATE_DATA_FAILURE,
    error,
  };
}

export function createFPDataResponse(data) {
  return {
    type: types.FP__CREATE_DATA_RESPONSE,
    data,
  };
}




export function updateFPDataRequest(token, fpObject) {
 
  return load({
    type: types.FP__UPDATE_DATA_REQUEST,
    token,
    fpObject,
  });
}

export function updateFPDataFailure(error) {
  return {
    type: types.FP__UPDATE_DATA_FAILURE,
    error,
  };
}

export function updateFPDataResponse(data) {
  
  return {
    type: types.FP__UPDATE_DATA_RESPONSE,
    data,
  };
}

// export function deletePADataRequest(token, id) {
//   return load({
//     type: types.PA__DELETE_DATA_REQUEST,
//     token,
//     id,
//   });
// }

// export function deletePADataResponse(data) {
  
//   return {
//     type: types.PA__DELETE_DATA_RESPONSE,
//     data,
//   };
// }

// export function deletePADataFailure(error) {
//   return {
//     type: types.PA__DELETE_DATA_FAILURE,
//     error,
//   };
// }
