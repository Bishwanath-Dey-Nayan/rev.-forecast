/* eslint-disable css-modules/no-unused-class */

/* eslint-disable   */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import s from './GridRowsColumn2.module.css';
import { currencyFormat, getTotal } from '../../utils/mathAndCurrency';
import {
  headerDesc,
  rowDesc,
  reportModeLists,
} from '../../constants/GfrReport.constant';
import GridEditableCell from '../GridCells/GridEditableCell';

class GridRowsColumn2 extends Component { 
  static propTypes = {
    row1Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row2Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row3Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row4Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row6Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row7Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row1Col2Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row2Col2Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row3Col2Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row4Col2Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row6Col2Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row7Col2Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row6Col3Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row7Col3Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    token: PropTypes.string,
    greyOut: PropTypes.bool,
  };

  static defaultProps = {
    row1Data: {},
    row2Data: {},
    row3Data: {},
    row4Data: {},
    row6Data: {},
    row7Data: {},
    row1Col2Data: {},
    row2Col2Data: {},
    row3Col2Data: {},
    row4Col2Data: {},
    row6Col2Data: {},
    row7Col2Data: {},
    row6col3Data: {},
    row7Col3Data: {},
    token: null,
    greyOut: false,
    isForecastPlanData: false
  };

  constructor(props) {
    super(props);
    const { row1Data, row2Data, row3Data, row4Data, row6Data, row7Data } = this.props;
    const {
      row1Col2Data,
      row2Col2Data,
      row3Col2Data,
      row4Col2Data,
      row6Col2Data,
      row7Col2Data,
    } = this.props;
    const {
      row6Col3Data,
      row7Col3Data,
    } = this.props;

    this.state = {
      row1Clicked: false,
      row2Clicked: false,
      row3Clicked: false,
      row4Clicked: false,
      row6Clicked: false,
      row7Clicked: false,
      row1col2Clicked: false,
      row2col2Clicked: false,
      row3col2Clicked: false,
      row4col2Clicked: false,
      row6col2Clicked: false,
      row7col2Clicked: false,
      row1col3Clicked: false,
      row2col3Clicked: false,
      row3col3Clicked: false,
      row4col3Clicked: false,
      row6col3Clicked: false,
      row7col3Clicked: false,
      row1Data,
      row2Data,
      row3Data,
      row4Data,
      row6Data,
      row7Data,
      row1Col2Data,
      row2Col2Data,
      row3Col2Data,
      row4Col2Data,
      row6Col3Data,
      row6Col2Data,
      row7Col2Data,
      row1Col3Data: getTotal(
        row1Data.growthYTD,
        row1Col2Data.growthBOY,
        '',
        '',
      ).toString(),
      row2Col3Data: getTotal(
        row2Data.growthYTD,
        row2Col2Data.growthBOY,
        '',
        '',
      ).toString(),
      row3Col3Data: getTotal(
        row3Data.growthYTD,
        row3Col2Data.growthBOY,
        '',
        '',
      ).toString(),
      row4Col3Data: getTotal(
        row4Data.growthYTD,
        row4Col2Data.growthBOY,
        '',
        '',
      ).toString(),
      row7Col3Data

    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    //

    //test
    if (nextProps !== prevState) {
      //nextProps.netIncomeTotal !== prevState.netIncomeTotal
      return {
        row1Data: nextProps.row1Data,
        row2Data: nextProps.row2Data,
        row3Data: nextProps.row3Data,
        row4Data: nextProps.row4Data,
        row1Col2Data: nextProps.row1Col2Data,
        row2Col2Data: nextProps.row2Col2Data,
        row3Col2Data: nextProps.row3Col2Data,
        row4Col2Data: nextProps.row4Col2Data,
        row1Col3Data: nextProps.row1Col3Data,
        row2Col3Data: nextProps.row2Col3Data,
        row3Col3Data: nextProps.row3Col3Data,
        row4Col3Data: nextProps.row4Col3Data,
        row6Col3Data: nextProps.row6Col3Data
      };
    } else return null;
  }
  onChange = e => {
    const {
      row1Data,
      row2Data,
      row3Data,
      row4Data,
      row1Col2Data,
      row2Col2Data,
      row3Col2Data,
      row4Col2Data,
      row1Col3Data,
      row2Col3Data,
      row3Col3Data,
      row4Col3Data,
      row6Col3Data,
      row7Col3Data,
    } = this.state;
    var row0 = {}; // eslint-disable-line
    switch (e.target.name) {
      case 'row1Data':
        row0 = row1Data;
        row0.growthYTD = e.target.value;
        this.setState({
          row1Data: row0,
          row1Col3Data: getTotal(
            row0.growthYTD,
            row1Col2Data.growthBOY,
            '',
            '',
          ).toString(),
        });
        break;
      case 'row2Data':
        row0 = row2Data;
        row0.growthYTD = e.target.value;
        this.setState({
          row2Data: row0,
          row2Col3Data: getTotal(
            row0.growthYTD,
            row2Col2Data.growthBOY,
            '',
            '',
          ).toString(),
        });
        break;
      case 'row3Data':
        row0 = row3Data;
        row0.growthYTD = e.target.value;
        this.setState({
          row3Data: row0,
          row3Col3Data: getTotal(
            row0.growthYTD,
            row3Col2Data.growthBOY,
            '',
            '',
          ).toString(),
        });
        break;
      case 'row4Data':
        row0 = row4Data;
        row0.growthYTD = e.target.value;
        this.setState({
          row4Data: row0,
          row4Col3Data: getTotal(
            row0.growthYTD,
            row4Col2Data.growthBOY,
            '',
            '',
          ).toString(),
        });
        break;
      case 'row6Data':
        this.setState({
          row6Data: e.target.value,
        });
        break;
      case 'row1Col2Data':
        row0 = row1Col2Data;
        row0.growthBOY = e.target.value;
        this.setState({
          row1Col2Data: row0,
          row1Col3Data: getTotal(
            row0.growthBOY,
            row1Data.growthYTD,
            '',
            '',
          ).toString(),
        });
        break;
      case 'row2Col2Data':
        row0 = row2Col2Data;
        row0.growthBOY = e.target.value;
        this.setState({
          row2Col2Data: row0,
          row2Col3Data: getTotal(
            row0.growthBOY,
            row2Data.growthYTD,
            '',
            '',
          ).toString(),
        });
        break;
      case 'row3Col2Data':
        row0 = row3Col2Data;
        row0.growthBOY = e.target.value;
        this.setState({
          row3Col2Data: row0,
          row3Col3Data: getTotal(
            row0.growthBOY,
            row3Data.growthYTD,
            '',
            '',
          ).toString(),
        });
        break;
      case 'row4Col2Data':
        row0 = row4Col2Data;
        row0.growthBOY = e.target.value;
        this.setState({
          row4Col2Data: row0,
          row3Col3Data: getTotal(
            row0.growthBOY,
            row3Data.growthYTD,
            '',
            '',
          ).toString(),
        });
        break;
      case 'row6Col2Data':
        this.setState({
          row6Col2Data: e.target.value,
        });
        break;
      case 'row1Col3Data':
        row0 = row1Col3Data;
        row0.growthTotal = e.target.value;
        this.setState({
          row1Col3Data: row0,
        });
        break;
      case 'row2Col3Data':
        row0 = row2Col3Data;
        row0.growthTotal = e.target.value;
        this.setState({
          row2Col3Data: row0,
        });
        break;
      case 'row3Col3Data':
        row0 = row3Col3Data;
        row0.growthTotal = e.target.value;
        this.setState({
          row3Col3Data: row0,
        });
        break;
      case 'row4Col3Data':
        row0 = row4Col3Data;
        row0.growthTotal = e.target.value;
        this.setState({
          row4Col3Data: row0,
        });
        break;
      case 'row6Col3Data':
        row0 = row6Col3Data;
        row0.growthTotal = e.target.value;
        this.setState({
          row6Col3Data: row0,
        });
        break;

      case 'row7Col3Data':
        row0 = row7Col3Data;
        row0.growthTotal = e.target.value;
        this.setState({
          row7Col3Data: row0,
        });
        break;
      default:
        row0 = {};
        break;
    }
  };

  setClicked = e => {
    this.setState({ [e.target.id]: true });
  };

  setClickedDflt = e => {
    const { token } = this.props;
    const {
      row1Data,
      row2Data,
      row3Data,
      row4Data,
      row1Col2Data,
      row2Col2Data,
      row3Col2Data,
      row4Col2Data,
      row1Col3Data,
      row2Col3Data,
      row3Col3Data,
      row4Col3Data,
      row6Col3Data,
      row7Col3Data,
    } = this.state;
    var row0 = {}; // eslint-disable-line
    switch (e.target.name) {
      case 'row1Data':
        row0 = row1Data;
        row0.growthYTD = e.target.value;
        if (!row0.rowDescription) row0.rowDescription = rowDesc.contrAwrdInPy;
        if (!row0.row) row0.row = 0;
        if (!row0.headerRow) row0.headerRow = 0;
        this.setState({
          row1Data: row0,
        });
        break;
      case 'row2Data':
        row0 = row2Data;
        row0.growthYTD = e.target.value;
        if (!row0.rowDescription) row0.rowDescription = rowDesc.contrAwrdYTD;
        if (!row0.row) row0.row = 1;
        if (!row0.headerRow) row0.headerRow = 1;
        this.setState({
          row2Data: row0,
        });
        break;
      case 'row3Data':
        row0 = row3Data;
        row0.growthYTD = e.target.value;
        this.setState({
          row3Data: row0,
        });
        break;
      case 'row4Data':
        row0 = row4Data;
        row0.growthYTD = e.target.value;
        this.setState({
          row4Data: row0,
        });
        break;
      case 'row6Data':
        this.setState({
          row6Data: e.target.value,
        });
        break;
      case 'row1Col2Data':
        row0 = row1Col2Data;
        row0.growthBOY = e.target.value;
        if (!row0.rowDescription) row0.rowDescription = rowDesc.contrAwrdInPy;
        if (!row0.row) row0.row = 0;
        if (!row0.headerRow) row0.headerRow = 0;
        this.setState({
          row1Col2Data: row0,
        });
        break;
      case 'row2Col2Data':
        row0 = row2Col2Data;
        row0.growthBOY = e.target.value;
        if (!row0.rowDescription) row0.rowDescription = rowDesc.contrAwrdYTD;
        if (!row0.row) row0.row = 1;
        if (!row0.headerRow) row0.headerRow = 1;
        this.setState({
          row2Col2Data: row0,
        });
        break;
      case 'row3Col2Data':
        row0 = row3Col2Data;
        row0.growthBOY = e.target.value;
        this.setState({
          row3Col2Data: row0,
        });
        break;
      case 'row4Col2Data':
        row0 = row4Col2Data;
        row0.growthBOY = e.target.value;
        if (!row0.rowDescription) row0.rowDescription = rowDesc.otherWork;
        if (!row0.row) row0.row = 3;
        if (!row0.headerRow) row0.headerRow = 3;
        this.setState({
          row4Col2Data: row0,
        });
        break;
      case 'row6Col2Data':
        this.setState({
          row6Col2Data: e.target.value,
        });
        break;
      case 'row1Col3Data':
        row0 = row1Col3Data;
        row0.growthTotal = e.target.value;
        this.setState({
          row1Col3Data: row0,
        });
        break;
      case 'row2Col3Data':
        row0 = row2Col3Data;
        row0.growthTotal = e.target.value;
        this.setState({
          row2Col3Data: row0,
        });
        break;
      case 'row3Col3Data':
        row0 = row3Col3Data;
        row0.growthTotal = e.target.value;
        this.setState({
          row3Col3Data: row0,
        });
        break;
      case 'row4Col3Data':
        row0 = row4Col3Data;
        row0.growthTotal = e.target.value;
        this.setState({
          row4Col3Data: row0,
        });
        break;
      case 'row6Col3Data':
        row0 = row6Col3Data;
        if (!row0.rowDescription) row0.rowDescription = rowDesc.plan;
        row0.growthTotal = e.target.value;
        this.setState({
          row6Col3Data: row0,
          isForecastPlanData: true
        });
        break;
      case 'row7Col3Data':
        row0 = row7Col3Data;
        if (!row0.rowDescription) row0.rowDescription = rowDesc.plan;
        row0.growthTotal = e.target.value;
        this.setState({
          row7Col3Data: row0,
          isForecastPlanData: true
        });
        break;
      default:
        row0 = {};
        break;
    }

    if (e.target.getAttribute("data-recordtype") == "forecastPlan") {
      this.props.updateforecastPlanCellData(row0);
    }
    else {
      this.props.updateGfrCellData(row0);
    }

    this.setState({ [e.target.id]: false });
    this.setState({ isForecastPlanData: false });
  };

  render() {
    const {
      row1Data,
      row1Clicked,
      row2Data,
      row2Clicked,
      row3Data,
      row3Clicked,
      row4Data,
      row4Clicked,
      row5Data,
      row5Clicked,
      row6Data,
      row6Clicked,
      row7Data,
      row7Clicked,
    } = this.state;
    const {
      row1Col2Data,
      row1col2Clicked,
      row2Col2Data,
      row2col2Clicked,
      row3Col2Data,
      row3col2Clicked,
      row4Col2Data,
      row4col2Clicked,
      row5Col2Data,
      row5col2Clicked,
      row6Col2Data,
      row6col2Clicked,
      row7Col2Data,
      row7col2Clicked,
      row6Col3Data,
      row6col3Clicked,
      row7Col3Data,
      row7col3Clicked,

    } = this.state;

    const row1Col3Data = getTotal(
      row1Data.growthYTD,
      row1Col2Data.growthBOY,
      '',
      '',
    );
    const row2Col3Data = getTotal(
      row2Data.growthYTD,
      row2Col2Data.growthBOY,
      '',
      '',
    );
    const row3Col3Data = getTotal(row3Data.growthYTD, row3Col2Data, '', '');
    const row4Col3Data = getTotal(
      row4Data.growthYTD,
      row4Col2Data.growthBOY,
      '',
      '',
    );
    //const       row5Col3Data=getTotal(row5Data.growthYTD,row5Col2Data.growthBOY,'','');
    // const row6Col3Data = getTotal(
    //   row6Data.growthYTD,
    //   row6Col2Data.growthBOY,
    //   '',
    //   '',
    // );

    const { greyOut } = this.props;
    return (
      <div className={s.mainContainer}>
        <GridEditableCell
          cellId="row1Clicked"
          name="row1Data"
          isClicked={row1Clicked}
          cssClass={s.row1Data}
          onBlurCallback={this.setClickedDflt}
          cellValue={row1Data.growthYTD}
          onChangeCallback={this.onChange}
          onClickCallBack={this.setClicked}
          onKeyDownCallback={this.setClicked}
          isEdtiable={(this.props.reportMode == reportModeLists.bothSelected) && this.props.ag_row1DataEditAble}
        />

        <GridEditableCell
          cellId="row2Clicked"
          name="row2Data"
          isClicked={row2Clicked}
          cssClass={s.row2Data}
          onBlurCallback={this.setClickedDflt}
          cellValue={row2Data.growthYTD}
          onChangeCallback={this.onChange}
          onClickCallBack={this.setClicked}
          onKeyDownCallback={this.setClicked}
          isEdtiable={(this.props.reportMode == reportModeLists.bothSelected) && this.props.isrow2DataEditable}
        />

        <div className={s.row3Data}>
          {greyOut ? (
            <div className={s.greyBackground} />
          ) : (
              <GridEditableCell
                cellId="row3Clicked"
                name="row3Data"
                isClicked={row3Clicked}
                cssClass=""
                onBlurCallback={this.setClickedDflt}
                cellValue={row3Data.growthYTD}
                onChangeCallback={this.onChange}
                onClickCallBack={this.setClicked}
                onKeyDownCallback={this.setClicked}
                isEdtiable={(this.props.reportMode == reportModeLists.bothSelected) && this.props.isRow3Editable}
              />
            )}
        </div>
        <div className={s.row4Data}>
          {greyOut ? (
            <div className={s.greyBackground} />
          ) : (
              <GridEditableCell
                cellId="row4Clicked"
                name="row4Data"
                isClicked={row4Clicked}
                cssClass=""
                onBlurCallback={this.setClickedDflt}
                cellValue={row4Data.growthYTD}
                onChangeCallback={this.onChange}
                onClickCallBack={this.setClicked}
                onKeyDownCallback={this.setClicked}
                isEdtiable={(this.props.reportMode == reportModeLists.bothSelected)}
              />
            )}
        </div>
        <div className={s.row5Data}>
          <div id="row5Clicked">
            {currencyFormat(
              getTotal(
                row1Data.growthYTD,
                row2Data.growthYTD,
                row3Data.growthYTD,
                row4Data.growthYTD,
              ).toString(),
            )}
          </div>
        </div>
        <GridEditableCell
          cellId="row6Clicked"
          name="row6Data"
          isClicked={row6Clicked}
          cssClass={s.row6Data}
          onBlurCallback={this.setClickedDflt}
          cellValue={this.props.row6Data.growthYTD}
          onChangeCallback={this.onChange}
          onClickCallBack={this.setClicked}
          onKeyDownCallback={this.setClicked}
          isEdtiable={false}
        />
        <GridEditableCell
          cellId="row7Clicked"
          name="row7Data"
          isClicked={row7Clicked}
          cssClass={s.row7Data}
          onBlurCallback={this.setClickedDflt}
          cellValue={row7Data.growthYTD}
          onChangeCallback={this.onChange}
          onClickCallBack={this.setClicked}
          onKeyDownCallback={this.setClicked}
          isEdtiable={false}
        />

        <GridEditableCell
          cellId="row1col2Clicked"
          name="row1Col2Data"
          isClicked={row1col2Clicked}
          cssClass={s.yellowBackground}
          onBlurCallback={this.setClickedDflt}
          cellValue={row1Col2Data.growthBOY}
          onChangeCallback={this.onChange}
          onClickCallBack={this.setClicked}
          onKeyDownCallback={this.setClicked}
          isEdtiable={(this.props.reportMode == reportModeLists.bothSelected)}
        />

        <GridEditableCell
          cellId="row2col2Clicked"
          name="row2Col2Data"
          isClicked={row2col2Clicked}
          cssClass={s.yellowBackground}
          onBlurCallback={this.setClickedDflt}
          cellValue={row2Col2Data.growthBOY}
          onChangeCallback={this.onChange}
          onClickCallBack={this.setClicked}
          onKeyDownCallback={this.setClicked}
          isEdtiable={(this.props.reportMode == reportModeLists.bothSelected)}
        />

        <GridEditableCell
          cellId="row3col2Clicked"
          name="row3Col2Data"
          isClicked={row3col2Clicked}
          cssClass={s.yellowBackground}
          onBlurCallback={this.setClickedDflt}
          cellValue={row3Col2Data}
          onChangeCallback={this.onChange}
          onClickCallBack={this.setClicked}
          onKeyDownCallback={this.setClicked}
          isEdtiable={(this.props.reportMode == reportModeLists.bothSelected) && this.props.isRow3Col2Editable}
        />

        <GridEditableCell
          cellId="row4col2Clicked"
          name="row4Col2Data"
          isClicked={row4col2Clicked}
          cssClass={s.yellowBackgroundTotal}
          onBlurCallback={this.setClickedDflt}
          cellValue={row4Col2Data.growthBOY}
          onChangeCallback={this.onChange}
          onClickCallBack={this.setClicked}
          onKeyDownCallback={this.setClicked}
          isEdtiable={(this.props.reportMode == reportModeLists.bothSelected)}
        />

        <div className={s.row5col2Data}>
          <div id="row5col2Clicked">
            {currencyFormat(
              getTotal(
                row1Col2Data.growthBOY,
                row2Col2Data.growthBOY,
                row3Col2Data,
                row4Col2Data.growthBOY,
              ),
            )}
          </div>
        </div>

        <GridEditableCell
          cellId="row6col2Clicked"
          name="row6col2Data"
          isClicked={row6col2Clicked}
          cssClass={s.row6col2Data}
          onBlurCallback={this.setClickedDflt}
          cellValue={this.props.row6Col2Data.growthBOY}
          onChangeCallback={this.onChange}
          onClickCallBack={this.setClicked}
          onKeyDownCallback={this.setClicked}
          isEdtiable={false}
        />

        <GridEditableCell
          cellId="row7col2Clicked"
          name="row7col2Data"
          isClicked={row7col2Clicked}
          cssClass={s.row7col2Data}
          onBlurCallback={this.setClickedDflt}
          cellValue={row7Col2Data.growthBOY}
          onChangeCallback={this.onChange}
          onClickCallBack={this.setClicked}
          onKeyDownCallback={this.setClicked}
          isEdtiable={false}
        />

        <div className={s.row1col3Data}>
          <div id="row1col3Clicked">{currencyFormat(row1Col3Data)}</div>
        </div>
        <div className={s.row2col3Data}>
          <div id="row2col3Clicked">{currencyFormat(row2Col3Data)}</div>
        </div>
        <div className={s.row3col3Data}>
          <div id="row3col3Clicked">{currencyFormat(row3Col3Data)} </div>
        </div>
        <div className={s.row4col3Data}>
          <div id="row4col3Clicked">{currencyFormat(row4Col3Data)}</div>
        </div>
        <div className={s.row5col3Data}>
          <div id="row5col3Clicked">
            {currencyFormat(
              getTotal(row1Col3Data, row2Col3Data, row3Col3Data, row4Col3Data),
            )}
          </div>
        </div>
        {/* <div className={s.row6col3Data}>
          <div id="row6col3Clicked">
            {currencyFormat(
              getTotal(row6Data, row6Col2Data, '', '').toString(),
            )}
          </div>
        </div> */}
        <GridEditableCell
          cellId="row6col3Clicked"
          name="row6Col3Data"
          isClicked={row6col3Clicked}
          cssClass={s.row6col3Data}
          onBlurCallback={this.setClickedDflt}
          cellValue={row6Col3Data.growthTotal}
          onChangeCallback={this.onChange}
          onClickCallBack={this.setClicked}
          onKeyDownCallback={this.setClicked}
          recordType="forecastPlan"
          isEdtiable={(this.props.reportMode == reportModeLists.bothSelected) && this.props.isrow6Col3Data}
        />
        <GridEditableCell
          cellId="row7col3Clicked"
          name="row7Col3Data"
          isClicked={row7col3Clicked}
          cssClass={s.row7col3Data}
          onBlurCallback={this.setClickedDflt}
          cellValue={row7Col3Data.growthTotal}
          onChangeCallback={this.onChange}
          onClickCallBack={this.setClicked}
          onKeyDownCallback={this.setClicked}
          recordType="forecastPlan"
          isEdtiable={(this.props.reportMode == reportModeLists.bothSelected)}
        />
      </div>
    );
  }
}

export default GridRowsColumn2;
