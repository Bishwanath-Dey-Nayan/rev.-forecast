/* eslint-disable css-modules/no-unused-class */
/* eslint-disable */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import s from './GridRowsColumn3.module.css';
import { currencyFormat, getTotal } from '../../utils/mathAndCurrency';
import GridEditableCell from '../GridCells/GridEditableCell';
import { headerDesc, rowDesc, reportModeLists } from '../../constants/GfrReport.constant';

class GridRowsColumn3 extends Component {
  static propTypes = {
    row1Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row2Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row3Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row4Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row6Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row7ata: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row1Col2Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row2Col2Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row3Col2Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row4Col2Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row6Col2Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row7Col2Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row6Col3Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row7Col3Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row4Col3Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    token: PropTypes.string,
    fourRowGroup: PropTypes.bool,
    twoRowGroup: PropTypes.bool,
    greyOut: PropTypes.bool,
    yellowBack: PropTypes.bool,
    fourRowYellow: PropTypes.bool,
    netIncome: PropTypes.bool,
    setNetIncome: PropTypes.func,
    netIncomeTotal: PropTypes.string,
  };

  static defaultProps = {
    row1Data: {},
    row2Data: {},
    row3Data: {},
    row4Data: {},
    row6Data: {},
    row7Data: {},
    row1Col2Data: {},
    row2Col2Data: {},
    row3Col2Data: {},
    row4Col2Data: {},
    row6Col2Data: {},
    row7Col2Data: {},
    row6Col3Data: {},
    row2Col3Data: {},
    row4Col3Data: {},
    row7Col3Data: {},
    fourRowGroup: false,
    twoRowGroup: false,
    token: null,
    greyOut: false,
    yellowBack: false,
    fourRowYellow: false,
    netIncome: false,
    setNetIncome: () => { },
    netIncomeTotal: '',
  };

  constructor(props) {
    super(props);
    const { row1Data, row2Data, row3Data, row4Data, row6Data, row7Data } = this.props;
    const {
      row1Col2Data,
      row2Col2Data,
      row3Col2Data,
      row4Col2Data,
      row6Col2Data,
      row7Col2Data,
      twoRowGroup,
      netIncomeTotal,
      netIncome,
      fourRowGroup
    } = this.props;
    const {
      row6Col3Data,
      row2Col3Data,
      row4Col3Data,
      row7Col3Data,
    } = this.props;

    this.state = {
      row1Clicked: false,
      row2Clicked: false,
      row3Clicked: false,
      row4Clicked: false,
      row6Clicked: false,
      row7Clicked: false,
      row1col2Clicked: false,
      row2col2Clicked: false,
      row3col2Clicked: false,
      row4col2Clicked: false,
      row6col2Clicked: false,
      row7col2Clicked: false,
      row1col3Clicked: false,
      row2col3Clicked: false,
      row3col3Clicked: false,
      row4col3Clicked: false,
      row6col3Clicked: false,
      row7col3Clicked: false,
      row1Data,
      row2Data,
      row3Data,
      row4Data,
      row6Data,
      row7Data,
      row1Col2Data,
      row2Col2Data,
      row3Col2Data,
      row4Col2Data,
      row6Col2Data,
      row7Col2Data,
      row6Col3Data,
      row1Col3Data: netIncome
        ? getTotal(row1Data, netIncomeTotal, '', '')
        : twoRowGroup
          ? getTotal(row1Data, row1Col2Data, '', '').toString()
          : getTotal(
            row1Data.contractYTD,
            row1Col2Data.contractBOY,
            '',
            '',
          ).toString(),
      row2Col3Data: twoRowGroup
        ? row2Col3Data
        : getTotal(
          row2Data.contractYTD,
          row2Col2Data.contractBOY,
          '',
          '',
        ).toString(),
      row3Col3Data: getTotal(
        row3Data.contractYTD,
        row3Col2Data.contractBOY,
        '',
        '',
      ).toString(),
      row4Col3Data: !twoRowGroup ? row4Col3Data : getTotal(
        row4Data.contractYTD,
        row4Col2Data.contractBOY,
        '',
        '',
      ).toString(),
      netIncomeTotal,
    };
  }

  componentDidMount() {
    const { netIncomeTotal } = this.props;

    this.setState({ netIncomeTotal });
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.netIncomeTotal !== this.state.netIncomeTotal) {
      this.setState({ netIncomeTotal: this.props.netIncomeTotal });
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps !== prevState) {
      return {
        netIncomeTotal: nextProps.netIncomeTotal,
        row1Data: nextProps.row1Data,
        row2Data: nextProps.row2Data,
        row3Data: nextProps.row3Data,
        row4Data: nextProps.row4Data,
        row5Data: nextProps.row5Data,
        row6Data: nextProps.row6Data,
        row7Data: nextProps.row7Data,

        row1Col2Data: nextProps.row1Col2Data,
        row2Col2Data: nextProps.row2Col2Data,
        row3Col2Data: nextProps.row3Col2Data,
        row4Col2Data: nextProps.row4Col2Data,
        row5Col2Data: nextProps.row5Col2Data,
        row6Col2Data: nextProps.row6Col2Data,
        row7Col2Data: nextProps.row7Col2Data,

        row1Col3Data: nextProps.row1Col3Data,
        row2Col3Data: nextProps.row2Col3Data,
        row3Col3Data: nextProps.row3Col3Data,
        row4Col3Data: nextProps.row4Col3Data,
        row5Col3Data: nextProps.row5Col3Data,
        row6Col3Data: nextProps.row6Col3Data,
        row7Col3Data: nextProps.row7Col3Data,
      };
    } else return null;
  }

  onChange = e => {
    const {
      row1Data,
      row2Data,
      row3Data,
      row4Data,
      row6Data,
      row7Data,
      row1Col2Data,
      row2Col2Data,
      row3Col2Data,
      row4Col2Data,
      row6Col2Data,
      row7Col2Data,
      row1Col3Data,
      row2Col3Data,
      row3Col3Data,
      row4Col3Data,
      row6Col3Data,
      row7Col3Data,
    } = this.state;
    const { fourRowGroup, twoRowGroup, setNetIncome } = this.props;
    var row0 = {}; // eslint-disable-line
    switch (e.target.name) {
      case 'row1Data':
        row0 = row1Data;
        row0.contractYTD = e.target.value;
        this.setState({
          row1Data: row0,
          row1Col3Data: getTotal(
            row0.contractYTD,
            row1Col2Data.contractBOY,
            '',
            '',
          ).toString(),
        });
        setNetIncome();
        break;
      case 'row2Data':
        if (fourRowGroup && twoRowGroup) {
          this.setState({
            row2Data: e.target.value,
          });
        } else {
          row0 = row2Data;
          row0.contractYTD = e.target.value;
          this.setState({
            row2Data: row0,
            row2Col3Data: getTotal(
              row0.contractYTD,
              row2Col2Data.contractBOY,
              '',
              '',
            ).toString(),
          });
        }
        setNetIncome();
        break;
      case 'row3Data':
        row0 = row3Data;
        row0.contractYTD = e.target.value;
        this.setState({
          row3Data: row0,
          row3Col3Data: getTotal(
            row0.contractYTD,
            row3Col2Data.contractBOY,
            '',
            '',
          ).toString(),
        });
        setNetIncome();
        break;
      case 'row4Data':
        if (fourRowGroup) {
          this.setState({
            row4Data: e.target.value,
          });
        } else {
          row0 = row4Data;
          row0.contractYTD = e.target.value;
          this.setState({
            row4Data: row0,
            row4Col3Data: getTotal(
              row0.contractYTD,
              row4Col2Data.contractBOY,
              '',
              '',
            ).toString(),
          });
        }
        setNetIncome();
        break;
      case 'row6Data':
        this.setState({
          row6Data: e.target.value,
        });
        break;
      case 'row7Data':
        this.setState({
          row6Data: e.target.value,
        });
        break;
      case 'row1Col2Data'://row1col2Data
        row0 = row1Col2Data;
        row0.contractBOY = e.target.value;
        this.setState({
          row1Col2Data: row0,
          row1Col3Data: getTotal(
            row0.contractBOY,
            row1Data.contractYTD,
            '',
            '',
          ).toString(),
        });
        setNetIncome();

        break;
      case 'row2Col2Data':
        if (fourRowGroup && twoRowGroup) {
          this.setState({
            row2Col2Data: e.target.value,
          });
        } else {
          row0 = row2Col2Data;
          row0.contractBOY = e.target.value;
          this.setState({
            row2Col2Data: row0,
            row2Col3Data: getTotal(
              row0.contractBOY,
              row2Data.contractYTD,
              '',
              '',
            ).toString(),
          });
        }
        setNetIncome();
        break;
      case 'row3Col2Data':
        row0 = row3Col2Data;
        row0.contractBOY = e.target.value;
        this.setState({
          row3Col2Data: row0,
          row3Col3Data: getTotal(
            row0.contractBOY,
            row3Data.contractYTD,
            '',
            '',
          ).toString(),
        });
        setNetIncome();
        break;
      case 'row4Col2Data':
        if (fourRowGroup) {
          this.setState({
            row4Col2Data: e.target.value,
          });
        } else {
          row0 = row4Col2Data;
          row0.contractBOY = e.target.value;
          this.setState({
            row4Col2Data: row0,
            row4Col3Data: getTotal(
              row0.contractBOY,
              row4Data.contractYTD,
              '',
              '',
            ).toString(),
          });
        }
        setNetIncome();
        break;
      case 'row6Col2Data':
        this.setState({
          row6Col2Data: e.target.value,
        });
        break;
      case 'row1Col3Data':
        this.setState({
          row1Col3Data: e.target.value,
        });
        break;
      case 'row2Col3Data':
        row0 = row2Col3Data;
        row0.contractTotal = e.target.value;
        this.setState({
          row2Col3Data: row0,
        });
        break;
      case 'row3Col3Data':
        this.setState({
          row3Col3Data: e.target.value,
        });
        break;
      case 'row4Col3Data':
        row0 = row4Col3Data;
        row0.contractTotal = e.target.value;
        this.setState({
          row4Col3Data: row0,
        });
        break;
      case 'row6Col3Data':
        row0 = row6Col3Data;
        row0.contractTotal = e.target.value;
        this.setState({
          row6Col3Data: row0,
        });
        break;
      case 'row6Col3Data':
        row0 = row7Col3Data;
        row0.contractTotal = e.target.value;
        this.setState({
          row7Col3Data: row0,
        });
        break;
      default:
        row0 = {};
        break;
    }
  };

  setClicked = e => {
    console.info('e.target.id', e.target.id);//eslint-disable-line
    this.setState({ [e.target.id]: true });
  };

  setClickedDflt = e => {
    const { token, fourRowGroup, twoRowGroup, setNetIncome } = this.props;
    const {
      row1Data,
      row2Data,
      row3Data,
      row4Data,
      row1Col2Data,
      row2Col2Data,
      row3Col2Data,
      row4Col2Data,
      row1Col3Data,
      row2Col3Data,
      row3Col3Data,
      row4Col3Data,
      row6Col3Data,
      row7Col3Data,
    } = this.state;
    var row0 = {}; // eslint-disable-line
    switch (e.target.name) {
      case 'row1Data':
        row0 = row1Data;
        row0.contractYTD = e.target.value;
        this.setState({
          row1Data: row0,
        });

        setNetIncome();
        break;
      case 'row2Data':
        if (twoRowGroup && fourRowGroup) {
          this.setState({
            row2Data: e.target.value,
          });
        } else {
          row0 = row2Data;
          row0.contractYTD = e.target.value;
          this.setState({
            row2Data: row0,
          });
        }
        break;
      case 'row3Data':
        row0 = row3Data;
        row0.contractYTD = e.target.value;
        this.setState({
          row3Data: row0,
        });
        break;
      case 'row4Data':
        if (fourRowGroup) {
          this.setState({
            row4Data: e.target.value,
          });
        } else {
          row0 = row4Data;
          row0.contractYTD = e.target.value;
          this.setState({
            row4Data: row0,
          });
        }
        break;
      case 'row6Data':
        this.setState({
          row6Data: e.target.value,
        });
        break;

      case 'row7Data':
        this.setState({
          row7Data: e.target.value,
        });
        break;
      case 'row1Col2Data':
        row0 = row1Col2Data;
        row0.contractBOY = e.target.value;
        if (!row0.rowDescription) {
          if (
            this.props.headerDescProp === headerDesc.rev ||
            this.props.headerDescProp === headerDesc.cto
          ) {
            row0.rowDescription = rowDesc.contrAwrdInPy;
          }
          else if (this.props.headerDescProp === headerDesc.adjToGrossProf) {
            row0.rowDescription = rowDesc.retro;
          }
          else if (this.props.headerDescProp === headerDesc.adjGrossProf
            || this.props.headerDescProp === headerDesc.gAndA
            || this.props.headerDescProp === headerDesc.otherExpense) {
            row0.rowDescription = rowDesc.total;
          }
        }
        if (!row0.row) row0.row = 0;
        if (!row0.headerRow) row0.headerRow = 0;
        this.setState({
          row1Col2Data: row0,
        });
        setNetIncome();
        break;
      case 'row2Col2Data':
        if (twoRowGroup && fourRowGroup) {
          this.setState({
            row2Col2Data: e.target.value,
          });
        } else {
          row0 = row2Col2Data;
          row0.contractBOY = e.target.value;
          if (!row0.rowDescription) {
            if (
              this.props.headerDescProp === headerDesc.rev ||
              this.props.headerDescProp === headerDesc.cto
            )
              row0.rowDescription = rowDesc.contrAwrdYTD;
            else if (this.props.headerDescProp === headerDesc.adjToGrossProf)
              row0.rowDescription = rowDesc.yardShop;
          }
          if (!row0.row) row0.row = 1;
          if (!row0.headerRow) row0.headerRow = 1;
          this.setState({
            row2Col2Data: row0,
          });
        }
        break;
      case 'row3Col2Data':
        row0 = row3Col2Data;
        row0.contractBOY = e.target.value;
        if (!row0.rowDescription) {
          if (
            this.props.headerDescProp === headerDesc.rev ||
            this.props.headerDescProp === headerDesc.cto
          )
            row0.rowDescription = rowDesc.pendingAwards;
        }
        if (!row0.row) row0.row = 2;
        if (!row0.headerRow) row0.headerRow = 2;
        this.setState({
          row3Col2Data: row0,
        });
        break;
      case 'row4Col2Data':
        if (fourRowGroup) {
          this.setState({
            row4Col2Data: e.target.value,
          });
        } else {
          row0 = row4Col2Data;
          row0.contractBOY = e.target.value;
          if (!row0.rowDescription) {
            if (
              this.props.headerDescProp === headerDesc.rev ||
              this.props.headerDescProp === headerDesc.cto
            )
              row0.rowDescription = rowDesc.otherWork;
          }
          if (!row0.row) row0.row = 3;
          if (!row0.headerRow) row0.headerRow = 3;
          this.setState({
            row4Col2Data: row0,
          });
        }
        break;
      case 'row6Col2Data':
        this.setState({
          row6Col2Data: e.target.value,
        });
        break;

      case 'row7Col2Data':
        this.setState({
          row7Col2Data: e.target.value,
        });
        break;
      case 'row2Col3Data':
        row0 = row2Col3Data;
        if (!row0.rowDescription) row0.rowDescription = rowDesc.plan;
        row0.contractTotal = e.target.value;
        this.setState({
          row2Col3Data: row0,
        });
        break;
      case 'row2Col3Data':
        row0 = row3Col3Data;
        if (!row0.rowDescription) row0.rowDescription = rowDesc.plan;
        row0.contractTotal = e.target.value;
        this.setState({
          row3Col3Data: row0,
        });
        break;
      case 'row6Col3Data':
        row0 = row6Col3Data;
        if (!row0.rowDescription) row0.rowDescription = rowDesc.plan;
        row0.contractTotal = e.target.value;
        this.setState({
          row6Col3Data: row0,
        });
        break;
      case 'row4Col3Data':
        row0 = row4Col3Data;
        if (!row0.rowDescription) row0.rowDescription = rowDesc.plan;
        row0.contractTotal = e.target.value;
        this.setState({
          row4Col3Data: row0,
        });
        break;

      case 'row7Col3Data':
        row0 = row6Col3Data;
        if (!row0.rowDescription) row0.rowDescription = rowDesc.plan;
        row0.contractTotal = e.target.value;
        this.setState({
          row7Col3Data: row0,
        });
        break;
    }
    if (e.target.getAttribute("data-recordtype") == "forecastPlan") {
      this.props.updateforecastPlanCellData(row0);
    }
    else {
      this.props.updateGfrCellData(row0);
    }
    this.setState({ [e.target.id]: false });
  };

  render() {
    const {
      row1Data,
      row1Clicked,
      row2Data,
      row2Clicked,
      row3Data,
      row3Clicked,
      row4Data,
      row4Clicked,
      row6Data,
      row6Clicked,
      row7Data,
      row7Clicked,
    } = this.state;
    const {
      row1Col2Data,
      row1col2Clicked,
      row2Col2Data,
      row2col2Clicked,
      row3Col2Data,
      row3col2Clicked,
      row4Col2Data,
      row4col2Clicked,
      row6Col2Data,
      row6col2Clicked,
      row7Col2Data,
      row7col2Clicked,
    } = this.state;
    const {
      row1Col3Data,
      row1col3Clicked,
      row2Col3Data,
      row2col3Clicked,
      row3Col3Data,
      row3col3Clicked,
      row4Col3Data,
      row4col3Clicked,
      row6Col3Data,
      row6col3Clicked,
      row7Col3Data,
      row7col3Clicked,
    } = this.state;
    const {
      fourRowGroup,
      twoRowGroup,
      greyOut,
      yellowBack,
      fourRowYellow,
      netIncome,
      netIncomeTotal,
      headerDescProp,
    } = this.props;

    var row5Data = getTotal(
      row1Data.contractYTD,
      row2Data.contractYTD,
      row3Data.contractYTD,
      row4Data.contractYTD,
    );
    var row5Col2Data = getTotal(
      row1Col2Data.contractBOY,
      row2Col2Data.contractBOY,
      row3Col2Data.contractBOY,
      row4Col2Data.contractBOY,
    );
    var row5Col3Data = getTotal(row5Data, row5Col2Data);

    return (
      <div className={s.mainContainer}>
        <div className={s.row1Data}>
          {fourRowGroup && twoRowGroup ? (
            // <div id="row1Clicked">
            //   {currencyFormat(
            //     fourRowGroup &&
            //       twoRowGroup &&
            //       (headerDescProp !== headerDesc.gAndA &&
            //         headerDescProp !== headerDesc.otherExpense)
            //       ? row1Data
            //       : row1Data.contractYTD,
            //   )}
            // </div>
            <GridEditableCell
              cellId="row1Clicked"
              name="row1Data"
              isClicked={row1Clicked}
              cssClass={s.row1Data}
              cellValue={(
                (fourRowGroup &&
                  twoRowGroup) &&
                  (headerDescProp !== headerDesc.gAndA &&
                    headerDescProp !== headerDesc.otherExpense
                    && headerDescProp != headerDesc.adjToGrossProf &&
                    headerDescProp != headerDesc.adjGrossProf &&
                    headerDescProp != headerDesc.otherExpense)
                  ? row1Data
                  : row1Data.contractYTD)
              }
              onBlurCallback={this.setClickedDflt}
              onChangeCallback={this.onChange}
              onClickCallBack={this.setClicked}
              onKeyDownCallback={this.setClicked}
              isEdtiable={(this.props.reportMode == reportModeLists.bothSelected) && this.props.isRow2Editable}
            />
          ) : (
              <div>
                {row1Clicked && this.props.isRow1Editable ? (
                  <input
                    autoFocus={true}
                    pattern="[0-9]*"
                    type="number"
                    onBlur={this.setClickedDflt}
                    value={
                      fourRowGroup && twoRowGroup
                        ? row1Data
                        : row1Data.contractYTD
                    }
                    onChange={this.onChange}
                    name="row1Data"
                    id="row1Clicked"
                  />
                ) : (
                    // <div
                    //   id="row1Clicked"
                    //   onClick={this.setClicked}
                    //   role="button"
                    //   onKeyDown={this.setClicked}
                    //   tabIndex="0"
                    // >
                    //   {currencyFormat(
                    //     fourRowGroup && twoRowGroup
                    //       ? row1Data
                    //       : row1Data.contractYTD,
                    //   )}
                    // </div>
                    <GridEditableCell
                      cellId="row1Clicked"
                      name="row1Data"
                      isClicked={row1Clicked}
                      cssClass={s.row1Data}
                      cellValue={(
                        fourRowGroup && twoRowGroup
                          ? row1Data
                          : row1Data.contractYTD
                      )
                      }
                      onChangeCallback={this.onChange}
                      onClickCallBack={this.setClicked}
                      onKeyDownCallback={this.setClicked}
                      isEdtiable={(this.props.reportMode == reportModeLists.bothSelected) && this.props.isRow1Editable}
                    />
                  )}
              </div>
            )}
        </div>
        <div
          className={
            fourRowGroup && !twoRowGroup ? s.row2Datatotal : s.row2Data
          }
        >
          {row2Clicked && this.props.isRow2Editable ? (
            <input
              autoFocus={true}
              pattern="[0-9]*"
              type="number"
              onBlur={this.setClickedDflt}
              value={
                twoRowGroup && fourRowGroup ? row2Data : row2Data.contractYTD
              }
              onChange={this.onChange}
              name="row2Data"
              id="row2Clicked"
            />
          ) : (
              // <div
              //   id="row2Clicked"
              //   onClick={this.setClicked}
              //   role="button"
              //   onKeyDown={this.setClicked}
              //   tabIndex="0"
              // >
              //   {currencyFormat(
              //     twoRowGroup && fourRowGroup ? row2Data : row2Data.contractYTD,
              //   )}
              // </div>
              <GridEditableCell
                cellId="row2Clicked"
                name="row2Data"
                isClicked={row2Clicked}
                cssClass={(fourRowGroup && !twoRowGroup ? s.row2Datatotal : s.row2Data)}
                cellValue={(
                  twoRowGroup && fourRowGroup ? row2Data.contractYTD : row2Data.contractYTD
                )
                }
                onChangeCallback={this.onChange}
                onClickCallBack={this.setClicked}
                onKeyDownCallback={this.setClicked}
                isEdtiable={(this.props.reportMode == reportModeLists.bothSelected) && this.props.isRow2Editable}
              />
            )}
        </div>
        <div className={twoRowGroup ? s.row3DataNB : s.row3Data}>
          {greyOut && !fourRowGroup ? (
            <div className={s.greyBackground} />
          ) : (
              <div>
                {fourRowGroup ? (
                  <div id="row3Clicked">
                    {currencyFormat(
                      fourRowGroup
                        ? getTotal(
                          row1Data.contractYTD,
                          row2Data.contractYTD,
                          '',
                          '',
                        ).toString()
                        : row3Data.contractYTD,
                    )}
                  </div>
                ) : (
                    <div>
                      {row3Clicked ? (
                        <input
                          autoFocus={true}
                          pattern="[0-9]*"
                          type="number"
                          onBlur={this.setClickedDflt}
                          value={
                            fourRowGroup
                              ? getTotal(
                                row1Data.contractYTD,
                                row2Data.contractYTD,
                                '',
                                '',
                              ).toString()
                              : row3Data.contractYTD
                          }
                          onChange={this.onChange}
                          name="row3Data"
                          id="row3Clicked"
                        />
                      ) : (
                          <GridEditableCell
                            cellId="row3Clicked"
                            name="row3Data"
                            isClicked={row3Clicked}
                            cssClass={(twoRowGroup ? s.row3DataNV : s.row3Data)}
                            cellValue={(
                              twoRowGroup?row3Data:
                              getTotal(
                                row1Data.contractYTD,
                                row2Data.contractYTD,
                                '',
                                '',
                              ).toString()
                             // fourRowGroup
                              //   ? getTotal(
                              //     row1Data.contractYTD,
                              //     row2Data.contractYTD,
                              //     '',
                              //     '',
                              //   ).toString()
                              //   : row3Data.contractYTD
                            )
                            }
                            onChangeCallback={this.onChange}
                            onClickCallBack={this.setClicked}
                            onKeyDownCallback={this.setClicked}
                            isEdtiable={(this.props.reportMode == reportModeLists.bothSelected)}
                          />
                        )}
                    </div>
                  )}
              </div>
            )}
        </div>
        <div
          className={
            twoRowGroup
              ? s.row4DataNV
              : fourRowGroup && !twoRowGroup
                ? s.row4DataNB
                : s.row4Data
          }
        >
          {greyOut && !fourRowGroup ? (
            <div className={s.greyBackground} />
          ) : (
              <div>
                {row4Clicked ? (
                  <input
                    autoFocus={true}
                    pattern="[0-9]*"
                    type="number"
                    onBlur={this.setClickedDflt}
                    value={fourRowGroup ? row4Data : row4Data.contractYTD}
                    onChange={this.onChange}
                    name="row4Data"
                    id="row4Clicked"
                  />
                ) : (
                    // <div
                    //   id="row4Clicked"
                    //   onClick={this.setClicked}
                    //   role="button"
                    //   onKeyDown={this.setClicked}
                    //   tabIndex="0"
                    // >
                    //   {currencyFormat(
                    //     fourRowGroup ? row4Data : row4Data.contractYTD,
                    //   )}
                    // </div>
                    <GridEditableCell
                      cellId="row4Clicked"
                      name="row4Data"
                      isClicked={row4Clicked}
                      cssClass={(twoRowGroup
                        ? s.row4DataNV
                        : fourRowGroup && !twoRowGroup
                          ? s.row4DataNB
                          : s.row4Data)}
                      cellValue={(
                        fourRowGroup ? row4Data.contractYTD : row4Data.contractYTD
                      )
                      }
                      onChangeCallback={this.onChange}
                      onClickCallBack={this.setClicked}
                      onKeyDownCallback={this.setClicked}
                      isEdtiable={false}
                    />
                  )}
              </div>
            )}
        </div>
        <div className={fourRowGroup ? s.row5DataNV : s.row5Data}>
          <div id="row5Clicked">{currencyFormat(row5Data)}</div>
        </div>
        <div className={fourRowGroup ? s.row6DataNV : s.row6Data}>
          {row6Clicked ? (
            <input
              autoFocus={true}
              pattern="[0-9]*"
              type="number"
              onBlur={this.setClickedDflt}
              value={row6Data}
              onChange={this.onChange}
              name="row6Data"
              id="row6Clicked"
            />
          ) : (
              // <div
              //   id="row6Clicked"
              //   onClick={this.setClicked}
              //   role="button"
              //   onKeyDown={this.setClicked}
              //   tabIndex="0"
              // >
              //   {currencyFormat(row6Data)}
              // </div>
              <GridEditableCell
                cellId="row6Clicked"
                name="row6Data"
                isClicked={row6Clicked}
                cssClass={(fourRowGroup ? s.row6DataNV : s.row6Data)}
                cellValue={row6Data.contractYTD}
                onChangeCallback={this.onChange}
                onClickCallBack={this.setClicked}
                onKeyDownCallback={this.setClicked}
                isEdtiable={false}
              />

            )}
        </div>

        <GridEditableCell
          cellId="row7Clicked"
          name="row7Data"
          isClicked={row7Clicked}
          cssClass={(fourRowGroup ? s.row7DataNV : s.row7Data)}
          cellValue={row7Data}
          onChangeCallback={this.onChange}
          onClickCallBack={this.setClicked}
          onKeyDownCallback={this.setClicked}
          isEdtiable={false}
        />

        <div
          className={
            yellowBack && !fourRowYellow ? s.yellowBackground : s.row1col2Data
          }
        >
          {((yellowBack && twoRowGroup && fourRowGroup) || (yellowBack && greyOut) || fourRowGroup || (fourRowGroup && twoRowGroup)) && (
            <GridEditableCell
              cellId="row1col2Clicked"
              name="row1Col2Data"
              isClicked={row1col2Clicked}
              cssClass={(yellowBack && !fourRowYellow ? s.yellowBackground : s.row1col2Data)}
              cellValue={(
                (fourRowGroup && twoRowGroup) && headerDescProp != headerDesc.gAndA
                  && headerDescProp != headerDesc.adjGrossProf
                  && headerDescProp != headerDesc.adjToGrossProf
                  && headerDescProp != headerDesc.otherExpense
                  ? row1Col2Data
                  : row1Col2Data.contractBOY
              )
              }
              onBlurCallback={this.setClickedDflt}
              onChangeCallback={this.onChange}
              onClickCallBack={this.setClicked}
              onKeyDownCallback={this.setClicked}
              isEdtiable={(this.props.reportMode == reportModeLists.bothSelected) && (this.props.isRow1Editable || (this.props.isRow1Col2DataEditable == true))}
            />
          )}
        </div>
        <div
          className={
            !fourRowGroup && yellowBack
              ? s.yellowBackground
              : fourRowGroup && !twoRowGroup
                ? s.row2col2DataTotal
                : s.row2col2Data
          }
        >
          {row2col2Clicked ? (
            <input
              autoFocus={true}
              pattern="[0-9]*"
              type="number"
              onBlur={this.setClickedDflt}
              value={
                twoRowGroup && fourRowGroup
                  ? row2Col2Data
                  : row2Col2Data.contractBOY
              }
              onChange={this.onChange}
              name="row2Col2Data"
              id="row2col2Clicked"
            />
          ) : (

              <GridEditableCell
                cellId="row2col2Clicked"
                name="row2col2Data"
                isClicked={row2col2Clicked}
                cssClass={(!fourRowGroup && yellowBack
                  ? s.yellowBackground
                  : fourRowGroup && !twoRowGroup
                    ? s.row2col2DataTotal
                    : s.row2col2Data)}
                cellValue={(
                  twoRowGroup && fourRowGroup
                    ? row2Col2Data.contractBOY
                    : row2Col2Data.contractBOY
                )
                }
                onChangeCallback={this.onChange}
                onClickCallBack={this.setClicked}
                onKeyDownCallback={this.setClicked}
                isEdtiable={(this.props.reportMode == reportModeLists.bothSelected)}
              />

            )}
        </div>
        <div
          className={
            !twoRowGroup && yellowBack
              ? s.yellowBackground
              : twoRowGroup
                ? s.row3col2DataNB
                : s.row3col2Data
          }
        >
          {fourRowGroup ? (
            <div id="row3col2Clicked">
              {currencyFormat(
                fourRowGroup
                  ? getTotal(
                    row1Col2Data.contractBOY,
                    row2Col2Data.contractBOY,
                    '',
                    '',
                  ).toString()
                  : row3Col2Data,
              )}
            </div>
          ) : (
              <div>
                {row3col2Clicked ? (
                  <input
                    autoFocus={true}
                    pattern="[0-9]*"
                    type="number"
                    onBlur={this.setClickedDflt}
                    value={row3Col2Data.contractBOY}
                    onChange={this.onChange}
                    name="row3Col2Data"
                    id="row3col2Clicked"
                  />
                ) : (
                    <GridEditableCell
                      cellId="row3col2Clicked"
                      name="row3Col2Data"
                      isClicked={row3col2Clicked}
                      cssClass={(!twoRowGroup && yellowBack
                        ? s.yellowBackground
                        : twoRowGroup
                          ? s.row3col2DataNB
                          : s.row3col2Data)}
                      cellValue={row3Col2Data.contractBOY}
                      onClickCallBack={this.setClicked}
                      onKeyDownCallback={this.setClicked}
                      isEdtiable={(this.props.reportMode == reportModeLists.bothSelected) && this.props.isRow1Editable}
                    />
                  )}
              </div>
            )}
        </div>
        <div
          className={
            !fourRowGroup && yellowBack
              ? s.yellowBackgroundTotal
              : twoRowGroup
                ? s.row4col2DataNV
                : fourRowGroup && !twoRowGroup
                  ? s.row4col2DataNB
                  : s.row4col2Data
          }
        >
          {row4col2Clicked ? (
            <input
              autoFocus={true}
              pattern="[0-9]*"
              type="number"
              onBlur={this.setClickedDflt}
              value={fourRowGroup ? row4Col2Data : row4Col2Data.contractBOY}
              onChange={this.onChange}
              name="row4Col2Data"
              id="row4col2Clicked"
            />
          ) : (
              <GridEditableCell
                cellId="row4col2Clicked"
                name="row4col2Data"
                isClicked={row4col2Clicked}
                cssClass={(!twoRowGroup && yellowBack
                  ? s.yellowBackground
                  : twoRowGroup
                    ? s.row3col2DataNV
                    : s.row3col2Data)}
                cellValue={(
                  fourRowGroup ? row4Col2Data.contractBOY : row4Col2Data.contractBOY
                )
                }
                onClickCallBack={this.setClicked}
                onKeyDownCallback={this.setClicked}
                isEdtiable={false}
              />
            )}
        </div>
        <div className={fourRowGroup ? s.row5col2DataNV : s.row5col2Data}>
          <div id="row5col2Clicked">{currencyFormat(row5Col2Data)}</div>
        </div>
        <div className={fourRowGroup ? s.row6col2DataNV : s.row6col2Data}>
          {row6col2Clicked ? (
            <input
              autoFocus={true}
              pattern="[0-9]*"
              type="number"
              onBlur={this.setClickedDflt}
              value={row6Col2Data}
              onChange={this.onChange}
              name="row6Col2Data"
              id="row6col2Clicked"
            />
          ) : (
              // <div
              //   id="row6col2Clicked"
              //   onClick={this.setClicked}
              //   role="button"
              //   onKeyDown={this.setClicked}
              //   tabIndex="0"
              // >
              //   {currencyFormat(row6Col2Data)}
              // </div>
              <GridEditableCell
                cellId="row6col2Clicked"
                name="row6col2Data"
                isClicked={row6col2Clicked}
                cssClass={(fourRowGroup ? s.row6col2DataNV : s.row6col2Data)}
                cellValue={row6Col2Data.contractBOY}
                onClickCallBack={this.setClicked}
                onKeyDownCallback={this.setClicked}
                isEdtiable={false}
              />
            )}
        </div>

        <GridEditableCell
          cellId="row7col2Clicked"
          name="row7col2Data"
          isClicked={row7col2Clicked}
          cssClass={(fourRowGroup ? s.row7col2DataNV : s.row7col2Data)}
          cellValue={row7Col2Data}
          onClickCallBack={this.setClicked}
          onKeyDownCallback={this.setClicked}
          isEdtiable={false}
        />
        <div className={s.row1col3Data}>
          <div id="row1col3Clicked">
            {currencyFormat(
              headerDescProp === headerDesc.rev ||
                headerDescProp === headerDesc.cto
                || headerDescProp === headerDesc.gAndA
                || headerDescProp === headerDesc.adjToGrossProf
                || headerDescProp === headerDesc.otherExpense
                || headerDescProp === headerDesc.adjGrossProf
                ? getTotal(
                  row1Data.contractYTD,
                  row1Col2Data.contractBOY,
                  '',
                  '',
                ).toString()
                : getTotal(row1Col3Data, '', '', '').toString(),
            )}
          </div>
        </div>
        <div
          className={
            fourRowGroup && !twoRowGroup ? s.row2col3DataTotal : s.row2col3Data
          }
        >
          <div id="row2col3Clicked">
            {
              twoRowGroup
                ? (<GridEditableCell
                  cellId="row2col3Clicked"
                  name="row2Col3Data"
                  isClicked={row2col3Clicked}
                  cssClass={fourRowGroup && !twoRowGroup ? s.row2col3DataTotal : s.row2col3Data}
                  onBlurCallback={this.setClickedDflt}
                  cellValue={row2Col3Data.contractTotal}
                  onChangeCallback={this.onChange}
                  onClickCallBack={this.setClicked}
                  onKeyDownCallback={this.setClicked}
                  recordType="forecastPlan"
                  isEdtiable={(this.props.reportMode == reportModeLists.bothSelected)}
                />)
                : (getTotal(
                  row2Data.contractYTD,
                  row2Col2Data.contractBOY,
                  '',
                  '',
                ).toString()
                )}
          </div>
        </div>
        <div className={twoRowGroup ? s.row3col3DataNB : s.row3col3Data}>
          {fourRowGroup ? (
            <div id="row3col3Clicked">
              {currencyFormat(
                fourRowGroup
                  ? ((headerDescProp === headerDesc.adjToGrossProf)
                    ? row3Col3Data
                    : getTotal(row1Col3Data, row2Col3Data, '', '')
                      .toString())
                  : getTotal(row3Data, row3Col3Data, '', '').toString(),
              )}
            </div>
          ) : (
              <div>
                <div id="row3col3Clicked">
                  {currencyFormat(
                    fourRowGroup
                      ? getTotal(
                        row1Col3Data.contractTotal,
                        row2Col3Data.contractTotal,
                        '',
                        '',
                      ).toString()
                      : getTotal(
                        row3Data.contractYTD,
                        row3Col2Data.contractBOY,
                        '',
                        '',
                      ).toString(),
                  )}
                </div>
              </div>
            )}
        </div>
        <div
          className={
            twoRowGroup
              ? s.row4col3DataNV
              : fourRowGroup && !twoRowGroup
                ? s.row4col3DataNB
                : s.row4col3Data
          }
        >
          <div id="row4col3Clicked">
            {!twoRowGroup
              ? (
                <GridEditableCell
                  cellId="row4col3Clicked"
                  name="row4Col3Data"
                  isClicked={row4col3Clicked}
                  cssClass={s.row4col3DataMod}
                  onBlurCallback={this.setClickedDflt}
                  cellValue={row4Col3Data.contractTotal}
                  onChangeCallback={this.onChange}
                  onClickCallBack={this.setClicked}
                  onKeyDownCallback={this.setClicked}
                  recordType="forecastPlan"
                  isEdtiable={(this.props.reportMode == reportModeLists.bothSelected)}
                />
              )
              : currencyFormat(
                getTotal(
                  row4Data.contractYTD,
                  row4Col2Data.contractBOY,
                  '',
                  '',
                ).toString(),
              )}
          </div>
        </div>
        <div className={fourRowGroup ? s.row5col3DataNV : s.row5col3Data}>
          <div id="row5col3Clicked">{currencyFormat(row5Col3Data)}</div>
        </div>
        {/* <div className={fourRowGroup ? s.row6col3DataNV : s.row6col3Data}>
          <div id="row6col3Clicked">
            {currencyFormat(
              getTotal(row6Data, row6Col2Data, '', '').toString(),
            )}
          </div>
        </div> */}
        <GridEditableCell
          cellId="row6col3Clicked"
          name="row6Col3Data"
          isClicked={row6col3Clicked}
          cssClass={fourRowGroup ? s.row6col3DataNV : s.row6col3Data}
          onBlurCallback={this.setClickedDflt}
          cellValue={row6Col3Data.contractTotal}
          onChangeCallback={this.onChange}
          onClickCallBack={this.setClicked}
          onKeyDownCallback={this.setClicked}
          recordType="forecastPlan"
          isEdtiable={(this.props.reportMode == reportModeLists.bothSelected) && this.props.isrow6Col3Data}
        />

        <GridEditableCell
          cellId="row7col3Clicked"
          name="row7Col3Data"
          isClicked={row7col3Clicked}
          cssClass={fourRowGroup ? s.row7col3DataNV : s.row7col3Data}
          onBlurCallback={this.setClickedDflt}
          cellValue={row7Col3Data}
          onChangeCallback={this.onChange}
          onClickCallBack={this.setClicked}
          onKeyDownCallback={this.setClicked}
          recordType="forecastPlan"
          isEdtiable={(this.props.reportMode == reportModeLists.bothSelected)}
        />
      </div>
    );
  }
}

export default GridRowsColumn3;
