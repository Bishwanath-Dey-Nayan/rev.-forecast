/* eslint-disable css-modules/no-unused-class */

import React from 'react';
import PropTypes from 'prop-types';

import s from './GroupHeader.module.css';
import './gridsize.css';

const GroupHeader = ({
  title,
  children,
  column1Top,
  column1Bottom,
  column2Top,
  column2Left,
  column2Center,
  column2Right,
  column3Top,
  column3Left,
  column3Center,
  column3Right,
  column4Top,
  column4Bottom,
  column5Top,
  column5Left,
  column5Right,
  column1Data,
  column2Data,
  column3Data,
  column4Data,
  column5Data,
  is2ColHeader,
  colorCode,
  cssPosition,
}) => (
  <div
    className={[colorCode, cssPosition].join(' ')}
    style={{ backgroundColor: `${colorCode}` }}
  >
    <div className={s.headerWithoutColor}>
      <div className={s.container}>
        <div className={s.tophr}>{title}</div>

        <div style={{ position: 'relative', background: '#fff' }}>
          <div className={s.mainContainer}>
            <div className={s.rowTitles}>{children}</div>
            {is2ColHeader ? (
              <div className={s.gridContainer2CellNV}>
                <div className={s.topCell}>{column1Top}</div>
                <div className={s.bottomCell}>{column1Bottom}</div>
                <div className={s.voidRow1} />
                <div className={s.voidRow2} />
                <div className={s.voidRow3} />
                <div className={s.voidRow4} />
                <div className={s.column1Data}>{column1Data}</div>
              </div>
            ) : (
              <div className={s.gridContainer2Cell}>
                <div className={s.topCell}>{column1Top}</div>
                <div className={s.bottomCell}>{column1Bottom}</div>
                <div className={s.voidRow1} />
                <div className={s.voidRow2} />
                <div className={s.voidRow3} />
                <div className={s.voidRow4} />
                <div className={s.column1Data}>{column1Data}</div>
              </div>
            )}
            {is2ColHeader ? (
              <div className={s.gridContainer4CellNV}>
                <div className={s.gridItem1Group1}>{column2Top}</div>
                <div className={s.gridItem2Group1}>{column2Left}</div>
                <div className={s.gridItem3Group1}>{column2Center}</div>
                <div className={s.gridItem4Group1}>{column2Right}</div>
                <div className={s.voidRow1} />
                <div className={s.voidRow2} />
                <div className={s.voidRow3} />
                <div className={s.voidRow4} />
                <div className={s.column2Data}>{column2Data}</div>
              </div>
            ) : (
              <div className={s.gridContainer4Cell}>
                <div className={s.gridItem1Group1}>{column2Top}</div>
                <div className={s.gridItem2Group1}>{column2Left}</div>
                <div className={s.gridItem3Group1}>{column2Center}</div>
                <div className={s.gridItem4Group1}>{column2Right}</div>
                <div className={s.voidRow1} />
                <div className={s.voidRow2} />
                <div className={s.voidRow3} />
                <div className={s.voidRow4} />
                <div className={s.column2Data}>{column2Data}</div>
              </div>
            )}
            <div className={s.gridContainer4CellCellThree}>
              {is2ColHeader ? (
                <div className={s.gridItem1Group2NV}>{column3Top}</div>
              ) : (
                <div className={s.gridItem1Group2}>{column3Top}</div>
              )}
              <div className={s.gridItem2Group2}>{column3Left}</div>
              <div className={s.gridItem3Group2}>{column3Center}</div>
              <div className={s.gridItem4Group2}>{column3Right}</div>
              <div className={s.voidRow1} />
              <div className={s.voidRow2} />
              <div className={s.voidRow3} />
              <div className={s.voidRow4} />
              <div className={s.column3Data}>{column3Data}</div>
            </div>
            {is2ColHeader ? (
              <div className={s.gridContainer2CellCell2NV}>
                <div className={s.topCell}>{column4Top}</div>
                <div className={s.bottomCell}>{column4Bottom}</div>
                <div className={s.voidRow1} />
                <div className={s.voidRow2} />
                <div className={s.voidRow3} />
                <div className={s.voidRow4} />
                <div className={s.column4Data}>{column4Data}</div>
              </div>
            ) : (
              <div className={s.gridContainer2CellCell2}>
                <div className={s.topCell}>{column4Top}</div>
                <div className={s.bottomCell}>{column4Bottom}</div>
                <div className={s.voidRow1} />
                <div className={s.voidRow2} />
                <div className={s.voidRow3} />
                <div className={s.voidRow4} />
                <div className={s.column4Data}>{column4Data}</div>
              </div>
            )}
            <div className={s.gridContainer3Cell}>
              {is2ColHeader ? (
                <div className={s.topCellThreeCellNV}>{column5Top}</div>
              ) : (
                <div className={s.topCellThreeCell}>{column5Top}</div>
              )}
              {is2ColHeader ? (
                <div className={s.bottomLeftCellThreeCellNV}>{column5Left}</div>
              ) : (
                <div className={s.bottomLeftCellThreeCell}>{column5Left}</div>
              )}
              <div className={s.bottomRightCellThreeCell}>{column5Right}</div>
              <div className={s.voidRow1} />
              <div className={s.voidRow2} />
              <div className={s.voidRow3} />
              <div className={s.voidRow4} />
              <div className={s.column5Data}>{column5Data}</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

GroupHeader.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  is2ColHeader: PropTypes.bool,
  column1Top: PropTypes.string,
  column1Bottom: PropTypes.string,
  column2Top: PropTypes.string,
  column2Left: PropTypes.string,
  column2Center: PropTypes.string,
  column2Right: PropTypes.string,
  column3Top: PropTypes.string,
  column3Left: PropTypes.string,
  column3Center: PropTypes.string,
  column3Right: PropTypes.string,
  column4Top: PropTypes.string,
  column4Bottom: PropTypes.string,
  column5Top: PropTypes.string,
  column5Left: PropTypes.string,
  column5Right: PropTypes.string,
  column1Data: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  column2Data: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  column3Data: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  column4Data: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  column5Data: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  colorCode: PropTypes.string,
  cssPosition: PropTypes.string,
};

GroupHeader.defaultProps = {
  is2ColHeader: false,
  column1Top: '',
  column1Bottom: '',
  column2Top: '',
  column2Left: '',
  column2Center: '',
  column2Right: '',
  column3Top: '',
  column3Left: '',
  column3Center: '',
  column3Right: '',
  column4Top: '',
  column4Bottom: '',
  column5Top: '',
  column5Left: '',
  column5Right: '',
  column2Data: null,
  column4Data: null,
  colorCode: '',
  cssPosition: '',
};

export default GroupHeader;
