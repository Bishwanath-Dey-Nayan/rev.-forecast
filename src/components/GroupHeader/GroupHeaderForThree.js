/* eslint-disable css-modules/no-unused-class */

import React from 'react';
import PropTypes from 'prop-types';

import s from './GroupHeader.module.css';
import './gridsize.css';

const GroupHeaderForThree = ({
  title,
  children,
  column3Top,
  column3Left,
  column3Center,
  column3Right,
  column5Top,
  column5Left,
  column5Right,
  column3Data,
  column5Data,
  is2ColHeader,
  colorCode,
  cssPosition,
  gridCategory,
}) => (
  <div
    className={[colorCode, cssPosition].join(' ')}
    style={{ backgroundColor: `${colorCode}` }}
  >
    <div className={s.headerWithoutColorThree}>
      <div className={s.containerThree}>
        <div className={s.tophr}>{title} </div>
        <div style={{ position: 'relative', background: '#fff' }}>
          <div className={[gridCategory, s.mainContainerThree].join(' ')}>
            <div className={s.rowTitlesThree}>{children}</div>

            <div className={s.gridContainer4CellCellThree}>
              {is2ColHeader ? (
                ''
              ) : (
                <div className={s.gridItem1Group2}>{column3Top}</div>
              )}
              <div className={s.gridItem2Group2}>{column3Left}</div>
              <div className={s.gridItem3Group2}>{column3Center}</div>
              <div className={s.gridItem4Group2}>{column3Right}</div>
              <div className={s.voidRow1} />
              <div className={s.voidRow2} />
              <div className={s.voidRow3} />
              <div className={s.voidRow4} />
              <div className={s.column3Data}>{column3Data}</div>
            </div>

            <div className={s.gridContainer3Cell}>
              {is2ColHeader ? (
                ''
              ) : (
                <div className={s.topCellThreeCell}>{column5Top}</div>
              )}
              {is2ColHeader ? (
                ''
              ) : (
                <div className={s.bottomLeftCellThreeCell}>{column5Left}</div>
              )}
              <div className={s.bottomRightCellThreeCell}>{column5Right}</div>
              <div className={s.voidRow1} />
              <div className={s.voidRow2} />
              <div className={s.voidRow3} />
              <div className={s.voidRow4} />
              <div className={s.column5Data}>{column5Data}</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

GroupHeaderForThree.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  is2ColHeader: PropTypes.bool,

  column3Top: PropTypes.string,
  column3Left: PropTypes.string,
  column3Center: PropTypes.string,
  column3Right: PropTypes.string,

  column5Top: PropTypes.string,
  column5Left: PropTypes.string,
  column5Right: PropTypes.string,

  column3Data: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,

  column5Data: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  colorCode: PropTypes.string,
  cssPosition: PropTypes.string,
  gridCategory: PropTypes.string,
};

GroupHeaderForThree.defaultProps = {
  is2ColHeader: false,

  column3Top: '',
  column3Left: '',
  column3Center: '',
  column3Right: '',

  column5Top: '',
  column5Left: '',
  column5Right: '',

  colorCode: '',
  cssPosition: '',
  gridCategory: '',
};

export default GroupHeaderForThree;
