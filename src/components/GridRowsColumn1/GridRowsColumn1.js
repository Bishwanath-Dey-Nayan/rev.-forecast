/* eslint-disable css-modules/no-unused-class */
/* eslint-disable */
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import s from './GridRowsColumn1.module.css';
import { currencyFormat, getTotal } from '../../utils/mathAndCurrency';
import { insertUpdateForecastReport } from '../../data';
import GridEditableCell from '../GridCells/GridEditableCell';
import {
  headerDesc,
  rowDesc,
  reportModeLists,
} from '../../constants/GfrReport.constant';


class GridRowsColumn1 extends Component {
  static propTypes = {
    row1Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row2Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row3Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row4Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row5Data: PropTypes.string,
    row6Data: PropTypes.string,
    row7Data: PropTypes.string,
    columnVisible: PropTypes.bool,
    token: PropTypes.string,
    greyOut: PropTypes.bool,
  };

  static defaultProps = {
    row1Data: {},
    row2Data: {},
    row3Data: {},
    row4Data: {},
    row5Data: '',
    row6Data: '',
    row7Data: '',
    columnVisible: false,
    token: null,
    greyOut: false,
  };

  constructor(props) {
    super(props);
    const {
      row1Data,
      row2Data,
      row3Data,
      row4Data,
      row5Data,
      row6Data,
      row7Data,
    } = this.props;
    this.state = {
      row1Clicked: false,
      row2Clicked: false,
      row3Clicked: false,
      row4Clicked: false,
      row5Clicked: false,
      row6Clicked: false,
      row1Data,
      row2Data,
      row3Data,
      row4Data,
      row5Data,
      row6Data,
      row7Data,
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    //
    //test
    if (nextProps !== prevState) {
      //nextProps.netIncomeTotal !== prevState.netIncomeTotal
      return {
        netIncomeTotal: nextProps.netIncomeTotal,
        row1Data: nextProps.row1Data,
        row2Data: nextProps.row2Data,
        row3Data: nextProps.row3Data,
        row4Data: nextProps.row4Data,
        row5Data: nextProps.row5Data,
        row6Data: nextProps.row6Data,
        row7Data: nextProps.row7Data,
      };
    } else return null;
  }
  onChange = e => {
    const { row1Data, row2Data, row3Data, row4Data } = this.state;
    var row0 = {}; // eslint-disable-line
    switch (e.target.name) {
      case 'row1Data':
        row0 = row1Data;
        row0.backlogBeginning = e.target.value;
        this.setState({
          row1Data: row0,
        });
        break;
      case 'row2Data':
        row0 = row2Data;
        row0.backlogBeginning = e.target.value;
        this.setState({
          row2Data: row0,
        });
        break;
      case 'row3Data':
        row0 = row3Data;
        row0.backlogBeginning = e.target.value;
        this.setState({
          row3Data: row0,
        });
        break;
      case 'row4Data':
        row0 = row4Data;
        row0.backlogBeginning = e.target.value;
        this.setState({
          row4Data: row0,
        });
        break;
      case 'row6Data':
        this.setState({
          row6Data: e.target.value,
        });
        break;
      case 'row7Data':
        this.setState({
          row7Data: e.target.value,
        });
        break;
      default:
        row0 = {};
        break;
    }
  };

  setClicked = e => {
    this.setState({ [e.target.id]: true });
  };

  setClickedDflt = e => {
    const { token } = this.props;
    const { row1Data, row2Data, row3Data, row4Data } = this.state;

    var row0 = {};
    switch (e.target.name) {
      case 'row1Data':
        row0 = row1Data;
        row0.backlogBeginning = e.target.value;
        if (!row0.rowDescription) row0.rowDescription = rowDesc.contrAwrdInPy;
        if (!row0.row) row0.row = 0;
        if (!row0.headerRow) row0.headerRow = 0;
        this.setState({
          row1Data: row0,
        });
        break;
      case 'row2Data':
        row0 = row2Data;
        row0.backlogBeginning = e.target.value;
        if (!row0.rowDescription) row0.rowDescription = rowDesc.contrAwrdYTD;
        if (!row0.row) row0.row = 1;
        if (!row0.headerRow) row0.headerRow = 1;
        this.setState({
          row2Data: row0,
        });
        break;
      case 'row3Data':
        row0 = row3Data;
        row0.backlogBeginning = e.target.value;
        if (!row0.rowDescription) row0.rowDescription = rowDesc.pendingAwardsm;
        if (!row0.row) row0.row = 2;
        if (!row0.headerRow) row0.headerRow = 2;
        this.setState({
          row3Data: row0,
        });
        break;
      case 'row4Data':
        row0 = row4Data;
        row0.backlogBeginning = e.target.value;
        if (!row0.rowDescription) row0.rowDescription = rowDesc.otherWork;
        if (!row0.row) row0.row = 3;
        if (!row0.headerRow) row0.headerRow = 3;
        this.setState({
          row4Data: row0,
        });
        break;
      case 'row6Data':
        this.setState({
          row6Data: e.target.value,
        });
        break;
      case 'row7Data':
        this.setState({
          row7Data: e.target.value,
        });
        break;
      default:
        row0 = {};
        break;
    }
    this.props.updateGfrCellData(row0);
    this.setState({ [e.target.id]: false });
  };

  render() {
    const { columnVisible, greyOut, isRow1Editale } = this.props;
    const {
      row1Data,
      row1Clicked,
      row2Data,
      row2Clicked,
      row3Data,
      row3Clicked,
      row4Data,
      row4Clicked,
      row5Data,
      row5Clicked,
      row6Data,
      row7Data,
      row6Clicked,
      row7Clicked,
    } = this.state;

    if (columnVisible) {
      return (
        <div className={s.mainContainer}>
          <GridEditableCell
            cellId="row1Clicked"
            name="row1Data"
            isClicked={row1Clicked}
            cssClass={s.row1Data}
            onBlurCallback={this.setClickedDflt}
            cellValue={row1Data.backlogBeginning}
            onChangeCallback={this.onChange}
            onClickCallBack={this.setClicked}
            onKeyDownCallback={this.setClicked}
            isEdtiable={(this.props.reportMode == reportModeLists.bothSelected) && this.props.isRow1Editable}
          />
          <div className={s.row2Data}>
            {greyOut ? (
              <div className={s.greyBackground} />
            ) : (
                <GridEditableCell
                  cellId="row2Clicked"
                  name="row2Data"
                  isClicked={row2Clicked}
                  cssClass={s.row2Data}
                  onBlurCallback={this.setClickedDflt}
                  cellValue={row2Data.backlogBeginning}
                  onChangeCallback={this.onChange}
                  onClickCallBack={this.setClicked}
                  onKeyDownCallback={this.setClicked}
                  isEdtiable={(reportMode == reportModeLists.bothSelected) && this.props.isRow2Editable}
                />
              )}
          </div>
          <div className={s.row3Data}>
            {greyOut ? (
              <div className={s.greyBackground} />
            ) : (
                <GridEditableCell
                  cellId="row3Clicked"
                  name="row3Data"
                  isClicked={row3Clicked}
                  cssClass={s.row3Data}
                  onBlurCallback={this.setClickedDflt}
                  cellValue={row3Data.backlogBeginning}
                  onChangeCallback={this.onChange}
                  onClickCallBack={this.setClicked}
                  onKeyDownCallback={this.setClicked}
                />
              )}
          </div>

          <div className={s.row4Data}>
            {greyOut ? (
              <div className={s.greyBackground} />
            ) : (
                <GridEditableCell
                  cellId="row4Clicked"
                  name="row4Data"
                  isClicked={row4Clicked}
                  cssClass={s.row4Data}
                  onBlurCallback={this.setClickedDflt}
                  cellValue={row4Data.backlogBeginning}
                  onChangeCallback={this.onChange}
                  onClickCallBack={this.setClicked}
                  onKeyDownCallback={this.setClicked}
                />
              )}
          </div>

          <GridEditableCell
            cellId="row5Clicked"
            name="row5Data"
            cssClass={s.row5Data}
            onBlurCallback={this.setClickedDflt}
            onChangeCallback={this.onChange}
            cellValue={getTotal(
              row1Data.backlogBeginning,
              row2Data.backlogBeginning,
              row3Data.backlogBeginning,
              row4Data.backlogBeginning,
            ).toString()}
          />
          {/* <div className={s.row5Data}>
            <div id="row5Clicked">
              {currencyFormat(
                getTotal(
                  row1Data.backlogBeginning,
                  row2Data.backlogBeginning,
                  row3Data.backlogBeginning,
                  row4Data.backlogBeginning,
                ).toString(),
              )}
            </div>
          </div> */}
          <GridEditableCell
            cellId="row6Clicked"
            name="row6Data"
            isClicked={row6Clicked}
            cssClass={s.row6Data}
            //onBlurCallback={this.setClickedDflt}
            cellValue={row6Data}
          //onChangeCallback={this.onChange}
          //onClickCallBack={this.setClicked}
          //onKeyDownCallback={this.setClicked}

          />
          <GridEditableCell
            cellId="row7Clicked"
            name="row7Data"
            isClicked={row7Clicked}
            cssClass={s.row7Data}
            cellValue={row7Data}
          />
        </div >
      );
    }
    return (
      <div className={s.mainContainerNotVisible}>
        <GridEditableCell
          cellId="row1Clicked"
          name="row1Data"
          isClicked={row1Clicked}
          cssClass={s.row1Data}
          onBlurCallback={this.setClickedDflt}
          cellValue={row1Data.backlogBeginning}
          onChangeCallback={this.onChange}
          onClickCallBack={this.setClicked}
          onKeyDownCallback={this.setClicked}
        />

        <GridEditableCell
          cellId="row2Clicked"
          name="row2Data"
          isClicked={row2Clicked}
          cssClass={s.row2Data}
          onBlurCallback={this.setClickedDflt}
          cellValue={row2Data.backlogBeginning}
          onChangeCallback={this.onChange}
          onClickCallBack={this.setClicked}
          onKeyDownCallback={this.setClicked}
        />

        <GridEditableCell
          cellId="row3Clicked"
          name="row3Data"
          isClicked={row3Clicked}
          cssClass={s.row3Data}
          onBlurCallback={this.setClickedDflt}
          cellValue={row3Data.backlogBeginning}
          onChangeCallback={this.onChange}
          onClickCallBack={this.setClicked}
          onKeyDownCallback={this.setClicked}
        />

        <GridEditableCell
          cellId="row4Clicked"
          name="row4Data"
          isClicked={row4Clicked}
          cssClass={s.row4Data}
          onBlurCallback={this.setClickedDflt}
          cellValue={row4Data.backlogBeginning}
          onChangeCallback={this.onChange}
          onClickCallBack={this.setClicked}
          onKeyDownCallback={this.setClicked}
        />

        <div className={s.row5Data}>
          {row5Clicked ? (
            <input
              autoFocus={true}
              pattern="[0-9]*"
              type="number"
              onBlur={this.setClickedDflt}
              value={getTotal(
                row1Data,
                row2Data,
                row3Data,
                row4Data,
              ).toString()}
              onChange={this.onChange}
              name="row5Data"
              id="row5Clicked"
            />
          ) : (
              // <div
              //   id="row5Clicked"
              //   onClick={this.setClicked}
              //   role="button"
              //   onKeyDown={this.setClicked}
              //   tabIndex="0"
              // >
              //   {currencyFormat(
              //     getTotal(row1Data, row2Data, row3Data, row4Data).toString(),
              //   )}
              // </div>
              <GridEditableCell
                cellId="row5Clicked"
                name="row5Data"
                isClicked={row5Clicked}
                cssClass={s.row5Data}
                cellValue={getTotal(row1Data, row2Data, row3Data, row4Data).toString()}
                onChangeCallback={this.onChange}
                onClickCallBack={this.setClicked}
                onKeyDownCallback={this.setClicked}
              />
            )}
        </div>
        <div className={s.row6Data}>
          {row6Clicked ? (
            <input
              autoFocus={true}
              pattern="[0-9]*"
              type="number"
              onBlur={this.setClickedDflt}
              value={row6Data}
              onChange={this.onChange}
              name="row6Data"
              id="row6Clicked"
            />
          ) : (
              // <div
              //   id="row6Clicked"
              //   onClick={this.setClicked}
              //   role="button"
              //   onKeyDown={this.setClicked}
              //   tabIndex="0"
              // >
              //   {currencyFormat(row6Data)}
              // </div>
              <GridEditableCell
                cellId="row6Clicked"
                name="row6Data"
                isClicked={row6Clicked}
                cssClass={s.row6Data}
                onBlurCallback={this.setClickedDflt}
                cellValue={row6Data.backlogBeginning}
                onChangeCallback={this.onChange}
                onClickCallBack={this.setClicked}
                onKeyDownCallback={this.setClicked}
                isEdtiable={(this.props.reportMode == reportModeLists.bothSelected)}
              />


            )}
          <GridEditableCell
            cellId="row7Clicked"
            name="row7Data"
            isClicked={row7Clicked}
            cssClass={s.row7Data}
            onBlurCallback={this.setClickedDflt}
            cellValue={row7Data}
            onChangeCallback={this.onChange}
            onClickCallBack={this.setClicked}
            onKeyDownCallback={this.setClicked}
            isEdtiable={(this.props.reportMode == reportModeLists.bothSelected)}
          />
        </div>
      </div>
    );
  }
}

export default GridRowsColumn1;
