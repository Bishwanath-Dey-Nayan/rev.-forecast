/* eslint-disable css-modules/no-unused-class */
/* eslint-disable react/prop-types */
/* eslint-disable lines-between-class-members */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prefer-stateless-function */
import React,{Component} from 'react';

import Select from 'react-select';
import moment from 'moment';

// import { PDFDownloadLink} from '@react-pdf/renderer';

import s from './ReportHeader.module.css';
import ss from '../common.module.css';
import download from '../Images/Download_Report.png';
import downloaded from '../Images/Downloaded.png';


const rstyle = {
  container: base => ({
    ...base,
    borderRadius:'20px',
  })
}

export default class ReportHeader extends Component  {
  // constructor(props)
  // {
  //   super(props);
  //   this.state={
  //     companyval:{value:this.props.selectedCompany.value,label:this.props.selectedCompany.label}
  //   }
  // }
  render(){

  const {
    companies,
    divisions,
    years,
    months,
    line2,
    line3,
    line4,
    date,
    selectedCompany,
    selectedDivision,
    selectedMonth,
    changeMonth,
    changeCompany,
    changeDivision,
    selectedYear,
    changeYear,
    onGoButtonClicked,
    isDDLChanged,
    onDownloadPdfClicked,
    pdfDownloadStatus
  }= this.props;
 // console.log('selected company in reportheader',this.props.selectedCompany); // eslint-disable-line
  
  return(
    <div className={s.root}>
      <div className={s.container}>
        <div style={{ width: '100%', height: '30px' }}>
          <div style={{ width: '75%', float: 'left', display: 'inline-block' }}>
            <div className={(ss.t, ss.s3_1, s.t3_1)} style={{ marginTop: '40px', marginLeft: '350px' }}>{line2}</div>
          </div>
          <div style={{ width: '20%', float: 'right', display: 'inline-block' }}>
             {(pdfDownloadStatus === 'default') && (<div style={{ marginLeft: '200px', marginTop: '50px', display: 'inherit' }}>
              <button style={{
                textAlign: 'center',
                fontSize: '18px',
                width: '230px',
                height: '45px',
                cursor: 'pointer',
                color: 'white',
                backgroundColor: 'black',
                fontWeight: '900',
                border: '4px solid',
                borderRadius: '30px',
                marginRight:'10px',
                marginLeft:'-40px'

              }} type='button' onClick={onDownloadPdfClicked}><img src={download} alt='download' style={{width:'23px',marginRight:'9px'}} />Download Report</button>
             </div>)}

            {(pdfDownloadStatus === "downloading") && (<div style={{ marginLeft: '200px', marginTop: '50px', display: 'inherit' }}>
              <button style={{
                textAlign: 'center',
                fontSize: '18px',
                width: '190px',
                height: '45px',
                cursor: 'pointer',
                color: 'white',
                backgroundColor: 'black',
                fontWeight: '900',
                border: '4px solid',
                borderRadius: '30px',
                marginLeft:'-40px'

              }} type='button' onClick={onDownloadPdfClicked}>Downloading...</button>
            </div> )}
            {(pdfDownloadStatus === "downloaded") && (<div style={{ marginLeft: '200px', marginTop: '50px', display: 'inherit' }}>
              <button style={{
                textAlign: 'center',
                fontSize: '18px',
                width: '190px',
                height: '45px',
                cursor: 'pointer',
                color: 'black',
                backgroundColor: 'white',
                fontWeight: '900',
                border: '4px solid',
                borderRadius: '30px',
                marginLeft:'-40px'


              }} disabled type='button' onClick={onDownloadPdfClicked}><img src={downloaded} alt='download' style={{width:'23px',marginRight:'9px'}} />Downloaded</button>
            </div> )}
            
            {/* {!isDDLChanged ? (<div style={{ marginLeft: '200px', marginTop: '50px', display: 'inherit' }}>
              <button style={{
                textAlign: 'center',
                fontSize: '18px',
                width: '190px',
                height: '45px',
                cursor: 'pointer',
                color: 'white',
                backgroundColor: 'black',
                fontWeight: '900',
                border: '4px solid',
                borderRadius: '8px'

              }} type='button' onClick={onDownloadPdfClicked}>Download Report</button>
            </div>) : (<div style={{ marginLeft: '200px', marginTop: '50px', display: 'inherit' }}>
              <button style={{
                textAlign: 'center',
                fontSize: '18px',
                width: '190px',
                height: '40px',
                cursor: 'pointer',
                color: 'black',
                backgroundColor: 'white',
                fontWeight: '900',
                border: '4px solid',
                borderRadius: '8px'

              }} type='button' onClick={onDownloadPdfClicked} disabled={isDDLChanged}>Download Report</button>
            </div>)} */}
          </div>
        </div>
      </div>
       <div style={{width:'100%',height:'30px',marginLeft:'-2px',marginTop:'-20px'}}>
            <div className={(ss.t, ss.s4_1, s.t5_1)} style={{marginTop:'-9px',fontWeight:'100'}}>{line3}</div>
      </div>
      <div style={{ width: '100%',height:'50px',marginBottom:'60px' }}>
        {/* firstcolumn */}
        <div style={{ width: '90%', float: 'left', display: 'inline-block',marginLeft:'-260px' }}>
          <div style={{ width: '20%', float: 'left', display: 'inline-block' }}>
            <div className={s.selectLabel} style={{marginLeft:'10px',width:'99%'}} >Company</div><br />
            {companies.length > 1 && (
              <div className={(ss.t, ss.s1_1, s.t2_1)}>
                {/* <div className={s.selectLabel}>Company</div> */}
                <Select
                 placeholder="Select Company..."
                  value={selectedCompany}
                  options={companies}
                  onChange={changeCompany}
                  className={[s.selectCompany, s.selectList].join(' ')}
                  styles={rstyle}
                />
              </div>
            )}
          </div>
          <div style={{ width: '20%',display: 'inline-block' }}>
          <div className={s.selectLabel}style={{marginLeft:'10px',width:'99%'}} >Market</div><br />
            {divisions && divisions.length > 0 && (
              <div className={(ss.t, ss.s2_1, s.t2_1)}>
                <Select
                  style={{borderRadius:'20px'}}
                  placeholder="All"
                  value={selectedDivision}
                  options={divisions}
                  onChange={changeDivision}
                  className={[s.selectDivision, s.selectList].join(' ')}
                />
              </div>
            )}
          </div>
          <div style={{ width: '15%',display: 'inline-block' }}>
          <div className={s.selectLabel} style={{marginLeft:'10px',width:'99%'}}>Year</div><br />
            <div className={(ss.t, ss.s2_1, s.t2_2)}>
              <Select
                placeholder="Select"
                value={selectedYear}
                options={years}
                onChange={changeYear}
                className={[s.selectYear, s.selectList].join(' ')}
                isOptionDisabled={option => option.disabled}
              />
            </div>
          </div>
          <div style={{ width: '15%',display: 'inline-block',marginLeft:'25px' }}>
          <div className={s.selectLabel} style={{marginLeft:'10px',width:'99%'}}>Month</div><br />
            <div className={(ss.t, ss.s2_1, s.t2_2)}> 
              <Select
                placeholder="All"
                value={selectedMonth}
                options={months}
                onChange={changeMonth}
                className={[s.selectMonth, s.selectList].join(' ')}
                isOptionDisabled={option => option.disabled}
                style={{borderRadius:'30px'}}
              />
            </div>
          </div>
          <div style={{ width: '10%',float:'right', display: 'inline-block' }}>
            {isDDLChanged ? (<div style={{ marginTop: '26px',marginLeft:'-300px' }}>
              <button style={{
                textAlign: 'center',
                fontSize: '20px',
                width: '120px',
                height: '45px',
                cursor: 'pointer',
                color: 'white',
                backgroundColor: '#5976FF',
                fontWeight: '900',
                border: '4px solid',
                borderRadius: '8px',
                zIndex:'3'

              }} type='button' onClick={onGoButtonClicked}>Submit</button>
            </div>) : (<div style={{ marginTop: '26px',marginLeft:'-300px'}}>
              <button style={{
                textAlign: 'center',
                fontSize: '20px',
                width: '120px',
                height: '40px',
                cursor: 'pointer',
                color: '#5976FF',
                backgroundColor: 'white',
                fontWeight: '900',
                border: '4px solid',
                borderRadius: '8px',
                zIndex:'3'

              }} type='button' onClick={onGoButtonClicked} disabled={!isDDLChanged}>Submit</button>
            </div>)}
          </div>
        </div>
        <div style={{ width: '28%', float: 'right', display: 'inline-block',marginBottom:'60px',marginTop:'-40px',zIndex:1 }}>
          {/* second column */}
          <div className={(ss.t, ss.s3_1, s.t4_1)}>{moment(date).format("dddd, MMMM Do, YYYY")}</div>
        </div>
      </div>



      <div style={{ width: '100%' }}>
        <div style={{ float: 'left' }}>
          {/* {companies.length > 1 && (
            <div className={(ss.t, ss.s1_1, s.t2_1)}>
              <div className={s.selectLabel}>Company</div>
              <Select
                placeholder="Choose Company..."
                value={selectedCompany}
                options={companies}
                onChange={changeCompany}
                className={[s.selectCompany, s.selectList].join(' ')}
              />
            </div>
          )} */}

          {/* {divisions && divisions.length > 0 && (
            <div className={(ss.t, ss.s2_1, s.t2_1)}>
              <div className={s.selectLabel}>Market</div>
              <Select
                placeholder="All"
                value={selectedDivision}
                options={divisions}
                onChange={changeDivision}
                className={[s.selectDivision, s.selectList].join(' ')}
              />
            </div>
          )} */}

          {/* <div className={(ss.t, ss.s2_1, s.t2_1)}>
            <div className={s.selectLabel}>Year</div>
            <Select
              placeholder="Select"
              value={selectedYear}
              options={years}
              onChange={changeYear}
              className={[s.selectYear, s.selectList].join(' ')}
              isOptionDisabled={option => option.disabled}
            />
          </div> */}
          {/* <div className={(ss.t, ss.s2_1, s.t2_1)}>
            <div className={s.selectLabel}>Month</div>
            <Select
              placeholder="All"
              value={selectedMonth}
              options={months}
              onChange={changeMonth}
              className={[s.selectMonth, s.selectList].join(' ')}
              isOptionDisabled={option => option.disabled}
            />
          </div> */}
          {/* {isDDLChanged ? (<div style={{ marginLeft: '310px', marginTop: '10px' }}>
            <button style={{
              textAlign: 'center',
              fontSize: '20px',
              width: '120px',
              height: '45px',
              cursor: 'pointer',
              color: 'white',
              backgroundColor: 'green',
              fontWeight: '900',
              border: '4px solid',
              borderRadius: '8px'

            }} type='button' onClick={onGoButtonClicked}>GO</button>
          </div>) : (<div style={{ marginLeft: '310px', marginTop: '10px' }}>
            <button style={{
              textAlign: 'center',
              fontSize: '20px',
              width: '120px',
              height: '40px',
              cursor: 'pointer',
              color: 'green',
              backgroundColor: 'white',
              fontWeight: '900',
              border: '4px solid',
              borderRadius: '8px'

            }} type='button' onClick={onGoButtonClicked} disabled={!isDDLChanged}>GO</button>
          </div>)} */}
        </div>

        {/* {!isDDLChanged ? (<div style={{ marginLeft: '240px', marginTop: '10px', display: 'inherit' }}>
          <button style={{
            textAlign: 'center',
            fontSize: '18px',
            width: '190px',
            height: '45px',
            cursor: 'pointer',
            color: 'white',
            backgroundColor: 'green',
            fontWeight: '900',
            border: '4px solid',
            borderRadius: '8px'

          }} type='button' onClick={onDownloadPdfClicked}>Generate Report</button>
        </div>) : (<div style={{ marginLeft: '240px', marginTop: '10px', display: 'inherit' }}>
          <button style={{
            textAlign: 'center',
            fontSize: '18px',
            width: '190px',
            height: '40px',
            cursor: 'pointer',
            color: 'green',
            backgroundColor: 'white',
            fontWeight: '900',
            border: '4px solid',
            borderRadius: '8px'

          }} type='button' onClick={onDownloadPdfClicked} disabled={isDDLChanged}>Generate Report</button>
        </div>)} */}


      </div>
      {/* <div
        style={{ textAlign: 'center', marginTop: '7px', marginBottom: '7px' }}
      >
        <img
          src={companyLogo}
          style={{ marginTop: '33px', marginBottom: '33px', width: '655px' }} 
          alt="altImage"
        />
      </div> */}

      {/* <div className={(ss.t, ss.s3_1, s.t3_1)}>{line2}</div> */}
      {/* <div className={(ss.t, ss.s3_1, s.t4_1)}>{date}</div> */}
      {/* <div className={(ss.t, ss.s4_1, s.t5_1)}>{line3}</div> */}
      <div className={(ss.t, ss.s4_1, s.t6_1)}>{line4}</div>
    </div>

  );
}
}



// ReportHeader.propTypes = {
//   companies: PropTypes.array.isRequired, // eslint-disable-line
//   divisions: PropTypes.array.isRequired, // eslint-disable-line
//   changeCompany: PropTypes.func.isRequired,
//   changeDivision: PropTypes.func.isRequired,
//   selectedCompany: PropTypes.object, // eslint-disable-line
//   selectedDivision: PropTypes.object, // eslint-disable-line
//   line2: PropTypes.string.isRequired,// eslint-disable-line
//   line3: PropTypes.string.isRequired,//eslint-disable-line
//   line4: PropTypes.string.isRequired,
//   pdfDownloadStatus:PropTypes.string.isRequired,
//   date: PropTypes.string.isRequired,
//   months: PropTypes.array.isRequired, //eslint-disable-line
//   years: PropTypes.array.isRequired, // eslint-disable-line
//   selectedMonth: PropTypes.object, // eslint-disable-line
//   changeMonth: PropTypes.func.isRequired, // eslint-disable-line
//   companyLogo: PropTypes.string, // eslint-disable-line
//   selectedYear: PropTypes.object, // eslint-disable-line
//   changeYear: PropTypes.func.isRequired, // eslint-disable-line
//   onGoButtonClicked: PropTypes.func.isRequired,
//   isDDLChanged: PropTypes.bool.isRequired, // eslint-disable-line
//   onDownloadPdfClicked: PropTypes.func.isRequired,
// };

// ReportHeader.defaultProps = {
//   selectedCompany: null,
//   selectedDivision: null,
//   selectedYear: null,
//   selectedMonth: null,
// };


