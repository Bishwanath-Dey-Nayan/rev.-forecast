/* eslint-env jest */
/* eslint-disable */

import React from 'react';
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import App from '../App';
import LoadingCircle from './LoadingCircle';

const middlewares = [thunk];
const mockStore = configureStore(middlewares);
const initialState = {};

describe('LoadingCircle', () => {
  // Testing just to make sure the component rendered matches the current LoadingCircle component
  test('renders a loadingCircle component', () => {
    const store = mockStore(initialState);
    const wrapper = renderer
      .create(
        <App context={{ insertCss: () => {}, fetch: () => {}, store }}>
          <LoadingCircle />
        </App>,
      )
      .toJSON();

    expect(wrapper).toMatchSnapshot();
  });
  // Testing functionality of the component - using enzyme's mount method to mount the component
  test('renders a loadingCircle component with an optional classname', () => {
    const store = mockStore(initialState);
    const wrapper = mount(
      <App context={{ insertCss: () => {}, fetch: () => {}, store }}>
        <LoadingCircle optionalClassName="foo" />
      </App>,
    );
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('.foo')).toHaveLength(1);
  });
});
