/* eslint-disable css-modules/no-unused-class */

import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './LoadingCircle.css';

const LoadingCircle = ({ optionalClassName }) => (
  <div
    className={cx(s.loadingCircle, {
      [s[optionalClassName]]: optionalClassName,
    })}
  />
);

LoadingCircle.propTypes = {
  optionalClassName: PropTypes.string,
};

LoadingCircle.defaultProps = {
  optionalClassName: '',
};

export default withStyles(s)(LoadingCircle);
