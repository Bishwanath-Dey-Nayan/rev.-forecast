/* eslint-disable css-modules/no-unused-class */
/* eslint-disable */
import React, { Component } from 'react';
import { currencyFormat, getTotal } from '../../utils/mathAndCurrency';

// OSTeam - new component
// to be used as a editable cell of a grid
class GridEditableCell extends Component {
  static defaultProps = {
    isEdtiable: true,
  };
  constructor(props) {
    super(props);    
  }

  render() {
    const {
      cellId,
      name,
      isClicked,
      cssClass,
      onBlurCallback,
      cellValue,
      onChangeCallback,
      onClickCallBack,
      onKeyDownCallback,
      isEdtiable,
      recordType,
    } = this.props;

    return (
      <div className={cssClass}>
        {isEdtiable ? (
          isClicked ? (
            <input
              autoFocus={true}
              pattern="[0-9]*"
              type="number"
              onBlur={onBlurCallback}
              value={cellValue||''}
              onChange={onChangeCallback}
              name={name}
              id={cellId}
              style={{width:'100%'}}
              data-recordtype={recordType}
            />
          ) : (
            <div
              id={cellId}
              onClick={onClickCallBack}
              role="button"
              onKeyDown={onKeyDownCallback}
              tabIndex="0"
            >
              {currencyFormat(cellValue)}
            </div>
          )
        ) : (
          <div role="button">{currencyFormat(cellValue)}</div>
        )}
      </div>
    );
  }
}
export default GridEditableCell;
