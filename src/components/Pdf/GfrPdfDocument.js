/* eslint-disable  */
import React from 'react';
import { connect } from 'react-redux';
import { getData } from '../../utils/GfrReportHelper';
import { getTotal, currencyFormat } from '../../utils/mathAndCurrency';
import { headerDesc, rowDesc, reportModeLists } from '../../constants/GfrReport.constant';
import moment from 'moment';
import s from './PdfStyles.css';
// import { bindActionCreators } from 'redux';
// import PropTypes from 'prop-types';
// import { filter } from 'lodash';

import * as gfrSelector from '../../store/gfr/gfr.selectors';

import { Document, Page, Text, View, Image, StyleSheet  } from '@react-pdf/renderer';
import * as PdfStyles from './PdfStyles';
import { relative } from 'path';

export const LabelAndValue = ({ label, value }) => (
    <View style={PdfStyles.labelValueStyles.itemRow}>
        <Text style={PdfStyles.labelValueStyles.itemLabel}>{label}</Text>
        <Text style={PdfStyles.labelValueStyles.itemDivider}>:  </Text>
        <Text style={PdfStyles.labelValueStyles.itemContent}>{value}</Text>
    </View>
);


const styles = StyleSheet.create({
    pageNumber: {
        position: 'absolute',
        fontSize: 12,
        bottom: 30,
        left: 0,
        right: 0,
        textAlign: 'center',
        color: 'black',
      },
    });
export const PaRow = ({ paRow, cellBackground, color }) => (
    <View style={{ flexDirection: 'row', marginBottom: 5, alignItems: 'stretch', width: '100%', }}>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, padding: 2 }}>{paRow.pendingAwardTitle}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color,marginLeft:'1px' }}>{moment(new Date(paRow.pendingAwardExpDate)).format('MM/DD/YY')}</Text>
        <Text style={{ width: '8%', fontSize: 10, backgroundColor: cellBackground, color: color, padding: 2, textAlign: 'right', }}>{currencyFormat(paRow.pendingAwardRevenueValue)}</Text>
        <Text style={{ width: '8%', fontSize: 10, backgroundColor: cellBackground, color: color, padding: 2, textAlign: 'right' }}>{currencyFormat(paRow.pendingAwardCTOValue)}</Text>
        <Text style={{ width: '8%', fontSize: 10, backgroundColor: cellBackground, color: color, padding: 2, textAlign: 'right' }}>{currencyFormat(paRow.pendingAwardFYEPrevRev)}</Text>
        <Text style={{ width: '8%', fontSize: 10, backgroundColor: cellBackground, color: color, padding: 2, textAlign: 'right' }}>{currencyFormat(paRow.pendingAwardFYEPrevCTO)}</Text>
        <Text style={{ width: '8%', fontSize: 10, backgroundColor: cellBackground, color: color, padding: 2, textAlign: 'right' }}>{currencyFormat(paRow.pendingAwardCurBacklogRev)}</Text>
        <Text style={{ width: '8%', fontSize: 10, backgroundColor: cellBackground, color: color, padding: 2, textAlign: 'right' }}>{currencyFormat(paRow.pendingAwardCurBacklogCTO)}</Text>
        <Text style={{ width: '8%', fontSize: 10, backgroundColor: cellBackground, color: color, padding: 2, textAlign: 'right' }}>{currencyFormat(paRow.pendingAwardFutureBacklogRev)}</Text>
        <Text style={{ width: '8%', fontSize: 10, backgroundColor: cellBackground, color: color, padding: 2, textAlign: 'right' }}>{currencyFormat(paRow.pendingAwardFutureBacklogCTO)}</Text>

    </View>
);
export const PaTitle = ({ paRow, cellBackground, color }) => (
    <View style={{ flexDirection: 'row', marginBottom: 5, alignItems: 'stretch', width: '100%', }}>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, padding: 2 }}>{paRow.pendingAwardTitle}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, padding: 2 }}>{paRow.pendingAwardExpDate}</Text>
        <Text style={{ width: '8%', fontSize: 10, backgroundColor: cellBackground, color: color, padding: 2, textAlign: 'center', }}>{paRow.pendingAwardRevenueValue}</Text>
        <Text style={{ width: '8%', fontSize: 10, backgroundColor: cellBackground, color: color, padding: 2, textAlign: 'center', }}>{paRow.pendingAwardCTOValue}</Text>
        <Text style={{ width: '8%', fontSize: 10, backgroundColor: cellBackground, color: color, padding: 2, textAlign: 'center', }}>{paRow.pendingAwardFYEPrevRev}</Text>
        <Text style={{ width: '8%', fontSize: 10, backgroundColor: cellBackground, color: color, padding: 2, textAlign: 'center', }}>{paRow.pendingAwardFYEPrevCTO}</Text>
        <Text style={{ width: '8%', fontSize: 10, backgroundColor: cellBackground, color: color, padding: 2, textAlign: 'center', }}>{paRow.pendingAwardCurBacklogRev}</Text>
        <Text style={{ width: '8%', fontSize: 10, backgroundColor: cellBackground, color: color, padding: 2, textAlign: 'center', }}>{paRow.pendingAwardCurBacklogCTO}</Text>
        <Text style={{ width: '8%', fontSize: 10, backgroundColor: cellBackground, color: color, padding: 2, textAlign: 'center', }}>{paRow.pendingAwardFutureBacklogRev}</Text>
        <Text style={{ width: '8%', fontSize: 10, backgroundColor: cellBackground, color: color, padding: 2, textAlign: 'center', }}>{paRow.pendingAwardFutureBacklogCTO}</Text>

    </View>
);
export const PaTotal = ({ paRow, cellBackground, color }) => (
    <View style={{ flexDirection: 'row', marginBottom: 5, alignItems: 'stretch', width: '100%', }}>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, padding: 2, textAlign: 'left' }}>{paRow.pendingAwardTitle}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, padding: 2 }}>{paRow.pendingAwardExpDate}</Text>
        <Text style={{ width: '8%', fontSize: 10, backgroundColor: cellBackground, color: color, padding: 2, textAlign: 'right', }}>{currencyFormat(paRow.pendingAwardRevenueValue)}</Text>
        <Text style={{ width: '8%', fontSize: 10, backgroundColor: cellBackground, color: color, padding: 2, textAlign: 'right', }}>{currencyFormat(paRow.pendingAwardCTOValue)}</Text>
        <Text style={{ width: '8%', fontSize: 10, backgroundColor: cellBackground, color: color, padding: 2, textAlign: 'right', }}>{currencyFormat(paRow.pendingAwardFYEPrevRev)}</Text>
        <Text style={{ width: '8%', fontSize: 10, backgroundColor: cellBackground, color: color, padding: 2, textAlign: 'right', }}>{currencyFormat(paRow.pendingAwardFYEPrevCTO)}</Text>
        <Text style={{ width: '8%', fontSize: 10, backgroundColor: cellBackground, color: color, padding: 2, textAlign: 'right', }}>{currencyFormat(paRow.pendingAwardCurBacklogRev)}</Text>
        <Text style={{ width: '8%', fontSize: 10, backgroundColor: cellBackground, color: color, padding: 2, textAlign: 'right', }}>{currencyFormat(paRow.pendingAwardCurBacklogCTO)}</Text>
        <Text style={{ width: '8%', fontSize: 10, backgroundColor: cellBackground, color: color, padding: 2, textAlign: 'right', }}>{currencyFormat(paRow.pendingAwardFutureBacklogRev)}</Text>
        <Text style={{ width: '8%', fontSize: 10, backgroundColor: cellBackground, color: color, padding: 2, textAlign: 'right', }}>{currencyFormat(paRow.pendingAwardFutureBacklogCTO)}</Text>

    </View>
);
//______________________________Revenue Row Started__________________________
export const RevenueRowMainHeader = ({ revenueRow, cellBackground, color }) => (
    <View style={{ flexDirection: 'row', marginBottom: 5, alignItems: 'stretch', width: '100%', }}>
        <Text style={{ width: '10%', fontSize: 12, backgroundColor: 'white', color: color, textAlign: 'left', marginLeft: '0px', padding: 2 }}>{revenueRow.revenueAwardTitle}</Text>
        <Text style={{ width: '10%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', marginLeft: '18px', padding: 2 }}>{revenueRow.revenueAwardExpDate}</Text>
        <Text style={{ width: '25%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', marginLeft: '18px', padding: 2 }}>{revenueRow.revenueAwardRevenueValue}</Text>
        <Text style={{ width: '25%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', marginLeft: '18px', padding: 2 }}>{revenueRow.revenueAwardCTOValue}</Text>
        <Text style={{ width: '10%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', marginLeft: '18px', padding: 2 }}>{revenueRow.revenueAwardFYEPrevRev}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', marginLeft: '18px', padding: 2 }}>{revenueRow.revenueAwardFYEPrevCTO}</Text>
    </View>
);

//[(revenueRow.revenueTitle === "")?'white':'cellBackground']
export const RevenueRow = ({ revenueRow, cellBackground, color }) => (
    <View style={{ flexDirection: 'row', marginBottom: 5, alignItems: 'stretch', width: '100%', }}>
        <Text style={{ width: '10%', fontSize: 10, backgroundColor: 'white', color: color, }}>{revenueRow.revenueTitle}</Text>
        <Text style={{ width: '10%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right', marginLeft: '18px' }}>{currencyFormat(revenueRow.revenue_B_Begining)}</Text>
        <Text style={{ width: '8.33%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right', marginLeft: '18px' }}>{currencyFormat(revenueRow.revenue_ag_YTD)}</Text>
        <Text style={{ width: '8.33%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right' }}>{currencyFormat(revenueRow.revenue_ag_BOY)}</Text>
        <Text style={{ width: '8.33%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right' }}>{currencyFormat(revenueRow.revenue_ag_Total)}</Text>
        <Text style={{ width: '8.33%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right', marginLeft: '18px' }}>{currencyFormat(revenueRow.revenue_ca_YTD)}</Text>
        <Text style={{ width: '8.33%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right' }}>{currencyFormat(revenueRow.revenue_ca_BOY)}</Text>
        <Text style={{ width: '8.33%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right' }}>{currencyFormat(revenueRow.revenue_ca_Total)}</Text>
        <Text style={{ width: '10%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right', marginLeft: '18px' }}>{currencyFormat(revenueRow.revenue_b_Ending)}</Text>
        <Text style={{ width: '7.5%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right', marginLeft: '18px' }}>{currencyFormat(revenueRow.revenue_ny_awards)}</Text>
        <Text style={{ width: '7.5%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right' }}>{currencyFormat(revenueRow.revenue_ny_Activity)}</Text>

    </View>

);

export const RevenueTitle = ({ revenueRow, cellBackground, color }) => (
    <View style={{ flexDirection: 'row', marginBottom: 5, alignItems: 'stretch', width: '100%', }}>
        <Text style={{ width: '10%', fontSize: 10, backgroundColor: 'white', color: color, padding: 2 }}>{revenueRow.revenueTitle}</Text>
        <Text style={{ width: '10%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', marginLeft: '18px', padding: 2 }}>{revenueRow.revenue_B_Begining}</Text>
        <Text style={{ width: '8.33%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', marginLeft: '18px', padding: 2 }}>{(revenueRow.revenue_ag_YTD)}</Text>
        <Text style={{ width: '8.33%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', padding: 2 }}>{(revenueRow.revenue_ag_BOY)}</Text>
        <Text style={{ width: '8.33%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', padding: 2 }}>{(revenueRow.revenue_ag_Total)}</Text>
        <Text style={{ width: '8.33%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right', marginLeft: '18px', padding: 2 }}>{(revenueRow.revenue_ca_YTD)}</Text>
        <Text style={{ width: '8.33%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right', padding: 2 }}>{(revenueRow.revenue_ca_BOY)}</Text>
        <Text style={{ width: '8.33%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', padding: 2 }}>{(revenueRow.revenue_ca_Total)}</Text>
        <Text style={{ width: '10%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', marginLeft: '18px', padding: 2 }}>{(revenueRow.revenue_b_Ending)}</Text>
        <Text style={{ width: '7.5%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', marginLeft: '18px', padding: 2 }}>{(revenueRow.revenue_ny_awards)}</Text>
        <Text style={{ width: '7.5%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', padding: 2 }}>{(revenueRow.revenue_ny_Activity)}</Text>

    </View>

);
//______________________________Revenue Row End___________________________

// _____________________________CTO Row Start______________________________
export const CTORowMainHeader = ({ ctoRow, cellBackground, color }) => (
    <View style={{ flexDirection: 'row', marginBottom: 5, alignItems: 'stretch', width: '100%', }}>
        <Text style={{ width: '10%', fontSize: 12, backgroundColor: 'white', color: color, textAlign: 'left', marginLeft: '0px', padding: 2 }}>{ctoRow.ctoTitle_col1}</Text>
        <Text style={{ width: '10%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', marginLeft: '18px', padding: 2 }}>{ctoRow.ctoTitle_col2}</Text>
        <Text style={{ width: '25%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', marginLeft: '18px', padding: 2 }}>{ctoRow.ctoTitle_col3}</Text>
        <Text style={{ width: '25%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', marginLeft: '18px', padding: 2 }}>{ctoRow.ctoTitle_col4}</Text>
        <Text style={{ width: '10%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', marginLeft: '18px', padding: 2 }}>{ctoRow.ctoTitle_col5}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', marginLeft: '18px', padding: 2 }}>{ctoRow.ctoTitle_col6}</Text>
    </View>
);

export const CTORow = ({ ctoRow, cellBackground, color }) => (
    <View style={{ flexDirection: 'row', marginBottom: 5, alignItems: 'stretch', width: '100%', }}>
        <Text style={{ width: '10%', fontSize: 10, backgroundColor: 'white', color: color, }}>{ctoRow.ctoTitle}</Text>
        <Text style={{ width: '10%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right', marginLeft: '18px' }}>{currencyFormat(ctoRow.cto_B_Begining)}</Text>
        <Text style={{ width: '8.33%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right', marginLeft: '18px' }}>{currencyFormat(ctoRow.cto_ag_YTD)}</Text>
        <Text style={{ width: '8.33%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right' }}>{currencyFormat(ctoRow.cto_ag_BOY)}</Text>
        <Text style={{ width: '8.33%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right' }}>{currencyFormat(ctoRow.cto_ag_Total)}</Text>
        <Text style={{ width: '8.33%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right', marginLeft: '18px' }}>{currencyFormat(ctoRow.cto_ca_YTD)}</Text>
        <Text style={{ width: '8.33%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right' }}>{currencyFormat(ctoRow.cto_ca_BOY)}</Text>
        <Text style={{ width: '8.33%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right' }}>{currencyFormat(ctoRow.cto_ca_Total)}</Text>
        <Text style={{ width: '10%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right', marginLeft: '18px' }}>{currencyFormat(ctoRow.cto_b_Ending)}</Text>
        <Text style={{ width: '7.5%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right', marginLeft: '18px' }}>{currencyFormat(ctoRow.cto_ny_awards)}</Text>
        <Text style={{ width: '7.5%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right' }}>{currencyFormat(ctoRow.cto_ny_Activity)}</Text>

    </View>

);

export const CTOTitle = ({ ctoRow, cellBackground, color }) => (
    <View style={{ flexDirection: 'row', marginBottom: 5, alignItems: 'stretch', width: '100%', }}>
        <Text style={{ width: '10%', fontSize: 10, backgroundColor: 'white', color: color, padding: 2 }}>{ctoRow.ctoTitle}</Text>
        <Text style={{ width: '10%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', marginLeft: '18px', padding: 2 }}>{ctoRow.cto_B_Begining}</Text>
        <Text style={{ width: '8.33%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', marginLeft: '18px', padding: 2 }}>{ctoRow.cto_ag_YTD}</Text>
        <Text style={{ width: '8.33%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', padding: 2 }}>{ctoRow.cto_ag_BOY}</Text>
        <Text style={{ width: '8.33%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', padding: 2 }}>{ctoRow.cto_ag_Total}</Text>
        <Text style={{ width: '8.33%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right', marginLeft: '18px', padding: 2 }}>{ctoRow.cto_ca_YTD}</Text>
        <Text style={{ width: '8.33%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right', padding: 2 }}>{ctoRow.cto_ca_BOY}</Text>
        <Text style={{ width: '8.33%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', padding: 2 }}>{ctoRow.cto_ca_Total}</Text>
        <Text style={{ width: '10%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', marginLeft: '18px', padding: 2 }}>{ctoRow.cto_b_Ending}</Text>
        <Text style={{ width: '7.5%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', marginLeft: '18px', padding: 2 }}>{ctoRow.cto_ny_awards}</Text>
        <Text style={{ width: '7.5%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', padding: 2 }}>{ctoRow.cto_ny_Activity}</Text>

    </View>
);
// _____________________________CTO Row End_________________________________

//______________________________Adjustment to Gross Profit__________________________

export const ATGPTitle = ({ atgpRow, cellBackground, color }) => (
    <View style={{ flexDirection: 'row', alignItems: 'stretch', width: '100%', marginBottom: 5, }}>
        <Text style={{ width: '20%', fontSize: 10, backgroundColor: 'white', color: color, textAlign: 'center', padding: 2 }}>{atgpRow.atgpTitle}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', marginLeft: '18px', padding: 2 }}>{atgpRow.atgp_YTD}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', padding: 2 }}>{atgpRow.atgp_BOY}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', padding: 2 }}>{atgpRow.atgp_Total}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', marginLeft: '18px', padding: 2 }}>{atgpRow.atgp_NextYear}</Text>

    </View>
);
export const ATGPRow = ({ atgpRow, cellBackground, color }) => (
    <View style={{ flexDirection: 'row', alignItems: 'stretch', width: '100%', marginBottom: 5, }}>
        <Text style={{ width: '20%', fontSize: 10, backgroundColor: 'white', color: color, textAlign: 'left', }}>{atgpRow.atgpTitle}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right', marginLeft: '18px' }}>{currencyFormat(atgpRow.atgp_YTD)}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right' }}>{currencyFormat(atgpRow.atgp_BOY)}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right' }}>{currencyFormat(atgpRow.atgp_Total)}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right', marginLeft: '18px' }}>{currencyFormat(atgpRow.atgp_NextYear)}</Text>

    </View>
);
//______________________________Adjustment to Gross Profit__________________________

//______________________________Adjusted Gross Profit________________________________

export const AGPTitle = ({ agpRow, cellBackground, color }) => (
    <View style={{ flexDirection: 'row', alignItems: 'stretch', width: '100%', marginBottom: 5 }}>
        <Text style={{ width: '20%', fontSize: 10, backgroundColor: 'white', color: color, padding: 2 }}>{agpRow.agpTitle}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', marginLeft: '18px', padding: 2 }}>{agpRow.agp_YTD}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', padding: 2 }}>{agpRow.agp_BOY}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', padding: 2 }}>{agpRow.agp_Total}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', marginLeft: '18px', padding: 2 }}>{agpRow.agp_NextYear}</Text>

    </View>
);
export const AGPRow = ({ agpRow, cellBackground, color }) => (
    <View style={{ flexDirection: 'row', alignItems: 'stretch', width: '100%', marginBottom: 5, }}>
        <Text style={{ width: '20%', fontSize: 10, backgroundColor: 'white', color: color, textAlign: 'left' }}>{agpRow.agpTitle}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right', marginLeft: '18px' }}>{currencyFormat(agpRow.agp_YTD)}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right', marginRight: '2px' }}>{currencyFormat(agpRow.agp_BOY)}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right' }}>{currencyFormat(agpRow.agp_Total)}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right', marginLeft: '18px' }}>{currencyFormat(agpRow.agp_NextYear)}</Text>

    </View>
);

//______________________________Adjusted Gross Profit________________________________


//_________________________________G&A  start________________________________________

export const GARow = ({ gaRow, cellBackground, color }) => (
    <View style={{ flexDirection: 'row', alignItems: 'stretch', width: '100%', marginBottom: 5, }}>
        <Text style={{ width: '20%', fontSize: 10, backgroundColor: 'white', color: color, textAlign: 'left' }}>{gaRow.gaTitle}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right', marginLeft: '18px' }}>{currencyFormat(gaRow.ga_YTD)}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right' }}>{currencyFormat(gaRow.ga_BOY)}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right' }}>{currencyFormat(gaRow.ga_Total)}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right', marginLeft: '18px' }}>{currencyFormat(gaRow.ga_NextYear)}</Text>

    </View>
);

export const GATitle = ({ gaRow, cellBackground, color }) => (
    <View style={{ flexDirection: 'row', alignItems: 'stretch', width: '100%', marginBottom: 5, }}>
        <Text style={{ width: '20%', fontSize: 10, backgroundColor: 'white', color: color, padding: 2 }}>{gaRow.gaTitle}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', marginLeft: '18px', padding: 2 }}>{gaRow.ga_YTD}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', padding: 2 }}>{gaRow.ga_BOY}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', padding: 2 }}>{gaRow.ga_Total}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', marginLeft: '18px', padding: 2 }}>{gaRow.ga_NextYear}</Text>

    </View>
);

//_________________________________G&A End___________________________________________

//Other Expense Start
export const OERow = ({ oeRow, cellBackground, color }) => (
    <View style={{ flexDirection: 'row', alignItems: 'stretch', width: '100%', marginBottom: 5, }}>
        <Text style={{ width: '20%', fontSize: 10, backgroundColor: "white", color: color, textAlign: 'left' }}>{oeRow.oeTitle}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right', marginLeft: '18px' }}>{currencyFormat(oeRow.oe_YTD)}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right', marginRight: '1px' }}>{currencyFormat(oeRow.oe_BOY)}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right' }}>{currencyFormat(oeRow.oe_Total)}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right', marginLeft: '18px' }}>{currencyFormat(oeRow.oe_NextYear)}</Text>

    </View>
);

export const OETitle = ({ oeRow, cellBackground, color }) => (
    <View style={{ flexDirection: 'row', alignItems: 'stretch', width: '100%', marginBottom: 5, }}>
        <Text style={{ width: '20%', fontSize: 10, backgroundColor: "white", color: color, padding: 2 }}>{oeRow.oeTitle}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', marginLeft: '18px', padding: 2 }}>{oeRow.oe_YTD}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', padding: 2 }}>{oeRow.oe_BOY}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', padding: 2 }}>{oeRow.oe_Total}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', marginLeft: '18px', padding: 2 }}>{oeRow.oe_NextYear}</Text>

    </View>
);
//Other Expense End

//Net Income Start
export const NETitle = ({ neRow, cellBackground, color }) => (
    <View style={{ flexDirection: 'row', alignItems: 'stretch', width: '100%', marginBottom: 5, }}>
        <Text style={{ width: '20%', fontSize: 10, backgroundColor: 'white', color: color, padding: 2 }}>{neRow.neTitle}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', marginLeft: '18px', padding: 2 }}>{neRow.ne_YTD}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', padding: 2 }}>{neRow.ne_BOY}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', padding: 2 }}>{neRow.ne_Total}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'center', marginLeft: '18px', padding: 2 }}>{neRow.ne_NextYear}</Text>

    </View>
);

export const NERow = ({ neRow, cellBackground, color }) => (
    <View style={{ flexDirection: 'row', alignItems: 'stretch', width: '100%', marginBottom: 5, }}>
        <Text style={{ width: '20%', fontSize: 10, backgroundColor: 'white', color: color, textAlign: 'left' }}>{neRow.neTitle}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right', marginLeft: '18px' }}>{currencyFormat(neRow.ne_YTD)}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right' }}>{currencyFormat(neRow.ne_BOY)}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right' }}>{currencyFormat(neRow.ne_Total)}</Text>
        <Text style={{ width: '15%', fontSize: 10, backgroundColor: cellBackground, color: color, textAlign: 'right', marginLeft: '18px' }}>{currencyFormat(neRow.ne_NextYear)}</Text>

    </View>
);
//Net Income End

class GfrPdfDocument extends React.Component {


    render() {
        const { data, contractData, companyCode, cyContractData, jdeData, pendingAwardsData, plrData } = this.props;
        var config = require('Config');

        let companyImageConfig = config['companies'][this.props.selectedCompany.value]['HEADER_IMAGE'];
        let companyColor = config['companies'][this.props.selectedCompany.value]['COLOR'];

        var imageFolder = require.context('../Images', true);
        let companyImage = require('../Images/' + companyImageConfig);

        //let companyImage = require('../Images/graycorConsCoInc.png');

        // _________________Value for the Pending Awards__________________
        let paTitle = {
            pendingAwardTitle: 'Pending Awards',
            pendingAwardExpDate: 'Exp Awd Date',
            pendingAwardRevenueValue: 'Revenue Value',
            pendingAwardCTOValue: 'CTO Value',
            pendingAwardFYEPrevRev: 'FYE 18 Revenue',
            pendingAwardFYEPrevCTO: 'FYE 18 CTO',
            pendingAwardCurBacklogRev: '2019 Backlog Rev',
            pendingAwardCurBacklogCTO: '2019 Backlog CTO',
            pendingAwardFutureBacklogRev: '2020 Backlog Rev',
            pendingAwardFutureBacklogCTO: '2020 Backlog CTO'
        }

        let paRows = pendingAwardsData;

        var paTotals = {
            pendingAwardTitle: 'Total',
            pendingAwardExpDate: '',
            pendingAwardRevenueValue: 0,
            pendingAwardCTOValue: 0,
            pendingAwardFYEPrevRev: 0,
            pendingAwardFYEPrevCTO: 0,
            pendingAwardCurBacklogRev: 0,
            pendingAwardCurBacklogCTO: 0,
            pendingAwardFutureBacklogRev: 0,
            pendingAwardFutureBacklogCTO: 0,
        };


        paRows.forEach(function (data, index) {
            paTotals.pendingAwardRevenueValue +=
                +data.pendingAwardRevenueValue || 0;
            paTotals.pendingAwardCTOValue += +data.pendingAwardCTOValue;
            paTotals.pendingAwardFYEPrevRev += +data.pendingAwardFYEPrevRev || 0;
            paTotals.pendingAwardFYEPrevCTO += +data.pendingAwardFYEPrevCTO || 0;
            paTotals.pendingAwardCurBacklogRev +=
                +data.pendingAwardCurBacklogRev || 0;
            paTotals.pendingAwardCurBacklogCTO +=
                +data.pendingAwardCurBacklogCTO || 0;
            paTotals.pendingAwardFutureBacklogRev +=
                +data.pendingAwardFutureBacklogRev || 0;
            paTotals.pendingAwardFutureBacklogCTO +=
                +data.pendingAwardFutureBacklogCTO || 0;
        });

        // ___________________value for the Pending Award End________________________

        // ________________________________content for the Revenue start___________________________________
        //contract awarded in prior year
        //backlog begining
        const row1Data = getData(data, headerDesc.rev, rowDesc.contrAwrdInPy);
        if (companyCode === 'GCC') {
            row1Data.backlogBeginning = (+contractData.REVENUE_FORECAST_PY || 0) -
                ((+contractData.PRIOR_YEAR_COST || 0) + (+contractData.OH_PRIOR_YEAR || 0));
        }
        let bb_row1Data = row1Data.backlogBeginning;

        //growth YTD
        if (companyCode === 'GCC') {
            row1Data.growthYTD = (+contractData.REVENUE_FORECAST || 0) - (+contractData.REVENUE_FORECAST_PY || 0);
        }
        if (companyCode === 'GIC' || companyCode === 'GSI') {
            row1Data.growthYTD = (contractData.CURR_YEAR_REVENUE || 0) + (contractData.REVENUE_BACKLOG || 0) - (row1Data.backlogBeginning || 0);
        }
        let ag_row1Data = row1Data.growthYTD;
        //growth BOY
        let ag_row1Col2Data = row1Data.growthBOY;
        //ag total
        let ag_row1Col3Data = getTotal(
            ag_row1Data,
            ag_row1Col2Data,
            '',
            '',
        );
        //contract activity 
        if (companyCode === 'GIC') {
            if ((+row1Data.growthYTD || 0) == 0) {
                //if no data in gfr , calculate it using jde data 
                row1Data.contractYTD = (+contractData.CURR_YEAR_REVENUE || 0);
            }
        }
        else {
            row1Data.contractYTD = (+contractData.CURR_YEAR_REVENUE || 0);
        }

        let ca_row1Data = row1Data.contractYTD;

        //contract BOY
        let ca_row1Col2Data = row1Data.contractBOY;
        //contract activity Total
        let ca_row1Col3Data = getTotal(
            ca_row1Data,
            ca_row1Col2Data,
            '',
            '',
        );
        //Backlog Ending
        let be_row1Data = (
            (+bb_row1Data || 0) +
            (+ag_row1Col3Data || 0) -
            (+ca_row1Col3Data || 0)
        );

        let nextYearGrowth = row1Data.nextYearGrowth;
        let nextYearActivity = row1Data.nextYearActivity;

        //contract Awarded YTD (row2Data)
        const row2Data = getData(data, headerDesc.rev, rowDesc.contrAwrdYTD);
        let bb_row2Data = +row2Data.backlogBeginning || 0;
        //growth YTD
        if (companyCode === 'GIC' || companyCode === "GSI") {
            row2Data.growthYTD = (cyContractData.CURR_YEAR_REVENUE || 0) + (cyContractData.REVENUE_BACKLOG || 0);
        }
        else if (companyCode === "GCC") {
            if ((+row2Data.growthYTD || 0) == 0) {
                row2Data.growthYTD = (cyContractData.REVENUE_FORECAST || 0);
            }
        }
        let ag_row2Data = row2Data.growthYTD;
        let ag_row2Col2Data = row2Data.growthBOY;
        let ag_row2Col3Data = getTotal(
            ag_row2Data,
            ag_row2Col2Data,
            '',
            '',
        );
        //contarct YTD
        if (companyCode === 'GCC') {
            if ((+row2Data.contractYTD || 0) == 0) {
                //if no data found on gfr,take it from the jdedata
                row2Data.contractYTD = (cyContractData.CURR_YEAR_REVENUE || 0);
            }
        }

        else if (companyCode === 'GIC' || companyCode === 'GSI') {
            row2Data.contractYTD = (cyContractData.CURR_YEAR_REVENUE || 0);
        }
        //else data is already collected from the gft data.
        let ca_row2Data = row2Data.contractYTD;

        let ca_row2Col2Data = row2Data.contractBOY;
        let ca_row2Col3Data = getTotal(
            ca_row2Data,
            ca_row2Col2Data,
            '',
            '',
        );
        //backlog Ending
        let be_row2Data = (
            (+bb_row2Data || 0) +
            (+ag_row2Col3Data || 0) -
            (+ca_row2Col3Data || 0)
        );
        //NEXT Year
        let row2nextYearGrowth = row2Data.nextYearGrowth;
        let row2nextYearActivity = row2Data.nextYearActivity;

        //pending Award (row3Data)
        const row3Data = getData(data, headerDesc.rev, rowDesc.pendingAwards);
        let bb_row3Data = +row3Data.backlogBeginning || 0;
        //growth YTD
        let ag_row3Data = row3Data.growthYTD;
        let ag_row3Col2Data = +paTotals.pendingAwardRevenueValue || 0;
        let ag_row3Col3Data = getTotal(
            ag_row3Data,
            ag_row3Col2Data,
            '',
            '',
        );
        //contract YTD
        let ca_row3Data = row3Data.contractYTD;
        let ca_row3Col2Data = paTotals.pendingAwardFYEPrevRev;
        let ca_row3Col3Data = getTotal(
            ca_row3Data,
            ca_row3Col2Data,
            '',
            '',
        );
        //backlog ending
        let be_row3Data = (
            (+bb_row3Data || 0) +
            (+ag_row3Col3Data || 0) -
            (+ca_row3Col3Data || 0)
        );
        //next year
        let row3nextYearGrowth = row3Data.nextYearGrowth;
        let row3nextYearActivity = row3Data.nextYearActivity;

        //Other Future Work
        const row4Data = getData(data, headerDesc.rev, rowDesc.otherWork);
        let bb_row4Data = +row4Data.backlogBeginning || 0;
        //growth YTD
        let ag_row4Data = row4Data.growthYTD;
        let ag_row4Col2Data = row4Data.growthBOY;
        let ag_row4Col3Data = getTotal(
            ag_row4Data,
            ag_row4Col2Data,
            '',
            '',
        );
        //contract YTD
        let ca_row4Data = row4Data.contractYTD;
        let ca_row4Col2Data = row4Data.contractBOY;
        let ca_row4Col3Data = getTotal(
            ca_row4Data,
            ca_row4Col2Data,
            '',
            '',
        );
        //backlog ending
        let be_row4Data = (
            (+bb_row4Data || 0) +
            (+ag_row4Col3Data || 0) -
            (+ca_row4Col3Data || 0)
        );
        //next year
        let row4nextYearGrowth = row4Data.nextYearGrowth;
        let row4nextYearActivity = row4Data.nextYearActivity;

        let revenueMainHeaderTitle =
        {
            revenueAwardTitle: '',
            revenueAwardExpDate: 'Backlog',
            revenueAwardRevenueValue: 'Awards/Growth',
            revenueAwardCTOValue: 'Contract Activity',
            revenueAwardFYEPrevRev: 'Backlog',
            revenueAwardFYEPrevCTO: 'Next Year'
        }

        let revenueTitle = {
            revenueTitle: '',
            revenue_B_Begining: 'Beginning',
            revenue_ag_YTD: 'Year-to-Date',
            revenue_ag_BOY: 'Balance of Year',
            revenue_ag_Total: 'Total',
            revenue_ca_YTD: 'Year-To-Date',
            revenue_ca_BOY: 'Balance of Year',
            revenue_ca_Total: 'Total',
            revenue_b_Ending: 'Ending',
            revenue_ny_awards: 'Awards/Growth',
            revenue_ny_Activity: 'Activity',
        }

        let revenueRows = [];
        let revenueRow1 = {
            revenueTitle: 'Contract Awarded in Prior Years',
            revenue_B_Begining: (bb_row1Data || 0),
            revenue_ag_YTD: ((ag_row1Data) || 0),
            revenue_ag_BOY: ((ag_row1Col2Data) || 0),
            revenue_ag_Total: ((ag_row1Col3Data) || 0),
            revenue_ca_YTD: ((ca_row1Data) || 0),
            revenue_ca_BOY: ((ca_row1Col2Data) || 0),
            revenue_ca_Total: ((ca_row1Col3Data) || 0),
            revenue_b_Ending: ((be_row1Data) || 0),
            revenue_ny_awards: ((nextYearGrowth) || 0),
            revenue_ny_Activity: ((nextYearActivity) || 0),
        }
        let revenueRow2 = {
            revenueTitle: 'Contract Awarded YTD',
            revenue_B_Begining: ((bb_row2Data) || 0),
            revenue_ag_YTD: ((ag_row2Data) || 0),
            revenue_ag_BOY: ((ag_row2Col2Data) || 0),
            revenue_ag_Total: ((ag_row2Col3Data) || 0),
            revenue_ca_YTD: ((ca_row2Data) || 0),
            revenue_ca_BOY: ((ca_row2Col2Data) || 0),
            revenue_ca_Total: ((ca_row2Col3Data) || 0),
            revenue_b_Ending: ((be_row2Data) || 0),
            revenue_ny_awards: ((row2nextYearGrowth) || 0),
            revenue_ny_Activity: ((row2nextYearActivity) || 0),
        }
        let revenueRow3 = {
            revenueTitle: 'Pending Awards',
            revenue_B_Begining: ((bb_row3Data) || 0),
            revenue_ag_YTD: ((ag_row3Data) || 0),
            revenue_ag_BOY: ((ag_row3Col2Data) || 0),
            revenue_ag_Total: ((ag_row3Col3Data) || 0),
            revenue_ca_YTD: ((ca_row3Data) || 0),
            revenue_ca_BOY: ((ca_row3Col2Data) || 0),
            revenue_ca_Total: ((ca_row3Col3Data) || 0),
            revenue_b_Ending: ((be_row3Data) || 0),
            revenue_ny_awards: ((row3nextYearGrowth) || 0),
            revenue_ny_Activity: ((row3nextYearActivity) || 0),
        }
        let revenueRow4 = {
            revenueTitle: 'Other Future Work',
            revenue_B_Begining: ((bb_row4Data) || 0),
            revenue_ag_YTD: ((ag_row4Data) || 0),
            revenue_ag_BOY: ((ag_row4Col2Data) || 0),
            revenue_ag_Total: ((ag_row4Col3Data) || 0),
            revenue_ca_YTD: ((ca_row4Data) || 0),
            revenue_ca_BOY: ((ca_row4Col2Data) || 0),
            revenue_ca_Total: ((ca_row4Col3Data) || 0),
            revenue_b_Ending: ((be_row4Data) || 0),
            revenue_ny_awards: ((row4nextYearGrowth) || 0),
            revenue_ny_Activity: ((row4nextYearActivity) || 0),
        }
        let revenueRow5 = {
            revenueTitle: '2019 Plan',
            revenue_B_Begining: '0',
            revenue_ag_YTD: '71486',
            revenue_ag_BOY: '0',
            revenue_ag_Total: '71486',
            revenue_ca_YTD: '654654',
            revenue_ca_BOY: '654654',
            revenue_ca_Total: '125555',
            revenue_b_Ending: '371635',

        }
        let revenueTotal = {
            revenueTitle: 'Total',
            revenue_B_Begining: 0,
            revenue_ag_YTD: 0,
            revenue_ag_BOY: 0,
            revenue_ag_Total: 0,
            revenue_ca_YTD: 0,
            revenue_ca_BOY: 0,
            revenue_ca_Total: 0,
            revenue_b_Ending: 0,
            revenue_ny_awards: 0,
            revenue_ny_Activity: 0,
        }

        revenueRows.push(revenueRow1);
        revenueRows.push(revenueRow2);
        revenueRows.push(revenueRow3);
        revenueRows.push(revenueRow4);


        revenueRows.forEach(function (data, index) {
            revenueTotal.revenue_B_Begining += (data.revenue_B_Begining || 0);
            revenueTotal.revenue_ag_YTD += (data.revenue_ag_YTD || 0);
            revenueTotal.revenue_ag_BOY += (data.revenue_ag_BOY || 0);
            revenueTotal.revenue_ag_Total += (data.revenue_ag_Total || 0);
            revenueTotal.revenue_ca_YTD += (data.revenue_ca_YTD || 0);
            revenueTotal.revenue_ca_BOY += (data.revenue_ca_BOY || 0);
            revenueTotal.revenue_ca_Total += (data.revenue_ca_Total || 0);
            revenueTotal.revenue_b_Ending += (data.revenue_b_Ending || 0);
            revenueTotal.revenue_ny_awards += (data.revenue_ny_awards || 0);
            revenueTotal.revenue_B_Begining += (data.revenue_ny_Activity || 0);

        })
        // ________________________content for the Revenue End_____________________

        // ______________________content for CTO Start__________________________
        const cto_row1Data = getData(data, headerDesc.cto, rowDesc.contrAwrdInPy);
        //contracts awarded in prior years (row1 Data)
        //backlog begining
        if (companyCode === 'GCC') {
            cto_row1Data.backlogBeginning = (+contractData.CTO_FORECAST_PY || 0) - (+contractData.OH_PRIOR_YEAR || 0);
        }

        let cto_bb_row1Data = cto_row1Data.backlogBeginning;
        //growth YTD

        if (companyCode === 'GCC') {
            cto_row1Data.growthYTD = (+contractData.FORECAST_CTO || 0) - (+contractData.CTO_FORECAST_PY || 0);
        }
        else if (companyCode === 'GSI') {
            cto_row1Data.growthYTD = (+contractData.CURR_YEAR_CTO || 0) + (+contractData.BACKLOG_CTO || 0) - (+cto_row1Data.backlogBeginning || 0);
        }
        else if (companyCode === 'GIC') {

            if ((+cto_row1Data.growthYTD || 0) == 0) {
                //if no data in gfr , calculate it using jde data 
                cto_row1Data.growthYTD = (+contractData.CURR_YEAR_CTO || 0) + (+contractData.BACKLOG_CTO || 0) - (+cto_row1Data.backlogBeginning || 0);
            }

        }
        let cto_ag_row1Data = cto_row1Data.growthYTD;
        let cto_ag_row1Col2Data = cto_row1Data.growthBOY;
        let cto_ag_row1Col3Data = getTotal(
            cto_ag_row1Data,
            cto_ag_row1Col2Data,
            '',
            '',
        );

        //contract Data
        cto_row1Data.contractYTD = (+contractData.CURR_YEAR_CTO);
        let cto_ca_row1Data = cto_row1Data.contractYTD;;
        let cto_ca_row1Col2Data = cto_row1Data.contractBOY;
        let cto_ca_row1Col3Data = getTotal(
            cto_ca_row1Data,
            cto_ca_row1Col2Data,
            '',
            '',
        );
        //backlog ending
        let cto_be_row1Data = cto_bb_row1Data + cto_ag_row1Col3Data - cto_ca_row1Col3Data;
        //Next Year
        let cto_row1nextYearGrowth = cto_row1Data.nextYearGrowth;
        let cto_row1nextYearActivity = cto_row1Data.nextYearActivity;

        //contracts awarded YTD(row2 Data)
        const cto_row2Data = getData(data, headerDesc.cto, rowDesc.contrAwrdYTD);
        //backlog begining
        let cto_bb_row2Data = +cto_row2Data.backlogBeginning || 0;
        //growth 
        if (companyCode === 'GCC') {
            if (+(cto_row2Data.growthYTD || 0) === 0) {
                cto_row2Data.growthYTD = (+cyContractData.FORECAST_CTO || 0);
            }
        }
        else if (companyCode === 'GSI') {
            cto_row2Data.growthYTD = (+cyContractData.CURR_YEAR_CTO || 0) + (+cyContractData.BACKLOG_CTO || 0);
        }
        else if (companyCode === 'GIC') {
            cto_row2Data.growthYTD = (+cyContractData.CURR_YEAR_CTO || 0) + (+cyContractData.BACKLOG_CTO || 0);
        }
        let cto_ag_row2Data = cto_row2Data.growthYTD;
        let cto_ag_row2Col2Data = cto_row2Data.growthBOY;
        let cto_ag_row2Col3Data = getTotal(
            cto_ag_row2Data,
            cto_ag_row2Col2Data,
            '',
            '',
        );

        //contract 
        if (companyCode === 'GCC') {
            if (+(cto_row2Data.contractYTD || 0) === 0) {
                cto_row2Data.contractYTD = (+cyContractData.CURR_YEAR_CTO || 0);
            }
        }
        else if (companyCode === 'GIC' || companyCode === 'GSI') {
            cto_row2Data.contractYTD = (+cyContractData.CURR_YEAR_CTO || 0);
        }
        // else data is already collected from the gfr data.
        let cto_ca_row2Data = cto_row2Data.contractYTD;
        let cto_ca_row2Col2Data = cto_row2Data.contractBOY;
        let cto_ca_row2Col3Data = getTotal(
            cto_ca_row2Data,
            cto_ca_row2Col2Data,
            '',
            '',
        );
        //backlog ending
        let cto_be_row2Data = cto_bb_row2Data + cto_ag_row2Col3Data - cto_ca_row2Col3Data;

        //next year
        let cto_row2nextYearGrowth = cto_row2Data.nextYearGrowth;
        let cto_row2nextYearActivity = cto_row2Data.nextYearActivity;

        //pending award(row3Data)
        const cto_row3Data = getData(jdeData, headerDesc.cto, rowDesc.pendingAwards);
        //backlog begining
        let cto_bb_row3Data = +cto_row3Data.backlogBeginning || 0;
        //growth
        let cto_ag_row3Data = +cto_row3Data.growthYTD || 0;
        let cto_ag_row3Col2Data = +paTotals.pendingAwardCTOValue || 0;
        let cto_ag_row3Col3Data = getTotal(
            cto_ag_row3Data,
            cto_ag_row3Col2Data,
            '',
            '',
        );
        //contract
        let cto_ca_row3Data = cto_row3Data.contractYTD;
        let cto_ca_row3Col2Data = +paTotals.pendingAwardFYEPrevCTO || 0;
        let cto_ca_row3Col3Data = getTotal(
            cto_ca_row3Data,
            cto_ca_row3Col2Data,
            '',
            '',
        );
        //backlog ending
        let cto_be_row3Data = cto_bb_row3Data + cto_ag_row3Col3Data - cto_ca_row3Col3Data;
        //next year
        let cto_row3nextYearGrowth = cto_row3Data.nextYearGrowth;
        let cto_row3nextYearActivity = cto_row3Data.nextYearActivity;

        //Other future work(row4 Data)
        const cto_row4Data = getData(jdeData, headerDesc.cto, rowDesc.otherWork);
        //backlog begining
        let cto_bb_row4Data = +cto_row4Data.backlogBeginning || 0;
        //growth
        let cto_ag_row4Data = +cto_row4Data.growthYTD || 0;
        let cto_ag_row4Col2Data = cto_row4Data.growthBOY || 0;
        let cto_ag_row4Col3Data = getTotal(
            cto_ag_row4Data,
            cto_ag_row4Col2Data,
            '',
            '',
        );
        //contract
        let cto_ca_row4Data = +cto_row4Data.contractYTD || 0;
        let cto_ca_row4Col2Data = +cto_row4Data.contractBOY || 0;
        let cto_ca_row4Col3Data = getTotal(
            cto_ca_row4Data,
            cto_ca_row4Col2Data,
            '',
            '',
        );
        //backlog ending
        let cto_be_row4Data = cto_bb_row4Data + cto_ag_row4Col3Data - cto_ca_row4Col3Data;
        //next year
        let cto_row4nextYearGrowth = cto_row4Data.nextYearGrowth;
        let cto_row4nextYearActivity = cto_row4Data.nextYearActivity;


        var cto_row5Data = getTotal(
            (+contractData.CURR_YEAR_CTO),
            cto_ca_row2Data,
            cto_ca_row3Data,
            cto_ca_row4Data,
        );

        var cto_row5Col2Data = getTotal(
            cto_ca_row1Col2Data,
            cto_ca_row2Col2Data,
            cto_ca_row3Col2Data,
            cto_ca_row4Col2Data,
        );
        

        var cto_row5Col3Data = getTotal(cto_row5Data, cto_row5Col2Data);

        let ctoMainHeaderTitle =
        {
            ctoTitle_col1: '',
            ctoTitle_col2: 'Backlog',
            ctoTitle_col3: 'Awards/Growth',
            ctoTitle_col4: 'Contract Activity',
            ctoTitle_col5: 'Backlog',
            ctoTitle_col6: 'Next Year'
        }

        let ctoTitle = {
            ctoTitle: '',
            cto_B_Begining: 'Beginning',
            cto_ag_YTD: 'Year-to-Date',
            cto_ag_BOY: 'Balance of Year',
            cto_ag_Total: 'Total',
            cto_ca_YTD: 'Year-To-Date',
            cto_ca_BOY: 'Balance of Year',
            cto_ca_Total: 'Total',
            cto_b_Ending: 'Ending',
            cto_ny_awards: 'Awards/Growth',
            cto_ny_Activity: 'Activity',
        }

        let ctoRows = [];
        let ctoRow1 = {
            ctoTitle: 'Contract Awarded in Prior Years',
            cto_B_Begining: cto_bb_row1Data,
            cto_ag_YTD: cto_ag_row1Data,
            cto_ag_BOY: cto_ag_row1Col2Data,
            cto_ag_Total: cto_ag_row1Col3Data,
            cto_ca_YTD: cto_ca_row1Data,
            cto_ca_BOY: cto_ca_row1Col2Data,
            cto_ca_Total: cto_ca_row1Col3Data,
            cto_b_Ending: cto_be_row1Data,
            cto_ny_awards: cto_row1nextYearGrowth,
            cto_ny_Activity: cto_row1nextYearActivity,
        }
        let ctoRow2 = {
            ctoTitle: 'Contract Awarded YTD',
            cto_B_Begining: cto_bb_row2Data,
            cto_ag_YTD: cto_ag_row2Data,
            cto_ag_BOY: cto_ag_row2Col2Data,
            cto_ag_Total: cto_ag_row2Col3Data,
            cto_ca_YTD: cto_ca_row2Data,
            cto_ca_BOY: cto_ca_row2Col2Data,
            cto_ca_Total: cto_ca_row2Col3Data,
            cto_b_Ending: cto_be_row2Data,
            cto_ny_awards: cto_row2nextYearGrowth,
            cto_ny_Activity: cto_row2nextYearActivity,
        }
        let ctoRow3 = {
            ctoTitle: 'Pending Awards',
            cto_B_Begining: cto_bb_row3Data,
            cto_ag_YTD: cto_ag_row3Data,
            cto_ag_BOY: cto_ag_row3Col2Data,
            cto_ag_Total: cto_ag_row3Col3Data,
            cto_ca_YTD: cto_ca_row3Data,
            cto_ca_BOY: ca_row3Col2Data,
            cto_ca_Total: cto_ca_row3Col3Data,
            cto_b_Ending: cto_be_row3Data,
            cto_ny_awards: cto_row3nextYearGrowth,
            cto_ny_Activity: cto_row3nextYearActivity,
        }
        let ctoRow4 = {
            ctoTitle: 'Other Future Work',
            cto_B_Begining: cto_bb_row4Data,
            cto_ag_YTD: cto_ag_row4Data,
            cto_ag_BOY: cto_ag_row4Col2Data,
            cto_ag_Total: cto_ag_row4Col3Data,
            cto_ca_YTD: cto_ca_row4Data,
            cto_ca_BOY: cto_ca_row4Col2Data,
            cto_ca_Total: cto_ca_row4Col3Data,
            cto_b_Ending: cto_be_row4Data,
            cto_ny_awards: cto_row4nextYearGrowth,
            cto_ny_Activity: cto_row4nextYearActivity,
        }
        let ctoRow5 = {
            ctoTitle: '2019 Plan',
            cto_B_Begining: '0',
            cto_ag_YTD: '71486',
            cto_ag_BOY: '0',
            cto_ag_Total: '71486',
            cto_ca_YTD: '654654',
            cto_ca_BOY: '654654',
            cto_ca_Total: '125555',
            cto_b_Ending: '371635',

        }
        let ctoTotal = {
            ctoTitle: 'Total',
            cto_B_Begining: 0,
            cto_ag_YTD: 0,
            cto_ag_BOY: 0,
            cto_ag_Total: 0,
            cto_ca_YTD: 0,
            cto_ca_BOY: 0,
            cto_ca_Total: 0,
            cto_b_Ending: 0,
            cto_ny_awards: 0,
            cto_ny_Activity: 0,
        }

        ctoRows.push(ctoRow1);
        ctoRows.push(ctoRow2);
        ctoRows.push(ctoRow3);
        ctoRows.push(ctoRow4);


        ctoRows.forEach(function (data, index) {
            ctoTotal.cto_B_Begining +=
                data.cto_B_Begining || 0;
            ctoTotal.cto_ag_YTD +=
                data.cto_ag_YTD || 0;
            ctoTotal.cto_ag_BOY +=
                data.cto_ag_BOY || 0;
            ctoTotal.cto_ag_Total +=
                data.cto_ag_Total || 0;
            ctoTotal.cto_ca_YTD +=
                data.cto_ca_YTD || 0;
            ctoTotal.cto_ca_BOY +=
                data.cto_ca_BOY || 0;
            ctoTotal.cto_ca_Total +=
                data.cto_ca_Total || 0;
            ctoTotal.cto_b_Ending +=
                data.cto_b_Ending || 0;
            ctoTotal.cto_ny_awards +=
                data.cto_ny_awards || 0;
            ctoTotal.cto_B_Begining +=
                data.cto_ny_Activity || 0;

        })
        //_________________content for CTO End__________________

        //__________________________content for Adjustment to Gross Profit start__________________________
        let atgp_row1Data = getData(
            data,
            headerDesc.adjToGrossProf,
            rowDesc.retro,
        );

        let atgp_YTD = (+plrData.C_RETRO_AMNT || 0);
        let atgp_BOY = +atgp_row1Data.contractBOY || 0;
        let atgp_Total = getTotal(
            atgp_YTD,
            atgp_BOY
        );
        let atgp_NextYear = +atgp_row1Data.nextYearGrowth || 0;

        //Yard and shop
        let atgp_row2Data = getData(data, headerDesc.adjToGrossProf, rowDesc.yardShop)
        let row2_atgp_YTD = +atgp_row2Data.contractYTD || 0;
        let row2_atgp_BOY = +atgp_row2Data.contractBOY || 0;
        let row2_atgp_Total = getTotal(
            row2_atgp_YTD,
            row2_atgp_BOY
        );
        let row2_atgp_NextYear = +atgp_row2Data.nextYearGrowth || 0;

        let atgp_row3Data = getTotal(
            atgp_YTD,
            row2_atgp_YTD
        );




        let atgpTitle = {
            atgpTitle: '',
            atgp_YTD: 'Year-To-Date',
            atgp_BOY: 'Balance of Year',
            atgp_Total: 'Total',
            atgp_NextYear: 'Next Year'

        }
        let atgpRows = [];
        let atgpRow1 = {
            atgpTitle: 'Retro',
            atgp_YTD: atgp_YTD,
            atgp_BOY: atgp_BOY,
            atgp_Total: atgp_Total,
            atgp_NextYear: atgp_NextYear

        }
        let atgpRow2 = {
            atgpTitle: 'Yard and Shop',
            atgp_YTD: row2_atgp_YTD,
            atgp_BOY: row2_atgp_BOY,
            atgp_Total: row2_atgp_Total,
            atgp_NextYear: row2_atgp_NextYear

        }
        let atgpRow4 = {
            atgpTitle: '2019 Plan',
            atgp_YTD: '646464',
            atgp_BOY: '646464',
            atgp_Total: '128128',

        }
        atgpRows.push(atgpRow1);
        atgpRows.push(atgpRow2);

        let atgpTotal = {
            atgpTitle: 'Total',
            atgp_YTD: 0,
            atgp_BOY: 0,
            atgp_Total: 0,
            atgp_NextYear: 0
        }

        atgpRows.forEach(function (data, index) {
            atgpTotal.atgp_YTD += +data.atgp_YTD || 0;
            atgpTotal.atgp_BOY += +data.atgp_BOY || 0;
            atgpTotal.atgp_Total += +data.atgp_Total || 0;
            atgpTotal.atgp_NextYear += +data.atgp_NextYear || 0;
        })

        //__________________________content for Adjustment to Gross Profit End__________________________

        //___________________________content for Adjusted Gross Profit start_____________________________


        let agp_row1Data = getData(
            data,
            headerDesc.adjGrossProf,
            rowDesc.total,
        );
        if (companyCode === "GCC") {
            agp_row1Data.contractYTD = (+plrData.D_ADJ_GROSS_PROF || 0);
        }
        else if (companyCode === "GIC" || companyCode === "GSI") {
            agp_row1Data.contractYTD = ((+cto_row5Data || 0)) - (+atgp_row3Data || 0);
        }
        let row1_agp_YTD = +agp_row1Data.contractYTD || 0;
        let row1_agp_BOY = +agp_row1Data.contractBOY || 0;
        let row1_agp_Total = getTotal(row1_agp_YTD, row1_agp_BOY);
        let row1_agp_NextYear = +agp_row1Data.nextYearGrowth || 0;

        let agpTitle = {
            agpTitle: '',
            agp_YTD: 'Year-To-Date',
            agp_BOY: 'Balance of Year',
            agp_Total: 'Total',
            agp_NextYear: 'Next Year'

        }
        let agpRow1 = {
            agpTitle: 'Total',
            agp_YTD: row1_agp_YTD,
            agp_BOY: row1_agp_BOY,
            agp_Total: row1_agp_Total,
            agp_NextYear: row1_agp_NextYear

        }
        let agpRow2 = {
            agpTitle: '2019',
            agp_YTD: '646464',
            agp_BOY: '646464',
            agp_Total: '128128',

        }

        //_____________________________content for Adusted Gross Profit End______________________________


        //______________________________content for the G&A Start_______________________________

        let ga_row1Data = getData(data,
            headerDesc.gAndA,
            rowDesc.total);
        let ga_row1Data_YTD = (+plrData.A_GA_EXPENSES || 0);
        let ga_row1Data_BOY = ga_row1Data.contractBOY;
        let ga_rowData_Total = getTotal(ga_row1Data_YTD, ga_row1Data_BOY);
        let ga_row1Data_NextYear = 5445;

        let gaTitle = {
            gaTitle: '',
            ga_YTD: 'Year-To-Date',
            ga_BOY: 'Balance of Year',
            ga_Total: 'Total',
            ga_NextYear: 'Next Year'

        }
        let gaRow1 = {
            gaTitle: 'Total',
            ga_YTD: ga_row1Data_YTD,
            ga_BOY: ga_row1Data_BOY,
            ga_Total: ga_rowData_Total,
            ga_NextYear: ga_row1Data_NextYear

        }
        let gaRow2 = {
            gaTitle: '2019 Plan',
            ga_YTD: '646464',
            ga_BOY: '646464',
            ga_Total: '128128',

        }

        //_______________________________content for the G&A End________________________________


        //Content for the OtherExpense Start

        let oe_row1Data = getData(data,
            headerDesc.otherExpense,
            rowDesc.total);
        let oe_row1Data_YTD = (+plrData.B_OTHER_INCOME_EXP || 0);
        let oe_row1Data_BOY = 0;
        let oe_row1Data_Total = getTotal(oe_row1Data_YTD, oe_row1Data_BOY);
        let oe_row1Data_NextYear = 5646;


        let oeTitle = {
            oeTitle: '',
            oe_YTD: 'Year-To-Date',
            oe_BOY: 'Balance of Year',
            oe_Total: 'Total',
            oe_NextYear: 'Next Year'

        }
        let oeRow1 = {
            oeTitle: 'Total',
            oe_YTD: oe_row1Data_YTD,
            oe_BOY: oe_row1Data_BOY,
            oe_Total: oe_row1Data_Total,
            oe_NextYear: oe_row1Data_NextYear

        }
        let oeRow2 = {
            oeTitle: '2019 Plan',
            oe_YTD: '646464',
            oe_BOY: '646464',
            oe_Total: '128128',

        }
        //Content for the OhterExpense ENd_______________

        //_________________Content for the Net Income Start_________________
        let ni_row1Data_YTD =
            row1_agp_YTD - ga_row1Data_YTD - oe_row1Data_YTD;

        let ni_row1Data_BOY = (+row1_agp_BOY || 0) - (+ga_row1Data_BOY || 0) - (+oe_row1Data_BOY || 0);
        let ni_row1Data_Total = ni_row1Data_YTD + ni_row1Data_BOY;
        let ni_row1Data_NextYear = 0;
        let neTitle = {
            neTitle: '',
            ne_YTD: 'Year-To-Date',
            ne_BOY: 'Balance of Year',
            ne_Total: 'Total',
            ne_NextYear: 'Next Year'

        }
        let neRow1 = {
            neTitle: 'Total',
            ne_YTD: ni_row1Data_YTD,
            ne_BOY: ni_row1Data_BOY,
            ne_Total: ni_row1Data_Total,
            ne_NextYear: ni_row1Data_NextYear

        }
        let neRow2 = {
            neTitle: '2019 Plan',
            ne_YTD: '646464',
            ne_BOY: '646464',
            ne_Total: '128128',

        }

        //____________________Content for the Net Income End__________________




        return (
            <Document>
                <Page size={[1200, 1400]} style={PdfStyles.commonStyles.body} >

                    {/* _____________PDF Header Section Start________________ */}
                    <View style={{ flexDirection: 'row', marginTop: 5, marginBottom: 15, }}>
                        <View style={{ textAlign: 'center', flexDirection: 'row', minHeight: '70px', width: '80%', marginHorizontal: 100 }}>
                            <Image src={companyImage} style={{ paddingLeft: '300px',width:'650px' }} />
                        </View>

                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 4, marginBottom: 20, }}>
                        <View style={{ textAlign: 'center', flexDirection: 'row' }}>
                            <Text>{'Business Projections As Of ' + this.props.selectedMonth.label + " " + this.props.selectedYear.label}</Text>
                        </View>
                    </View>

                    <View style={{ flexDirection: 'row', marginBottom:1, }}>
                        <View style={PdfStyles.commonStyles.leftColumn}>
                            <LabelAndValue label={'Company'} value={this.props.selectedCompany.label} />
                            <LabelAndValue label={'Market'} value={this.props.selectedDivision.label} />
                            <LabelAndValue label={'Year'} value={this.props.selectedYear.label} />
                            <LabelAndValue label={'Month'} value={this.props.selectedMonth.label} />
                            <Text>   </Text>
                            <Text style={PdfStyles.commonStyles.Header}>All Numbers in 000`s</Text>
                        </View >
                        <View style={PdfStyles.commonStyles.rightColumn}>
                            <View>
                                <Text>  </Text>
                                <Text>  </Text>
                                <Text> </Text>
                                <Text></Text>
                                <Text style={{ textAlign: 'right', marginRight: "20px", fontSize: 12,fontWeight:'bold' }}>{this.props.todayDate}</Text>
                            </View>
                        </View>

                    </View>
                    {/* ________________PDF header section End______________ */}


                    {/* _________________________Revenue Section  Started_______________________*/}
                    <View style={{ height: '300px', width: '100%', backgroundColor: companyColor }}>
                        <View style={{ paddingTop: '20px' }}>

                            <View style={{
                                height: '240px', width: '95%', backgroundColor: 'white', marginLeft: '20px', marginRight: '-5px', paddingTop: '0px', paddingBottom: '5px'
                                , marginBottom: '10px', borderRadius: '25'
                            }}>
                                <View style={{ width: '100%', height: '30px', borderRadius: 25, backgroundColor: '#DDDCDC', marginBottom: '5px', marginTop: '0px', paddingTop: '5px' }}>
                                    <Text style={{ paddingLeft: '10px', fontSize: 20, fontWeight: 'bold' }}>REVENUE</Text>
                                </View>

                                <View style={{ alignItems: 'stretch', width: '95%', height: '210px', marginLeft: '8px' }}>
                                    <View style={{ flexDirection: 'row', width: '100%', height: '20px' }}>
                                        <RevenueRowMainHeader revenueRow={revenueMainHeaderTitle} cellBackground='#595959' color='white' />
                                    </View>
                                    <View style={{ flexDirection: 'row', width: '100%', height: '20px' }}>
                                        <RevenueTitle revenueRow={revenueTitle} cellBackground='#A29A95' color='white' />
                                    </View>
                                    <View style={{ flexDirection: 'row', width: '100%', height: '28px' }}>
                                        <RevenueRow revenueRow={revenueRow1} cellBackground='white' color='black' />
                                    </View>
                                    <View style={{ flexDirection: 'row', width: '100%', height: '20px' }}>
                                        <RevenueRow revenueRow={revenueRow2} cellBackground='white' color='black' />
                                    </View>
                                    <View style={{ flexDirection: 'row', width: '100%', height: '20px' }}>
                                        <RevenueRow revenueRow={revenueRow3} cellBackground='white' color='black' />
                                    </View>
                                    <View style={{ flexDirection: 'row', width: '100%', height: '20px' }}>
                                        <RevenueRow revenueRow={revenueRow4} cellBackground='white' color='black' />
                                    </View>
                                    <View style={{ width: '104%', flexDirection: 'row', borderBottomWidth: 2, borderBottomColor: '#112131', borderBottomStyle: 'solid', alignItems: 'stretch', marginBottom: '5px' }}></View>
                                    <View style={{ flexDirection: 'row', width: '100%', height: '20px' }}>
                                        <RevenueRow revenueRow={revenueTotal} cellBackground='white' color='black' />
                                    </View>
                                    <View style={{ flexDirection: 'row', width: '100%', height: '20px' }}>
                                        <RevenueRow revenueRow={revenueRow5} cellBackground='white' color='black' />
                                    </View>

                                </View>
                            </View>


                        </View>
                    </View>
                    {/* Revenue Section End */}

                    {/* _____________________CTO Section Start________________________________ */}

                    <View style={{ height: '300px', width: '100%', backgroundColor: companyColor }}>
                        <View style={{ paddingTop: '20px' }}>

                            <View style={{
                                height: '240px', width: '95%', backgroundColor: 'white', marginLeft: '20px', marginRight: '-5px', paddingTop: '0px', paddingBottom: '5px'
                                , borderRadius: 25
                            }}>
                                <View style={{ width: '100%', height: '30px', borderRadius: 25, backgroundColor: '#DDDCDC', marginBottom: '5px', paddingTop: '-5px' }}>
                                    <Text style={{ paddingLeft: '10px', fontSize: 20, fontWeight: 'bold' }}>CTO</Text>
                                </View>

                                <View style={{ alignItems: 'stretch', width: '95%', height: '210px', marginLeft: '8px' }}>
                                    <View style={{ flexDirection: 'row', width: '100%', height: '20px' }}>
                                        <CTORowMainHeader ctoRow={ctoMainHeaderTitle} cellBackground='#595959' color='white' />
                                    </View>
                                    <View style={{ flexDirection: 'row', width: '100%', height: '20px' }}>
                                        <CTOTitle ctoRow={ctoTitle} cellBackground='#A29A95' color='white' />
                                    </View>
                                    <View style={{ flexDirection: 'row', width: '100%', height: '28px' }}>
                                        <CTORow ctoRow={ctoRow1} cellBackground='white' color='black' />
                                    </View>
                                    <View style={{ flexDirection: 'row', width: '100%', height: '20px' }}>
                                        <CTORow ctoRow={ctoRow2} cellBackground='white' color='black' />
                                    </View>
                                    <View style={{ flexDirection: 'row', width: '100%', height: '20px' }}>
                                        <CTORow ctoRow={ctoRow3} cellBackground='white' color='black' />
                                    </View>
                                    <View style={{ flexDirection: 'row', width: '100%', height: '20px' }}>
                                        <CTORow ctoRow={ctoRow4} cellBackground='white' color='black' />
                                    </View>
                                    <View style={{ width: '104%', flexDirection: 'row', borderBottomWidth: 2, borderBottomColor: '#112131', borderBottomStyle: 'solid', alignItems: 'stretch', marginBottom: '5px' }}></View>
                                    <View style={{ flexDirection: 'row', width: '100%', height: '20px' }}>
                                        <CTORow ctoRow={ctoTotal} cellBackground='white' color='black' />
                                    </View>
                                    <View style={{ flexDirection: 'row', width: '100%', height: '20px' }}>
                                        <CTORow ctoRow={ctoRow5} cellBackground='white' color='black' />
                                    </View>

                                </View>
                            </View>


                        </View>
                    </View>
                    {/* ____________________________CTO Section End______________________ */}

                    {/* __________Adjustment to gross profit and Adjusted gross profit section start__________ */}
                    <View style={{ height: '200px', width: '100%', backgroundColor: companyColor, marginTop: '20px' }}>
                        <View style={{ paddingTop: '10px' }}>

                            <View style={{ display: 'flex', flexDirection: 'row', marginTop: '15px' }}>
                                {/* Adjustment to gross profit start*/}

                                <View style={{
                                    height: '160px', width: '45%', backgroundColor: 'white', marginLeft: '25px', marginRight: '-5px', paddingTop: '0px', paddingBottom: '5px'
                                    , borderRadius: 25
                                }}>
                                    <View style={{ width: '100%', height: '30px', borderRadius: 25, backgroundColor: '#DDDCDC', marginBottom: '5px', paddingTop: '-5px' }}>
                                        <Text style={{ paddingLeft: '10px', fontSize: 20, fontWeight: 'bold' }}>ADJUSTMENT TO GROSS PROFIT</Text>
                                    </View>
                                    <View style={{ alignItems: 'stretch', width: '95%', height: '130px', marginLeft: '8px' }}>
                                        <View style={{ flexDirection: 'row', width: '100%', height: '20px' }}>
                                            <ATGPTitle atgpRow={atgpTitle} cellBackground='#A29A95' color='white' />
                                        </View>
                                        <View style={{ flexDirection: 'row', width: '100%', height: '20px' }}>
                                            <ATGPRow atgpRow={atgpRow1} cellBackground='white' color='black' />
                                        </View>
                                        <View style={{ flexDirection: 'row', width: '100%', height: '20px' }}>
                                            <ATGPRow atgpRow={atgpRow2} cellBackground='white' color='black' />
                                        </View>
                                        <View style={{ width: '90%', flexDirection: 'row', borderBottomWidth: 2, borderBottomColor: '#112131', borderBottomStyle: 'solid', alignItems: 'stretch', marginBottom: '5px' }}></View>
                                        <View style={{ flexDirection: 'row', width: '100%', height: '20px' }}>
                                            <ATGPRow atgpRow={atgpTotal} cellBackground='white' color='black' />
                                        </View>
                                        <View style={{ flexDirection: 'row', width: '100%', height: '20px' }}>
                                            <ATGPRow atgpRow={atgpRow4} cellBackground='white' color='black' />
                                        </View>


                                    </View>

                                </View>
                                {/* Adjustment to gross profit end */}

                                {/* Adjusted Gross Profit start */}

                                <View style={{
                                    height: '160px', width: '45%', backgroundColor: 'white', marginLeft: '45px', marginRight: '-5px', paddingTop: '0px', paddingBottom: '5px'
                                    , borderRadius: 25
                                }}>
                                    <View style={{ width: '100%', height: '30px', borderRadius: 25, backgroundColor: '#DDDCDC', marginBottom: '5px', paddingTop: '-5px' }}>
                                        <Text style={{ paddingLeft: '10px', fontSize: 20, fontWeight: 'bold' }}>ADJUSTED GROSS PROFIT</Text>
                                    </View>
                                    <View style={{ alignItems: 'stretch', width: '95%', height: '130px', marginLeft: '8px' }}>
                                        <View style={{ flexDirection: 'row', width: '100%', height: '20px' }}>
                                            <AGPTitle agpRow={agpTitle} cellBackground='#A29A95' color='white' />
                                        </View>
                                        <View style={{ flexDirection: 'row', width: '100%', height: '20px' }}>
                                            <AGPRow agpRow={agpRow1} cellBackground='white' color='black' />
                                        </View>
                                        <View style={{ flexDirection: 'row', width: '100%', height: '20px' }}>
                                            <AGPRow agpRow={agpRow2} cellBackground='white' color='black' />
                                        </View>


                                    </View>

                                </View>

                                {/* Adjusted Gross Profit End */}
                            </View>
                        </View>

                    </View>
                    {/* ___________Adjustment to gross profit and Adjusted gross profit section end________________ */}

                    {/* __________G&A and Other Income Expense section start__________ */}

                    <View style={{ height: '200px', width: '100%', backgroundColor: companyColor, marginTop: '30px' }}>
                        <View style={{ paddingTop: '10px' }}>

                            <View style={{ display: 'flex', flexDirection: 'row', marginTop: '15px' }}>

                                {/* G&A Section  start*/}
                                <View style={{
                                    height: '160px', width: '45%', backgroundColor: 'white', marginLeft: '25px', marginRight: '-5px', paddingTop: '0px', paddingBottom: '5px'
                                    , float: 'left', borderRadius: 25
                                }}>
                                    <View style={{ width: '100%', height: '30px', borderRadius: 25, backgroundColor: '#DDDCDC', marginBottom: '5px', paddingTop: '-5px' }}>
                                        <Text style={{ paddingLeft: '10px', fontSize: 20, fontWeight: 'bold' }}>G&A</Text>
                                    </View>
                                    <View style={{ alignItems: 'stretch', width: '95%', height: '130px', marginLeft: '8px' }}>
                                        <View style={{ flexDirection: 'row', width: '100%', height: '20px' }}>
                                            <GATitle gaRow={gaTitle} cellBackground='#A29A95' color='white' />
                                        </View>
                                        <View style={{ flexDirection: 'row', width: '100%', height: '20px' }}>
                                            <GARow gaRow={gaRow1} cellBackground='white' color='black' />
                                        </View>
                                        <View style={{ flexDirection: 'row', width: '100%', height: '20px' }}>
                                            <GARow gaRow={gaRow2} cellBackground='white' color='black' />
                                        </View>


                                    </View>

                                </View>

                                {/* G&A Section end */}

                                {/* Other Expense Section Start */}
                                <View style={{
                                    height: '160px', width: '45%', backgroundColor: 'white', marginLeft: '20px', marginLeft: '45px', marginRight: '-5px', paddingTop: '0px', paddingBottom: '5px'
                                    , float: 'left', borderRadius: 25
                                }}>
                                    <View style={{ width: '100%', height: '30px', borderRadius: 25, backgroundColor: '#DDDCDC', marginBottom: '5px', paddingTop: '-5px' }}>
                                        <Text style={{ paddingLeft: '10px', fontSize: 20, fontWeight: 'bold' }}>OTHER EXPENSE/(INCOME)</Text>
                                    </View>
                                    <View style={{ alignItems: 'stretch', width: '95%', height: '130px', marginLeft: '8px' }}>
                                        <View style={{ flexDirection: 'row', width: '100%', height: '20px' }}>
                                            <OETitle oeRow={oeTitle} cellBackground='#A29A95' color='white' />
                                        </View>
                                        <View style={{ flexDirection: 'row', width: '100%', height: '20px' }}>
                                            <OERow oeRow={oeRow1} cellBackground='white' color='black' />
                                        </View>
                                        <View style={{ flexDirection: 'row', width: '100%', height: '20px' }}>
                                            <OERow oeRow={oeRow2} cellBackground='white' color='black' />
                                        </View>


                                    </View>


                                </View>

                                {/* Other Expense Section End */}
                            </View>
                        </View>

                    </View>

                    {/* ___________G&A and Other Income Expense section end________________ */}

                    {/* ____________Net Income Section Start____________________ */}
                    <View style={{ width: '60%', marginLeft: '240px', height: '200px', backgroundColor: companyColor, marginTop: '30px' }}>
                        <View style={{ paddingTop: '20px' }}>
                            <View style={{
                                height: '160px', width: '95%', backgroundColor: 'white', marginLeft: '20px', marginRight: '-5px', paddingTop: '0px', paddingBottom: '5px'
                                , borderRadius: 25
                            }}>
                                <View style={{ width: '100%', height: '30px', borderRadius: 25, backgroundColor: '#DDDCDC', marginBottom: '5px', paddingTop: '-5px' }}>
                                    <Text style={{ paddingLeft: '10px', fontSize: 20, fontWeight: 'bold' }}>NET INCOME</Text>
                                </View>
                                <View style={{ alignItems: 'stretch', width: '95%', height: '130px', marginLeft: '40px' }}>
                                    <View style={{ flexDirection: 'row', width: '100%', height: '20px' }}>
                                        <NETitle neRow={neTitle} cellBackground='#A29A95' color='white' />
                                    </View>
                                    <View style={{ flexDirection: 'row', width: '100%', height: '20px' }}>
                                        <NERow neRow={neRow1} cellBackground='white' color='black' />
                                    </View>
                                    <View style={{ flexDirection: 'row', width: '100%', height: '20px' }}>
                                        <NERow neRow={neRow2} cellBackground='white' color='black' />
                                    </View>


                                </View>
                            </View>
                        </View>

                    </View>
                    {/* ______________Net Income Section End_______________________ */}

                    {/* _____________________________Pending Award Section Started________________________ */}
                    <View style={{ minHeight: '200px', width: '100%', backgroundColor: companyColor, marginTop: '20px', display: 'relative' }}>
                        <View style={{ paddingTop: '20px' }}>

                            <View style={{
                                minHeight: '160px', width: '95%', borderRadius: 25, backgroundColor: 'white', marginLeft: '20px', marginRight: '-5px', paddingBottom: '5px'
                            ,marginBottom:'25px'}}>
                                <View style={{ width: '100%', minHeight: '30px', borderRadius: 25, backgroundColor: '#DDDCDC', marginBottom: '5px', paddingTop: '-5px' }}>
                                    <Text style={{ paddingLeft: '10px', fontSize: 20, fontWeight: 'bold' }}>Pending Awards</Text>
                                </View>

                                <View style={{ alignItems: 'stretch', width: '100%', marginLeft: '2%' }}>
                                    <View style={{ flexDirection: 'row', width: '100%', }}>
                                        <PaTitle paRow={paTitle} cellBackground='#595959' color='white' />
                                    </View>
                                    {/* <View style={{ flexDirection: 'row', width: '100%', }}>
                                        <PaRow paRow={paRow1} cellBackground='white' color='black' />
                                    </View>

                                    <View style={{ flexDirection: 'row', width: '100%', }}>
                                        <PaRow paRow={paRow2} cellBackground='white' color='black' />
                                    </View> */}
                                    {
                                        paRows.map(function (data, index) {
                                            return (
                                                <View style={{ flexDirection: 'row', width: '100%', }}>
                                                    <PaRow paRow={data} cellBackground='white' color='black' />
                                                </View>
                                            )
                                        })
                                    }
                                    <View style={{ width:'94%',flexDirection: 'row', borderBottomWidth: 2, borderBottomColor: '#112131', borderBottomStyle: 'solid', alignItems: 'stretch', }}></View>
                                    <View style={{ flexDirection: 'row', width: '100%', }}>
                                        <PaTotal paRow={paTotals} cellBackground='white' color='black' />
                                    </View>
                                </View>
                            </View>

                        </View>
                    </View>
                    {/* ____________________________Pending Award Section End_____________________________ */}

                    <Text style={styles.pageNumber} render={({ pageNumber, totalPages }) => (
                        `${pageNumber} / ${totalPages}`
                    )} fixed />
                </Page>
            </Document>
        );
    }
}

export default GfrPdfDocument;
