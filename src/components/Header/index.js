/* eslint-disable react/button-has-type */
/* eslint react/prop-types: 0 */
/* eslint react/no-unescaped-entities": 0 */
import React from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router-dom';

import Link from '../Link';
import './Header.css';
import cms from '../../messages';
import logoUrl from '../Images/companyLogo.png';
import icon from '../Images/logoff_icon.png';


// const crumb = (part, partIndex, parts) => {
//   const path = ['', ...parts.slice(0, partIndex + 1)].join("/");
//   return <Link key={path} to={path} >{part}</Link>
// }

const Breadcrumbs = () => <Route path="*" render={props => {
  let parts = props.location.pathname.split("/");
  // console.log("parts",props.location.pathname);// eslint-disable-line
  let url;
  let place = parts[parts.length - 2];
  if(place === 'gfrReport')
  {
    place = "FORECAST REPORT";
    url = "/#/gfrReport";
  }
  // console.log('place',place);// eslint-disable-line

  parts = parts.slice(1, parts.length - 1);
  // console.log('second parts',parts);// eslint-disable-line
  // should be used for the child routhe
  // const name = parts.map(crumb)+" > "+ place; // eslint-disable-line
  const name = " > "+ place; // eslint-disable-line
  // console.log('name',name);// eslint-disable-line
  return <a href={url} style={{color:'white',textDecoration:'none'}}>{name}</a>
}} />


const Header = () => (
  <div className="headerroot">
    <div>
      <a href="/#/logout">
        {/* <button className="logoutButton"><i className="fa fa-arrow-circle-o-left"  style={{fontSize:'20px'}}/> {cms.logout}</button> */}
        <button className="logoutButton"><i><img src={icon} alt="" width="25px" height="20px" /></i> {cms.logout}</button>
      </a>
    </div>
    <div className="brandContainer">
      <Link to="/">
        <img src={logoUrl} alt="brandLogo" className="imageIcon"  style={{width:'120px',height:'35px'}}/>
      </Link>
    </div>

    <div className="linkContainer" style={{marginTop:'10px',fontSize:'20px'}}>
     <a href="/" style={{color:'white',textDecoration:'none'}}>DASHBOARD</a>
      <Breadcrumbs />
    </div>
  </div>
);



Header.contextTypes = {
  pathname: PropTypes.string,
};

export default Header;