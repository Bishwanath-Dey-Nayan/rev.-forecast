/* eslint-disable css-modules/no-unused-class */
/* eslint-disable */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { currencyFormat, getTotal } from '../../utils/mathAndCurrency';

import s from './GridRowsColumn5.module.css';
import GridEditableCell from '../GridCells/GridEditableCell';
import { headerDesc, rowDesc, reportModeLists } from '../../constants/GfrReport.constant';

class GridRowsColumn5 extends Component {
  static propTypes = {
    row1Data: PropTypes.oneOfType([
      PropTypes.object, // eslint-disable-line
      PropTypes.string,
    ]),
    row2Data: PropTypes.oneOfType([
      PropTypes.object, // eslint-disable-line
      PropTypes.string,
    ]),
    row3Data: PropTypes.object, // eslint-disable-line
    row4Data: PropTypes.object, // eslint-disable-line
    row6Data: PropTypes.string,
    row1Col2Data: PropTypes.oneOfType([
      PropTypes.object, // eslint-disable-line
      PropTypes.string,
    ]),
    row2Col2Data: PropTypes.oneOfType([
      PropTypes.object, // eslint-disable-line
      PropTypes.string,
    ]),
    row3Col2Data: PropTypes.oneOfType([
      PropTypes.object, // eslint-disable-line
      PropTypes.string,
    ]),
    row4Col2Data: PropTypes.oneOfType([
      PropTypes.object, // eslint-disable-line
      PropTypes.string,
    ]),
    row6Col2Data: PropTypes.string,
    token: PropTypes.string,
    col1Hidden: PropTypes.bool,
    fourRowGroup: PropTypes.bool,
    twoRowGroup: PropTypes.bool,
    yellowBack: PropTypes.bool,
  };

  static defaultProps = {
    row1Data: {},
    row2Data: {},
    row3Data: {},
    row4Data: {},
    row6Data: '',
    row1Col2Data: {},
    row2Col2Data: {},
    row3Col2Data: {},
    row4Col2Data: {},
    row6Col2Data: '',
    col1Hidden: false,
    fourRowGroup: false,
    twoRowGroup: false,
    token: null,
    yellowBack: false,
  };

  constructor(props) {
    super(props);
    const { row1Data, row2Data, row3Data, row4Data, row6Data } = this.props;
    const {
      row1Col2Data,
      row2Col2Data,
      row3Col2Data,
      row4Col2Data,
      row6Col2Data,
    } = this.props;
    this.state = {
      row1Clicked: false,
      row2Clicked: false,
      row3Clicked: false,
      row4Clicked: false,
      row6Clicked: false,
      row1col2Clicked: false,
      row2col2Clicked: false,
      row3col2Clicked: false,
      row4col2Clicked: false,
      row6col2Clicked: false,
      row1Data,
      row2Data,
      row3Data,
      row4Data,
      row6Data,
      row1Col2Data,
      row2Col2Data,
      row3Col2Data,
      row4Col2Data,
      row6Col2Data,
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps !== prevState) {
      return {
        netIncomeTotal: nextProps.netIncomeTotal,
        row1Data: nextProps.row1Data,
        row2Data: nextProps.row2Data,
        row3Data: nextProps.row3Data,
        row4Data: nextProps.row4Data,
        row5Data: nextProps.row5Data,
        row6Data: nextProps.row6Data,

        row1Col2Data: nextProps.row1Col2Data,
        row2Col2Data: nextProps.row2Col2Data,
        row3Col2Data: nextProps.row3Col2Data,
        row4Col2Data: nextProps.row4Col2Data,
        row5Col2Data: nextProps.row5Col2Data,
        row6Col2Data: nextProps.row6Col2Data,
      };
    } else return null;
  }
  onChange = e => {
    const {
      row1Data,
      row2Data,
      row3Data,
      row4Data,
      row1Col2Data,
      row2Col2Data,
      row3Col2Data,
      row4Col2Data,
    } = this.state;

    const { fourRowGroup, twoRowGroup } = this.props;

    var row0 = {}; // eslint-disable-line
    switch (e.target.name) {
      case 'row1Data':
        row0 = row1Data;
        row0.nextYearGrowth = e.target.value;
        this.setState({
          row1Data: row0,
        });
        break;
      case 'row2Data':
        row0 = row2Data;
        row0.nextYearGrowth = e.target.value;
        this.setState({
          row2Data: row0,
        });
        break;
      case 'row3Data':
        row0 = row3Data;
        row0.nextYearGrowth = e.target.value;
        this.setState({
          row3Data: row0,
        });
        break;
      case 'row4Data':
        row0 = row4Data;
        row0.nextYearGrowth = e.target.value;
        this.setState({
          row4Data: row0,
        });
        break;
      case 'row6Data':
        this.setState({
          row6Data: e.target.value,
        });
        break;
      case 'row1Col2Data':
        row0 = row1Col2Data;
        row0.nextYearActivity = e.target.value;
        this.setState({
          row1Col2Data: row0,
        });
        break;
      case 'row2Col2Data':
        if (twoRowGroup) {
          this.setState({
            row2Col2Data: e.target.value,
          });
        } else {
          row0 = row2Col2Data;
          row0.nextYearActivity = e.target.value;
          this.setState({
            row2Col2Data: row0,
          });
        }
        break;
      case 'row3Col2Data':
        row0 = row3Col2Data;
        row0.nextYearActivity = e.target.value;
        this.setState({
          row3Col2Data: row0,
        });
        break;
      case 'row4Col2Data':
        if (fourRowGroup) {
          this.setState({
            row4Col2Data: e.target.value,
          });
        } else {
          row0 = row4Col2Data;
          row0.nextYearActivity = e.target.value;
          this.setState({
            row4Col2Data: row0,
          });
        }
        break;
      case 'row6Col2Data':
        this.setState({
          row6Col2Data: e.target.value,
        });
        break;
      default:
        row0 = {};
        break;
    }
  };

  setClicked = e => {
    this.setState({ [e.target.id]: true });
  };

  setClickedDflt = e => {
    const { token, fourRowGroup, twoRowGroup } = this.props;
    const {
      row1Data,
      row2Data,
      row3Data,
      row4Data,
      row1Col2Data,
      row2Col2Data,
      row3Col2Data,
      row4Col2Data,
    } = this.state;

    var row0 = {}; // eslint-disable-line
    switch (e.target.name) {
      case 'row1Data':
        row0 = row1Data;
        row0.nextYearGrowth = e.target.value;
        if (!row0.rowDescription) {
          if (
            this.props.headerDescProp === headerDesc.rev ||
            this.props.headerDescProp === headerDesc.cto
          )
            row0.rowDescription = rowDesc.contrAwrdInPy;
          else if (this.props.headerDescProp === headerDesc.adjToGrossProf)
            row0.rowDescription = rowDesc.retro;
        }
        if (!row0.row) row0.row = 0;
        if (!row0.headerRow) row0.headerRow = 0;

        this.setState({
          row1Data: row0,
        });
        break;
      case 'row2Data':
        row0 = row2Data;
        row0.nextYearGrowth = e.target.value;
        if (!row0.rowDescription) {
          if (
            this.props.headerDescProp === headerDesc.rev ||
            this.props.headerDescProp === headerDesc.cto
          )
            row0.rowDescription = rowDesc.contrAwrdYTD;
          else if (this.props.headerDescProp === headerDesc.adjToGrossProf)
            row0.rowDescription = rowDesc.yardShop;
        }
        if (!row0.row) row0.row = 1;
        if (!row0.headerRow) row0.headerRow = 1;
        this.setState({
          row2Data: row0,
        });
        break;
      case 'row3Data':
        row0 = row3Data;
        row0.nextYearGrowth = e.target.value;
        if (!row0.rowDescription) {
          if (
            this.props.headerDescProp === headerDesc.rev ||
            this.props.headerDescProp === headerDesc.cto
          )
            row0.rowDescription = rowDesc.pendingAwards;
        }
        if (!row0.row) row0.row = 2;
        if (!row0.headerRow) row0.headerRow = 2;
        this.setState({
          row3Data: row0,
        });
        break;
      case 'row4Data':
        row0 = row4Data;
        row0.nextYearGrowth = e.target.value;
        if (!row0.rowDescription) {
          if (
            this.props.headerDescProp === headerDesc.rev ||
            this.props.headerDescProp === headerDesc.cto
          )
            row0.rowDescription = rowDesc.otherWork;
        }
        if (!row0.row) row0.row = 3;
        if (!row0.headerRow) row0.headerRow = 3;
        this.setState({
          row4Data: row0,
        });
        break;
      case 'row6Data':
        this.setState({
          row6Data: e.target.value,
        });
        break;
      case 'row1Col2Data':
        row0 = row1Col2Data;
        row0.nextYearActivity = e.target.value;
        if (!row0.rowDescription) {
          if (
            this.props.headerDescProp === headerDesc.rev ||
            this.props.headerDescProp === headerDesc.cto
          )
            row0.rowDescription = rowDesc.contrAwrdInPy;
          else if (this.props.headerDescProp === headerDesc.adjToGrossProf)
            row0.rowDescription = rowDesc.retro;
        }
        if (!row0.row) row0.row = 0;
        if (!row0.headerRow) row0.headerRow = 0;
        this.setState({
          row1Col2Data: row0,
        });
        break;
      case 'row2Col2Data':
        if (twoRowGroup && fourRowGroup) {
          this.setState({
            row2Col2Data: e.target.value,
          });
        } else {
          row0 = row2Col2Data;
          row0.nextYearActivity = e.target.value;

          if (!row0.rowDescription) {
            if (
              this.props.headerDescProp === headerDesc.rev ||
              this.props.headerDescProp === headerDesc.cto
            )
              row0.rowDescription = rowDesc.contrAwrdYTD;
            else if (this.props.headerDescProp === headerDesc.adjToGrossProf)
              row0.rowDescription = rowDesc.yardShop;
          }
          if (!row0.row) row0.row = 1;
          if (!row0.headerRow) row0.headerRow = 1;

          this.setState({
            row2Col2Data: row0,
          });
        }
        break;
      case 'row3Col2Data':
        row0 = row3Col2Data;
        row0.nextYearActivity = e.target.value;
        if (!row0.rowDescription) {
          if (
            this.props.headerDescProp === headerDesc.rev ||
            this.props.headerDescProp === headerDesc.cto
          )
            row0.rowDescription = rowDesc.pendingAwards;
        }
        if (!row0.row) row0.row = 2;
        if (!row0.headerRow) row0.headerRow = 2;
        this.setState({
          row3Col2Data: row0,
        });
        break;
      case 'row4Col2Data':
        if (fourRowGroup) {
          this.setState({
            row4Col2Data: e.target.value,
          });
        } else {
          row0 = row4Col2Data;
          row0.nextYearActivity = e.target.value;
          if (!row0.rowDescription) {
            if (
              this.props.headerDescProp === headerDesc.rev ||
              this.props.headerDescProp === headerDesc.cto
            )
              row0.rowDescription = rowDesc.otherWork;
          }
          if (!row0.row) row0.row = 3;
          if (!row0.headerRow) row0.headerRow = 3;

          this.setState({
            row4Col2Data: row0,
          });
        }
        break;
      case 'row6Col2Data':
        this.setState({
          row6Col2Data: e.target.value,
        });
        break;
      default:
        row0 = {};
        break;
    }
    this.props.updateGfrCellData(row0);
    this.setState({ [e.target.id]: false });
  };

  render() {
    const { col1Hidden, fourRowGroup, twoRowGroup, yellowBack } = this.props;
    const {
      row1Data,
      row1Clicked,
      row2Data,
      row2Clicked,
      row3Data,
      row3Clicked,
      row4Data,
      row4Clicked,
      row6Data,
      row6Clicked,
    } = this.state;
    const {
      row1Col2Data,
      row1col2Clicked,
      row2Col2Data,
      row2col2Clicked,
      row3Col2Data,
      row3col2Clicked,
      row4Col2Data,
      row4col2Clicked,
      row6Col2Data,
      row6col2Clicked,
    } = this.state;

    return (
      <div className={s.mainContainer}>
        <GridEditableCell
          cellId="row1Clicked"
          name="row1Data"
          isClicked={row1Clicked}
          cssClass={col1Hidden ? s.row1DataNV : s.row1Data}
          onBlurCallback={this.setClickedDflt}
          cellValue={row1Data.nextYearGrowth}
          onChangeCallback={this.onChange}
          onClickCallBack={this.setClicked}
          onKeyDownCallback={this.setClicked}
          isEdtiable={(this.props.reportMode == reportModeLists.bothSelected)}
        />

        <GridEditableCell
          cellId="row2Clicked"
          name="row2Data"
          isClicked={row2Clicked}
          cssClass={col1Hidden ? s.row2DataNV : s.row2Data}
          onBlurCallback={this.setClickedDflt}
          cellValue={row2Data.nextYearGrowth}
          onChangeCallback={this.onChange}
          onClickCallBack={this.setClicked}
          onKeyDownCallback={this.setClicked}
          isEdtiable={(this.props.reportMode == reportModeLists.bothSelected)}
        />

        <GridEditableCell
          cellId="row3Clicked"
          name="row3Data"
          isClicked={row3Clicked}
          cssClass={fourRowGroup ? s.row3DataNV : s.row3Data}
          onBlurCallback={this.setClickedDflt}
          cellValue={row3Data.nextYearGrowth}
          onChangeCallback={this.onChange}
          onClickCallBack={this.setClicked}
          onKeyDownCallback={this.setClicked}
          isEdtiable={(this.props.reportMode == reportModeLists.bothSelected)}
        />

        <GridEditableCell
          cellId="row4Clicked"
          name="row4Data"
          isClicked={row4Clicked}
          cssClass={fourRowGroup ? s.row4DataNV : s.row4Data}
          onBlurCallback={this.setClickedDflt}
          cellValue={row4Data.nextYearGrowth}
          onChangeCallback={this.onChange}
          onClickCallBack={this.setClicked}
          onKeyDownCallback={this.setClicked}
          isEdtiable={(this.props.reportMode == reportModeLists.bothSelected)}
        />

        <div className={fourRowGroup ? s.row5DataNV : s.row5Data}>
          <div id="row5Clicked">
            {currencyFormat(
              getTotal(
                row1Data.nextYearGrowth,
                row2Data.nextYearGrowth,
                row3Data.nextYearGrowth,
                row4Data.nextYearGrowth,
              ).toString(),
            )}
          </div>
        </div>

        <GridEditableCell
          cellId="row6Clicked"
          name="row6Data"
          isClicked={row6Clicked}
          cssClass={s.row6DataNV}
          onBlurCallback={this.setClickedDflt}
          cellValue={row6Data.nextYearGrowth}
          onChangeCallback={this.onChange}
          onClickCallBack={this.setClicked}
          onKeyDownCallback={this.setClicked}
          isEdtiable={(this.props.reportMode == reportModeLists.bothSelected)}
        />

        <GridEditableCell
          cellId="row1col2Clicked"
          name="row1Col2Data"
          isClicked={row1col2Clicked}
          cssClass={s.row1col2Data}
          onBlurCallback={this.setClickedDflt}
          cellValue={
            fourRowGroup && twoRowGroup
              ? row1Col2Data
              : row1Col2Data.nextYearActivity
          }
          onChangeCallback={this.onChange}
          onClickCallBack={this.setClicked}
          onKeyDownCallback={this.setClicked}
          isEdtiable={!(fourRowGroup && twoRowGroup)}
          isEdtiable={(this.props.reportMode == reportModeLists.bothSelected)}
        />

        <GridEditableCell
          cellId="row2col2Clicked"
          name="row2Col2Data"
          isClicked={row2col2Clicked}
          cssClass={
            fourRowGroup && !twoRowGroup
              ? s.row2col2DataTotal
              : fourRowGroup && twoRowGroup
                ? s.row2col2DataNV
                : s.row2col2Data
          }
          onBlurCallback={this.setClickedDflt}
          cellValue={
            twoRowGroup && fourRowGroup
              ? row2Col2Data
              : row2Col2Data.nextYearActivity
          }
          onChangeCallback={this.onChange}
          onClickCallBack={this.setClicked}
          onKeyDownCallback={this.setClicked}
          isEdtiable={(this.props.reportMode == reportModeLists.bothSelected) && !(fourRowGroup && twoRowGroup)}
        />

        <div className={twoRowGroup ? s.row3col2DataNV : s.row3col2Data}>
          {fourRowGroup ? (
            // <div id="row3col2Clicked">
            //   {currencyFormat(
            //     fourRowGroup
            //       ? getTotal(
            //           row1Col2Data.nextYearActivity,
            //           row2Col2Data.nextYearActivity,
            //           '',
            //           '',
            //         ).toString()
            //       : row3Col2Data.nextYearActivity,
            //   )}
            // </div>
            <GridEditableCell
              cellId="row3col2Clicked"
              name="row3Col2Data"
              isClicked={row3col2Clicked}
              cssClass={
                twoRowGroup ? s.row3col2DataNV : s.row3col2Data
              }
              onBlurCallback={this.setClickedDflt}
              cellValue={
                (
                  fourRowGroup
                    ? getTotal(
                      row1Col2Data.nextYearActivity,
                      row2Col2Data.nextYearActivity,
                      '',
                      '',
                    ).toString()
                    : row3Col2Data.nextYearActivity
                )
              }

              isEdtiable={(this.props.reportMode == reportModeLists.bothSelected) && !(fourRowGroup && twoRowGroup)}
            />

          ) : (
              <div>
                {row3col2Clicked ? (
                  <input
                    autoFocus={true}
                    pattern="[0-9]*"
                    type="number"
                    onBlur={this.setClickedDflt}
                    value={
                      fourRowGroup
                        ? getTotal(
                          row1Col2Data.nextYearActivity,
                          row2Col2Data.nextYearActivity,
                          '',
                          '',
                        ).toString()
                        : row3Col2Data.nextYearActivity
                    }
                    onChange={this.onChange}
                    name="row3Col2Data"
                    id="row3col2Clicked"
                  />
                ) : (
                    // <div
                    //   id="row3col2Clicked"
                    //   onClick={this.setClicked}
                    //   role="button"
                    //   onKeyDown={this.setClicked}
                    //   tabIndex="0"
                    // >
                    //   {currencyFormat(
                    //     fourRowGroup
                    //       ? getTotal(
                    //           row1Col2Data.nextYearActivity,
                    //           row2Col2Data.nextYearActivity,
                    //           '',
                    //           '',
                    //         ).toString()
                    //       : row3Col2Data.nextYearActivity,
                    //   )}
                    // </div>
                    <GridEditableCell
                      cellId="row3col2Clicked"
                      name="row3Col2Data"
                      isClicked={row3col2Clicked}
                      cssClass={
                        twoRowGroup ? s.row3col2DataNV : s.row3col2Data
                      }
                      onBlurCallback={this.setClickedDflt}
                      cellValue={
                        (
                          fourRowGroup
                            ? getTotal(
                              row1Col2Data.nextYearActivity,
                              row2Col2Data.nextYearActivity,
                              '',
                              '',
                            ).toString()
                            : row3Col2Data.nextYearActivity
                        )
                      }

                      isEdtiable={(this.props.reportMode == reportModeLists.bothSelected)}
                    />
                  )}
              </div>
            )}
        </div>
        <div
          className={
            twoRowGroup
              ? s.row4col2DataNV
              : fourRowGroup && !twoRowGroup
                ? s.row4col2DataNB
                : s.row4col2Data
          }
        >
          {row4col2Clicked ? (
            <input
              autoFocus={true}
              pattern="[0-9]*"
              type="number"
              onBlur={this.setClickedDflt}
              value={row4Col2Data.nextYearActivity}
              onChange={this.onChange}
              name="row4Col2Data"
              id="row4col2Clicked"
            />
          ) : (
              // <div
              //   id="row4col2Clicked"
              //   onClick={this.setClicked}
              //   role="button"
              //   onKeyDown={this.setClicked}
              //   tabIndex="0"
              // >
              //   {currencyFormat(
              //     fourRowGroup ? row4Col2Data : row4Col2Data.nextYearActivity,
              //   )}
              // </div>
              <GridEditableCell
                cellId="row4col2Clicked"
                name="row4Col2Data"
                isClicked={row4col2Clicked}
                cssClass={
                  twoRowGroup
                    ? s.row4col2DataNV
                    : fourRowGroup && !twoRowGroup
                      ? s.row4col2DataNB
                      : s.row4col2Data
                }
                onBlurCallback={this.setClickedDflt}
                cellValue={
                  (
                    fourRowGroup ? row4Col2Data : row4Col2Data.nextYearActivity
                  )
                }
                //onChangeCallback={this.onChange}
                onClickCallBack={this.setClicked}
                onKeyDownCallback={this.setClicked}
                isEdtiable={(this.props.reportMode == reportModeLists.bothSelected)}
              />

            )}
        </div>
        {/* <div className={fourRowGroup ? s.row5col2DataNV : s.row5col2Data}>
          <div id="row5col2Clicked">
            {currencyFormat(
              getTotal(
                row1Data.nextYearActivity,
                row2Data.nextYearActivity,
                row3Data.nextYearActivity,
                row4Data.nextYearActivity,
              ).toString(),
            )}
          </div>
        </div> */}
        <GridEditableCell
          cellId="row5col2Clicked"
          name="row5Col2Data"
          cssClass={
            fourRowGroup ? s.row5col2DataNV : s.row5col2Data
          }
          onBlurCallback={this.setClickedDflt}
          cellValue={
            
            (
              getTotal(
                row1Data.nextYearActivity,
                row2Data.nextYearActivity,
                row3Data.nextYearActivity,
                row4Data.nextYearActivity,
              ).toString()
            )
            
          }

          isEdtiable={(this.props.reportMode == reportModeLists.bothSelected) && !(fourRowGroup && twoRowGroup)}
        />

        <div className={s.row6col2DataNV}>
          {row6col2Clicked ? (
            <input
              autoFocus={true}
              pattern="[0-9]*"
              type="number"
              onBlur={this.setClickedDflt}
              value={row6Col2Data}
              onChange={this.onChange}
              name="row6Col2Data"
              id="row6col2Clicked"
            />
          ) : (
              <div
                id="row6col2Clicked"
                onClick={this.setClicked}
                role="button"
                onKeyDown={this.setClicked}
                tabIndex="0"
              >
                {currencyFormat(row6Col2Data)}
              </div>
            )}
        </div>
      </div>
    );
  }
}

export default GridRowsColumn5;
