/* eslint-disable css-modules/no-unused-class */
/* eslint-disable */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { currencyFormat, getTotal } from '../../utils/mathAndCurrency';
import s from './GridRowsColumn4.module.css';
import { insertUpdateForecastReport } from '../../data';
import GridEditableCell from '../GridCells/GridEditableCell';
import { headerDesc, rowDesc, reportModeLists } from '../../constants/GfrReport.constant';

class GridRowsColumn4 extends Component {
  static propTypes = {
    row1Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row2Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row3Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row4Data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // eslint-disable-line
    row6Data: PropTypes.string,
    token: PropTypes.string,
  };

  static defaultProps = {
    row1Data: {},
    row2Data: {},
    row3Data: {},
    row4Data: {},
    row6Data: '',
    token: null,
  };

  constructor(props) {
    super(props);
    const { row1Data, row2Data, row3Data, row4Data, row6Data } = this.props;

    this.state = {
      row1Clicked: false,
      row2Clicked: false,
      row3Clicked: false,
      row4Clicked: false,
      row6Clicked: false,
      row1Data,
      row2Data,
      row3Data,
      row4Data,
      row6Data,
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps !== prevState) {
      return {
        netIncomeTotal: nextProps.netIncomeTotal,
        row1Data: nextProps.row1Data,
        row2Data: nextProps.row2Data,
        row3Data: nextProps.row3Data,
        row4Data: nextProps.row4Data,
        row5Data: nextProps.row5Data,
        row6Data: nextProps.row6Data,
      };
    } else return null;
  }
  onChange = e => {
    const { row1Data, row2Data, row3Data, row4Data } = this.state;

    var row0 = {}; // eslint-disable-line
    switch (e.target.name) {
      case 'row1Data':
        row0 = row1Data;
        row0.backlogEnding = e.target.value;
        this.setState({
          row1Data: row0,
        });
        break;
      case 'row2Data':
        row0 = row2Data;
        row0.backlogEnding = e.target.value;
        this.setState({
          row2Data: row0,
        });
        break;
      case 'row3Data':
        row0 = row3Data;
        row0.backlogEnding = e.target.value;
        this.setState({
          row3Data: row0,
        });
        break;
      case 'row4Data':
        row0 = row4Data;
        row0.backlogEnding = e.target.value;
        this.setState({
          row4Data: row0,
        });
        break;
      case 'row6Data':
        this.setState({
          row6Data: e.target.value,
        });
        break;
      default:
        row0 = {};
        break;
    }
  };

  setClicked = e => {
    this.setState({ [e.target.id]: true });
  };

  setClickedDflt = e => {
    const { token } = this.props;
    const { row1Data, row2Data, row3Data, row4Data } = this.state;

    var row0 = {}; // eslint-disable-line
    switch (e.target.name) {
      case 'row1Data':
        row0 = row1Data;
        row0.backlogEnding = e.target.value;
        insertUpdateForecastReport(token, row0)
          .then(resp => resp.json())
          .then(res => console.info('result:', res))
          .catch(updErr => console.error('updErr:', updErr));
        this.setState({
          row1Data: row0,
        });
        break;
      case 'row2Data':
        row0 = row2Data;
        row0.backlogEnding = e.target.value;
        insertUpdateForecastReport(token, row0)
          .then(resp => resp.json())
          .then(res => console.info('result:', res))
          .catch(updErr => console.error('updErr:', updErr));
        this.setState({
          row2Data: row0,
        });
        break;
      case 'row3Data':
        row0 = row3Data;
        row0.backlogEnding = e.target.value;
        insertUpdateForecastReport(token, row0)
          .then(resp => resp.json())
          .then(res => console.info('result:', res))
          .catch(updErr => console.error('updErr:', updErr));
        this.setState({
          row3Data: row0,
        });
        break;
      case 'row4Data':
        row0 = row4Data;
        row0.backlogEnding = e.target.value;
        insertUpdateForecastReport(token, row0)
          .then(resp => resp.json())
          .then(res => console.info('result:', res))
          .catch(updErr => console.error('updErr:', updErr));
        this.setState({
          row4Data: row0,
        });
        break;
      case 'row6Data':
        this.setState({
          row6Data: e.target.value,
        });
        break;
      default:
        row0 = {};
        break;
    }

    this.setState({ [e.target.id]: false });
  };

  render() {
    const {
      row1Data,
      row1Clicked,
      row2Data,
      row2Clicked,
      row3Data,
      row3Clicked,
      row4Data,
      row4Clicked,
      row6Data,
      row6Clicked,
    } = this.state;

    const row5Data = getTotal(row1Data, row2Data, row3Data, row4Data);
    return (
      <div className={s.mainContainer}>
        {/* <div className={s.row1Data}>
          <div id="row1Clicked">{currencyFormat(row1Data)}</div>
        </div> */}
        <GridEditableCell
          cellId="row1Clicked"
          name="row1Data"
          cssClass={s.row1Data}
          cellValue={row1Data}
          isEdtiable={(this.props.reportMode == reportModeLists.bothSelected)}
        />
        {/* <div className={s.row2Data}>
          <div id="row2Clicked">{currencyFormat(row2Data)}</div>
        </div> */}
        <GridEditableCell
          cellId="row2Clicked"
          name="row2Data"
          cssClass={s.row2Data}
          cellValue={row2Data}
          isEdtiable={(this.props.reportMode == reportModeLists.bothSelected)}
        />
        {/* <div className={s.row3Data}>
          <div id="row3Clicked">{currencyFormat(row3Data)}</div>
        </div> */}
        <GridEditableCell
          cellId="row3Clicked"
          name="row3Data"
          cssClass={s.row3Data}
          cellValue={row3Data}
          isEdtiable={(this.props.reportMode == reportModeLists.bothSelected)}
        />
        {/* <div className={s.row4Data}>
          <div id="row4Clicked">{currencyFormat(row4Data)}</div>
        </div> */}
        <GridEditableCell
          cellId="row4Clicked"
          name="row4Data"
          cssClass={s.row4Data}
          cellValue={row4Data}
          isEdtiable={(this.props.reportMode == reportModeLists.bothSelected)}
        />
        {/* <div className={s.row5Data}>
          <div id="row5Clicked">{currencyFormat(row5Data)}</div>
        </div> */}
        <GridEditableCell
          cellId="row5Clicked"
          name="row5Data"
          cssClass={s.row5Data}
          cellValue={row5Data}
          isEdtiable={(this.props.reportMode == reportModeLists.bothSelected)}
        />
        <div className={s.row6Data}>
          {row6Clicked ? (
            <input
              autoFocus={true}
              pattern="[0-9]*"
              type="number"
              onBlur={this.setClickedDflt}
              value={row6Data}
              onChange={this.onChange}
              name="row6Data"
              id="row6Clicked"
            />
          ) : (
              // <div
              //   id="row6Clicked"
              //   onClick={this.setClicked}
              //   role="button"
              //   onKeyDown={this.setClicked}
              //   tabIndex="0"
              // >
              //   {currencyFormat(row6Data)}
              // </div>
              <GridEditableCell
              cellId="row6Clicked"
              name="row6Data"
              isClicked={row6Clicked}
              cssClass={s.row6Data}
               cellValue={row6Data}
              //onChangeCallback={this.onChange}
              onClickCallBack={this.setClicked}
              onKeyDownCallback={this.setClicked}
              isEdtiable={(this.props.reportMode == reportModeLists.bothSelected)}
            />
            )}
        </div>
      </div>
    );
  }
}

export default GridRowsColumn4;
