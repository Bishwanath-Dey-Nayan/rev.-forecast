/* eslint-disable */

import React from 'react';
import PropTypes from 'prop-types';
import * as Datetime from 'react-datetime';
import { currencyFormat, getTotal } from '../../utils/mathAndCurrency';

import s from './PendingAwards.module.css';
import Popup from 'reactjs-popup';
import { reportModeLists } from '../../constants/GfrReport.constant';
import 'react-datetime/css/react-datetime.css';
import moment from 'moment';

const PendingAwards = ({
  title,
  children,
  bottomLine,
  addPendingAwards,
  colseAddPendingAward,
  openAwardInputField,
  onChangeHandler,
  onChangeHandlerDate,
  onPendingAwardSubmit,
  pendingAwardsInputRow,
  paTotals,
  colorCode,
  selectedYear,
  pendingAwardFormErrors,
  reportMode,
  companyCode,
  selectedMonth,
  isMaintenanceClicked

}) => {
  //let currentMonth = moment().month(this.props.selectedMonth.label).format("M");
  console.log("currentMonth", selectedMonth);
  let isNewYear = (selectedMonth > 9) ? true : false;
  let disable = (isMaintenanceClicked === true) ? false : true;
  return (
    <div
      className={s.topHeaderWithoutColor}
      style={{ backgroundColor: `${colorCode}` }}
    >
      <div
        className={s.container}
        style={{
          background: '#fff',
          position: 'relative',
          marginLeft: '31px',
          padding: '2px',
          paddingTop: '0px',
          paddingLeft: '0px',
          width: '96%',
          top: '38px',
          marginBottom: '30px',
          paddingRight: '0px',
          float: 'left',
        }}
      >
        <div className={s.tophr}>
          <span
            style={{
              lineHeight: '50px',
              fontSize: '28px',
              color: '#000',
              fontWeight: 'bold',
              paddingLeft: '10px',
            }}
          >
            {title}{' '}
          </span>

          <div className={s.btnContainer} style={{ marginBottom: '3px' }}>
            {(reportMode == reportModeLists.bothSelected) && (<button
              onClick={addPendingAwards}
              className={s.addNewButton}
              type="button"
            >
              Add
          </button>)}
          </div>
        </div>
        <div style={{ float: 'left', width: '100%', marginLeft: '15px', overflow: 'hidden' }}>
          <div
            className={s.mainContainer}
            style={{ paddingLeft: '0px', border: 'none' }}
          >
            {companyCode === "ALL" && (
              <div className={s.gridContainer2CellNV} style={{ marginLeft: '5.1px' }}>
                <div
                  className={s.topCell}
                  style={{ paddingBottom: '11px', paddingTop: '11px', textAlign: 'left', paddingLeft: '5px' }}
                >
                  Company
            </div>
              </div>)
            }
            <div className={s.gridContainer2CellNV} style={{ width: '170px' }}>
              <div
                className={s.topCell}
                style={{
                  paddingBottom: '11px',
                  paddingTop: '11px',
                  marginLeft: '0px',
                  textAlign: 'left',
                  paddingLeft: '5px'
                }}
              >
                Pending Awards
            </div>
            </div>
            <div className={s.gridContainer2CellNV}>
              <div
                className={s.topCell}
                style={{ paddingBottom: '11px', paddingTop: '11px' }}
              >
                Exp Awd Date
            </div>
            </div>
            <div className={s.gridContainer2CellNV}>
              <div
                className={s.topCell}
                style={{ paddingBottom: '11px', paddingTop: '11px', textAlign: 'right', paddingRight: '5px' }}
              >
                Revenue Value
            </div>
            </div>
            <div className={s.gridContainer2CellNV}>
              <div
                className={s.topCell}
                style={{ paddingBottom: '11px', paddingTop: '11px', textAlign: 'right', paddingRight: '5px' }}
              >
                CTO Value
            </div>
            </div>
            <div className={s.gridContainer2CellNV}>
              <div
                className={s.topCell}
                style={{
                  paddingBottom: '11px',
                  paddingTop: '11px',
                  marginLeft: '0px',
                  textAlign: 'right',
                  paddingRight: '5px'
                }}
              >
                {isNewYear ? ` FYE ${selectedYear} Rev` : `FYE ${selectedYear - 1}  Rev`}
              </div>
            </div>
            <div className={s.gridContainer2CellNV}>
              <div
                className={s.topCell}
                style={{ paddingBottom: '11px', paddingTop: '11px', textAlign: 'right', paddingRight: '5px' }}
              >
                {isNewYear ? ` FYE ${selectedYear} CTO` : `FYE ${selectedYear - 1} CTO`}
              </div>
            </div>
            <div className={s.gridContainer2CellNV} style={{ width: '170px' }}>
              <div
                className={s.topCell}
                style={{
                  paddingBottom: '11px',
                  paddingTop: '11px',
                  marginLeft: '0px',
                  textAlign: 'right',
                  paddingRight: '5px'
                }}
              >
                {isNewYear ? `${selectedYear + 1} Backlog Revenue` : `${selectedYear} Backlog Rev`}
              </div>
            </div>
            <div className={s.gridContainer2CellNV} style={{ width: '170px' }}>
              <div
                className={s.topCell}
                style={{
                  paddingBottom: '11px',
                  paddingTop: '11px',
                  marginLeft: '0px',
                  textAlign: 'right',
                  paddingRight: '5px'
                }}
              >
                {isNewYear ? `${selectedYear + 1} Backlog CTO` : `${selectedYear} Backlog CTO`}
              </div>
            </div>
            <div className={s.gridContainer2CellNV} style={{ width: '170px' }}>
              <div
                className={s.topCell}
                style={{
                  paddingBottom: '11px',
                  paddingTop: '11px',
                  marginLeft: '0px',
                  textAlign: 'right',
                  paddingRight: '5px'
                }}
              >
                {isNewYear ? `${selectedYear + 2} Backlog Rev.` : `${selectedYear + 1} Backlog Rev.`}
              </div>
            </div>
            <div className={s.gridContainer2CellNV} style={{ width: '158px' }}>
              <div
                className={s.topCell}
                style={{
                  paddingBottom: '11px',
                  paddingTop: '11px',
                  marginLeft: '0px',
                  textAlign: 'right',
                  paddingRight: '5px'
                }}
              >
                {isNewYear ? `${selectedYear + 2} Backlog CTO` : `${selectedYear + 1} Backlog CTO`}
              </div>
            </div>
          </div>
          <div className={s.rowTitles}>{children}</div>
          <div
            className={s.mainContainer}
            style={{ paddingLeft: '0px', border: 'none', borderTop: '1px solid' }}
          >

            {(companyCode === "ALL") ? (<div
              className={s.titleColBottomLine}
              style={{ paddingLeft: '0px', width: '290px', marginLeft: '9px', marginLeft: '150px' }}
            >
              Total
                 </div>

            ) :
              (<div
                className={s.titleColBottomLine}
                style={{ paddingLeft: '0px', width: '290px' }}
              >
                Total
                 </div>)
            }


            <div
              className={s.bottomCell}
              style={{ marginLeft: '10px', border: 'none', width: '10px' }}
            />

            <div
              className={s.bottomCell}
              style={{ border: 'none', textAlign: 'right', width: '140px' }}
            >
              {currencyFormat(paTotals.pendingAwardRevenueValue)}
            </div>
            <div
              className={s.bottomCell}
              style={{ textAlign: 'right', width: '140px' }}
            >
              {currencyFormat(paTotals.pendingAwardCTOValue)}
            </div>
            <div
              className={s.bottomCell}
              style={{ textAlign: 'right', width: '140px' }}
            >
              {currencyFormat(paTotals.pendingAwardFYEPrevRev)}
            </div>
            <div
              className={s.bottomCell}
              style={{ textAlign: 'right', width: '140px' }}
            >
              {currencyFormat(paTotals.pendingAwardFYEPrevCTO)}
            </div>

            <div
              className={s.bottomCell}
              style={{ textAlign: 'right', width: '170px' }}
            >
              {currencyFormat(paTotals.pendingAwardCurBacklogRev)}
            </div>

            <div
              className={s.bottomCell}
              style={{ textAlign: 'right', width: '170px' }}
            >
              {currencyFormat(paTotals.pendingAwardCurBacklogCTO)}
            </div>

            <div
              className={s.bottomCell}
              style={{ textAlign: 'right', width: '170px' }}
            >
              {currencyFormat(paTotals.pendingAwardFutureBacklogRev)}
            </div>

            <div
              className={s.bottomCell}
              style={{ textAlign: 'right', width: '170px' }}
            >
              {currencyFormat(paTotals.pendingAwardFutureBacklogCTO)}
            </div>

            {/* </div> */}
          </div>
          {openAwardInputField ? (
            <Popup
              open={openAwardInputField}
              position="right center"
              contentStyle={{ width: '550px' }}
            >
              <div className="modal">
                <a className={s.modalClose} onClick={colseAddPendingAward}>
                  &times;
              </a>
                <div className={s.modalHeader}> Pending Award </div>
                <div className={s.modalContent}>
                  <div style={{ textAlign: 'center' }}>
                    <label style={{ color: 'red' }}> {pendingAwardFormErrors.pendingAwardTitle.length != null && (
                      pendingAwardFormErrors.pendingAwardTitle
                    )}</label>
                  </div>
                  <form onSubmit={onPendingAwardSubmit} autoComplete="off">
                    <div className={s.newRow}>
                      <div className={s.modalRow}>
                        <label className={s.modalRowLabel}><span className="required" style={{ color: 'red' }}>*</span>Project Name</label>
                        <input
                          name="pendingAwardTitle"
                          className={s.modalRowInput}
                          value={pendingAwardsInputRow.pendingAwardTitle}
                          onChange={onChangeHandler}
                          required
                        />
                        <input type="checkbox" name="maintenanceFlag" className={s.modalCheckBox}
                          onChange={onChangeHandler}
                        />
                        <label className={s.modalCheckBoxLabel}>Maintenance Project</label>
                      </div>
                      <div className={s.modalRow}>
                        <label className={s.modalRowLabel}>Exp Awd Date</label>
                        <Datetime
                          name="pendingAwardExpDate"
                          closeOnSelect={true}
                          closeOnTab={true}
                          className={s.modalRowInput}
                          inputProps={{
                            placeholder: '',
                            className: s.modalRowInput,
                            readOnly: true,
                          }}
                          timeFormat={false}
                          dateFormat="MM/DD/YY"
                          value={moment(pendingAwardsInputRow.pendingAwardExpDate).format('MM-DD-YY')}
                          onChange={onChangeHandlerDate}
                        />
                        <input type="checkbox" name="priorYearFlag" style={{ marginLeft: '8px' }}
                          onChange={onChangeHandler}
                          disabled={disable}
                        />
                        <label className={s.modalCheckBoxLabel}>Prior Year Contract</label>
                      </div>
                      <div className={s.modalRow}>
                        <label className={s.modalRowLabel}>Revenue Value</label>
                        <input
                          name="pendingAwardRevenueValue"
                          type="number"
                          className={s.modalRowInput}
                          value={pendingAwardsInputRow.pendingAwardRevenueValue}
                          onChange={onChangeHandler}
                        />
                      </div>
                      <div className={s.modalRow}>
                        <label className={s.modalRowLabel}>CTO Value</label>
                        <input
                          name="pendingAwardCTOValue"
                          type="number"
                          className={s.modalRowInput}
                          value={pendingAwardsInputRow.pendingAwardCTOValue}
                          onChange={onChangeHandler}
                        />
                      </div>
                      <div className={s.modalRow}>
                        <label className={s.modalRowLabel}>
                          {isNewYear ? `${selectedYear} FYE Prev Revenue` : `${selectedYear - 1} FYE Prev Revenue`}
                          {/* {selectedYear - 1} FYE Prev Revenue */}
                        </label>
                        <input
                          name="pendingAwardFYEPrevRev"
                          type="number"
                          className={s.modalRowInput}
                          value={pendingAwardsInputRow.pendingAwardFYEPrevRev}
                          onChange={onChangeHandler}
                        />
                      </div>
                      <div className={s.modalRow}>
                        <label className={s.modalRowLabel}>
                          {isNewYear ? `${selectedYear} FYE Prev CTO` : `${selectedYear - 1} FYE Prev CTO`}
                          {/* {selectedYear - 1} FYE Prev CTO */}
                        </label>
                        <input
                          name="pendingAwardFYEPrevCTO"
                          type="number"
                          className={s.modalRowInput}
                          value={pendingAwardsInputRow.pendingAwardFYEPrevCTO}
                          onChange={onChangeHandler}
                        />
                      </div>
                      <div className={s.modalRow}>
                        <label className={s.modalRowLabel}>
                          {isNewYear ? `${selectedYear + 1} Backlog Revenue` : `${selectedYear} Backlog Revenue`}
                          {/* {selectedYear} Backlog Revenue */}
                        </label>
                        <input
                          name="pendingAwardCurBacklogRev"
                          type="number"
                          className={s.modalRowInput}
                          value={pendingAwardsInputRow.pendingAwardCurBacklogRev}
                          onChange={onChangeHandler}
                        />
                      </div>
                      <div className={s.modalRow}>
                        <label className={s.modalRowLabel}>
                          {isNewYear ? `${selectedYear + 1} Backlog CTO` : `${selectedYear} Backlog CTO`}
                          {/* {selectedYear} Backlog CTO */}
                        </label>
                        <input
                          name="pendingAwardCurBacklogCTO"
                          type="number"
                          className={s.modalRowInput}
                          value={pendingAwardsInputRow.pendingAwardCurBacklogCTO}
                          onChange={onChangeHandler}
                        />
                      </div>
                      <div className={s.modalRow}>
                        <label className={s.modalRowLabel}>
                          {isNewYear ? `${selectedYear + 2} Future Backlog Revenue` : `${selectedYear + 1} Future Backlog Revenue`}
                          {/* {selectedYear + 1} Future Backlog Revenue */}
                        </label>
                        <input
                          name="pendingAwardFutureBacklogRev"
                          type="number"
                          className={s.modalRowInput}
                          value={
                            pendingAwardsInputRow.pendingAwardFutureBacklogRev
                          }
                          onChange={onChangeHandler}
                        />
                      </div>
                      <div className={s.modalRow}>
                        <label className={s.modalRowLabel}>
                          {isNewYear ? `${selectedYear + 2} Future Backlog CTO` : `${selectedYear + 1} Future Backlog CTO`}
                          {/* {selectedYear + 1} Future Backlog CTO */}
                        </label>
                        <input
                          name="pendingAwardFutureBacklogCTO"
                          type="number"
                          className={s.modalRowInput}
                          value={
                            pendingAwardsInputRow.pendingAwardFutureBacklogCTO
                          }
                          onChange={onChangeHandler}
                        />
                      </div>
                    </div>
                  </form>
                </div>
                <div className={s.modalActions}>
                  <button
                    className={s.cancelButton}
                    onClick={() => {
                      colseAddPendingAward();
                    }}
                  >
                    Cancel
                </button>
                  <button
                    type="submit"
                    className={s.submitButton}
                    value="Submit"
                    onClick={onPendingAwardSubmit}
                  >
                    Submit
                </button>
                </div>
              </div>
            </Popup>
          ) : null}
          {false ? (
            <div>
              <form onSubmit={onPendingAwardSubmit} autoComplete="off">
                <div className={s.newRow}>
                  <ul>
                    <li>
                      <div
                        style={{ width: '224px', marginLeft: '10px' }}
                        className={s.titleCol}
                      >
                        <input
                          name="pendingAwardTitle"
                          style={{ width: '100%' }}
                          value={pendingAwardsInputRow.pendingAwardTitle}
                          onChange={onChangeHandler}
                        />
                      </div>
                      <div
                        className={s.col1}
                        style={{ width: '100px', marginLeft: '10px' }}
                      >
                        <input
                          name="pendingAwardExpDate"
                          style={{ width: '100%' }}
                          value={pendingAwardsInputRow.pendingAwardExpDate}
                          onChange={onChangeHandler}
                        />
                      </div>
                      <div
                        className={s.col2}
                        style={{ width: '100px', marginLeft: '10px' }}
                      >
                        <input
                          name="pendingAwardRevenueValue"
                          type="number"
                          style={{ width: '100%' }}
                          value={pendingAwardsInputRow.pendingAwardRevenueValue}
                          onChange={onChangeHandler}
                        />
                      </div>
                      <div
                        className={s.col3}
                        style={{ width: '100px', marginLeft: '10px' }}
                      >
                        <input
                          name="pendingAwardCTOValue"
                          type="number"
                          style={{ width: '100%' }}
                          value={pendingAwardsInputRow.pendingAwardCTOValue}
                          onChange={onChangeHandler}
                        />
                      </div>
                      <div
                        className={s.col4}
                        style={{ width: '100px', marginLeft: '10px' }}
                      >
                        <input
                          name="pendingAwardFYEPrevRev"
                          type="number"
                          style={{ width: '100%' }}
                          value={pendingAwardsInputRow.pendingAwardFYEPrevRev}
                          onChange={onChangeHandler}
                        />
                      </div>
                      <div
                        className={s.col5}
                        style={{ width: '100px', marginLeft: '10px' }}
                      >
                        <input
                          name="pendingAwardFYEPrevCTO"
                          type="number"
                          style={{ width: '100%' }}
                          value={pendingAwardsInputRow.pendingAwardFYEPrevCTO}
                          onChange={onChangeHandler}
                        />
                      </div>
                      <div
                        className={s.col6}
                        style={{ width: '100px', marginLeft: '10px' }}
                      >
                        <input
                          name="pendingAwardCurBacklogRev"
                          type="number"
                          style={{ width: '100%' }}
                          value={pendingAwardsInputRow.pendingAwardCurBacklogRev}
                          onChange={onChangeHandler}
                        />
                      </div>
                      <div
                        className={s.col7}
                        style={{ width: '100px', marginLeft: '10px' }}
                      >
                        <input
                          name="pendingAwardCurBacklogCTO"
                          type="number"
                          style={{ width: '100%' }}
                          value={pendingAwardsInputRow.pendingAwardCurBacklogCTO}
                          onChange={onChangeHandler}
                        />
                      </div>
                      <div
                        className={s.col8}
                        style={{ width: '100px', marginLeft: '10px' }}
                      >
                        <input
                          name="pendingAwardFutureBacklogRev"
                          type="number"
                          style={{ width: '100%' }}
                          value={
                            pendingAwardsInputRow.pendingAwardFutureBacklogRev
                          }
                          onChange={onChangeHandler}
                        />
                      </div>
                      <div
                        className={s.col9}
                        style={{ width: '100px', marginLeft: '10px' }}
                      >
                        <input
                          name="pendingAwardFutureBacklogCTO"
                          type="number"
                          style={{ width: '100%' }}
                          value={
                            pendingAwardsInputRow.pendingAwardFutureBacklogCTO
                          }
                          onChange={onChangeHandler}
                        />
                      </div>

                      <button
                        type="submit"
                        className={s.submitButton}
                        value="Submit"
                      >
                        Submit
                    </button>
                    </li>
                  </ul>
                </div>
              </form>
            </div>
          ) : null}
        </div>
      </div>
    </div>
  );
}

PendingAwards.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  bottomLine: PropTypes.arrayOf(
    PropTypes.shape({
      idx: PropTypes.number,
      titleBottomLine: PropTypes.string,
      col1BottomLine: PropTypes.string,
      col2BottomLine: PropTypes.string,
      col3BottomLine: PropTypes.string,
      col4BottomLine: PropTypes.string,
      col5BottomLine: PropTypes.string,
      col6BottomLine: PropTypes.string,
      col7BottomLine: PropTypes.string,
      col8BottomLine: PropTypes.string,
      col9BottomLine: PropTypes.string,
    }),
  ),
  addPendingAwards: PropTypes.func,
  pendingAwardsList: PropTypes.array,
  pendingAwardsInputRow: PropTypes.object,
  paTotals: PropTypes.object,
  colorCode: PropTypes.string.isRequired,
};

PendingAwards.defaultProps = {
  bottomLine: [],
};

export default PendingAwards;
