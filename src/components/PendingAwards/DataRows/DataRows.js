/* eslint-disable css-modules/no-unused-class */
/* eslint-disable */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { currencyFormat, getTotal } from '../../../utils/mathAndCurrency';

import s from './DataRows.module.css';
import moment from 'moment';
import { getCompanyCodeForPA } from '../../../store/gfr/gfr.selectors';

import * as Datetime from 'react-datetime';
import 'react-datetime/css/react-datetime.css';

class DataRows extends Component {
  static propTypes = {
    dr: PropTypes.arrayOf(
      PropTypes.shape({
        idx: PropTypes.number,
        title: PropTypes.string,
        col1: PropTypes.string,
        col2: PropTypes.string,
        col3: PropTypes.string,
        col4: PropTypes.string,
        col5: PropTypes.string,
        col6: PropTypes.string,
        col7: PropTypes.string,
        col8: PropTypes.string,
        col9: PropTypes.string,
      }),
    ),
    pendingAwardsList: PropTypes.array, // eslint-disable-line
    pendingAwardEditbtn: PropTypes.func, // eslint-disable-line
    pendingAwardDeletebtn: PropTypes.func, // eslint-disable-line
  };

  static defaultProps = {
    dr: [],
  };

  constructor(props) {
    super(props);
    const { dr } = this.props;
    this.state = {
      rowClicked: false,

      dr: [
        {
          title: dr.title,
          col1: dr.col1,
          col2: dr.col2,
          col3: dr.col3,
          col4: dr.col4,
          col5: dr.col5,
          col6: dr.col6,
          col7: dr.col7,
          col8: dr.col8,
          col9: dr.col9,
        },
      ],
    };
  }

  onChange = () => {
    this.setState({});
  };

  setClicked = e => {
    this.setState({ [e.target.id]: true });
  };

  handleChange = (e, id, columnName) => {
    this.props.onChangeHandlerObject(e, id, columnName);
    //this.setState({...this.state, [name]: value});
  };

  setClickedDflt = (e, index, columnName) => {
    if (columnName === 'pendingAwardExpDate') {
      this.props.onPendingAwardUpdate(columnName, index, e.toDate());
      this.setState({ [`awardExpDate_${index}`]: false });
    } else {
      this.props.onPendingAwardUpdate(columnName, index, e.target.value);
      this.setState({ [e.target.getAttribute('id')]: false });
    }
  };

  render() {
    const {
      pendingAwardsList,
      pendingAwardDeletebtn,
      onPendingAwardUpdate,
      onChangeHandler,
    } = this.props;
    let backgroundcolor = '';
    function color(id)
    {
      if(id%2==0){ return backgroundcolor='white'}else{ return backgroundcolor='#d8dada'}
    }
    const context = this;
    
    return (
      <div id="liaddshapes" style={{ overflowX: 'hidden',overflowY:'scroll', marginRight: '20px', height: '300px',whiteSpace:'nowrap'}}>
        {pendingAwardsList &&
          pendingAwardsList.map((data, id) => (
            <div
              className={'PendingAwards_mainContainer__3Hzwb'}
              style={{ paddingLeft: '0px' }}
              key={id}
            >
              
              <div  className={s.paRow} style={{ float: 'left',backgroundColor:`${color(id)}`,marginTop:'5px' }}>
                {/* Added */}
                {(this.props.companyCode === "ALL") &&
                (
                (context.state[`companyName_${id}`] ? (
                  context.state[`companyName_${id}`]
                ) : (
                    false
                  )) ? (
                    <input
                      className={s.topCell}
                      autoFocus={true}
                      type="number"
                      onBlur={e => {
                        this.setClickedDflt(e, id, 'companyName');
                      }}
                      value={data.pendingAwardRevenueValue}
                      onChange={e => {
                        this.handleChange(e, id, 'companyName');
                      }}
                      name="row1Data"
                      id={`companyName_${id}`}
                    />
                  ) : (
                    <div
                      className={s.topCell}
                      id={`companyName_${id}`}
                      onClick={this.setClicked}
                      role="button"
                      onKeyDown={this.setClicked}
                      tabIndex="0"
                      style={{ textAlign: 'left', marginLeft:'2px',marginTop:'9px' }}
                    >
                      {getCompanyCodeForPA(data.company)}

                    </div>
                  )
                )}

                {/* Added End */}





                {(context.state[`title_${id}`] ? (
                  context.state[`title_${id}`]
                ) : (
                    false
                  )) ? (
                    <input
                      className={s.topCell}
                      style={{ width: '170px', textAlign: 'left' }}
                      autoFocus={true}
                      type="text"
                      onBlur={e => {
                        this.setClickedDflt(e, id, 'pendingAwardTitle');
                      }}
                      value={data.pendingAwardTitle}
                      onChange={e => {
                        this.handleChange(e, id, 'pendingAwardTitle');
                      }}
                      name="row1Data"
                      id={`title_${id}`}
                    />
                  ) : (
                    <div
                      className={s.topCell}
                      style={{ width: '170px', textAlign: 'left',marginTop:'9px' }}
                      id={`title_${id}`}
                      onClick={this.setClicked}
                      role="button"
                      onKeyDown={this.setClicked}
                      tabIndex="0"
                    >
                      {data.pendingAwardTitle}
                    </div>
                  )}

                {(context.state[`awardExpDate_${id}`] ? (
                  context.state[`awardExpDate_${id}`]
                ) : (
                    false
                  )) ? (
                    <Datetime
                      name="row1Data"
                      closeOnSelect={true}
                      closeOnTab={true}
                      id={`awardExpDate_${id}`}
                      autoFocus={true}
                      open={true}
                      value={moment(new Date(data.pendingAwardExpDate)).format(
                        'MM/DD/YY',
                      )}
                      className={s.topCell}
                      inputProps={{
                        placeholder: '',
                        style: { textAlign: 'left' },
                        readOnly: true,
                      }}
                      timeFormat={false}
                      onChange={e => {
                        this.handleChange(e, id, 'pendingAwardExpDate');
                      }}
                      onBlur={e => {
                        this.setClickedDflt(e, id, 'pendingAwardExpDate');
                      }}
                      dateFormat="MM/DD/YY"
                      style={{ textAlign: 'left' }}
                    />
                  ) : (
                    <div
                      className={s.topCell}
                      style={{ textAlign: 'center',marginTop:'9px' }}
                      id={`awardExpDate_${id}`}
                      onClick={this.setClicked}
                      role="button"
                      onKeyDown={this.setClicked}
                      tabIndex="0"
                    >
                      {moment(new Date(data.pendingAwardExpDate)).format(
                        'MM/DD/YY',
                      )}
                    </div>
                  )}

                {(context.state[`awardRevenueValue_${id}`] ? (
                  context.state[`awardRevenueValue_${id}`]
                ) : (
                    false
                  )) ? (
                    <input
                      className={s.topCell}
                      autoFocus={true}
                      type="number"
                      onBlur={e => {
                        this.setClickedDflt(e, id, 'pendingAwardRevenueValue');
                      }}
                      value={data.pendingAwardRevenueValue}
                      onChange={e => {
                        this.handleChange(e, id, 'pendingAwardRevenueValue');
                      }}
                      name="row1Data"
                      id={`awardRevenueValue_${id}`}
                    />
                  ) : (
                    <div
                      className={s.topCell}
                      id={`awardRevenueValue_${id}`}
                      onClick={this.setClicked}
                      role="button"
                      onKeyDown={this.setClicked}
                      tabIndex="0"
                      style={{ marginTop: '9px' }}
                    >
                      {currencyFormat(data.pendingAwardRevenueValue)}
                      {/*      {data.pendingAwardRevenueValue}  */}
                    </div>
                  )}

                {(context.state[`awardCTOValue_${id}`] ? (
                  context.state[`awardCTOValue_${id}`]
                ) : (
                    false
                  )) ? (
                    <input
                      className={s.topCell}
                      autoFocus={true}
                      type="number"
                      onBlur={e => {
                        this.setClickedDflt(e, id, 'pendingAwardCTOValue');
                      }}
                      value={data.pendingAwardCTOValue}
                      onChange={e => {
                        this.handleChange(e, id, 'pendingAwardCTOValue');
                      }}
                      name="row1Data"
                      id={`awardCTOValue_${id}`}
                    />
                  ) : (
                    <div
                      className={s.topCell}
                      id={`awardCTOValue_${id}`}
                      onClick={this.setClicked}
                      role="button"
                      onKeyDown={this.setClicked}
                      tabIndex="0"
                      style={{marginTop:'9px'}}
                    >
                      {currencyFormat(data.pendingAwardCTOValue)}

                      {/*  {data.pendingAwardCTOValue}  */}
                    </div>
                  )}

                {(context.state[`pendingAwardFYEPrevRev_${id}`] ? (
                  context.state[`pendingAwardFYEPrevRev_${id}`]
                ) : (
                    false
                  )) ? (
                    <input
                      className={s.topCell}
                      autoFocus={true}
                      type="number"
                      onBlur={e => {
                        this.setClickedDflt(e, id, 'pendingAwardFYEPrevRev');
                      }}
                      value={data.pendingAwardFYE18Revenue}
                      onChange={e => {
                        this.handleChange(e, id, 'pendingAwardFYEPrevRev');
                      }}
                      name="row1Data"
                      id={`pendingAwardFYEPrevRev_${id}`}
                    />
                  ) : (
                    <div
                      className={s.topCell}
                      id={`pendingAwardFYEPrevRev_${id}`}
                      onClick={this.setClicked}
                      role="button"
                      onKeyDown={this.setClicked}
                      tabIndex="0"
                      style={{marginTop:'9px'}}
                    >
                      {currencyFormat(data.pendingAwardFYEPrevRev)}
                      {/* {data.pendingAwardFYEPrevRev
                      ? currencyFormat(data.pendingAwardFYEPrevRev)
                      : ''} */}
                      {/*    {data.pendingAwardFYE18Revenue} */}
                    </div>
                  )}

                {(context.state[`pendingAwardFYEPrevCTO_${id}`] ? (
                  context.state[`pendingAwardFYEPrevCTO_${id}`]
                ) : (
                    false
                  )) ? (
                    <input
                      className={s.topCell}
                      autoFocus={true}
                      type="number"
                      onBlur={e => {
                        this.setClickedDflt(e, id, 'pendingAwardFYEPrevCTO');
                      }}
                      value={data.pendingAwardFYEPrevCTO}
                      onChange={e => {
                        this.handleChange(e, id, 'pendingAwardFYEPrevCTO');
                      }}
                      name="row1Data"
                      id={`apendingAwardFYEPrevCTO_${id}`}
                    />
                  ) : (
                    <div
                      className={s.topCell}
                      id={`pendingAwardFYEPrevCTO_${id}`}
                      onClick={this.setClicked}
                      role="button"
                      onKeyDown={this.setClicked}
                      tabIndex="0"
                      style={{marginTop:'9px'}}
                    >
                      {currencyFormat(data.pendingAwardFYEPrevCTO)}
                      {/* {data.pendingAwardFYEPrevCTO
                      ? currencyFormat(data.pendingAwardFYEPrevCTO)
                      : ''} */}
                      {/*    {data.pendingAwardFYE18CTO} */}
                    </div>
                  )}

                {(context.state[`pendingAwardCurBacklogRev_${id}`] ? (
                  context.state[`pendingAwardCurBacklogRev_${id}`]
                ) : (
                    false
                  )) ? (
                    <input
                      className={s.topCell}
                      style={{ width: '170px' }}
                      autoFocus={true}
                      type="number"
                      onBlur={e => {
                        this.setClickedDflt(e, id, 'pendingAwardCurBacklogRev');
                      }}
                      value={data.pendingAwardCurBacklogRev}
                      onChange={e => {
                        this.handleChange(e, id, 'pendingAwardCurBacklogRev');
                      }}
                      name="row1Data"
                      id={`pendingAwardCurBacklogRev_${id}`}
                    />
                  ) : (
                    <div
                      className={s.topCell}
                      style={{ width: '170px',marginTop:'9px' }}
                      id={`pendingAwardCurBacklogRev_${id}`}
                      onClick={this.setClicked}
                      role="button"
                      onKeyDown={this.setClicked}
                      tabIndex="0"
                    >
                      {currencyFormat(data.pendingAwardCurBacklogRev)}
                      {/* {data.pendingAwardCurBacklogRev
                      ? currencyFormat(data.pendingAwardCurBacklogRev)
                      : ''} */}

                      {/*    {data.pendingAwardBacklogRev2019} */}
                    </div>
                  )}

                {(context.state[`pendingAwardCurBacklogCTO_${id}`] ? (
                  context.state[`pendingAwardCurBacklogCTO_${id}`]
                ) : (
                    false
                  )) ? (
                    <input
                      className={s.topCell}
                      style={{ width: '170px' }}
                      autoFocus={true}
                      type="number"
                      onBlur={e => {
                        this.setClickedDflt(e, id, 'pendingAwardCurBacklogCTO');
                      }}
                      value={data.pendingAwardBacklogCTO2019}
                      onChange={e => {
                        this.handleChange(e, id, 'pendingAwardCurBacklogCTO');
                      }}
                      name="row1Data"
                      id={`pendingAwardCurBacklogCTO_${id}`}
                    />
                  ) : (
                    <div
                      className={s.topCell}
                      style={{ width: '170px',marginTop:'9px' }}
                      id={`pendingAwardCurBacklogCTO_${id}`}
                      onClick={this.setClicked}
                      role="button"
                      onKeyDown={this.setClicked}
                      tabIndex="0"
                    >
                      {currencyFormat(data.pendingAwardCurBacklogCTO)}
                      {/* {data.pendingAwardCurBacklogCTO
                      ? currencyFormat(data.pendingAwardCurBacklogCTO)
                      : ''} */}
                      {/*      {data.pendingAwardBacklogCTO2019}  */}
                    </div>
                  )}

                {(context.state[`pendingAwardFutureBacklogRev_${id}`] ? (
                  context.state[`pendingAwardFutureBacklogRev_${id}`]
                ) : (
                    false
                  )) ? (
                    <input
                      className={s.topCell}
                      style={{ width: '170px' }}
                      autoFocus={true}
                      type="number"
                      onBlur={e => {
                        this.setClickedDflt(
                          e,
                          id,
                          'pendingAwardFutureBacklogRev',
                        );
                      }}
                      value={data.pendingAwardBacklogRev2020}
                      onChange={e => {
                        this.handleChange(e, id, 'pendingAwardFutureBacklogRev');
                      }}
                      name="row1Data"
                      id={`pendingAwardFutureBacklogRev_${id}`}
                    />
                  ) : (
                    <div
                      className={s.topCell}
                      style={{ width: '160px',marginTop:'9px' }}
                      id={`pendingAwardFutureBacklogRev_${id}`}
                      onClick={this.setClicked}
                      role="button"
                      onKeyDown={this.setClicked}
                      tabIndex="0"
                    >

                      {currencyFormat(data.pendingAwardFutureBacklogRev)}
                      {/* {data.pendingAwardFutureBacklogRev
                      ? currencyFormat(data.pendingAwardFutureBacklogRev)
                      : ''} */}

                      {/*    {data.pendingAwardBacklogRev2020} */}
                    </div>
                  )}

                {(context.state[`pendingAwardFutureBacklogCTO_${id}`] ? (
                  context.state[`pendingAwardFutureBacklogCTO_${id}`]
                ) : (
                    false
                  )) ? (
                    <input
                      className={s.topCell}
                      style={{ width: '170px' }}
                      autoFocus={true}
                      type="number"
                      onBlur={e => {
                        this.setClickedDflt(
                          e,
                          id,
                          'pendingAwardFutureBacklogCTO',
                        );
                      }}
                      value={data.pendingAwardBacklogCTO2020}
                      onChange={e => {
                        this.handleChange(e, id, 'pendingAwardFutureBacklogCTO');
                      }}
                      name="row1Data"
                      id={`pendingAwardFutureBacklogCTO_${id}`}
                    />
                  ) : (
                    <div
                      className={s.topCell}
                      style={{ width: '170px',paddingRight:'6px',marginTop:'9px' }}
                      id={`pendingAwardFutureBacklogCTO_${id}`}
                      onClick={this.setClicked}
                      role="button"
                      onKeyDown={this.setClicked}
                      tabIndex="0"
                    >
                      {currencyFormat(data.pendingAwardFutureBacklogCTO)}
                      {/* {data.pendingAwardFutureBacklogCTO
                      ? currencyFormat(data.pendingAwardFutureBacklogCTO)
                      : ''} */}
                      {/*    {data.pendingAwardBacklogCTO2020}  */}
                    </div>
                  )}

                <div className={s.editBtn} style={{ marginLeft: '12px', width: '98px',marginLeft:'18px' }}>
                  <button
                    type="button"
                    className={s.deleteButton}
                    onClick={() => {
                      pendingAwardDeletebtn(data.id);
                    }}
                    style={{height:'29px',marginBottom:'5px',marginRight:'4px'}}
                  >
                    Delete
                  </button>
                </div>
              </div>
            </div>
          ))}
      </div>
    );
  }
}

export default DataRows;
