/* eslint-disable  */
import React from 'react';
import PropTypes from 'prop-types';

import './Layout.css';
import Header from '../Header';

const Layout = ({ children }) => (
  <div className="layoutroot">
    <Header />
    {children}
  </div>
);

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
