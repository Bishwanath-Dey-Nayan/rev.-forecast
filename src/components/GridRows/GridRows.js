/* eslint-disable css-modules/no-unused-class */
import React from 'react';
import PropTypes from 'prop-types';

import s from './GridRows.module.css';

const GridRows = ({
  row1Title,
  row2Title,
  row3Title,
  row4Title,
  row5Title,
  row6Title,
  row7Title
}) => (
  <div className={s.mainContainer}>
    <div className={s.row1Title}>{row1Title}</div>
    <div className={s.row2Title}>{row2Title}</div>
    <div className={s.row3Title}>{row3Title}</div>
    <div className={s.row4Title}>{row4Title}</div>
    <div className={s.row5Title}>{row5Title}</div>
    <div className={s.row6Title}>{row6Title}</div>
    <div className={s.row7Title}>{row7Title}</div>
  </div>
);

GridRows.propTypes = {
  row1Title: PropTypes.string,
  row2Title: PropTypes.string,
  row3Title: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
  row4Title: PropTypes.string,
  row5Title: PropTypes.string,
  row6Title: PropTypes.string,
  row7Title:PropTypes.string,
};

GridRows.defaultProps = {
  row1Title: '',
  row2Title: '',
  row3Title: '',
  row4Title: '',
  row5Title: '',
  row6Title: '',
  row7Title:'',
};

export default GridRows;
