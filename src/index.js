// eslint-disable
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import * as Sentry from '@sentry/browser';
import configureStore from './store/configureStore';
import RouterApp from './router';

Sentry.init({
  dsn: 'https://aae911b329714f81bf87ce1df321f7a0@sentry.io/1396858',
});

const store = configureStore();

if (window === window.parent && typeof window !== 'undefined') {
  render(
    <Provider store={store}>
      <RouterApp />
    </Provider>,
    document.getElementById('root'), //eslint-disable-line
  );
}
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
// serviceWorker.unregister();
